<!DOCTYPE html>
<html class="restaurant" lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="application-name" content="&nbsp;" />
    <meta name="msapplication-TileColor" content="#FFFFFF" />
    <meta name="msapplication-TileImage" content="//www.thebidadarivillasandspa.com/lumonata-content/themes/custom/favicon/mstile-144x144.png" />
    <meta name="msapplication-square70x70logo" content="//www.thebidadarivillasandspa.com/lumonata-content/themes/custom/favicon/mstile-70x70.png" />
    <meta name="msapplication-square150x150logo" content="//www.thebidadarivillasandspa.com/lumonata-content/themes/custom/favicon/mstile-150x150.png" />
    <meta name="msapplication-wide310x150logo" content="//www.thebidadarivillasandspa.com/lumonata-content/themes/custom/favicon/mstile-310x150.png" />
    <meta name="msapplication-square310x310logo" content="//www.thebidadarivillasandspa.com/lumonata-content/themes/custom/favicon/mstile-310x310.png" />

    <title>Reservation - The Bidadari Villas &amp; Spa</title>

    <meta name="description" content="The Yubi Reservation Direct Booking" />
    <meta name="keywords" content="direct booking, the yubi reservation" />







    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="//www.thebidadarivillasandspa.com/lumonata-content/themes/custom/favicon/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="//www.thebidadarivillasandspa.com/lumonata-content/themes/custom/favicon/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="//www.thebidadarivillasandspa.com/lumonata-content/themes/custom/favicon/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="//www.thebidadarivillasandspa.com/lumonata-content/themes/custom/favicon/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon-precomposed" sizes="60x60" href="//www.thebidadarivillasandspa.com/lumonata-content/themes/custom/favicon/apple-touch-icon-60x60.png" />
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="//www.thebidadarivillasandspa.com/lumonata-content/themes/custom/favicon/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="//www.thebidadarivillasandspa.com/lumonata-content/themes/custom/favicon/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="//www.thebidadarivillasandspa.com/lumonata-content/themes/custom/favicon/apple-touch-icon-152x152.png" />

    <link rel="icon" type="image/png" href="//www.thebidadarivillasandspa.com/lumonata-content/themes/custom/favicon/favicon-196x196.png" sizes="196x196" />
    <link rel="icon" type="image/png" href="//www.thebidadarivillasandspa.com/lumonata-content/themes/custom/favicon/favicon-96x96.png" sizes="96x96" />
    <link rel="icon" type="image/png" href="//www.thebidadarivillasandspa.com/lumonata-content/themes/custom/favicon/favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="//www.thebidadarivillasandspa.com/lumonata-content/themes/custom/favicon/favicon-16x16.png" sizes="16x16" />
    <link rel="icon" type="image/png" href="//www.thebidadarivillasandspa.com/lumonata-content/themes/custom/favicon/favicon-128.png" sizes="128x128" />

    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" type="text/css" rel="stylesheet" media="screen" />


    <!-- <link rel="preload" href="//www.thebidadarivillasandspa.com/lumonata-content/themes/custom/css/font.css?v=2.2.0" as="style" onload="this.onload=null;this.rel='stylesheet'"> -->
    <!-- <link rel="preload" href="//www.thebidadarivillasandspa.com/lumonata-content/themes/custom/css/plugin.css?v=2.2.0" as="style" onload="this.onload=null;this.rel='stylesheet'"> -->

    <!-- <link href="//www.thebidadarivillasandspa.com/lumonata-content/themes/custom/css/style.min.css?v=2.2.0" rel="stylesheet"> -->
    <!-- <link rel="preload" href="//www.thebidadarivillasandspa.com/lumonata-content/themes/custom/css/style.min.css?v=2.2.0" as="style" onload="this.onload=null;this.rel='stylesheet'"> -->

    <!-- <link href="//www.thebidadarivillasandspa.com/lumonata-content/themes/custom/css/mobile.min.css?v=2.2.0" rel="stylesheet"> -->
    <!-- <link rel="preload" href="//www.thebidadarivillasandspa.com/lumonata-content/themes/custom/css/mobile.min.css?v=2.2.0" as="style" onload="this.onload=null;this.rel='stylesheet'"> -->
    


    <link href="//www.thebidadarivillasandspa.com/lumonata-content/themes/custom/css/lord.min.css?v=2.2.0" rel="stylesheet">
    
    <script async>
        <?php 
            $checkinDate = $_GET['check_in_date'];
            $checkoutDate = $_GET['check_out_date'];
            $adultCount = $_GET['number_adults'];
            
        ?>
        window.location.href = "https://thebidadarivillasandspa.reserveonline.id/book/474?checkin=<?php echo $checkinDate; ?>&checkout=<?php echo $checkoutDate; ?>&adult=<?php echo $adultCount; ?>";
    </script>

    <!--Google Tag Manager -->
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-WKXLGFH');
    </script>
    <!--End Google Tag Manager -->

    <script type="text/javascript" src="https://app.userguest.com/ug.js"></script>
    <script type="text/javascript">
        uguest.init(["D3NATU5U6I"]);
    </script>


    <!-- Start Covid-19 Update -->

    <div id="alertbanner" style="width:100%;position:fixed;z-index:100;background:rgb(200,177,120);display:flex;justify-content:center;align-items:center;height:30px;top:0;overflow:hidden">
        <div style="width: 80%;">
            <i class="tandaseru">!</i>
            <a href="https://www.thebidadarivillasandspa.com/prevention-and-control-of-hygiene-cleanliness-safety" target="_blank" style="float:left;font-size: 16px;color:#fff;margin-top:2px;">Prevention and Control of Hygiene Cleanliness &amp; Safety</a>
            <button style="float:right;border:0;font-size:20px;color:#FFF;background:rgb(200,177,120);cursor:pointer" onclick="closeAlertBanner()">X</button>
        </div>
    </div>


    <div id="alertbanner2" style="height:30px;"></div>

    <script type="text/javascript">
        function closeAlertBanner() {
            $("#alertbanner").hide();
            $("#alertbanner2").hide();
            $("#ft-world").css('transform', 'translateY(0px)');
            request = $.ajax({
                url: "/hide-alila-statement",
                type: "post",
                data: 'hide'
            });

            // callback handler that will be called on success
            request.done(function(response, textStatus, jqXHR) {
                // log a message to the console
                console.log("hiding");
            });

            // callback handler that will be called on failure
            request.fail(function(jqXHR, textStatus, errorThrown) {
                // log the error to the console
                console.error("The following error occured: " +
                    textStatus, errorThrown
                );
            });
        }
    </script>
    <!-- End Covid-19 Update -->

    <style>
        #ft-world:not(.temp_hide) {
            transform: translateY(30px);
        }

        .tandaseru {
            width: 20px;
            height: 20px;
            border-radius: 50%;
            background-color: yellow;
            margin-top: 5px;
            margin-right: 20px;
            display: block;
            float: left;
            font-style: normal;
            text-align: center;
            color: rgb(200, 177, 120);
            font-weight: bolder;
            line-height: 20px;
        }
    </style>
    <!-- iChronoz css -->
		<?php $version = '2.0.7.1' ?>
		<link rel="stylesheet" type="text/css" href="<?php echo 'ichronoz/css/ichronoz.css?'.$version ?>" />
		<link rel="stylesheet" type="text/css" href="<?php echo 'ichronoz/css/chr_custom.css?'.$version ?>" />
		<link rel="stylesheet" type="text/css" href="<?php echo 'ichronoz/css/font-awesome.min.css?'.$version ?> " />
	<!-- End iChronoz css -->
    <!-- iChronoz js -->
    <!-- just comment this line if jquery exist before -->
    <script type="text/javascript" src="<?php echo 'ichronoz/js/jquery.min.js?' . $version ?>"></script>

    <script type="text/javascript" src="<?php echo 'ichronoz/js/angular.min.js?' . $version ?> "></script>
    <script type="text/javascript" src="<?php echo 'ichronoz/js/angular-modal-service.js?' . $version ?> "></script>
    <script type="text/javascript" src="<?php echo 'ichronoz/js/ui-bootstrap.min.js?' . $version ?> "></script>
    <script type="text/javascript" src="<?php echo 'ichronoz/js/angular-ui-router.min.js?' . $version ?> "></script>
    <script type="text/javascript" src="<?php echo 'ichronoz/js/angular-idle.min.js?' . $version ?> "></script>
    <script type="text/javascript" src="<?php echo 'ichronoz/js/angular-sanitize.min.js?' . $version ?> "></script>
    <script type="text/javascript" src="<?php echo 'ichronoz/js/angular-animate.js?' . $version ?> "></script>
    <script type="text/javascript" src="<?php echo 'ichronoz/js/angular-filter.min.js?' . $version ?> "></script>
    <script type="text/javascript" src="<?php echo 'ichronoz/js/app.js?' . $version ?> "></script>
    <script type="text/javascript" src="<?php echo 'ichronoz/js/book.js?' . $version ?> "></script>
    <script type="text/javascript" src="<?php echo 'ichronoz/js/calendar.js?' . $version ?> "></script>
    <?php
    $api = "d1474200d32c6c40faa0b30cf2fd60fa0"; // change this value with hotel apikey
    $max_adult = 6; // max adult in search form
    $max_child = 2; // max child in search form
    $max_pax = 9; // max adult in search form

    //Valid String POST / GET / REQUEST
    function inputSecurity($validate = null)
    {
        if ($validate == null) {
            foreach ($_REQUEST as $key => $val) {
                if (is_string($val)) {
                    $_REQUEST[$key] = htmlentities($val);
                } else if (is_array($val)) {
                    $_REQUEST[$key] = inputSecurity($val);
                }
            }
            foreach ($_GET as $key => $val) {
                if (is_string($val)) {
                    $_GET[$key] = htmlentities($val, ENT_QUOTES, 'UTF-8');
                } else if (is_array($val)) {
                    $_GET[$key] = inputSecurity($val);
                }
            }
            foreach ($_POST as $key => $val) {
                if (is_string($val)) {
                    $_POST[$key] = htmlentities($val, ENT_QUOTES, 'UTF-8');
                } else if (is_array($val)) {
                    $_POST[$key] = inputSecurity($val);
                }
            }
        } else {
            foreach ($validate as $key => $val) {
                if (is_string($val)) {
                    $validate[$key] = htmlentities($val, ENT_QUOTES, 'UTF-8');
                } else if (is_array($val)) {
                    $validate[$key] = inputSecurity($val);
                }
                return $validate;
            }
        }
    }
    inputSecurity();
    if (!isset($_POST['cd']) || $_POST['cd'] == '') {
        if (isset($_GET['fy'])) {
            $fy = $_GET['fy'];
            $fm = $_GET['fm'];
            $fd = $_GET['fd'];
            $fromdate = strtotime("$fy-$fm-$fd");
        } else {
            $fromdate = time();
            $fy = date('Y', strtotime($fromdate));
            $fm = date('m', strtotime($fromdate));
            $fd = date('d', strtotime($fromdate));
        }

        $now = time();
        if ($now <= $fromdate) {
            $bsk = strtotime("$fy-$fm-$fd +1 days");
            $cd = date("j", $fromdate);
            $cm = date("n", $fromdate);
            $cy = date("Y", $fromdate);

            $cod = date("j", $bsk);
            $com = date("n", $bsk);
            $coy = date("Y", $bsk);
        } else {
            $now = strtotime("tomorrow");
            $bsk = strtotime("+ 2 days");
            $cd = date("j", $now);
            $cm = date("n", $now);
            $cy = date("Y", $now);

            $cod = date("j", $bsk);
            $com = date("n", $bsk);
            $coy = date("Y", $bsk);
        }
    }
    if(isset($_GET['check_in_date']) && $_GET['check_in_date'] != ''){
        $arrival = strtotime($_GET['check_in_date']);
		$cd = date("j", $arrival );
		$cm = date("n", $arrival );
		$cy = date("Y", $arrival );
    }
    $nite = 1;
    if(isset($_GET['check_out_date']) && $_GET['check_out_date'] != ''){
        $departure = strtotime($_GET['check_out_date']);
		$cod = date("j", $departure );
		$com = date("n", $departure );
        $coy = date("Y", $departure );
        
        $timeDiff = abs($departure - $arrival);

        $numberDays = $timeDiff/86400;  // 86400 seconds in one day

        // and you might want to convert to integer
        $nite = intval($numberDays);
    }
    
    $adult = 2;
    if(isset($_GET['number_adults']) && $_GET['number_adults'] != ''){
        $adult = $_GET['number_adults'];
    }
    $child = 0;
    if(isset($_GET['number_childs']) && $_GET['number_childs'] != ''){
        $child = $_GET['number_childs'];
    }
    
    $cd = isset($_REQUEST['cd']) ? $_REQUEST['cd'] : $cd;
    $cm = isset($_REQUEST['cm']) ? $_REQUEST['cm'] : $cm;
    $cy = isset($_REQUEST['cy']) ? $_REQUEST['cy'] : $cy;
    $cod = isset($_REQUEST['cod']) ? $_REQUEST['cod'] : $cod;
    $com = isset($_REQUEST['com']) ? $_REQUEST['com'] : $com;
    $coy = isset($_REQUEST['coy']) ? $_REQUEST['coy'] : $coy;
    $pax = isset($_REQUEST['pax']) ? $_REQUEST['pax'] : 1;
    $adult = isset($_REQUEST['adult']) ? $_REQUEST['adult'] : $adult;
    $child = isset($_REQUEST['child']) ? $_REQUEST['child'] : $child;
    $nite = isset($_REQUEST['nite']) ? $_REQUEST['nite'] : $nite;
    $cbs = isset($_REQUEST['cbs']) ? $_REQUEST['cbs'] : 0;
    $promo = isset($_REQUEST['promo']) ? $_REQUEST['promo'] : '';
    $utc = isset($_REQUEST['utc']) ? $_REQUEST['utc'] : '';
    $hid = isset($_REQUEST['hid']) ? $_REQUEST['hid'] : '';
    $url = isset($_REQUEST['url']) ? $_REQUEST['url'] : '';
    ?>
    <script type="text/javascript" charset="utf-8">
        var objek = new Array('<?php echo $hid . "', '" . $cd . "', '" . $cm . "', '" . $cy . "', '" . $cod . "', '" . $com . "', '" . $coy . "', '" . $pax . "', '" . $adult . "', '" . $child . "', '" . $url . "', '" . $api . "', '" . $utc . "', '" . $nite . "', '" . $cbs . "', '" . $promo . "', '" . $max_adult . "', '" . $max_child . "', '" . $max_pax ?>');
        var tags = objek;
    </script>
    <!-- End iChronoz js -->
</head>

<body class="bidadari-body">

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WKXLGFH" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->





    <header id="ft-world">
        <div class="container-wrapp">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="logo">
                            <a href="//www.thebidadarivillasandspa.com/">
                                <img src="//www.thebidadarivillasandspa.com/lumonata-content/themes/custom/images/logo.svg" alt="Bidadari Villa" />
                            </a>
                        </div>

                        <nav id="desktop-menu" class="desktop-menu">
                            <ul class="header-menu" id="navMenu-4570">
                                <li class=' home'><a href="http://thebidadarivillasandspa.com">Home</a></li>
                                <li class=' rooms'><a href="http://www.thebidadarivillasandspa.com/rooms/">Rooms</a></li>
                                <li class=' services-facilities'><a href="http://www.thebidadarivillasandspa.com/services/">Services & Facilities</a></li>
                                <li class=' restaurant'><a href="http://www.thebidadarivillasandspa.com/restaurant/the-yubi-restaurant.html">Restaurant</a></li>
                                <li class=' spa'><a href="http://www.thebidadarivillasandspa.com/spa/the-yubi-spa.html">Spa</a></li>
                                <li class=' activities'><a href="http://www.thebidadarivillasandspa.com/activities/">Activities</a></li>
                                <li class=' news'><a href="http://www.thebidadarivillasandspa.com/#">News</a>
                                    <ul class="" id="">
                                        <li class=' prevention-and-control-of-hygiene-cleanliness-safety'><a href="http://www.thebidadarivillasandspa.com/prevention-and-control-of-hygiene-cleanliness-safety.html">Prevention and Control of Hygiene Cleanliness & Safety</a></li>
                                    </ul>
                                </li>
                                <li class=' gallery'><a href="http://www.thebidadarivillasandspa.com/gallery/">Gallery</a></li>
                                <li class=' locations'><a href="http://www.thebidadarivillasandspa.com/locations/">Locations</a></li>
                                <li class=' offers'><a href="http://www.thebidadarivillasandspa.com/offers/">Offers</a></li>
                            </ul>
                        </nav>

                        <nav id="mobile-menu" class="mobile-menu">
                            <ul class="header-menu" id="navMenu-3849">
                                <li class=' home'><a href="http://thebidadarivillasandspa.com">Home</a></li>
                                <li class=' rooms'><a href="http://www.thebidadarivillasandspa.com/rooms/">Rooms</a></li>
                                <li class=' services-facilities'><a href="http://www.thebidadarivillasandspa.com/services/">Services & Facilities</a></li>
                                <li class=' restaurant'><a href="http://www.thebidadarivillasandspa.com/restaurant/the-yubi-restaurant.html">Restaurant</a></li>
                                <li class=' spa'><a href="http://www.thebidadarivillasandspa.com/spa/the-yubi-spa.html">Spa</a></li>
                                <li class=' activities'><a href="http://www.thebidadarivillasandspa.com/activities/">Activities</a></li>
                                <li class=' news'><a href="http://www.thebidadarivillasandspa.com/#">News</a>
                                    <ul class="" id="">
                                        <li class=' prevention-and-control-of-hygiene-cleanliness-safety'><a href="http://www.thebidadarivillasandspa.com/prevention-and-control-of-hygiene-cleanliness-safety.html">Prevention and Control of Hygiene Cleanliness & Safety</a></li>
                                    </ul>
                                </li>
                                <li class=' gallery'><a href="http://www.thebidadarivillasandspa.com/gallery/">Gallery</a></li>
                                <li class=' locations'><a href="http://www.thebidadarivillasandspa.com/locations/">Locations</a></li>
                                <li class=' offers'><a href="http://www.thebidadarivillasandspa.com/offers/">Offers</a></li>
                            </ul>
                        </nav>

                        <button class="hamburger hamburger--emphatic" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <a href="javascript:;" class="book-now book-trigger">Book Now</a>
    </header>

    <div class="wrapper">
        <section class="why-book-direct actv">
            <div class="wbd-con actv" id="promo-content" style="display:none">
                <a class="cc_"></a>
                <div class="lo_">
                    <h2>Why Book Direct?</h2>
                    <p>Get special benefits when booking online at The Bidadari Villas &amp; Spa</p>
                    <ul>
                        <li>Balinese Heritage Welcome Drink &amp;&nbsp;cold face towel upon arrival</li>
                        <li>FREE Wi-Fi throughout villa area</li>
                        <li>FREE One Time Floating Breakfast for min. stay 3 nights</li>
                        <li>FREE&nbsp;ONE WAY Airport Transfer for Min. stay 3 nights</li>
                        <li>Enjoy 10% Off Spa at Yubi Spa</li>
                        <li>Enjoy 10% Off&nbsp;for Food at Yubi&nbsp;Restaurant</li>
                        <li>Enjoy 20% Off for Activities in Finns Recreation Club</li>
                        <li>FREE shuttle service to Canggu and Seminyak area (Drop only based on schedule)</li>
                        <li>BEST RATE GUARANTEED</li>
                    </ul>
                </div>
                <a class="bk_" href="https://secure.staah.com/common-cgi/package/packagebooking.pl?propertyId=12695&unk=2&header=&flexible=&frame=&source=&layout=&Ln=en&ccode=TBV888" target="_blank">BOOK NOW</a>
            </div>
        </section>
        <section class="why-book-direct-button" style="display:block;visibility:visible">
            <div class="button-display-promo">
                <button id="button-display-promo">Why book direct with us?
                </button>
            </div>
        </section>
        <div class="detail-hero">
            <div class="container-wrapp">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="inner">
                                <div class="center-content">
                                    <div class="boxs">
                                        <h1>Reservation</h1>
                                        <nav></nav>
                                        <a class="submit discover-more" href="javascript:;">Reserve</a>
                                        <a class="discover-more">Discover More <img src="//www.thebidadarivillasandspa.com/lumonata-content/themes/custom/images/discover-white.svg" alt="" /></a>
                                    </div>
                                </div>

                                <div class="slide">
                                    <figure>
                                        <img class="slide-lazy-load" src="//www.thebidadarivillasandspa.com/lumonata-functions/mthumb.php?src=https://www.thebidadarivillasandspa.com/lumonata-content/files/201903/mandiri-7932-1553504155-thumbnail.jpg&q=80&w=15" data-src="//www.thebidadarivillasandspa.com/lumonata-functions/mthumb.php?src=https://www.thebidadarivillasandspa.com/lumonata-content/files/201903/mandiri-7932-1553504155-large.jpg&q=80&h=900" alt="">
                                    </figure>
                                    <figure>
                                        <img class="slide-lazy-load" src="//www.thebidadarivillasandspa.com/lumonata-functions/mthumb.php?src=https://www.thebidadarivillasandspa.com/lumonata-content/files/201903/mandiri-7946-1553504153-thumbnail.jpg&q=80&w=15" data-src="//www.thebidadarivillasandspa.com/lumonata-functions/mthumb.php?src=https://www.thebidadarivillasandspa.com/lumonata-content/files/201903/mandiri-7946-1553504153-large.jpg&q=80&h=900" alt="">
                                    </figure>
                                    <figure>
                                        <img class="slide-lazy-load" src="//www.thebidadarivillasandspa.com/lumonata-functions/mthumb.php?src=https://www.thebidadarivillasandspa.com/lumonata-content/files/201903/mandiri-7942-1553504152-thumbnail.jpg&q=80&w=15" data-src="//www.thebidadarivillasandspa.com/lumonata-functions/mthumb.php?src=https://www.thebidadarivillasandspa.com/lumonata-content/files/201903/mandiri-7942-1553504152-large.jpg&q=80&h=900" alt="">
                                    </figure>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="detail-restaurant next-section">
            <div class="container-wrapp">
                <div class="container-fluid">
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <!-- iChronoz Content -->
                                <div class="chr_container">
                                    <div ng-app="ichronoz" class="chr_row chronoz" scroll>
                                        <div ui-view></div>
                                        <section data-ng-controller="sessionExpired">
                                        </section>	
                                    </div>
                                </div>
                            <!-- End iChronoz Content -->
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <section class="book-reserve-inq">
            <div class="ovly_09"></div>

            <div class="inq-con">

                <a class="ccc_"></a>

                <form>
                    <input type="hidden" name="url_ajax" value="//www.thebidadarivillasandspa.com/ajax-function/">
                    <input type="hidden" name="titz_" value="Restaurant">
                    <div class="fc_">
                        <h2>Reservation Inquiry</h2>

                        <!-- <label class="g66 type_option">
          <span>SPA TYPE: *</span>
          <input autocomplete="off" readonly class="rq_" type="text" name="spa_type" placeholder="Select Package List">
          <ul class="opt_">
            <li>Bidadari Package (120 minutes)</li>
            <li>Yubi Rama – Shinta Package B (150 minutes)</li>
            <li>Traditional Balinese Massage (120 minutes)</li>
          </ul>
        </label> -->
                        <label class="g66 type_option">
                            <span>NO OF PERSON: *</span>
                            <input autocomplete="off" readonly class="rq_" type="text" name="guest" value="1 Person">
                            <ul class="opt_">
                                <li>1 Person</li>
                                <li>2 Persons</li>
                                <li>3 Person</li>
                                <li>4 Person</li>
                            </ul>
                        </label>
                        <label class="g66 type_field">
                            <span>PROMOTION CODE (IF ANY):</span>
                            <input type="text" name="promo_code" placeholder="Your Code">
                        </label>
                        <label for="thedate__01" class="g66 type_option">
                            <span>DATES: *</span>
                            <input autocomplete="off" class="rq_" type="text" id="thedate__01" name="date" placeholder="Choose Date" readonly>
                        </label>
                        <label class="g66 koi siblings">
                            <span>TIME: * <br> <br></span>
                            <div class="oo0">
                                <div class="kakak type_option">
                                    <input class="rq_" type="text" name="time" placeholder="Minute" autocomplete="off" readonly>
                                    <ul class="opt_ scd">
                                        <li>Other..</li>
                                        <li>15 Minutes</li>
                                        <li>30 Minutes</li>
                                        <li>45 Minutes</li>
                                    </ul>
                                </div>
                                <div class="kakak type_option">
                                    <input class="rq_" type="text" name="hour" placeholder="Hour" autocomplete="off" readonly>
                                    <ul class="opt_ scd">
                                        <li>Other..</li>
                                        <li>12 : 00 AM</li>
                                        <li>1 : 00 AM</li>
                                        <li>2 : 00 AM</li>
                                        <li>3 : 00 AM</li>
                                        <li>4 : 00 AM</li>
                                        <li>5 : 00 AM</li>
                                        <li>6 : 00 AM</li>
                                        <li>7 : 00 AM</li>
                                        <li>8 : 00 AM</li>
                                        <li>9 : 00 AM</li>
                                        <li>10 : 00 AM</li>
                                        <li>11 : 00 AM</li>
                                        <li>12 : 00 PM</li>
                                    </ul>
                                </div>
                            </div>
                        </label>

                        <label class="g66 type_field">
                            <span>WHERE ARE YOU STAYING? *</span>
                            <input class="rq_" type="text" name="current_stay" placeholder="Type Here">
                        </label>

                        <label class="g66 type_option">
                            <span>SPECIAL REQUEST (OPTIONAL):</span>
                            <!-- <input type="text" name="package_list" placeholder="Choose Date"> -->
                            <textarea name="special_request" placeholder="Type Here"></textarea>
                        </label>
                    </div>

                    <div class="fc_">
                        <h2>Contact Details</h2>
                        <label class="g66 type_field">
                            <span>NAME: *</span>
                            <input class="rq_" type="text" name="guest_name" placeholder="Your Name">
                        </label>
                        <label class="g66 type_option opopmo">
                            <span class="push_">NATIONALITY: *</span>
                            <!-- <input class="rq_" type="text" name="nationality" placeholder="Select Your Nationality"> -->
                            <select readOnly class="kkkk_" class="rq_" name="nationality">
                                <option value="">Select Your Nationality</option>
                                <option value="Afghanistan">Afghanistan</option>
                                <option value="Aland Islands">Aland Islands</option>
                                <option value="Albania">Albania</option>
                                <option value="Algeria">Algeria</option>
                                <option value="American Samoa">American Samoa</option>
                                <option value="Andorra">Andorra</option>
                                <option value="Angola">Angola</option>
                                <option value="Anguilla">Anguilla</option>
                                <option value="Antarctica">Antarctica</option>
                                <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                <option value="Argentina">Argentina</option>
                                <option value="Armenia">Armenia</option>
                                <option value="Aruba">Aruba</option>
                                <option value="Australia">Australia</option>
                                <option value="Austria">Austria</option>
                                <option value="Azerbaijan">Azerbaijan</option>
                                <option value="Bahamas">Bahamas</option>
                                <option value="Bahrain">Bahrain</option>
                                <option value="Bangladesh">Bangladesh</option>
                                <option value="Barbados">Barbados</option>
                                <option value="Belarus">Belarus</option>
                                <option value="Belgium">Belgium</option>
                                <option value="Belize">Belize</option>
                                <option value="Benin">Benin</option>
                                <option value="Bermuda">Bermuda</option>
                                <option value="Bhutan">Bhutan</option>
                                <option value="Bolivia">Bolivia</option>
                                <option value="Bonaire, Saint Eustatius and Saba">Bonaire, Saint Eustatius and Saba</option>
                                <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                                <option value="Botswana">Botswana</option>
                                <option value="Bouvet Island">Bouvet Island</option>
                                <option value="Brazil">Brazil</option>
                                <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                                <option value="British Virgin Islands">British Virgin Islands</option>
                                <option value="Brunei">Brunei</option>
                                <option value="Bulgaria">Bulgaria</option>
                                <option value="Burkina Faso">Burkina Faso</option>
                                <option value="Burundi">Burundi</option>
                                <option value="Cambodia">Cambodia</option>
                                <option value="Cameroon">Cameroon</option>
                                <option value="Canada">Canada</option>
                                <option value="Cape Verde">Cape Verde</option>
                                <option value="Cayman Islands">Cayman Islands</option>
                                <option value="Central African Republic">Central African Republic</option>
                                <option value="Chad">Chad</option>
                                <option value="Chile">Chile</option>
                                <option value="China">China</option>
                                <option value="Christmas Island">Christmas Island</option>
                                <option value="Cocos Islands">Cocos Islands</option>
                                <option value="Colombia">Colombia</option>
                                <option value="Comoros">Comoros</option>
                                <option value="Cook Islands">Cook Islands</option>
                                <option value="Costa Rica">Costa Rica</option>
                                <option value="Croatia">Croatia</option>
                                <option value="Cuba">Cuba</option>
                                <option value="Curacao">Curacao</option>
                                <option value="Cyprus">Cyprus</option>
                                <option value="Czech Republic">Czech Republic</option>
                                <option value="Democratic Republic of the Congo">Democratic Republic of the Congo</option>
                                <option value="Denmark">Denmark</option>
                                <option value="Djibouti">Djibouti</option>
                                <option value="Dominica">Dominica</option>
                                <option value="Dominican Republic">Dominican Republic</option>
                                <option value="East Timor">East Timor</option>
                                <option value="Ecuador">Ecuador</option>
                                <option value="Egypt">Egypt</option>
                                <option value="El Salvador">El Salvador</option>
                                <option value="Equatorial Guinea">Equatorial Guinea</option>
                                <option value="Eritrea">Eritrea</option>
                                <option value="Estonia">Estonia</option>
                                <option value="Ethiopia">Ethiopia</option>
                                <option value="Falkland Islands">Falkland Islands</option>
                                <option value="Faroe Islands">Faroe Islands</option>
                                <option value="Fiji">Fiji</option>
                                <option value="Finland">Finland</option>
                                <option value="France">France</option>
                                <option value="French Guiana">French Guiana</option>
                                <option value="French Polynesia">French Polynesia</option>
                                <option value="French Southern Territories">French Southern Territories</option>
                                <option value="Gabon">Gabon</option>
                                <option value="Gambia">Gambia</option>
                                <option value="Georgia">Georgia</option>
                                <option value="Germany">Germany</option>
                                <option value="Ghana">Ghana</option>
                                <option value="Gibraltar">Gibraltar</option>
                                <option value="Greece">Greece</option>
                                <option value="Greenland">Greenland</option>
                                <option value="Grenada">Grenada</option>
                                <option value="Guadeloupe">Guadeloupe</option>
                                <option value="Guam">Guam</option>
                                <option value="Guatemala">Guatemala</option>
                                <option value="Guernsey">Guernsey</option>
                                <option value="Guinea">Guinea</option>
                                <option value="Guinea-Bissau">Guinea-Bissau</option>
                                <option value="Guyana">Guyana</option>
                                <option value="Haiti">Haiti</option>
                                <option value="Heard Island and McDonald Islands">Heard Island and McDonald Islands</option>
                                <option value="Honduras">Honduras</option>
                                <option value="Hong Kong">Hong Kong</option>
                                <option value="Hungary">Hungary</option>
                                <option value="Iceland">Iceland</option>
                                <option value="India">India</option>
                                <option value="Indonesia">Indonesia</option>
                                <option value="Iran">Iran</option>
                                <option value="Iraq">Iraq</option>
                                <option value="Ireland">Ireland</option>
                                <option value="Isle of Man">Isle of Man</option>
                                <option value="Israel">Israel</option>
                                <option value="Italy">Italy</option>
                                <option value="Ivory Coast">Ivory Coast</option>
                                <option value="Jamaica">Jamaica</option>
                                <option value="Japan">Japan</option>
                                <option value="Jersey">Jersey</option>
                                <option value="Jordan">Jordan</option>
                                <option value="Kazakhstan">Kazakhstan</option>
                                <option value="Kenya">Kenya</option>
                                <option value="Kiribati">Kiribati</option>
                                <option value="Kosovo">Kosovo</option>
                                <option value="Kuwait">Kuwait</option>
                                <option value="Kyrgyzstan">Kyrgyzstan</option>
                                <option value="Laos">Laos</option>
                                <option value="Latvia">Latvia</option>
                                <option value="Lebanon">Lebanon</option>
                                <option value="Lesotho">Lesotho</option>
                                <option value="Liberia">Liberia</option>
                                <option value="Libya">Libya</option>
                                <option value="Liechtenstein">Liechtenstein</option>
                                <option value="Lithuania">Lithuania</option>
                                <option value="Luxembourg">Luxembourg</option>
                                <option value="Macao">Macao</option>
                                <option value="Macedonia">Macedonia</option>
                                <option value="Madagascar">Madagascar</option>
                                <option value="Malawi">Malawi</option>
                                <option value="Malaysia">Malaysia</option>
                                <option value="Maldives">Maldives</option>
                                <option value="Mali">Mali</option>
                                <option value="Malta">Malta</option>
                                <option value="Marshall Islands">Marshall Islands</option>
                                <option value="Martinique">Martinique</option>
                                <option value="Mauritania">Mauritania</option>
                                <option value="Mauritius">Mauritius</option>
                                <option value="Mayotte">Mayotte</option>
                                <option value="Mexico">Mexico</option>
                                <option value="Micronesia">Micronesia</option>
                                <option value="Moldova">Moldova</option>
                                <option value="Monaco">Monaco</option>
                                <option value="Mongolia">Mongolia</option>
                                <option value="Montenegro">Montenegro</option>
                                <option value="Montserrat">Montserrat</option>
                                <option value="Morocco">Morocco</option>
                                <option value="Mozambique">Mozambique</option>
                                <option value="Myanmar">Myanmar</option>
                                <option value="Namibia">Namibia</option>
                                <option value="Nauru">Nauru</option>
                                <option value="Nepal">Nepal</option>
                                <option value="Netherlands">Netherlands</option>
                                <option value="New Caledonia">New Caledonia</option>
                                <option value="New Zealand">New Zealand</option>
                                <option value="Nicaragua">Nicaragua</option>
                                <option value="Niger">Niger</option>
                                <option value="Nigeria">Nigeria</option>
                                <option value="Niue">Niue</option>
                                <option value="Norfolk Island">Norfolk Island</option>
                                <option value="North Korea">North Korea</option>
                                <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                                <option value="Norway">Norway</option>
                                <option value="Oman">Oman</option>
                                <option value="Pakistan">Pakistan</option>
                                <option value="Palau">Palau</option>
                                <option value="Palestinian Territory">Palestinian Territory</option>
                                <option value="Panama">Panama</option>
                                <option value="Papua New Guinea">Papua New Guinea</option>
                                <option value="Paraguay">Paraguay</option>
                                <option value="Peru">Peru</option>
                                <option value="Philippines">Philippines</option>
                                <option value="Pitcairn">Pitcairn</option>
                                <option value="Poland">Poland</option>
                                <option value="Portugal">Portugal</option>
                                <option value="Puerto Rico">Puerto Rico</option>
                                <option value="Qatar">Qatar</option>
                                <option value="Republic of the Congo">Republic of the Congo</option>
                                <option value="Reunion">Reunion</option>
                                <option value="Romania">Romania</option>
                                <option value="Russia">Russia</option>
                                <option value="Rwanda">Rwanda</option>
                                <option value="Saint Barthelemy">Saint Barthelemy</option>
                                <option value="Saint Helena">Saint Helena</option>
                                <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                                <option value="Saint Lucia">Saint Lucia</option>
                                <option value="Saint Martin">Saint Martin</option>
                                <option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
                                <option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>
                                <option value="Samoa">Samoa</option>
                                <option value="San Marino">San Marino</option>
                                <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                                <option value="Saudi Arabia">Saudi Arabia</option>
                                <option value="Senegal">Senegal</option>
                                <option value="Serbia">Serbia</option>
                                <option value="Seychelles">Seychelles</option>
                                <option value="Sierra Leone">Sierra Leone</option>
                                <option value="Singapore">Singapore</option>
                                <option value="Sint Maarten">Sint Maarten</option>
                                <option value="Slovakia">Slovakia</option>
                                <option value="Slovenia">Slovenia</option>
                                <option value="Solomon Islands">Solomon Islands</option>
                                <option value="Somalia">Somalia</option>
                                <option value="South Africa">South Africa</option>
                                <option value="South Georgia and the South Sandwich Islands">South Georgia and the South Sandwich Islands</option>
                                <option value="South Korea">South Korea</option>
                                <option value="South Sudan">South Sudan</option>
                                <option value="Spain">Spain</option>
                                <option value="Sri Lanka">Sri Lanka</option>
                                <option value="Sudan">Sudan</option>
                                <option value="Suriname">Suriname</option>
                                <option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
                                <option value="Swaziland">Swaziland</option>
                                <option value="Sweden">Sweden</option>
                                <option value="Switzerland">Switzerland</option>
                                <option value="Syria">Syria</option>
                                <option value="Taiwan">Taiwan</option>
                                <option value="Tajikistan">Tajikistan</option>
                                <option value="Tanzania">Tanzania</option>
                                <option value="Thailand">Thailand</option>
                                <option value="Togo">Togo</option>
                                <option value="Tokelau">Tokelau</option>
                                <option value="Tonga">Tonga</option>
                                <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                                <option value="Tunisia">Tunisia</option>
                                <option value="Turkey">Turkey</option>
                                <option value="Turkmenistan">Turkmenistan</option>
                                <option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
                                <option value="Tuvalu">Tuvalu</option>
                                <option value="U.S. Virgin Islands">U.S. Virgin Islands</option>
                                <option value="Uganda">Uganda</option>
                                <option value="Ukraine">Ukraine</option>
                                <option value="United Arab Emirates">United Arab Emirates</option>
                                <option value="United Kingdom">United Kingdom</option>
                                <option value="United States">United States</option>
                                <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                                <option value="Uruguay">Uruguay</option>
                                <option value="Uzbekistan">Uzbekistan</option>
                                <option value="Vanuatu">Vanuatu</option>
                                <option value="Vatican">Vatican</option>
                                <option value="Venezuela">Venezuela</option>
                                <option value="Vietnam">Vietnam</option>
                                <option value="Wallis and Futuna">Wallis and Futuna</option>
                                <option value="Western Sahara">Western Sahara</option>
                                <option value="Yemen">Yemen</option>
                                <option value="Zambia">Zambia</option>
                                <option value="Zimbabwe">Zimbabwe</option>

                            </select>
                        </label>
                        <label class="g66 type_field">
                            <span>EMAIL ADDRESS: *</span>
                            <input class="rq_" type="email" name="email" placeholder="Your Email Address">
                        </label>
                        <label class="g66 type_field siblings hmm">
                            <span>PHONE: *</span>
                            <input class="rq_" type="tel" name="phone" id="phone" placeholder="Your Phone Number">
                            <!-- <input class="the-tel" type="text" name="package_list" placeholder="Your Phone Number"> -->
                        </label>
                        <label class="g66 type_option siblings hmm">
                            <span>HOW WOULD YOU PPREFER US TO CONTACT YOU? *</span>

                            <input id="r-phone" type="radio" name="radio_prefer_" value="Phone" checked>
                            <label for="r-phone">Phone</label>

                            <input id="r-email" type="radio" name="radio_prefer_" value="Email">
                            <label for="r-email">Email</label>
                        </label>
                        <div class="small_capt">
                            <p>
                                Please be informed that by sending this Reservation form, your booking has not yet guaranteed. Our staff will contact you to proceed with your booking.
                            </p>

                            <p>
                                Should you not receive any respond within <u>24 hours</u>,
                                please email us at <a href="mailto:reservation@thebidadarivillasandspa.com" target="_blank">reservation@thebidadarivillasandspa.com</a>.
                            </p>

                            <p>
                                Please tick on the box below.
                            </p>
                            <div class="recaptcha__">
                                <div class="g-recaptcha" data-theme="light" data-sitekey="6LeUp5EUAAAAAI7CazgF7cfxwqkdbov956-kRFwZ"></div>
                            </div>
                        </div>
                        <a class="inq_send_btn rest__">
                            <span>SEND</span>
                        </a>
                    </div>
                </form>
            </div>
        </section>


        <div class="newsletter-subscribe">
            <div class="container-wrapp">
                <div class="container-fluid">
                    <hr />
                    <div class="row align-items-end">
                        <div class="col-md-6">
                            <small>Stay Connected</small>
                            <p>Keep up to date on the villas in Seminyak and all stories around.</p>
                        </div>
                        <div class="col-md-6">
                            <form name="newsletter-form" method="POST" action="//www.thebidadarivillasandspa.com/ajax-function/">
                                <fieldset>
                                    <input type="email" name="subscribe_email" placeholder="YOUR EMAIL ADDRESS">
                                    <input type="text" name="pkey" value="subscribe-newsletter" class="sr-only">
                                    <button type="submit">
                                        <span>Sign Up</span>
                                        <span>Loading</span>
                                    </button>
                                </fieldset>
                            </form>

                            <div class="sr-only">
                                <div id="subscribe-response" class="subscribe-response">
                                    <!-- <h2>Subscribing To Newsletter</h2> -->
                                    <p></p>
                                    <!-- <button data-fancybox-close="" class="btn btn-primary">Close</button> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <footer>
            <div class="inner gray-white-half-bg">
                <div class="container-wrapp">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="article">
                                    <small>The Bidadari Villas & Spa</small>
                                    <p style="text-align: justify;">The Bidadari Villas &amp; Spa is located 30 minutes from Ngurah Rai Bali International Airport.<br /><br />Its strategic location only takes 15 minutes drive&nbsp;to the famous tourist attraction such as Potato Head Seminyak, Finns Club and Echo Beach Canggu.</p>

                                    <ul>
                                        <li><a href="https://goo.gl/maps/2aqoFrMaEm72" target="_blank">Jl. Bumbak No. 27 Banjar Anyar Kelod - Kerobokan, Kuta Utara - Bali - Indonesia</a></li>
                                        <li><a href="tel:(+62361)847-5566"><b>T.</b> (+62 361) 847-5566</a></li>
                                        <li><a href="tel:(+62361)847-5566"><b>F.</b> (+62 361) 847-5566</a></li>
                                        <li><a href="https://api.whatsapp.com/send?phone=6287860634896" target="_blank"><b>WA.</b> (+62) 8786-0634-896</a></li>
                                        <li><a href="mailto:reservation@thebidadarivillasandspa.com"><b>E.</b> reservation@thebidadarivillasandspa.com</a></li>


                                        <li class="sosmed_">
                                            <a href="https://www.facebook.com/pg/thebidadarivillasandspa" target="_blank">
                                                <img src="//www.thebidadarivillasandspa.com/lumonata-content/themes/custom/images/facebook.svg">
                                                The Bidadari Villas & Spa
                                            </a>
                                        </li>

                                        <li class="sosmed_">
                                            <a href="https://www.instagram.com/thebidadarivillasandspa" target="_blank">
                                                <img src="//www.thebidadarivillasandspa.com/lumonata-content/themes/custom/images/instagram.svg">
                                                @thebidadarivillasandspa
                                            </a>
                                        </li>

                                    </ul>
                                </div>

                                <a class="scroll-top">Return To The Top <img class="lazy-load" data-src="//www.thebidadarivillasandspa.com/lumonata-content/themes/custom/images/arrow-right-long.svg" alt="arrow" /></a>
                            </div>
                            <div class="col-md-6">
                                <div class="awards">
                                    <small>Awards</small>
                                    <div class="slide">
                                        <div class="item">
                                            <figure>
                                                <img src="//www.thebidadarivillasandspa.com/lumonata-content/themes/custom/images/tripadvisor.png" alt="" />
                                            </figure>
                                            <p>Recommended On Tripadvisor</p>
                                        </div>
                                        <div class="item">
                                            <figure>
                                                <img src="//www.thebidadarivillasandspa.com/lumonata-content/themes/custom/images/tripadvisor.png" alt="" />
                                            </figure>
                                            <p>Certificate of Excellence 2017</p>
                                        </div>
                                        <div class="item">
                                            <figure>
                                                <img src="//www.thebidadarivillasandspa.com/lumonata-content/themes/custom/images/tripadvisor.png" alt="" />
                                            </figure>
                                            <p>Certificate of Excellence 2016</p>
                                        </div>
                                        <div class="item">
                                            <figure>
                                                <img src="//www.thebidadarivillasandspa.com/lumonata-content/themes/custom/images/tripadvisor.png" alt="" />
                                            </figure>
                                            <p>Certificate of Excellence 2015</p>
                                        </div>
                                        <div class="item">
                                            <figure>
                                                <img src="//www.thebidadarivillasandspa.com/lumonata-content/themes/custom/images/tripadvisor.png" alt="" />
                                            </figure>
                                            <p>The Bidadari Villas and Spa Rated </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="other-properties clearfix">
                                    <small>Other Properties</small>
                                    <div class="item">
                                        <figure>
                                            <a href="https://www.thelightvillas.com/" target="_blank">
                                                <img src="//www.thebidadarivillasandspa.com/lumonata-functions/mthumb.php?src=https://www.thebidadarivillasandspa.com/lumonata-content/themes/custom/images/logo-the-light-villas.png&q=80&w=100" alt="The Light Villas" />
                                            </a>
                                        </figure>
                                    </div>
                                    <div class="item">
                                        <figure>
                                            <a href="http://www.baliyubivillas.com/" target="_blank">
                                                <img src="//www.thebidadarivillasandspa.com/lumonata-functions/mthumb.php?src=https://www.thebidadarivillasandspa.com/lumonata-content/themes/custom/images/logo-yubi.png&q=80&w=120" alt="Bali Yubi Villas" />
                                            </a>
                                        </figure>
                                    </div>
                                    <div class="item">
                                        <figure>
                                            <a href="https://www.balisunhotel.com/" target="_blank">
                                                <img src="//www.thebidadarivillasandspa.com/lumonata-functions/mthumb.php?src=https://www.thebidadarivillasandspa.com/lumonata-content/themes/custom/images/logo-thesunhotel-black.png&q=80&w=180" alt="Bali Yubi Villas" />
                                            </a>
                                        </figure>
                                    </div>
                                </div>
                                <nav>
                                    <ul class="footer-menu clearfix" id="navMenu-9951">
                                        <li class=' contact'><a href="http://www.thebidadarivillasandspa.com/contact-us.html">Contact</a></li>
                                        <li class=' faq'><a href="http://www.thebidadarivillasandspa.com/faq.html">FAQ</a></li>
                                        <li class=' term-conditions'><a href="http://www.thebidadarivillasandspa.com/term-conditions.html">Term & Conditions</a></li>
                                    </ul>

                                    <small>&copy; 2020 The Bidadari Villas and Spa</small>
                                </nav>

                                <a class="scroll-top">Return To The Top <img class="lazy-load" data-src="//www.thebidadarivillasandspa.com/lumonata-content/themes/custom/images/arrow-right-long.svg" alt="arrow" /></a>
                            </div>
                        </div>
                        <div class="row sr-only">
                            <input type="text" name="site-url" value="www.thebidadarivillasandspa.com" />
                            <input type="text" name="image-url" value="//www.thebidadarivillasandspa.com/lumonata-content/themes/custom/images" />
                        </div>
                    </div>
                </div>
            </div>
        </footer>

        <div class="sr-only">
            <div id="popup-booking" class="popup-booking">
                <div class="popup-content">
                    <div class="popup-boxs">
                        <div class="inner">
                            <h2>Book Your Stay</h2>
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- <form name="popup_booking_form" action="//www.thebidadarivillasandspa.com/booking/book.php" method="POST"> -->
                                        <form class="wakil_book" name="popup_booking_form">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <fieldset>
                                                        <label>Check In</label>
                                                        <input type="text" name="pop_check_in" class="date-picker" placeholder="Choose date" autocomplete="off" readonly />
                                                    </fieldset>
                                                </div>
                                                <div class="col-md-6">
                                                    <fieldset>
                                                        <label>Check Out</label>
                                                        <input type="text" name="pop_check_out" class="date-picker" placeholder="Choose date" autocomplete="off" readonly />
                                                    </fieldset>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <fieldset>
                                                        <label>Adult</label>
                                                        <select id="cc_adult_" name="adult" class="popup-select-option" autocomplete="off">
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                        </select>
                                                    </fieldset>
                                                </div>
                                                <div class="col-md-6">
                                                    <fieldset>
                                                        <label>Child</label>
                                                        <select id="cc_child_" name="child" class="popup-select-option" autocomplete="off">
                                                            <option value="0">0</option>
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                        </select>
                                                    </fieldset>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <input type="text" id="pop" class="sr-only" name="pop" value="1">
                                                    <input type="text" id="url" class="sr-only" name="url" value="">
                                                    <input type="text" id="cbs" class="sr-only" name="cbs" value="0">
                                                    <input type="text" id="pax" class="sr-only" name="pax" value="1">

                                                    <button class="submit book-now-btn ggwp_" type="submit">Book Now</button>

                                                    <div class="book-direct gopop">
                                                        <a href="javascript:;">Why Book Direct <img class="lazy-load" data-src="//www.thebidadarivillasandspa.com/lumonata-content/themes/custom/images/info.svg" alt="icon" /></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <nav>
                        <ul class="footer-menu clearfix" id="navMenu-9951">
                            <li class=' contact'><a href="http://www.thebidadarivillasandspa.com/contact-us.html">Contact</a></li>
                            <li class=' faq'><a href="http://www.thebidadarivillasandspa.com/faq.html">FAQ</a></li>
                            <li class=' term-conditions'><a href="http://www.thebidadarivillasandspa.com/term-conditions.html">Term & Conditions</a></li>
                        </ul>

                        <p>&copy; 2020 The Bidadari Villas and Spa</p>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" value="1" id="header_method" />
    <input type="hidden" value="https://app-apac.thebookingbutton.com/properties/thebidadarivillasandspadirect" id="link_booking_engine" />
    <input type="hidden" value="https://api.whatsapp.com/send?phone=6287860634896" id="wa_link" />
    <!-- NOTE: POPUP -->
    <section class="why-book-direct-popup">
        <div class="ovly_popup"></div>
        <div class="wbd-con_popup">
            <a class="cc_popup"></a>
            <div class="lo_popup">
                <h2>Why Book Direct?</h2>
                <p>Get special benefits when booking online at The Bidadari Villas &amp; Spa</p>
                <ul>
                    <li>Balinese Heritage Welcome Drink &amp;&nbsp;cold face towel upon arrival</li>
                    <li>FREE Wi-Fi throughout villa area</li>
                    <li>FREE One Time Floating Breakfast for min. stay 3 nights</li>
                    <li>FREE&nbsp;ONE WAY Airport Transfer for Min. stay 3 nights</li>
                    <li>Enjoy 10% Off Spa at Yubi Spa</li>
                    <li>Enjoy 10% Off&nbsp;for Food at Yubi&nbsp;Restaurant</li>
                    <li>Enjoy 20% Off for Activities in Finns Recreation Club</li>
                    <li>FREE shuttle service to Canggu and Seminyak area (Drop only based on schedule)</li>
                    <li>BEST RATE GUARANTEED</li>
                </ul>
            </div>
            <a class="bk_popup_2" href="https://secure.staah.com/common-cgi/package/packagebooking.pl?propertyId=12695&unk=2&header=&flexible=&frame=&source=&layout=&Ln=en&ccode=TBV888">BOOK NOW</a>
        </div>
    </section>


    <a class="floating-wa-btn" href="https://api.whatsapp.com/send?phone=6287860634896" target="_blank">
        <img class="lazy-load" data-src="//www.thebidadarivillasandspa.com/lumonata-content/themes/custom/images/whatsapp.svg" alt="wa" />
    </a>

    <script src=" //ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js "></script>
    <script src=" //www.thebidadarivillasandspa.com/lumonata-content/themes/custom/js/plugin.js "></script>
    <script src=" //www.google.com/recaptcha/api.js "></script>
    <script src=" //cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js "></script>


    <script src="//www.thebidadarivillasandspa.com/lumonata-content/themes/custom/js/script.min.js?v=2.2.5"></script>

    <!-- Sojern Tag v6_js, Pixel Version: 1 -->
    <script>
        (function() {

            var params = {};

            /* Please do not modify the below code. */
            var cid = [];
            var paramsArr = [];
            var cidParams = [];
            var pl = document.createElement('script');
            var defaultParams = {
                "vid": "hot"
            };
            for (key in defaultParams) {
                params[key] = defaultParams[key];
            };
            for (key in cidParams) {
                cid.push(params[cidParams[key]]);
            };
            params.cid = cid.join('|');
            for (key in params) {
                paramsArr.push(key + '=' + encodeURIComponent(params[key]))
            };
            pl.type = 'text/javascript';
            pl.async = true;
            pl.src = 'https://beacon.sojern.com/pixel/p/140882?f_v=v6_js&p_v=1&' + paramsArr.join('&');
            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(pl);
        })();
    </script>
    <!-- End Sojern Tag -->

    <!-- <script type="application/javascript" language="php"  src='https://watchmyrate.com/wmr.php?secure_code=sX/2xc2M6nOU1ja5XcMHrQ&date_cmformat=dd-M-yy'></script>  -->

    <script type="text/javascript">
        // change text book for covid19 
        // $('.homepage-hero .hero-right .booking-form .core_book .submit,.fixed-button header .book-now,.detail-room-amenities .inner button,.detail-hero .inner .boxs .submit,.archive-hero .inner .submit,.detail-page .page-content .af-c .submit').html('BOOK NOW <br>& PAY LATER'); 
        // please end the covid19
    </script>

    <style type="text/css">
        .homepage-hero .hero-right .booking-form .core_book .submit,
        header .book-now,
        .detail-room-amenities .inner button,
        .detail-hero .inner .boxs .submit,
        .archive-hero .inner .submit,
        .detail-page .page-content .af-c .submit {
            line-height: 20px;
            text-align: center;
        }

        header .book-now,
        .detail-hero .inner .boxs .submit,
        .archive-hero .inner .submit,
        .detail-hero .inner .center-content .boxs .submit,
        .detail-page .page-content .af-c .submit {
            display: flex;
            justify-content: center;
            align-items: center;
            flex-direction: column;
        }
    </style>

</body>

</html>