<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<?php

		require_once( realpath( dirname(__FILE__) . '/..' ) . '/lumonata_config.php' );
		require_once( realpath( dirname(__FILE__) . '/..' ) . '/lumonata-classes/user_privileges.php' );
		require_once( realpath( dirname(__FILE__) . '/..' ) . '/lumonata-classes/admin_menu.php' );
		require_once( realpath( dirname(__FILE__) . '/..' ) . '/lumonata-classes/actions.php' );
		require_once( realpath( dirname(__FILE__) . '/..' ) . '/lumonata-functions/settings.php' );
		require_once( realpath( dirname(__FILE__) . '/..' ) . '/lumonata-admin/admin_functions.php' );
		
		define( 'PLUGINS_PATH', realpath( dirname(__FILE__) . '/..' ) . '/lumonata-plugins' );

		if( !defined( 'SITE_URL' ) )
		{
		    $site = get_meta_data( 'site_url' );
		    $part = explode( '.', $_SERVER[ 'HTTP_HOST' ] );
		    $surl = ( $part[ 0 ] == 'www' ? 'www.' . $site : $site );
		    
		    define( 'SITE_URL', $surl );
		}

		if( !defined( 'HTSERVER' ) )
		{
			$ssl = get_meta_data( 'ssl_config' );
		    
		    define( 'HTSERVER', $ssl == 1 ? 'https:' : 'http:' );

		    if( isset( $_SERVER['HTTPS'] ) && $_SERVER['HTTPS'] == 'on' && $ssl == 0 )
		    {
		    	header( 'Location:http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] );

		    	exit();
		    }
		    elseif( !isset( $_SERVER['HTTPS'] ) && $ssl == 1 )
		    {
		    	header( 'Location:https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] );

		    	exit();
		    }
		}

		require_once( realpath( dirname(__FILE__) . '/..' ) . '/lumonata-functions/rewrite.php' );
		require_once( realpath( dirname(__FILE__) . '/..' ) . '/lumonata-functions/languages.php' );
		require_once( realpath( dirname(__FILE__) . '/..' ) . '/lumonata-functions/plugins.php' );
		require_once( realpath( dirname(__FILE__) . '/..' ) . '/lumonata-functions/menus.php' );

		$ftheme    = get_meta_data( 'front_theme', 'themes' );
		$api       = get_meta_data( 'ichronoz_api_key' );
		$site_url  = get_meta_data( 'site_url' );

		$temp_url  = '//' . $site_url . '/lumonata-content/themes/' . $ftheme;
		$book_url  = '//' . $site_url . '/booking/ichronoz';
		$version   = '2.0.6';
		$max_adult = 10;
		$max_child = 4;
		$max_pax   = 9;

	?>	

    <meta name="application-name" content="&nbsp;"/>
    <meta name="msapplication-TileColor" content="#FFFFFF" />
    <meta name="msapplication-TileImage" content="<?php echo $temp_url; ?>/favicon/mstile-144x144.png" />
    <meta name="msapplication-square70x70logo" content="<?php echo $temp_url; ?>/favicon/mstile-70x70.png" />
    <meta name="msapplication-square150x150logo" content="<?php echo $temp_url; ?>/favicon/mstile-150x150.png" />
    <meta name="msapplication-wide310x150logo" content="<?php echo $temp_url; ?>/favicon/mstile-310x150.png" />
    <meta name="msapplication-square310x310logo" content="<?php echo $temp_url; ?>/favicon/mstile-310x310.png" />

	<title>Booking - <?php echo trim( web_title() ); ?></title>

    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo $temp_url; ?>/favicon/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo $temp_url; ?>/favicon/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo $temp_url; ?>/favicon/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo $temp_url; ?>/favicon/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon-precomposed" sizes="60x60" href="<?php echo $temp_url; ?>/favicon/apple-touch-icon-60x60.png" />
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?php echo $temp_url; ?>/favicon/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?php echo $temp_url; ?>/favicon/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?php echo $temp_url; ?>/favicon/apple-touch-icon-152x152.png" />
    
    <link rel="icon" type="image/png" href="<?php echo $temp_url; ?>/favicon/favicon-196x196.png" sizes="196x196" />
    <link rel="icon" type="image/png" href="<?php echo $temp_url; ?>/favicon/favicon-96x96.png" sizes="96x96" />
    <link rel="icon" type="image/png" href="<?php echo $temp_url; ?>/favicon/favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="<?php echo $temp_url; ?>/favicon/favicon-16x16.png" sizes="16x16" />
    <link rel="icon" type="image/png" href="<?php echo $temp_url; ?>/favicon/favicon-128.png" sizes="128x128" />

	<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css?<?php echo $version; ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo $book_url; ?>/js/venobox/venobox.css?<?php echo $version; ?>" media="screen" />
	<link rel="stylesheet" type="text/css" href="<?php echo $book_url; ?>/css/font-awesome.css?<?php echo $version; ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo $book_url; ?>/css/ichronoz.css?<?php echo $version; ?>" />

    <link href="<?php echo $temp_url; ?>/css/style.min.css?v=1.0.0" rel="stylesheet">
    <link href="<?php echo $temp_url; ?>/css/mobile.min.css?v=1.0.0" rel="stylesheet">

	<?php

		function input_security( $validate = null )
		{ 
		    if( $validate == null )
		    { 
		        foreach( $_REQUEST as $key => $val )
		        { 
		            if( is_string( $val ) )
		            { 
		                $_REQUEST[ $key ] = htmlentities( $val ); 
		            }
		            elseif( is_array( $val ) )
		            { 
		                $_REQUEST[ $key ] = input_security( $val ); 
		            } 
		        }

		        foreach( $_GET as $key => $val )
		        { 
		            if( is_string( $val ) )
		            { 
		                $_GET[ $key ] = htmlentities( $val, ENT_QUOTES, 'UTF-8' ); 
		            }
		            elseif( is_array( $val ) )
		            { 
		                $_GET[ $key ] = input_security( $val ); 
		            } 
		        } 

		        foreach( $_POST as $key => $val )
		        { 
		            if( is_string( $val ) )
		            { 
		                $_POST[ $key ] = htmlentities( $val, ENT_QUOTES, 'UTF-8' ); 
		            }
		            elseif( is_array( $val ) )
		            { 
		                $_POST[ $key ] = input_security( $val ); 
		            } 
		        }
		    }
		    else
		    { 
		        foreach( $validate as $key => $val )
		        { 
		            if( is_string( $val ) )
		            { 
		                $validate[ $key ] = htmlentities( $val, ENT_QUOTES, 'UTF-8' ); 
		            }
		            elseif( is_array( $val ) )
		            { 
		                $validate[ $key ] = input_security( $val ); 
		            }

		            return $validate; 
		        } 
		    } 
		}

		//-- Validate String POST / GET / REQUEST        
	    input_security();

	    $from_date  = empty( $_POST[ 'check_in' ] ) ? date( 'd F Y' ) :  $_POST[ 'check_in' ];
	    $until_date = empty( $_POST[ 'check_out' ] ) ? date( 'd F Y', strtotime( '+1 day' ) ) :  $_POST[ 'check_out' ];

	    $fdate = new DateTime( $from_date );
		$udate = new DateTime( $until_date );

		$nite = $udate->diff( $fdate )->format( '%a' );

		if( $nite <= 0 )
		{
			$udate = new DateTime( date( 'd F Y', strtotime( '+2 days' ) ) );
		}

        //-- Set Variable
		$cd    = isset( $_REQUEST['cd'] ) ? $_REQUEST['cd'] : $fdate->format( 'j' );
	    $cm    = isset( $_REQUEST['cm'] ) ? $_REQUEST['cm'] : $fdate->format( 'n' );
	    $cy    = isset( $_REQUEST['cy'] ) ? $_REQUEST['cy'] : $fdate->format( 'Y' );

	    $cod   = isset( $_REQUEST['cod'] ) ? $_REQUEST['cod'] : $udate->format( 'j' );
	    $com   = isset( $_REQUEST['com'] ) ? $_REQUEST['com'] : $udate->format( 'n' );
	    $coy   = isset( $_REQUEST['coy'] ) ? $_REQUEST['coy'] : $udate->format( 'Y' );

	    $pax   = isset( $_REQUEST['pax'] ) ? $_REQUEST['pax'] : 1;
	    $cbs   = isset( $_REQUEST['cbs'] ) ? $_REQUEST['cbs'] : 0;
	    $adult = isset( $_REQUEST['adult'] ) ? $_REQUEST['adult'] : 1;
	    $child = isset( $_REQUEST['child'] ) ? $_REQUEST['child'] : 0;
	    $nite  = isset( $_REQUEST['nite'] ) ? $_REQUEST['nite'] : $nite;

	    $utc   = isset( $_REQUEST['utc'] ) ? $_REQUEST['utc'] : '';
	    $hid   = isset( $_REQUEST['hid'] ) ? $_REQUEST['hid'] : '';
	    $url   = isset( $_REQUEST['url'] ) ? $_REQUEST['url'] : '';
	    $promo = isset( $_REQUEST['promo'] ) ? $_REQUEST['promo'] : '';

	?>
</head>
<body class="booking">
    <header class="cream-bg">
        <div class="container-wrapp">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="logo">
                            <a href="//<?php echo $site_url; ?>">
                                <img src="<?php echo $temp_url; ?>/images/logo.svg" alt="Bidadari Villa" />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div class="wrapper">
		<div class="booking-header cream-bg">
		    <div class="container-wrapp">
		        <div class="container-fluid">
		            <div class="row">
		                <div class="col-md-12">
		                    <h1>Check Availability</h1>
		                    <div class="desc">
		                        <p>
		                        	Should you need any help or any question, please contact us at<br/>
			                        <a href="mailto:<?php echo get_meta_data( 'contact_info_email' ); ?>?subject=feedback"><?php echo get_meta_data( 'contact_info_email' ); ?></a> or call 
	                   				<a href="tel:<?php echo get_meta_data( 'contact_info_phone' ); ?>"><?php echo get_meta_data( 'contact_info_phone' ); ?></a>
	                   			</p>
		                    </div>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
    	<div class="booking-availability">
	        <div class="container-wrapp">
	            <div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<!-- iChronoz Content -->
							<div class="chr_container">
								<div ng-app="ichronoz" class="chr_row chronoz">
									<div ui-view></div>
									<section data-ng-controller="sessionExpired"></section>	
								</div>
							</div>
							<!-- End iChronoz Content -->
						</div>
					</div>
	            </div>
	        </div>
	    </div>

        <footer>
            <div class="container-wrapp">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <nav>
                                <?php echo the_menus( 'menuset=Footer&show_title=false&addClass=footer-menu clearfix' ); ?>

                                <small>&copy; <?php echo date( 'Y' ); ?> The Bidadari Luxury Villas and Spa</small>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>

	<script src="<?php echo $book_url; ?>/js/jquery.min.js?<?php echo $version; ?>"></script>
	<script src="<?php echo $book_url; ?>/js/angular.min.js?<?php echo $version; ?>"></script>
	<script src="<?php echo $book_url; ?>/js/angular-modal-service.js?<?php echo $version; ?>"></script>
	<script src="<?php echo $book_url; ?>/js/ui-bootstrap.min.js?<?php echo $version; ?>"></script>
	<script src="<?php echo $book_url; ?>/js/angular-ui-router.min.js?<?php echo $version; ?>"></script>
	<script src="<?php echo $book_url; ?>/js/angular-idle.min.js?<?php echo $version; ?>"></script>
	<script src="<?php echo $book_url; ?>/js/angular-sanitize.min.js?<?php echo $version; ?>"></script>
	<script src="<?php echo $book_url; ?>/js/angular-animate.js?<?php echo $version; ?>"></script>
	<script src="<?php echo $book_url; ?>/js/angular-filter.min.js?<?php echo $version; ?>"></script>

	<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js?<?php echo $version; ?>"></script>

	<script src="<?php echo $book_url; ?>/js/app.js?<?php echo $version; ?>"></script>
	<script src="<?php echo $book_url; ?>/js/book.js?<?php echo $version; ?>"></script>
	<script src="<?php echo $book_url; ?>/js/calendar.js?<?php echo $version; ?>"></script>
	<script src="<?php echo $book_url; ?>/js/venobox/venobox.min.js?<?php echo $version; ?>"></script>

	<script charset="utf-8">
		var objek = new Array( '<?php echo $hid . "', '" . $cd . "', '" . $cm . "', '" . $cy . "', '" . $cod . "', '" . $com . "', '" . $coy . "', '" . $pax . "', '" . $adult . "', '" . $child . "', '" . $url . "', '" . $api . "', '" . $utc . "', '" . $nite . "', '" . $cbs . "', '" . $promo . "', '" . $max_adult . "', '" . $max_child . "', '" . $max_pax ?>' );
		var tags  = objek;
	</script>
	<!-- End iChronoz js -->	
</body>
</html>