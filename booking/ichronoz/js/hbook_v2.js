	var defaultDay 		= 1;   	// Sets default days
	var defaultNights 	= 1; 	// Sets default nights
	var defaultYears 	= 4; 	// Sets default years
	var datePriorToday 	= "Check-in Date selected is prior Today's date. Please select next Today's date...";
	var datePriorTodayCheckout 	= "Check-out Date selected is prior Check-in date. Please select next Check-in date...";
	var formTopPosition = "40px";
	var formTopPositionAll = "90px";
	
	if(document.getElementById("searcForm")){
		document.getElementById("searcForm").style.visibility = "hidden";
		
		// var $ = jQuery.noConflict();
		jQuery(document).ready(function($) {
			var urlnya = window.location.host;

			// default condition form check-in date
			$('#datetimepicker1').datetimepicker({
		        format: 'DD/MM/YYYY',
		        minDate: moment().add(-1, 'days'),
		        //useCurrent: true, //Important! See issue #1075
		    });
		    // default condition form check-out date
		    $('#datetimepicker2').datetimepicker({
		        format: 'DD/MM/YYYY',
		        //useCurrent: true, //Important! See issue #1075
		    });

		    // on change check-in datepicker
		    $("#datetimepicker1").on("dp.change", function (e) {
		        
		        // set checkin date
		        var daten = new Date(e.date);
		        $("#cd").val( daten.getDate() );
		        $("#cy").val( daten.getFullYear() );
		        //set check-in month
		        // daten.setMonth( daten.getMonth() + 1);
				$("#cm").val( daten.getMonth() + 1 );
				// end set checkin date

				// set check-out date
				var daten2 = new Date(e.date);
		        daten2.setDate(daten2.getDate()+1); 

		        $("#cod").val( daten2.getDate() );
		        $("#coy").val( daten2.getFullYear() );
		        $('#datetimepicker2').data("DateTimePicker").minDate(daten2);
		        $('#datetimepicker2').data("DateTimePicker").date(daten2);
		        //set check-out month
		        // daten2.setMonth( daten2.getMonth() + 1);
				$("#com").val( daten2.getMonth() + 1 );
				// end set check-out date
			});
			
		    $('#url').val(urlnya);

		    chrFirstLoad(document.searcForm);
		    
		    document.getElementById("searcForm").style.visibility = "visible";

		});
	
	}// end isset searcForm	
    
    //function to load for first time / default condition
	var chrFirstLoad = function(form) {
		// Set nite
		if(document.getElementById("nite")){
			document.getElementById("nite").value = defaultNights;
			// the Check-in Dates
			chrCurDate();
		}

	}
	//function to set current date
	var chrCurDate = function() {
		var d = new Date();
		var dateNow = new Date(d.getTime() + (defaultDay * 86400000));
		var dayNow = dateNow.getDate();
		var monthNow = dateNow.getMonth()+1;
		var yearNow = dateNow.getFullYear();
		
		//set hiden input checkin
		document.getElementById("cd").value = dayNow;
		document.getElementById("cm").value = monthNow;
		document.getElementById("cy").value = yearNow;
		
		//chekcin plus default night
		dateNow.setDate(dateNow.getDate()+parseInt(document.getElementById("nite").value));
		//dateNow.setDate(dateNow.getDate()+defaultNights);
		
		//set hiden input
		document.getElementById("cod").value = ('0' + dateNow.getDate()).slice(-2);
		monnya = dateNow.setMonth(dateNow.getMonth() + 1);
		document.getElementById("com").value =  dateNow.getMonth();
		document.getElementById("coy").value = dateNow.getFullYear();
	}
	//function to check all condition on submit search form
	var chrCheckCond = function(form) {
		var checkin = document.getElementById("check_in").value.split("/");
		if(checkin != ''){
			var skg = new Date();
			var n = new Date(skg.getFullYear(), skg.getMonth(), skg.getDate());
			var d = new Date(checkin[2], parseInt(checkin[1])-1, checkin[0], n.getHours(), n.getMinutes(), n.getSeconds(), n.getMilliseconds());
			
			if(n > d){
				alert(datePriorToday);
				chrCurDate(form);
				return false;
			}

			var checkout = document.getElementById("check_out").value.split("/");
			var checkoutDate = new Date(checkout[2], parseInt(checkout[1])-1, checkout[0], n.getHours(), n.getMinutes(), n.getSeconds(), n.getMilliseconds());

			// set nite
			var date1 = d;
			var date2 = checkoutDate;
			var timeDiff = Math.abs(date2.getTime() - date1.getTime());
			var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
			//set nite
			document.getElementById("nite").value = diffDays;
		}
	};