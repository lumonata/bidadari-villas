== iChronoz Changelog ==

= 2.0.7.1 =
* Promocode banner
* Promocode applied

= 2.0.7 =
* N night deposit
* Promocode detail 
* Show refundable text
* Show Deposit payment text 

= 2.0.6 = 
* exchange currency to ichronoz server

= 2.0.5 = 
* php file to html
* mandatory field confirmation
* support GET and POST method
* Update 2017-02-08

= 2.0.4 =
* Search form option
* *Updated 2016-12-05*

= 2.0.3-1 =
* Increase Search Performance
* *Updated 2016-12-05*

= 2.0.3 =
* Promo list option style
* Percentege discount
* Update CSS
* *Updated 2016-10-01*
