<!DOCTYPE html>
<html lang="en">
<head>
  <title>Example Home Page</title>
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
</head>
<body>
	<div class="row">
		<nav class="navbar navbar-default navbar-fixed-top">
		  <div class="container">
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span> 
		      </button>
		      <a class="navbar-brand" href="#">Hotel Logo</a>
		    </div>
		    <div class="collapse navbar-collapse" id="myNavbar">
		      <ul class="nav navbar-nav navbar-right">
		        <li><a href="#about">Home</a></li>
		        <li><a href="#services">Promo</a></li>
		        <li><a href="#portfolio">Package</a></li>
		        <li><a href="#pricing">Location</a></li>
		        <li><a href="#contact">Contact</a></li>
		      </ul>
		    </div>
		  </div>
		</nav>
		<br /><br /><br />
	</div>

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<?php include "reservation_form_horizontal.php" ?>
			</div>
		</div>
	</div>
	
</body>
</html>