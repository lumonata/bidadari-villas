(function() {
	var path = 'ichronoz/view';
	var app = angular.module('ichronoz', ['ui.rCalendar','ngSanitize', 'ngIdle', 'view-directives','angularModalService','ui.bootstrap', 'ui.router','angular.filter'], function($httpProvider){	
		$httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
	});
  
  	app.config(function($stateProvider, $urlRouterProvider) {
		$urlRouterProvider.otherwise('/');
		$stateProvider
			// HOME STATES AND NESTED VIEWS
			.state('book', {
				url: '/',
				templateUrl: path+'/room-list.html',
				controller: 'MainController',
			})
			// nested list with custom controller
			.state('reserve', {
				url: '^/reservation/:id',
				views:{
					'@': {
					  templateUrl: path+'/reservation.html',
					  controller: 'ReserveController'
					}
				  },
				
			})
			.state('confirm', {
				url: '^/confirm',
				views:{
					'@': {
					  templateUrl: path+'/confirmation.html',
					  controller: 'ConfirmController'
					}
				  },
				
			})
			.state('wait', {
				url: '^/wait',
				views:{
					'@': {
					  templateUrl: path+'/wait.html',
					  controller: 'WaitController'
					}
				  },
				
			})
			.state('thanks', {
				url: '^/thanks',
				views:{
					'@': {
					  templateUrl: path+'/thanks.html',
					  controller: 'ThanksController'
					}
				  },
				
			})
			.state('thanksApi', {
				url: '^/thanksApi',
				views:{
					'@': {
					  templateUrl: path+'/thanksApi.html',
					  controller: 'ThanksApiController'
					}
				  },
				
			});
	});
  
  	app.filter('currencys', ['$filter', '$locale', function($filter, $locale) {
			return function (num, cur) {
				var sym = $locale.NUMBER_FORMATS.CURRENCY_SYM;
				if(cur == "IDR")
					return $filter('currency')(num, ' ', 0);
				else
					return $filter('currency')(num, ' ');
			};
		}
	]);
  
  	app.filter('split', function() {
	  return function(input, delimiter) {
		delimiter = delimiter || ','

		return input.split(delimiter);
	  }
	});

	app.filter('splitToArray', function() {
	  return function(input, delimiter, splitIndex) {
            // do some bounds checking here to ensure it has that index
            return input.split(delimiter)[splitIndex];
        }
    });
  
  	app.filter('each', function() {
	  return function(input, total) {
		total = parseInt(total);
		for (var i=0; i<total; i++)
		  input.push(i);
		return input;
	  };
	});
  
  	//search form service
  	app.factory("FormService", function() {
	  var service = {};
	  service.hid = ''; service.fd = '';  service.fm = '';  service.fy = '';
	  service.td = '';  service.tm = '';  service.ty = '';  service.pax = '';
	  service.adu = ''; service.chi = ''; service.url = ''; service.api = ''; service.utc = '';

	  service.updateValue = function(hid, fd, fm, fy, td, tm, ty, pax, adu, chi, url, api, utc){
		this.hid = hid; this.fd = fd;   this.fm = fm;
		this.fy = fy;   this.td = td;   this.tm = tm;
		this.ty = ty;   this.pax = pax; this.adu = adu;
		this.chi = chi; this.url = url; this.api = api; this.utc = utc;
	  }
	  
	  return service;
	});
  
  	//service for Rooms
  	app.factory('RoomService', ['$modal', function ($modal) { 
		var prod = '';
		return {
			getReserve: function() {
				return prod;
			},
			setReserve: function(products) {
				prod = products;
			},
			//getPersentase
			persen: function(oldRate, newRate) {
				persentase = ((oldRate - newRate) / oldRate) *100;
				persentase = persentase.toFixed(2);
				return persentase;
			},
			converter: function(product,ModalService) {
				$modal.open({
		          templateUrl: path+'/currencyConverter.html',
		          windowClass: 'modal-danger',
		          controller: 'ConverterController',
		          resolve: {
			         product: function () {
			           return product;
			         }
			       },
			      windowClass: 'chronoz-modal chronoz chr-bs',
		          backdrop:'static',
		        });
			}
		};
	}]);
  
  	//service for additional service
  	app.factory('AddService', [function () { 
		var srvc = '';
		return {
			getReserve: function() {
				return srvc;
			},
			setReserve: function(add) {
				srvc = add;
			}
		};
	}]);
  
	//service for Info
	app.factory('InfoService', [function () { 
		var srvc = '';
		return {
			getReserve: function() {
				return srvc;
			},
			setReserve: function(add) {
				srvc = add;
			}
		};
	}]);
  
  	app.factory('OptService', [function () { 
		var srvc = '';
		return {
			getReserve: function() {
				return srvc;
			},
			setReserve: function(opt) {
				srvc = opt;
			}
		};
	}]);
  
  	//service for Room detail
  	app.factory('ReserveService', [function () { 
		var prod = '';
		return {
			getReserve: function() {
				return prod;
			},
			setReserve: function(products) {
				prod = products;
			}
		};
	}]);

  	// Refreshing Page
  	app.factory('Refreshing', ['$state','$stateParams', function ($state, $stateParams) { 
		var prod = '';
		return {
			pageRefresh: function() {
				alert("refreshing page");
				
				return 'refresh';
			},
			
		};
	}]);
    
    app.config(function(IdleProvider, KeepaliveProvider, $provide) {
		IdleProvider.idle(5);
		IdleProvider.timeout(60*10);
		KeepaliveProvider.interval(5); 
    });

	app.run(['Idle','$rootScope', function(Idle, $rootScope) {
		$rootScope.paymentUrl = '';
		Idle.watch();
	}]);
  	
  	app.run(function($rootScope) {
        $rootScope.globalPajak = function(srvc, govTax, total_value) {
            srvcCharge = (total_value * srvc) / 100;
            ppn = ( (total_value + srvcCharge) * govTax) / 100;
            pajak = srvcCharge + ppn;
            return pajak;
        };
        $rootScope.onePajak = function(srvc, govTax, total_value, type) {
            srvcCharge = (total_value * srvc) / 100;
            ppn = ( (total_value + srvcCharge) * govTax) / 100;
            if(type == 'srvc_charge')
            	return srvcCharge;
            else if(type == 'gov_tax')
            	return ppn;
        };

        $rootScope.depositGuarantee = function(option, value, deposit, total, product) {
            if(option == 0){ // for day
            	// guarantee = value * deposit;
            	//new guarantee day
            	daynya = 0;
            	guarantee = 0;
            	angular.forEach(product.allotments, function(item) {
            		if(daynya < deposit){
						guarantee += item.rate;
						daynya++;
            		}
				});

            	return guarantee;
            }
            else if(option == 1){ // for %
            	guarantee = (total * deposit) / 100;
            	return guarantee;
            }
        };
        $rootScope.plural = function(value) {
	      if(value > 1)
	        return "s";
	      else
	        return "";
	    };
    });

  	app.controller("WaitController",["$timeout","$state","$location","$window","$rootScope",function(t,o,e,a,r){console.log("wait.."),console.log(r.paymentUrl),""==r.paymentUrl&&(a.location.href=a.document.location.origin),a.location.href=r.paymentUrl}]),app.controller("ThanksController",["$timeout","$location","$window",function(t,o,e){t(function(){e.location.href=e.document.location.origin},3e4)}]),app.controller("ThanksApiController",["$timeout","$location","$window",function(t,o,e){t(function(){e.location.href=e.document.location.origin},3e4)}]),app.controller("sessionExpired",["$scope","Idle","Keepalive","$modal",function(t,o,e,a){function r(){t.warning&&(t.warning.close(),t.warning=null),t.timedout&&(t.timedout.close(),t.timedout=null)}t.started=!1,t.$on("IdleStart",function(){}),t.$on("IdleEnd",function(){r()}),t.$on("IdleTimeout",function(){r(),t.timedout=a.open({templateUrl:path+"/timedout-dialog.html",windowClass:"modal-danger",controller:"ModalInstanceCtrl",backdrop:"static"})})}]),app.controller("ModalInstanceCtrl",["$scope","$modalInstance","$state","$stateParams","Idle","$window",function(t,o,e,a,r,n){t.ok=function(){n.location.reload(),e.go("book"),r.watch(),o.dismiss("cancel")}}]),app.controller("ModalPopupController",["$scope","$modalInstance","$state","$stateParams","Idle","$window","product",function(t,o,e,a,r,n,c){t.product=c,t.close=function(){o.dismiss("cancel")}}]),app.controller("MainController",["$modal","$filter","$window","$http","$rootScope","$scope","FormService","ModalService","RoomService","AddService","InfoService","$state","$stateParams","$timeout",function(t,o,e,a,r,n,c,u,d,l,s,p,m,f){r.searchShow=!1,n.havePromoCode=!1,n.roomTypeImage="",n.defaultDay=1,n.defaultNights=2,n.defaultYears=4,n.datePriorToday="Check-in Date selected is prior Today's date. Please select next Today's date...",n.chrGetDay=function(t){return n.weekday=new Array(7),n.weekday[0]="Sunday",n.weekday[1]="Monday",n.weekday[2]="Tuesday",n.weekday[3]="Wednesday",n.weekday[4]="Thursday",n.weekday[5]="Friday",n.weekday[6]="Saturday",n.weekday[t]},n.chrGetDay2=function(t){return n.weekday=new Array(7),n.weekday[0]="Sun",n.weekday[1]="Mon",n.weekday[2]="Tue",n.weekday[3]="Wed",n.weekday[4]="Thu",n.weekday[5]="Fri",n.weekday[6]="Sat",n.weekday[t]},n.chrGetMonth=function(t){return n.month=new Array,n.month[0]="Jan",n.month[1]="Feb",n.month[2]="Mar",n.month[3]="Apr",n.month[4]="May",n.month[5]="Jun",n.month[6]="Jul",n.month[7]="Aug",n.month[8]="Sep",n.month[9]="Oct",n.month[10]="Nov",n.month[11]="Dec",n.month[t]},n.getDay=function(t,o,e,a){n.n=new Date,n.d=new Date(e,o-1,t,n.n.getHours(),n.n.getMinutes(),n.n.getSeconds(),n.n.getMilliseconds()),n.dayCheckin=n.chrGetDay(n.d.getDay()),n.monthCheckin=n.chrGetMonth(n.d.getMonth()),n.yearCheckin=n.d.getFullYear(),n.cday=n.dayCheckin,n.cdays=n.dayCheckin+", "+("0"+n.d.getDate()).slice(-2)+" "+n.monthCheckin+" "+n.yearCheckin,r.checkinDate=n.d,n.f=new Date(e,o-1,t,n.n.getHours(),n.n.getMinutes(),n.n.getSeconds(),n.n.getMilliseconds()),n.f.setDate(n.f.getDate()+parseInt(a)),n.dayCheckout=n.chrGetDay2(n.f.getDay()),n.monthCheckout=n.chrGetMonth(n.f.getMonth()),n.yearCheckout=n.f.getFullYear(),n.td=n.f.getDate(),n.tm=n.f.getMonth()+1,n.ty=n.f.getFullYear(),n.coday=n.dayCheckout+", "+("0"+n.f.getDate()).slice(-2)+" "+n.monthCheckout+" "+n.yearCheckout},n.chrCheckout=function(){n.getDay(n.fd,n.fm,n.fy,n.nite)},n.Math=e.Math,n.myMessage=!0,n.products=[],n.add=[],n.info=[],n.convertCur=function(t,o){n.myLoading=!0,"USD"==t?(curFrom=t,curTo=o):(curFrom=o,curTo=t);var e=r.apiUrl+"/exchange?";a.jsonp(e,{params:{curFrom:curFrom,curTo:curTo,api:n.api,callback:"JSON_CALLBACK"}}).success(function(o){r.exchange="IDR"==t?1/o.rate:1*o.rate,console.log(r.exchange),n.myLoading=!1}).error(function(t,o){console.log("result failed")})},n.getLastMinuteDeal=function(t,e,a){n.skg=new Date,n.dateNow=o("date")(n.skg,"d/M/yyyy"),n.searchNow=t+"/"+e+"/"+a,n.dateNow==n.searchNow?r.lastMinuteDeal=!0:r.lastMinuteDeal=!1},n.addForm=function(){if(n.n=new Date,n.d=new Date(n.fy,n.fm-1,n.fd,n.n.getHours(),n.n.getMinutes(),n.n.getSeconds(),n.n.getMilliseconds()),n.cbs=0,n.n>n.d)return alert("Check-in Date selected is prior Today's date. Please select next Today's date..."),n.fd=r.fd,n.fm=r.fm,n.fy=r.fy,n.getDay(n.fd,n.fm,n.fy,n.nite),!1;n.myLoading=!0,n.getLastMinuteDeal(n.fd,n.fm,n.fy),c.updateValue(n.hid,n.fd,n.fm,n.fy,n.td,n.tm,n.ty,n.pax,n.adu,n.chi,n.url,n.api,n.utc);var t=r.apiUrl+"/availabilityV27/v3?";a.jsonp(t,{params:{id:n.hid,fd:n.fd,fm:n.fm,fy:n.fy,td:n.td,tm:n.tm,ty:n.ty,pax:n.pax,adu:n.adu,chi:n.chi,url:n.url,api:n.api,utc:n.utc,callback:"JSON_CALLBACK"}}).success(function(t){""==t||"gagal"==t.message?(n.myMessage=!1,n.hname=t.hname,n.hphone=t.hphone,n.gagal=t,n.showDescriptionNoResult=!1,n.loadEvents(t.calendarData),n.info=t.info,n.searchShow=!1):(n.myMessage=!0,n.myMessage=!0,d.setReserve(t.data),l.setReserve(t.add),s.setReserve(t.info),n.products=t.data,n.add=t.add,n.info=t.info,n.orderByField=[n.info.sorting],1==n.info.promoListType?n.reverseSort=!1:(n.orderByField="rate",n.reverseSort=!1),r.taxes=parseInt(t.govTax)+parseInt(t.srvcTax),r.govTax=parseInt(t.govTax),r.srvcTax=parseInt(t.srvcTax),t.info.cur!=t.info.localCur?n.convertCur(t.info.cur,t.info.localCur):r.exchange=0),r.fd=n.fd,r.fm=n.fm,r.fy=n.fy,n.myLoading=!1,n.searchShow=!n.searchShow}).error(function(t,o){n.myMessage=!1,n.myLoading=!1,e.location.href=e.document.location.origin})},n.customResult=null,n.showCustom=function(o,e){n.showCustoms=t.open({templateUrl:path+"/custom.html",windowClass:"modal-danger",controller:"ModalPopupController",resolve:{product:function(){return o},lastMinuteDeal:function(){return e}},windowClass:"chronoz-modal chronoz chr-bs",backdrop:"static"})},n.showTerm=function(o,e){n.showCustoms=t.open({templateUrl:path+"/term.html",windowClass:"modal-danger",controller:"ModalPopupController",resolve:{product:function(){return o},lastMinuteDeal:function(){return e}},windowClass:"chronoz-modal chronoz chr-bs",backdrop:"static"})},n.currencyConverter=function(t){d.converter(t,u)},n.getPersen=function(t,o){return d.persen(t,o)},n.step=1,n.isStep=function(t){return n.step===t},n.setStep=function(t){n.step=t},n.reservationEmail=function(){r.url=n.url,r.api=n.api,r.utc=n.utc;var t=r.apiUrl+"/response/reservation";a.jsonp(t,{params:{fd:n.fd,fm:n.fm,fy:n.fy,nite:n.nite,td:n.td,tm:n.tm,ty:n.ty,pax:n.pax,adu:n.adu,chi:n.chi,email:n.noEmail,hid:r.hid,fn:n.fn,ln:r.ln,url:r.url,api:r.api,utc:r.utc,callback:"JSON_CALLBACK"}}).success(function(t){p.go("thanks")}).error(function(t){p.transitionTo("book")})},n.cekBookingEmail="",n.cekBookingCode="",n.bookingStatus="",n.cekBooking=function(){r.url=n.url,r.api=n.api,r.utc=n.utc;var t=r.apiUrl+"/response/cekBooking";a.jsonp(t,{params:{email:n.cekBookingEmail,code:n.cekBookingCode,hid:r.hid,url:r.url,api:r.api,utc:r.utc,callback:"JSON_CALLBACK"}}).success(function(t){null!=t?n.bookingStatus=t:""==n.cekBookingCode?n.bookingStatus="Please Check your email address or code":n.bookingStatus="Result not found"}).error(function(t){p.transitionTo("book")})},n.isShowDetail=!0,n.cekRoomTypeImage=function(t,o){return n.roomTypeImage==t||1!=o||(n.roomTypeImage=t,!1)},n.min=function(t){return o("min")(o("map")(t,"rate"))},n.getValueArray=function(t,e,a){return o("splitToArray")(t,e,a)},n.changeMode=function(t){n.mode=t},n.today=function(){n.currentDate=new Date},n.isToday=function(){var t=new Date,o=new Date(n.currentDate);return t.setHours(0,0,0,0),o.setHours(0,0,0,0),t.getTime()===o.getTime()},n.loadEvents=function(t){n.eventSource=t},n.onEventSelected=function(t){console.log("All good!")},n.toOptions=function(t,o){result=[];for(var e=o;e<=t;e++)result.push(e);return result},n.formInit=function(t){for(n.myLoading=!0,n.hid=t[0],n.fd=t[1],n.fm=t[2],n.fy=t[3],n.td=t[4],n.tm=t[5],n.ty=t[6],n.pax=t[7],n.adu=t[8],n.chi=t[9],n.url=t[10],n.api=t[11],n.utc=t[12],n.nite=t[13],n.cbs=t[14],n.promo=t[15],r.max_adu_opt=n.toOptions(t[16],1),r.max_chi_opt=n.toOptions(t[17],0),r.max_pax_opt=n.toOptions(t[18],1),r.fd?(n.fd=r.fd,n.fm=r.fm,n.fy=r.fy):(r.fd=n.fd,r.fm=n.fm,r.fy=n.fy),n.getDay(n.fd,n.fm,n.fy,n.nite),c.updateValue(t[0],t[1],t[2],t[3],t[4],t[5],t[6],t[7],t[8],t[9],t[10],t[11],t[12]),r.hid=n.hid,r.url=n.url,r.api=n.api,r.utc=n.utc,n.d=new Date,n.y=n.d.getFullYear(),n.yearOpt=[],i=n.y;i<=n.y+(n.defaultYears-1);i++)n.yearOpt.push(i);n.getLastMinuteDeal(n.fd,n.fm,n.fy),r.apiUrl="https://api.ichronoz.net";var o=r.apiUrl+"/availabilityV27/v3?";a.jsonp(o,{params:{id:n.hid,fd:n.fd,fm:n.fm,fy:n.fy,td:n.td,tm:n.tm,ty:n.ty,pax:n.pax,adu:n.adu,chi:n.chi,url:n.url,api:n.api,utc:n.utc,promo:n.promo,callback:"JSON_CALLBACK"}}).success(function(t,o){""==t||"gagal"==t.message?(n.showDescriptionNoResult=!1,n.myMessage=!1,n.hname=t.hname,n.hphone=t.hphone,n.gagal=t,n.loadEvents(t.calendarData),n.info=t.info):(n.myMessage=!0,d.setReserve(t.data),l.setReserve(t.add),s.setReserve(t.info),n.products=t.data,n.add=t.add,n.info=t.info,n.orderByField=[n.info.sorting],1==n.info.promoListType?n.reverseSort=!1:(n.orderByField="rate",n.reverseSort=!1),r.taxes=parseInt(t.govTax)+parseInt(t.srvcTax),r.govTax=parseInt(t.govTax),r.srvcTax=parseInt(t.srvcTax),t.info.cur!=t.info.localCur?n.convertCur(t.info.cur,t.info.localCur):r.exchange=0,1==n.cbs&&(n.myLoading=!1,$(".venobox").venobox(),$(".venobox_cancel").venobox({framewidth:"100%",titleattr:"data-title",numeratio:!0,infinigall:!0,overlayclose:!1}),$("#fram_cancel").attr("href",t.info.url),$("#fram_cancel")[0].click(),f(function(){$(".vbox-close, .vbox-overlay").trigger("click"),p.go("book")},9e5))),n.myLoading=!1}).error(function(t,o){n.myMessage=!1,n.myLoading=!1,alert("We apologize for any inconvenience - This Page is Under Maintenance...")})},n.formInit(e.tags),n.$on("msgid",function(t,o){r.fd=n.fd=o[0].fd,r.fm=n.fm=o[0].fm+1,r.fy=n.fy=o[0].fy,n.chrCheckout(),n.showDescriptionNoResult=!0})}]),app.controller("ReserveController",["$modal","$scope","$rootScope","ReserveService","RoomService","AddService","OptService","InfoService","ModalService","$state","$stateParams","FormService",function(t,o,e,a,r,n,c,i,u,d,l,s){o.exchange=e.exchange,""==r.getReserve()&&d.transitionTo("book"),o.myLoading=!0,o.myMessage=!1,e.indexnya=l.id,o.plus=0,o.products=r.getReserve(),o.add=n.getReserve(),o.info=i.getReserve(),o.taxes=e.taxes,o.govTax=e.govTax,o.srvcTax=e.srvcTax,o.product=o.products[l.id],o.adult=parseInt(s.adu),o.child=parseInt(s.chi),o.room="Villa"==o.product.accType?parseInt(o.product.rooms):parseInt(s.pax),e.extraBedQty=o.product.jumlah,o.minQty=e.extraBedQty,o.changeQty=function(){o.product.adult>o.product.maxPax?(o.product.jumlah=parseInt(o.product.jumlah)+parseInt(o.product.roomPerVilla),o.product.jumlah>o.product.maxExtrabed&&(o.product.jumlah=o.product.maxExtrabed),o.minQty=o.product.jumlah):o.product.jumlah>0&&(o.product.jumlah=o.product.jumlah-o.product.roomPerVilla,o.minQty=o.minQty-o.product.roomPerVilla),o.product.jumlah<o.minQty&&(o.product.jumlah=o.minQty)},o.changeQtyExtrabed=function(){o.product.jumlah<o.minQty&&(o.product.jumlah=o.minQty)},o.optional={childBreakfast:{status:"1"==o.product.breakfast,qty:0,subtotalCB:0},adultBreakfast:{status:"1"==o.product.breakfast,qty:0,subtotalAB:0},adultExtrabed:{qty:0,adultPax:0}},o.plusUnit=function(){o.plus=1},o.currencyConverter=function(t){r.converter(t,u)},o.getTotalBeforeTax=function(){var t=0;return angular.forEach(o.product.services,function(o){o.qty&&(t+=o.qty*o.rate)}),t+=1!=o.product.breakfast&&o.product.adult>0&&o.optional.adultBreakfast.status?o.product.adult*o.product.breakfastRate*o.product.rooms:0,t+=o.product.jumlah>0?o.product.jumlah*o.product.extrabedRate*o.product.rooms:0,t+=o.product.rooms*o.product.rate},o.getTotal=function(){var t=0;return angular.forEach(o.product.services,function(o){o.qty&&(t+=o.qty*o.rate)}),t+=1!=o.product.breakfast&&o.product.adult>0&&o.optional.adultBreakfast.status?o.product.adult*o.product.breakfastRate*o.product.rooms:0,t+=o.product.jumlah>0?o.product.jumlah*o.product.extrabedRate*o.product.rooms:0,t+=o.product.rooms*o.product.rate,0==o.product.taxIncluded&&(t+=e.globalPajak(parseInt(o.product.srvcTax),parseInt(o.product.govTax),t)),t},o.taxInclude=function(t){return includeTax=e.globalPajak(parseInt(o.product.srvcTax),parseInt(o.product.govTax),t),0==o.product.taxIncluded?total=t+includeTax:total=t,total},o.saveAll=function(t){o.loadImage=!0,0==o.optional.adultBreakfast.status?o.breakfastRate=0:o.breakfastRate=o.product.breakfastRate,o.optional={childBreakfast:{status:o.optional.childBreakfast.status,qty:o.product.child,item:o.breakfastRate*o.product.rooms,subtotalCB:o.product.child*o.breakfastRate*o.product.rooms},adultBreakfast:{status:o.optional.adultBreakfast.status,qty:o.product.adult*parseInt(o.product.roomPerVilla),item:o.breakfastRate*o.product.rooms,subtotalAB:o.product.adult*o.breakfastRate*o.product.rooms},adultExtrabed:{qty:o.product.jumlah,item:o.product.extrabedRate*o.product.rooms,subtotalExtra:o.product.jumlah*o.product.extrabedRate*o.product.rooms}},d.go("confirm"),n.setReserve(o.product.services),c.setReserve(o.optional),a.setReserve(o.product),s.adu=o.product.adult,s.chi=o.product.child,s.pax=o.product.rooms;o.loadImage=!1},o.getPersen=function(t,o){return r.persen(t,o)},o.myLoading=!1,o.customResult=null,o.showCustom=function(e,a){o.showCustoms=t.open({templateUrl:path+"/custom.html",windowClass:"modal-danger",controller:"ModalPopupController",resolve:{product:function(){return e},lastMinuteDeal:function(){return a}},windowClass:"chronoz-modal chronoz chr-bs",backdrop:"static"})},o.showTerm=function(e,a){o.showCustoms=t.open({templateUrl:path+"/term.html",windowClass:"modal-danger",controller:"ModalPopupController",resolve:{product:function(){return e},lastMinuteDeal:function(){return a}},windowClass:"chronoz-modal chronoz chr-bs",backdrop:"static"})}}]),app.controller("ConfirmController",["$modal","$http","$scope","$rootScope","RoomService","ReserveService","AddService","OptService","InfoService","$state","$stateParams","$window","ModalService","$timeout",function(t,o,e,a,r,n,c,i,u,d,l,s,p,m){if(e.paymentDesc="",e.exchange=a.exchange,""==r.getReserve())d.transitionTo("book");else{e.payment=!0,e.tabActive="newGuest",e.myLoading=!1,e.indexnya=a.indexnya,e.product=n.getReserve(),e.add=c.getReserve(),e.optional=i.getReserve(),e.info=u.getReserve(),e.taxes=a.taxes,e.govTax=a.govTax,e.srvcTax=a.srvcTax,e.additional=!1,1==e.product.breakfast&&0==e.optional.adultBreakfast.status&&(e.optional.adultBreakfast.qty=0,e.optional.adultBreakfast.subtotalAB=0),e.extrabeds=a.extraBedQty,e.optional.adultExtrabed.qty<1&&(e.optional.adultExtrabed.qty=0,e.optional.adultExtrabed.subtotalExtra=0),e.getTotalBeforeTax=function(){var t=0;return angular.forEach(e.add,function(o){o.qty&&(t+=o.qty*o.rate)}),t+=1!=e.product.breakfast&&e.optional.adultBreakfast.qty>0?e.optional.adultBreakfast.subtotalAB:0,t+=e.optional.adultExtrabed.qty>0?e.optional.adultExtrabed.subtotalExtra:0,t+=e.product.rooms*e.product.rate},e.getTotal=function(){var t=0;return angular.forEach(e.add,function(o){o.qty&&(t+=o.qty*o.rate)}),t+=1!=e.product.breakfast&&e.optional.adultBreakfast.qty>0?e.optional.adultBreakfast.subtotalAB:0,t+=e.optional.adultExtrabed.qty>0?e.optional.adultExtrabed.subtotalExtra:0,t+=e.product.rooms*e.product.rate,0==e.product.taxIncluded&&(t+=a.globalPajak(e.product.srvcTax,e.product.govTax,t)),t},e.taxInclude=function(t){return includeTax=a.globalPajak(parseInt(e.product.srvcTax),parseInt(e.product.govTax),t),0==e.product.taxIncluded?total=t+includeTax:total=t,total};e.service=[],e.add.forEach(function(t){e.service.push(JSON.stringify(t))}),e.booking=[{arriveDetail:"",comment:"",mailingList:!0,arriveBy:"",cTitle:"",cFname:"",cLname:"",cPhone:"",cMobile:"",cAddress:"",cEmail:"",cCity:"",cState:"",cZipcode:"",cCountry:"",arriveBy:"",arriveDetail:"",roomId:"",roomName:"",arriveDate:"",departureDate:"",night:"",rooms:"",paxs:"",promoType:"",roomSubtotal:"",payment:!0,aggrement:!1,reviews:[]}],e.getGuest=function(t){e.myLoading=!0;var r=a.apiUrl+"/availability/guest/10";o.jsonp(r,{params:{email:t,url:e.url,api:e.api,utc:e.utc,callback:"JSON_CALLBACK"}}).success(function(t){e.myMessage="Returning Guest",e.booking.payment=!0,e.booking.cTitle=t.guest.gtitle,e.booking.cFname=t.guest.gfirst_name,e.booking.cLname=t.guest.glast_name,e.booking.cPhone=t.guest.gphone,e.booking.cMobile=t.guest.gmobile,e.booking.cEmail=t.guest.gemail,e.booking.cAddress=t.guest.gaddress,e.booking.cCity=t.guest.gcity,e.booking.cState=t.guest.gstate,e.booking.cZipcode=t.guest.gzipcode,e.booking.cCountry=t.guest.gcountry,e.booking.cPassport=t.guest.gpassport,e.booking.cNationality=t.guest.gnationality,e.myLoading=!1}).error(function(t){e.myMessage="New Guest",e.booking.cTitle="",e.booking.cFname="",e.booking.cLname="",e.booking.cPhone="",e.booking.cMobile="",e.booking.cAddress="",e.booking.cCity="",e.booking.cState="",e.booking.cZipcode="",e.booking.cCountry="",e.myLoading=!1})},e.submitForm=function(t){t?(e.booking.payment=!1,e.bookNow()):e.booking.payment=!0},e.bookNow=function(){for(var t in e.loadImage=!0,e.addServices={items:[]},e.roomRates={items:[]},e.addServices.items.push({id:e.product.id,name:e.product.name+" - "+e.product.typeName+" "+e.product.rooms+" Room x "+e.product.night+" Night",price:e.taxInclude(e.product.rooms*e.product.rate),qty:1,amount:e.taxInclude(e.product.rooms*e.product.rate),type:1}),1!=e.product.breakfast&&0!=e.optional.adultBreakfast.status&&e.addServices.items.push({id:e.product.id,name:"Breakfast for "+e.product.adult*parseInt(e.product.roomPerVilla)*e.product.rooms+" Adult x "+e.product.night+" Night",price:e.taxInclude(e.optional.adultBreakfast.subtotalAB),qty:1,amount:e.taxInclude(e.optional.adultBreakfast.subtotalAB),type:1}),e.extrabeds>0&&e.addServices.items.push({id:e.product.id,name:"Extrabed for "+e.optional.adultExtrabed.qty+" Bed x "+e.product.night+" Night",price:e.taxInclude(e.optional.adultExtrabed.subtotalExtra),qty:1,amount:e.taxInclude(e.optional.adultExtrabed.subtotalExtra),type:1}),e.add)e.addServices.items.push({id:e.add[t].idAdd,name:e.add[t].name,price:e.taxInclude(e.add[t].qty*e.add[t].rate),qty:e.add[t].qty,amount:e.taxInclude(e.add[t].qty*e.add[t].rate),type:0});for(var t in e.product.allotments)e.roomRates.items.push({date:e.product.allotments[t].dates,oldRate:e.taxInclude(e.product.allotments[t].oldRate),rate:e.taxInclude(e.product.allotments[t].rate)});e.rateTax=e.taxInclude(e.product.rate),e.optional.adultBreakfast.subtotalAB=e.taxInclude(e.optional.adultBreakfast.subtotalAB),e.optional.adultExtrabed.subtotalExtra=e.taxInclude(e.optional.adultExtrabed.subtotalExtra),e.product.desc=e.product.desc+" "+e.product.surchargeDescription+" "+e.product.specialDescription,e.totalGuarantee=a.depositGuarantee(e.product.guaranteeOpt,e.taxInclude(e.product.rate/e.product.night*e.product.rooms),e.product.guarantee,e.getTotal(),e.product);var n=a.apiUrl+"/availability/create/posts",c={hid:e.product.hid,roomId:e.product.id,roomName:e.product.name,promoType:e.product.promoType,rate:e.rateTax,night:e.product.night,roomDesc:e.product.desc,rooms:e.product.rooms,promoDate:e.product.promoDate,cutOff:e.product.cutOfDay,lastMinDeal:e.product.lastMinute,condition:e.product.condition,govTax:e.product.govTax,srvcTax:e.product.srvcTax,arrivalDate:e.product.fromdate,departureDate:e.product.todate,paymentOption:e.booking.paymentOption,total:e.getTotal(),roomRate:e.roomRates,currency:e.product.cur,adult:e.product.adult,child:e.product.child,breakfastInclude:e.product.breakfast,roomPerVilla:e.product.roomPerVilla,exchangeRate:a.exchange,surchargeDescription:e.product.surchargeDescription,totalGuarantee:e.totalGuarantee,totalRest:e.getTotal()-e.totalGuarantee,breakfast:e.optional.adultBreakfast.qty,breakfastCharge:e.optional.adultBreakfast.subtotalAB,extrabed:e.optional.adultExtrabed.qty,extrabedCharge:e.optional.adultExtrabed.subtotalExtra,addSrvc:e.addServices,cTitle:e.booking.cTitle,cFname:e.booking.cFname,cLname:e.booking.cLname,cPhone:e.booking.cPhone,cMobile:e.booking.cMobile,cAddress:e.booking.cAddress,cCity:e.booking.cCity,cState:e.booking.cState,cZipcode:e.booking.cZipcode,cCountry:e.booking.cCountry,cEmail:e.booking.cEmail,cNationality:e.booking.cNationality,cPassport:e.booking.cPassport,arriveBy:e.booking.arriveBy,arriveDetail:e.booking.arriveDetail,smook:e.booking.smookOption,comment:e.booking.comment,mailingList:e.booking.mailingList,beddingPreff:e.booking.beddingPreff,id:1,url:a.url,api:a.api,utc:a.utc,model:"posts",callback:"JSON_CALLBACK"};o({method:"POST",url:n,data:$.param(c),headers:{"Content-Type":"application/x-www-form-urlencoded"}}).success(function(t){s.location.href=null!=t?t:s.document.location.origin,r.setReserve(""),e.loadImage=!1}).error(function(t){console.log(t)})},e.getPersen=function(t,o){return r.persen(t,o)},e.customResult=null,e.showCustom=function(o,a){e.showCustoms=t.open({templateUrl:path+"/custom.html",windowClass:"modal-danger",controller:"ModalPopupController",resolve:{product:function(){return o},lastMinuteDeal:function(){return a}},windowClass:"chronoz-modal chronoz chr-bs",backdrop:"static"})},e.showTerm=function(o,a){e.showCustoms=t.open({templateUrl:path+"/term.html",windowClass:"modal-danger",controller:"ModalPopupController",resolve:{product:function(){return o},lastMinuteDeal:function(){return a}},windowClass:"chronoz-modal chronoz chr-bs",backdrop:"static"})},e.showTerms=function(o,a){e.showCustomss=t.open({templateUrl:path+"/terms.html",windowClass:"modal-danger",controller:"ModalPopupController",resolve:{product:function(){return o},lastMinuteDeal:function(){return a}},windowClass:"chronoz-modal chronoz chr-bs",backdrop:"static"})}}}]),app.controller("ConverterController",["$http","$scope","$rootScope","$modalInstance","$state","$stateParams","Idle","$window","product",function(t,o,e,a,r,n,c,i,u){o.myLoading=!1,o.product=u,o.display=!0,o.close=function(){a.dismiss("cancel")},o.code="USD",o.convertCur=function(a,r){o.myLoading=!0,"IDR"==o.product.cur?(curFrom=o.code,curTo=a):(curFrom=a,curTo=o.code);var n=e.apiUrl+"/exchange?";t.jsonp(n,{params:{curFrom:curFrom,curTo:curTo,api:o.api,callback:"JSON_CALLBACK"}}).success(function(t){"IDR"==o.product.cur?o.convertVal=r/t.rate:o.convertVal=r*t.rate,console.log(r),console.log(t.rate),console.log(o.convertVal),o.myLoading=!1}).error(function(t,o){console.log("result failed")})},o.convertCur(u.cur,u.rate)}]);
  
})();
