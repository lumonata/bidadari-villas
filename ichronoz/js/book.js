(function(){
	var path = 'ichronoz/view';

	var app = angular.module('view-directives',[]);
	
	app.directive('ngEnter', function () {
		return function (scope, element, attrs) {
			element.bind("keydown keypress", function (event) {
				if(event.which === 13) {
					scope.$apply(function (){
						scope.$eval(attrs.ngEnter);
					});

					event.preventDefault();
				}
			});
		};
	});
	
	app.directive("searchForm", function() {
		return {
		  restrict: 'E',
		  templateUrl: path+'/search-form.html'
		};
	  });

	app.directive("banner", function() {
		return {
		  restrict:"E",
		  templateUrl: path+'/banner.html'
		};
	  });
		
	app.directive("roomItems", function() {
		return {
		  restrict: 'E',
		  templateUrl: path+'/room-items.html'
		};
	  });
	
	app.directive("roomItemsRecommend", function() {
		return {
		  restrict: 'E',
		  templateUrl: path+'/room-items-recommend.html'
		};
	  });
	  
	app.directive("roomItems2", function() {
		return {
		  restrict: 'E',
		  templateUrl: path+'/room-items2.html'
		};
	  });
	
	app.directive("roomItems3", function() {
		return {
		  restrict: 'E',
		  templateUrl: path+'/room-items3.html'
		};
	  });
 	  
    app.directive("notResult", function() {
		return {
		  restrict:"E",
		  templateUrl: path+'/not-result.html'
		};
	  });
    app.directive("cekBooking", function() {
		return {
		  restrict:"E",
		  templateUrl: path+'/cek-booking.html'
		};
	  });
	  
	app.directive("bookingList", function() {
		return {
		  restrict: "E",
		  templateUrl: path+'/booking-list.html',
		  controller: function() {
			this.list = 1;

			this.isSet = function(checkList) {
			  return this.list === checkList;
			};

			this.setList = function(activeList) {
			  this.list = activeList;
			};
		  },
		  controllerAs: "list"
		};
	  });

	app.directive("roomList", function() {
		return {
		  restrict:"E",
		  templateUrl: path+'/room-list.html'
		};
	  });
	  
	app.directive("reservation", function() {
		return {
		  restrict:"E",
		  templateUrl: path+'/reservation.html'
		};
	  });

	app.directive("reservationSummary", function() {
		return {
		  restrict:"E",
		  templateUrl: path+'/reservationSummary.html'
		};
	  });

	app.directive("reservationDescription", function() {
		return {
		  restrict:"E",
		  templateUrl: path+'/reservationDescription.html'
		};
	  });

	app.directive("scroll", function ($window) {
	    return function(scope, element, attrs) {
	      
	        angular.element($window).bind("scroll", function() {
	            if (this.pageYOffset >= 100 && this.pageYOffset <= 199) {
	            	
	                 scope.scrolled = true;
	                 scope.wrapperClass = 'chr_fixed-wrapper chr_fixed-wrapper-fix';
	                 scope.wrapperFixed = 'chr_fixed';
	                 scope.summaryDetails = true;
	                 // console.log('Scrolled below header.');
	             } else if (this.pageYOffset >= 200) {
	                 scope.scrolled = true;
	                 scope.wrapperClass = 'chr_fixed-wrapper chr_fixed-wrapper-fix';
	                 scope.wrapperFixed = 'chr_fixed';
	                 scope.summaryDetails = false;
	                 // console.log('Scrolled below header.');
	             } else {
	                 scope.scrolled = false;
	                 scope.wrapperClass = 'chr_fixed-wrapper';
	                 scope.wrapperFixed = '';
	                 scope.summaryDetails = true;
	                 // console.log('Header is in view.');
	             }
	            scope.$apply();
	        });
	    };
	});

	
	 	  
})();
