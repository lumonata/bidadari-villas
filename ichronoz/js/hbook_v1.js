	var defaultDay 		= 1;   	// Sets default days
	var defaultNights 	= 2; 	// Sets default nights
	var defaultYears 	= 4; 	// Sets default years
	var datePriorToday 	= "Check-in Date selected is prior Today's date. Please select next Today's date...";
	
	function chrGetDay(d) {
		var weekday = new Array(7);
		weekday[0] = "Sunday";
		weekday[1] = "Monday";
		weekday[2] = "Tuesday";
		weekday[3] = "Wednesday";
		weekday[4] = "Thursday";
		weekday[5] = "Friday";
		weekday[6] = "Saturday";
		return weekday[d];
	}
	
	function chrGetDay2(d) {
		var weekday = new Array(7);
		weekday[0] = "Sun";
		weekday[1] = "Mon";
		weekday[2] = "Tue";
		weekday[3] = "Wed";
		weekday[4] = "Thu";
		weekday[5] = "Fri";
		weekday[6] = "Sat";
		return weekday[d];
	}
	
	function chrGetMonth(d) {
		var month = new Array();
		month[0] = "Jan";
		month[1] = "Feb";
		month[2] = "Mar";
		month[3] = "Apr";
		month[4] = "May";
		month[5] = "Jun";
		month[6] = "Jul";
		month[7] = "Aug";
		month[8] = "Sep";
		month[9] = "Oct";
		month[10] = "Nov";
		month[11] = "Dec";
		return month[d];
	}
	
	function chrFirstLoad(form) {
		// Set nite
		form.nite.selectedIndex = defaultNights-1;
		// the Check-in Dates
		chrCurDate(form);
		// Set the Check-out Dates
		chrCheckout(form);
	}
	
	function chrCurDate(form) {
		var d = new Date();
		var dateNow = new Date(d.getTime() + (defaultDay * 86400000));
		var dayNow = dateNow.getDate();
		var monthNow = dateNow.getMonth();
		var yearNow = dateNow.getFullYear() - form.cy.options[0].text;
		
		// Set Check-in Dates
		//document.getElementById("cday").innerHTML = chrGetDay(dateNow.getDay());
		form.cd.selectedIndex = dayNow-1;
		form.cm.selectedIndex = monthNow;
		form.cy.selectedIndex = yearNow;
	}
	
	function chrCheckout(form) {
		var n = new Date();
		var d = new Date(form.cy.options[form.cy.selectedIndex].text, form.cm.selectedIndex, form.cd.selectedIndex+1, n.getHours(), n.getMinutes(), n.getSeconds(), n.getMilliseconds() );
		
		d.setDate(d.getDate() + (form.nite.selectedIndex+1));
		var dayCheckout   = chrGetDay2(d.getDay());
		var monthCheckout = chrGetMonth(d.getMonth());
		var yearCheckout  = d.getFullYear();
		document.getElementById("coday").innerHTML =  dayCheckout + ', ' + ("0" + d.getDate()).slice(-2) + " " + monthCheckout + " " + yearCheckout;
			
		//set hiden input
		document.getElementById("cod").value = d.getDate();
		document.getElementById("com").value = d.getMonth();
		document.getElementById("coy").value = d.getFullYear();
		//alert(d.getFullYear());
		
		var f = new Date(form.cy.options[form.cy.selectedIndex].text, form.cm.selectedIndex, form.cd.selectedIndex+1, n.getHours(), n.getMinutes(), n.getSeconds(), n.getMilliseconds() );
		var dayCheckin   = chrGetDay(f.getDay());
		document.getElementById("cday").innerHTML = dayCheckin;
	}
	
	function chrCheckCond(form) {
		var n = new Date();
		var d = new Date(form.cy.options[form.cy.selectedIndex].text, form.cm.selectedIndex, form.cd.selectedIndex+1, n.getHours(), n.getMinutes(), n.getSeconds(), n.getMilliseconds() );
		
		if(d < n){
			alert(datePriorToday);
			chrCurDate(form);
			chrCheckout(form)
			return false;
		}else{
			chrCheckout(form)
		}
	}
	
	//Generate years options for year select list
	function yearOpt(form){
		d = new Date();
		y = d.getFullYear();
		for(i = y ; i <= y+(defaultYears-1) ; i++ ){
		document.write('<option value="' + i + '">' + i + '</option>');
		}
	}
	
