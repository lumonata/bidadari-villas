function show_popup( htitle, message, callback )
{
    jQuery('<div></div>').dialog({
        modal: true,
        draggable: false,
        resizable: false,
        title: htitle,
        open: function()
        {
            jQuery(this).html(message);
        },
        buttons:
        {
            Close: function()
            {
                jQuery( this ).dialog('close');

                if( callback!='' )
                {
                    eval(callback);
                }
            }
        }
    });
}

function delete_user_func( table, pkey )
{
    jQuery('.delete-link').unbind();
    jQuery('.delete-link').on('click', function(e){
        var url = jQuery(this).attr('href');
        var prm = new Object();
            prm.pkey = pkey;
            prm.prc  = 'delete';
            prm.id   = jQuery(this).attr('id');

        jQuery('<div></div>').dialog({
            modal: true,
            draggable: false,
            resizable: false,
            title: 'Notification',
            open: function(){
                jQuery(this).html( 'Are you sure want to delete this user?' );
            },
            buttons: {
                Yes: function(){
                    var popup = jQuery(this);

                    jQuery.ajax({
                        method: 'POST',
                        url: url,
                        data: prm,
                        dataType : 'json',
                        success: function(e){
                            popup.dialog('close');

                            table.ajax.reload(function(){

                                if( e.result == 'success' )
                                {
                                    show_popup( 'Notification', 'This user has been successfully deleted' );
                                }
                                else if( e.result == 'failed' )
                                {
                                    show_popup( 'Warning', 'Sorry, This user can\'t be deleted. Please try again later' );
                                }

                                set_button_access();

                                delete_user_func( table, pkey );

                            }, false);
                        },
                        error: function(e){
                           popup.dialog('close');

                           show_popup( 'Warning', 'Sorry, This user can\'t be deleted. Please try again later' );
                        }
                    });
                },
                No: function(){
                    jQuery( this ).dialog('close');
                }
            }
        });

        return false;
    });
}

function delete_article_func( table, pkey, type )
{
    jQuery('.delete-link').unbind();
    jQuery('.delete-link').on('click', function(e){
        var url = jQuery(this).attr('href');
        var prm = new Object();
            prm.pkey  = pkey;
            prm.state = type;
            prm.prc   = 'delete';
            prm.id    = jQuery(this).attr('id');

        jQuery('<div></div>').dialog({
            modal: true,
            draggable: false,
            resizable: false,
            title: 'Notification',
            open: function(){
                jQuery(this).html( 'Are you sure want to delete this data?' );
            },
            buttons: {
                Yes: function(){
                    var popup = jQuery(this);

                    jQuery.ajax({
                        method: 'POST',
                        url: url,
                        data: prm,
                        dataType : 'json',
                        success: function(e){
                            popup.dialog('close');

                            table.ajax.reload(function(){

                                if( e.result == 'success' )
                                {
                                    show_popup( 'Notification', 'This data has been successfully deleted' );
                                }
                                else if( e.result == 'failed' )
                                {
                                    show_popup( 'Warning', 'Sorry, This data can\'t be deleted. Please try again later' );
                                }

                                set_button_access();

                                delete_article_func( table, pkey, type );

                            }, false);
                        },
                        error: function(e){
                           popup.dialog('close');

                           show_popup( 'Warning', 'Sorry, This data can\'t be deleted. Please try again later' );
                        }
                    });
                },
                No: function(){
                    jQuery( this ).dialog('close');
                }
            }
        });

        return false;
    });
}

function delete_rule_func( table, pkey, type )
{
    jQuery('.delete-link').unbind();
    jQuery('.delete-link').on('click', function(e){
        var url = jQuery(this).attr('href');
        var prm = new Object();
            prm.pkey  = pkey;
            prm.group = type;
            prm.prc   = 'delete';
            prm.id    = jQuery(this).attr('id');

        jQuery('<div></div>').dialog({
            modal: true,
            draggable: false,
            resizable: false,
            title: 'Notification',
            open: function(){
                jQuery(this).html( 'Are you sure want to delete this data?' );
            },
            buttons: {
                Yes: function(){
                    var popup = jQuery(this);

                    jQuery.ajax({
                        method: 'POST',
                        url: url,
                        data: prm,
                        dataType : 'json',
                        success: function(e){
                            popup.dialog('close');

                            table.ajax.reload(function(){

                                if( e.result == 'success' )
                                {
                                    show_popup( 'Notification', 'This data has been successfully deleted' );
                                }
                                else if( e.result == 'failed' )
                                {
                                    show_popup( 'Warning', 'Sorry, This data can\'t be deleted. Please try again later' );
                                }

                                set_button_access();

                                delete_rule_func( table, pkey, type );

                            }, false);
                        },
                        error: function(e){
                           popup.dialog('close');

                           show_popup( 'Warning', 'Sorry, This data can\'t be deleted. Please try again later' );
                        }
                    });
                },
                No: function(){
                    jQuery( this ).dialog('close');
                }
            }
        });

        return false;
    });
}

function delete_language_func( table, pkey )
{
    jQuery('.delete-link').unbind();
    jQuery('.delete-link').on('click', function(e){
        var url = jQuery(this).attr('href');
        var prm = new Object();
            prm.pkey  = pkey;
            prm.prc   = 'delete';
            prm.id    = jQuery(this).attr('id');

        jQuery('<div></div>').dialog({
            modal: true,
            draggable: false,
            resizable: false,
            title: 'Notification',
            open: function(){
                jQuery(this).html( 'Are you sure want to delete this data?' );
            },
            buttons: {
                Yes: function(){
                    var popup = jQuery(this);

                    jQuery.ajax({
                        method: 'POST',
                        url: url,
                        data: prm,
                        dataType : 'json',
                        success: function(e){
                            popup.dialog('close');

                            table.ajax.reload(function(){

                                if( e.result == 'success' )
                                {
                                    show_popup( 'Notification', 'This language has been successfully deleted' );
                                }
                                else if( e.result == 'failed' )
                                {
                                    show_popup( 'Warning', 'Sorry, This language can\'t be deleted. Please try again later' );
                                }

                                set_button_access();

                                delete_language_func( table, pkey );

                            }, false);
                        },
                        error: function(e){
                           popup.dialog('close');

                           show_popup( 'Warning', 'Sorry, This language can\'t be deleted. Please try again later' );
                        }
                    });
                },
                No: function(){
                    jQuery( this ).dialog('close');
                }
            }
        });

        return false;
    });
}

function set_button_access()
{
    jQuery('input[name=select_all]').removeAttr('checked');

    jQuery('.table td input').each(function(){
        jQuery('.table td input').removeAttr('checked');
    });
    
    jQuery('input[name=select_all]').on('change', function(){
        var checked_status = this.checked;
        
        jQuery('.select').each(function(){
            this.checked = checked_status;

            if( checked_status )
            {
                jQuery('input[name=edit]').removeClass('btn_edit_disable');
                jQuery('input[name=edit]').addClass('btn_edit_enable');
                jQuery('input[name=edit]').removeAttr('disabled');
                
                jQuery('input[name=delete]').removeClass('btn_delete_disable');
                jQuery('input[name=delete]').addClass('btn_delete_enable');
                jQuery('input[name=delete]').removeAttr('disabled');
                
                jQuery('input[name=publish]').removeClass('btn_publish_disable');
                jQuery('input[name=publish]').addClass('btn_publish_enable');
                jQuery('input[name=publish]').removeAttr('disabled');
                
                jQuery('input[name=unpublish]').removeClass('btn_save_changes_disable');
                jQuery('input[name=unpublish]').addClass('btn_save_changes_enable');
                jQuery('input[name=unpublish]').removeAttr('disabled');
            }
            else
            {
                jQuery('input[name=edit]').removeClass('btn_edit_enable');
                jQuery('input[name=edit]').addClass('btn_edit_disable');
                jQuery('input[name=edit]').attr('disabled', 'disabled');
                
                jQuery('input[name=delete]').removeClass('btn_delete_enable');
                jQuery('input[name=delete]').addClass('btn_delete_disable');
                jQuery('input[name=delete]').attr('disabled', 'disabled');
                
                jQuery('input[name=publish]').removeClass('btn_publish_enable');
                jQuery('input[name=publish]').addClass('btn_publish_disable');
                jQuery('input[name=publish]').attr('disabled', 'disabled');
                
                jQuery('input[name=unpublish]').removeClass('btn_save_changes_enable');
                jQuery('input[name=unpublish]').addClass('btn_save_changes_disable');
                jQuery('input[name=unpublish]').attr('disabled', 'disabled');
                
            }
        });
    });

    jQuery('.select').unbind('change');
    jQuery('.select').on('change', function(){
       if( jQuery('.select:checked').length > 0 )
       {
            jQuery('input[name=edit]').removeClass('btn_edit_disable');
            jQuery('input[name=edit]').addClass('btn_edit_enable');
            jQuery('input[name=edit]').removeAttr('disabled');
            
            jQuery('input[name=delete]').removeClass('btn_delete_disable');
            jQuery('input[name=delete]').addClass('btn_delete_enable');
            jQuery('input[name=delete]').removeAttr('disabled');
            
            jQuery('input[name=publish]').removeClass('btn_publish_disable');
            jQuery('input[name=publish]').addClass('btn_publish_enable');
            jQuery('input[name=publish]').removeAttr('disabled');
            
            jQuery('input[name=unpublish]').removeClass('btn_save_changes_disable');
            jQuery('input[name=unpublish]').addClass('btn_save_changes_enable');
            jQuery('input[name=unpublish]').removeAttr('disabled');
       }
       else
       {
            jQuery('input[name=edit]').removeClass('btn_edit_enable');
            jQuery('input[name=edit]').addClass('btn_edit_disable');
            jQuery('input[name=edit]').attr('disabled', 'disabled');
            
            jQuery('input[name=delete]').removeClass('btn_delete_enable');
            jQuery('input[name=delete]').addClass('btn_delete_disable');
            jQuery('input[name=delete]').attr('disabled', 'disabled');
            
            jQuery('input[name=publish]').removeClass('btn_publish_enable');
            jQuery('input[name=publish]').addClass('btn_publish_disable');
            jQuery('input[name=publish]').attr('disabled', 'disabled');
            
            jQuery('input[name=unpublish]').removeClass('btn_save_changes_enable');
            jQuery('input[name=unpublish]').addClass('btn_save_changes_disable');
            jQuery('input[name=unpublish]').attr('disabled', 'disabled');
       }
    });
}

function set_language_button_action()
{
    jQuery('.button-language ul li').click(function(){
        var val = jQuery(this).find('a.btn').attr('data-filter');

        jQuery('.button-language ul li').removeClass('active');
        jQuery('.content-lang').removeClass('active');
        jQuery('.' + val).addClass('active');
        jQuery(this).addClass('active');

        return false;
    });
}

function sef_scheme_action( index, post_id, type, sef, url )
{    
    jQuery('#the_sef_' + index).click(function(){
        jQuery('#the_sef_' + index).hide();
        jQuery('#sef_box_' + index).show();
    });

    jQuery('#edit_sef_' + index).click(function(){
        jQuery('#the_sef_' + index).hide();
        jQuery('#sef_box_' + index).show();
    });

    if( typeof post_id != 'undefined' )
    {
        jQuery('#done_edit_sef_'+ index).on('click', function(){                                                              
            var new_sef = jQuery('#sef_box_val_'+ index).val();
            var more    = new_sef.length > 50 ? '...' : '';
            var prm     = new Object;
                prm.title      = jQuery('.article-title').val();
                prm.pkey       = "update-sef-scheme";
                prm.post_id    = post_id;
                prm.new_sef    = new_sef;
                prm.type       = type;

            jQuery('#the_sef_'+ index).show();
            jQuery('#sef_box_'+ index).hide();

            jQuery.ajax({
                url: url,
                data: prm,
                type: 'POST',
                dataType: 'json',
                success: function( e ){
                    if( e.result == 'failed' )
                    {
                        jQuery('#sef_box_val_'+ index).val( sef );
                        jQuery('#the_sef_content_'+ index).html( sef.substr( 0, 50 ) );
                    }
                    else if( e.result == 'success' )
                    {
                        console.log(new_sef);
                        jQuery('#the_sef_content_'+ index).html( new_sef.substr(0,50) + more );
                    }
                }
            });
        });
    }
    else
    {
        jQuery('#done_edit_sef_' + index).click(function(){
            var new_sef = jQuery('.sef-box-' + index).val();
            var more    = new_sef.length > 50 ? '...' : '';

            jQuery('#the_sef_content_' + index).html( new_sef.substr( 0, 50 ) + more );
            jQuery('#the_sef_' + index).show();
            jQuery('#sef_box_' + index).hide();
        });
    }
}

function set_navigation_action()
{
    jQuery('a#applications').click(function(){
        jQuery('#applications_list').slideToggle( 100 );

        return false;
    });


    jQuery('a#plugins').click(function(){
        jQuery('#plugins_list').slideToggle( 100 );

        return false;
    });
}

jQuery(document).ready(function(){
    set_navigation_action();
    set_language_button_action();   
});