
<?php

/*
| -----------------------------------------------------------------------------
| Set Frontend Actions
| -----------------------------------------------------------------------------
*/
function init_actions()
{
    // add_actions( 'header-actions', 'get_custom_css', '//'.TEMPLATE_URL.'/css/font.css' );
    

    add_actions( 'footer-actions', 'get_custom_javascript', '//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js');
    // add_actions( 'footer-actions', 'get_custom_javascript', '//'.TEMPLATE_URL.'/js/blazy.min.js' );



    if( isset( $actions->action[ '404_page' ] ) === false )
    {
        // add_actions( 'header-actions', 'get_custom_css', '//'.TEMPLATE_URL.'/css/plugin.css');

        add_actions( 'footer-actions', 'get_custom_javascript', '//'.TEMPLATE_URL.'/js/plugin.js');
    }
}

/*
| -----------------------------------------------------------------------------
| Set Frontend Template
| -----------------------------------------------------------------------------
*/
function set_frontend_template()
{
    global $actions;

    set_template( TEMPLATE_PATH . '/index.html' );

    set_footer_awards_content();

    add_block( 'not-found-block', 'nf-block' );
    add_block( 'content-block', 'ct-block' );

    if( isset( $actions->action[ '404_page' ] ) && $actions->action[ '404_page' ][ 'func_name' ][ 0 ] )
    {
        parse_template( 'not-found-block', 'nf-block' );
    }
    else
    {
        add_variable( 'link_why', get_meta_data('hm_link_promo'));
        parse_template( 'content-block', 'ct-block' );
    }
}

/*
| -----------------------------------------------------------------------------
| Set Frontend Content
| -----------------------------------------------------------------------------
*/
function set_frontend_content()
{
    init_actions();
 
    if( is_home() )
    {
        return homepage_content();
    }
    elseif( is_ajax_function() )
    {
        return ajax_content();
    }
    elseif( is_archive( 'activities' ) )
    {
        return activities_archive_content();
    }
    elseif( is_archive( 'gallery' ) )
    {
        return galleries_archive_content();
    }
    elseif( is_archive( 'locations' ) )
    {
        return locations_archive_content();
    }
    elseif( is_archive( 'offers' ) )
    {
        return offers_archive_content();
    }
    elseif( is_details( 'offers' ) )
    {
        return offers_detail_content();
    }
    elseif( is_archive( 'rooms' ) )
    {
        return rooms_archive_content();
    }
    elseif( is_details( 'restaurant' ) )
    {
        return restaurant_detail_content();
    }
    elseif( is_details( 'activities' ) )
    {
        return activities_detail_content();
    }
    elseif( is_archive( 'services' ) )
    {
        return services_archive_content();
    }
    elseif( is_details( 'rooms' ) )
    {
        return rooms_detail_content();
    }
    elseif( is_details( 'spa' ) )
    {
        return spa_detail_content();
    }
    elseif( is_page( 'page_name=contact-us' ) )
    {
        return contact_content();
    }
    elseif( is_thanks() )
    {
        return thanks_content();
    }
    elseif( is_page() )
    {
        return page_content();
    }
    else
    {
        return page_404_content();
    }
}

/*
| -----------------------------------------------------------------------------
| Homepage Content
| -----------------------------------------------------------------------------
*/
function homepage_content()
{
    set_template( TEMPLATE_PATH . '/template/home.html', 'home-template' );


    // NOTE: GET DATE
    $stop_date = new DateTime();
    $day_today = $stop_date->format('j');
    $month_today = $stop_date->format('M');
    $fulldate_today = $stop_date->format('Y-n-j ');
    add_variable( 'day_today', $day_today );
    add_variable( 'month_today', $month_today );
    add_variable( 'fulldate_today', $fulldate_today );

    $stop_date->modify('+1 day');
    $day_tommorow = $stop_date->format('j');
    $month_tommorow = $stop_date->format('M');
    $fulldate_tommorow = $stop_date->format('Y-n-j ');
    add_variable( 'day_tommorow', $day_tommorow );
    add_variable( 'month_tommorow', $month_tommorow );
    add_variable( 'fulldate_tommorow', $fulldate_tommorow );
    
    add_variable( 'welcome_img', get_meta_data('homepage_top_image','static_setting'));
    add_variable( 'welcome_link', get_meta_data('hm_link_promo'));
    add_variable( 'status_poster', get_meta_data('banner_option'));

    //-- Set Meta Data
    set_homepage_metadata();

    //-- Set Why Stay With Us Section
    set_homepage_reason_content();

    //-- Set Why Stay With Us Section
    set_homepage_review_content();

    //-- Set Map Locations
    set_homepage_location_content();

    //-- Set Slideshow Section
    set_homepage_slideshow_content();

    //-- Set Room Type Section
    set_homepage_room_type_content();

    //-- Set Activities Section
    set_homepage_activities_content();

    //-- Set Special Offers Section
    set_homepage_special_promo_content();

    //-- Set Triple Box Media & Brief Section
    set_homepage_triple_box_media_content();
    add_block( 'homepage-promo-block', 'hp-block', 'home-template' );
    add_block( 'content-block', 'c-block', 'home-template' );
    
    $get_article_promo = get_page_detail( 'promo' );
   
    extract($get_article_promo);
    $promo_image = post_gallery( $id, 'pages' );
    $showpromo = '';

     

    if ($get_article_promo['lstatus'] == 'draft') {
        $showpromo = 'style="display:none"';
    }
    if (empty($promo_image)) {
        $showpromo = 'style="display:none"';
    }
    add_variable( 'showpromo', $showpromo );

    $html_poster = '<div class="ovly-poster">
                      <img class="noselect pp-cl" src="'.get_theme_img().'/icon_close.svg">
                    </div>
                    <button class="button-promo" id="button-promo">
                      <img src="'.get_theme_img().'/icon-prev-white.svg"/>
                    </button>
                    <div class="promo-slider">';

    foreach($promo_image as $value) {
      add_variable( 'src_image_thumb', $value['img_thumb'] );
      // add_variable( 'src_image', $value['img_medium'] );
      add_variable( 'link_image', $value['img_alt'] );

      add_variable( 'src_image', optimized_images( HTSERVER.$value['img_medium'], 700 ) );
      add_variable( 'src_image_m', optimized_images( HTSERVER.$value['img_medium'], 300 ) );

      $html_poster .= '
        <a href="'.$value['img_alt'].'" target="_blank">
              <img class="lazy-load the-poster image-promo" 
                   data-src="'.optimized_images( HTSERVER.$value['img_medium'], 700 ).'" 
                   m-src="'.optimized_images( HTSERVER.$value['img_medium'], 300 ).'" 
                   alt="Special Promotion">
              </a>
      ';

      parse_template( 'homepage-promo-block', 'hp-block', true);
    }

    $html_poster .= '<button class="button-promo-next" id="button-promo-next">
          <img src="'.get_theme_img().'/icon-next-white.svg"/>
        </button>';

        add_variable( 'data_poster', $html_poster );
    
    add_variable( 'site-url', site_url() );
    add_variable( 'image-url', get_theme_img() );
    add_variable( 'sticky-note', sticky_notes() );
        add_variable( 'landing_link_video', get_meta_data( 'landing_link_video' ) );

    add_variable( 'section-one-title', get_meta_data( 'landing_page_section_1_title' ) );
    add_variable( 'section-one-content', get_meta_data( 'landing_page_section_1_content' ) );
    add_variable( 'section-one-subtitle', get_meta_data( 'landing_page_section_1_subtitle' ) );

    add_variable( 'section-two-title', get_meta_data( 'landing_page_section_2_title' ) );
    add_variable( 'section-two-subtitle', get_meta_data( 'landing_page_section_2_subtitle' ) );
    add_variable( 'section-two-link-text', get_meta_data( 'landing_page_section_2_link_text' ) );

    add_variable( 'section-three-title', get_meta_data( 'landing_page_section_3_title' ) );
    add_variable( 'section-three-link-text', get_meta_data( 'landing_page_section_3_link_text' ) );

    add_variable( 'section-four-title', get_meta_data( 'landing_page_section_4_title' ) );
    add_variable( 'section-four-content', get_meta_data( 'landing_page_section_4_content' ) );

    add_variable( 'section-five-link-text', get_meta_data( 'landing_page_section_5_link_text' ) );
    add_variable( 'section-five-button-text', get_meta_data( 'landing_page_section_5_button_text' ) );

    add_variable( 'section-six-title', get_meta_data( 'landing_page_section_6_title' ) );
    add_variable( 'section-six-content', get_meta_data( 'landing_page_section_6_content' ) );

    add_variable( 'section-seven-link', get_meta_data( 'landing_page_section_7_link' ) );
    add_variable( 'section-seven-title', get_meta_data( 'landing_page_section_7_title' ) );
    add_variable( 'section-seven-link-text', get_meta_data( 'landing_page_section_7_link_text' ) );
        
    add_actions( 'body-class-actions', 'get_body_class', 'homepage' );

    // add_actions( 'header-actions', 'get_custom_css', '//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css' );
    // add_actions( 'header-actions', 'get_custom_css', '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css' );

    add_actions( 'footer-actions', 'get_custom_javascript', '//'.TEMPLATE_URL.'/js/home_plugin.js');
    // add_actions( 'footer-actions', 'get_custom_javascript', '//maps.googleapis.com/maps/api/js?key=AIzaSyC9mKKb71I104S_XsT8jhw1DQ2FGqtLkds&callback=init_map_location' );

    parse_template( 'content-block', 'c-block', false );

    return return_template( 'home-template' );
}

/*
| -----------------------------------------------------------------------------
| Homepage - Set Metadata
| -----------------------------------------------------------------------------
*/
function set_homepage_metadata()
{
    $mtitle = strip_tags( get_meta_data( 'meta_title' ) );
    $mkey   = strip_tags( get_meta_data( 'meta_keywords' ) );
    $mdesc  = strip_tags( get_meta_data( 'meta_description' ) );
    $mtitle = empty( $mtitle ) ? trim( web_title() ) : $mtitle;

    add_meta_data_value( $mtitle, $mkey, $mdesc );
}

/*
| -----------------------------------------------------------------------------
| Homepage - Set Slideshow
| -----------------------------------------------------------------------------
*/
function set_homepage_slideshow_content()
{
    add_block( 'homepage-slide-loop-block', 'hsl-block', 'home-template' );
    add_block( 'homepage-slide-block', 'hs-block', 'home-template' );

    $data = get_homepage_slideshow();

    if( empty( $data ) === false )
    {
        $i = 0;

        foreach( $data as $d )
        {
            add_variable( 'slide-name', $d['img_name'] );
            add_variable( 'slide-thumb', $d['img_low_res'] );
            add_variable( 'slide-url', optimized_images( HTSERVER . $d['img_url'], 1400 ) );
            add_variable( 'slide-url-thumb', optimized_images( HTSERVER . $d['img_url'], 500 ) );
            parse_template( 'homepage-slide-loop-block', 'hsl-block', true );

            $i++;
        }
        
        parse_template( 'homepage-slide-block', 'hs-block', false );
    }
}

/*
| -----------------------------------------------------------------------------
| Homepage - Set Triple Box Media & Brief
| -----------------------------------------------------------------------------
*/
function set_homepage_triple_box_media_content()
{
    add_block( 'triple-box-loop-block', 'tbl-block', 'home-template' );
    add_block( 'triple-box-block', 'tb-block', 'home-template' );

    $data = get_triple_box_media();

    if( empty( $data ) === false )
    {
        $i = 0;

        foreach( $data as $d )
        {
            if( $i == 0 )
            {
                $class = 'bottom';
            }
            elseif( end( $data ) == $d )
            {
                $class = 'top';
            }
            else
            {
                $class = 'middle';
            }

            add_variable( 'triple-box-img-class', $class );
            add_variable( 'triple-box-img-url', $d['img_url'] );
            add_variable( 'triple-box-img-name', $d['img_name'] );
            add_variable( 'triple-box-img-thumb-url', $d['img_low_res'] );

            parse_template( 'triple-box-loop-block', 'tbl-block', true );

            $i++;
        }
        
        parse_template( 'triple-box-block', 'tb-block', false );
    }
}

/*
| -----------------------------------------------------------------------------
| Homepage - Set Room Type
| -----------------------------------------------------------------------------
*/
function set_homepage_room_type_content()
{
    add_block( 'room-type-loop-block', 'rtl-block', 'home-template' );
    add_block( 'room-type-block', 'rt-block', 'home-template' );

    $nimg = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mM0MTL+DwAC1QGar5rpbgAAAABJRU5ErkJggg==';
    $data = get_archive_list( 'rooms', 3 );

    if( $data['rows'] > 0 )
    {
        foreach( $data['items'] as $d )
        {
            add_variable( 'title', $d['post_title'] );
            add_variable( 'permalink', $d['post_permalink'] );
            add_variable( 'brief-text', empty( $d['post_brief'] ) ? '' : '<p>' . $d['post_brief'] . '</p>' );

            if( empty( $d['post_featured_img'] ) )
            {
                add_variable( 'featured-img-thumb', $nimg );
                add_variable( 'featured-img', $nimg );
            }
            else
            {
                add_variable( 'featured-img-thumb', optimized_images( HTSERVER . $d['post_featured_img'], 15 ) );
                add_variable( 'featured-img', optimized_images( HTSERVER . $d['post_featured_img'], null, 700 ) );
            }

            parse_template( 'room-type-loop-block', 'rtl-block', true );
        }

        add_variable( 'archive-rooms-link', get_archive_link( 'rooms' ) );
        
        parse_template( 'room-type-block', 'rt-block' );
    }
}

/*
| -----------------------------------------------------------------------------
| Homepage - Set Special Offers
| -----------------------------------------------------------------------------
*/
function set_homepage_special_promo_content()
{
    add_block( 'special-promo-loop-block', 'spl-block', 'home-template' );
    add_block( 'special-promo-block', 'sp-block', 'home-template' );

    $nimg = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mM0MTL+DwAC1QGar5rpbgAAAABJRU5ErkJggg==';
    $data = get_archive_list( 'offers', 5 );

    if( $data['rows'] > 0 )
    {
        foreach( $data['items'] as $d )
        {
            add_variable( 'title', $d['post_title'] );
            add_variable( 'permalink', $d['post_permalink'] );

            if( empty( $d['post_featured_img'] ) )
            {
                add_variable( 'featured-img-thumb', $nimg );
                add_variable( 'featured-img', $nimg );
            }
            else
            {
                add_variable( 'featured-img-thumb', optimized_images( HTSERVER . $d['post_featured_img'], 15 ) );
                add_variable( 'featured-img', optimized_images( HTSERVER . $d['post_featured_img'], 820 ) );
            }

            parse_template( 'special-promo-loop-block', 'spl-block', true );
        }

        add_variable( 'archive-offers-link', get_archive_link( 'offers' ) );

        parse_template( 'special-promo-block', 'sp-block' );
    }
}

/*
| -----------------------------------------------------------------------------
| Homepage - Set Why Stay With Us
| -----------------------------------------------------------------------------
*/
function set_homepage_reason_content()
{
    add_block( 'reason-loop-block', 'rl-block', 'home-template' );
    add_block( 'reason-block', 'r-block', 'home-template' );


    $nimg = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mM0MTL+DwAC1QGar5rpbgAAAABJRU5ErkJggg==';
    $data = get_homepage_slide();
    $surl = site_url();

    if( empty( $data ) === false )
    {
        foreach( $data as $d )
        {
            if( intval( $d['use_link'] ) == 0 )
            {
                add_variable( 'permalink', 'javascript:;' );
            }
            else
            {
                if( empty( $d['link_url'] ) )
                {
                    if( $d['link_type'] == 'homepage' )
                    {
                        add_variable( 'permalink', '//' . $surl . '/' );
                    }
                    elseif( $d['link_type'] == 'archive' )
                    {
                        add_variable( 'permalink', '//' . $surl . '/' . $d['post_link_id'] . '/' );
                    }
                    elseif( $d['link_type'] == 'detail' )
                    {
                        add_variable( 'permalink', permalink( $d['post_link_id'] ) );
                    }
                    else
                    {
                        add_variable( 'permalink', 'javascript:;' );
                    }
                }
                else
                {
                    add_variable( 'permalink', $d['link_url'] );
                }
            }

            if( empty( $d['img_url'] ) )
            {
                add_variable( 'featured-img-thumb', $nimg );
                add_variable( 'featured-img', $nimg );
            }
            else
            {
                add_variable( 'featured-img-thumb', optimized_images( HTSERVER . $d['img_url'], 15 ) );
                add_variable( 'featured-img', optimized_images( HTSERVER . $d['img_url'], 820 ) );
            }

            add_variable( 'title', $d['title'] );
            add_variable( 'subtitle', $d['subtitle'] );
            add_variable( 'permalink-target', $d['link_target'] );

            parse_template( 'reason-loop-block', 'rl-block', true );
        }

        parse_template( 'reason-block', 'r-block' );
    }
}

/*
| -----------------------------------------------------------------------------
| Homepage - Set Activities
| -----------------------------------------------------------------------------
*/
function set_homepage_activities_content()
{
    add_block( 'activity-loop-block', 'al-block', 'home-template' );
    add_block( 'activity-block', 'a-block', 'home-template' );

    $nimg = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mM0MTL+DwAC1QGar5rpbgAAAABJRU5ErkJggg==';
    $data = get_archive_list( 'activities', 5 );

    if( $data['rows'] > 0 )
    {
        extract( $data['items'][0] );

        foreach( $data['items'] as $i => $d )
        {
            add_variable( 'slide-title', $d['post_title'] );
            add_variable( 'slide-content', $d['post_brief'] );
            add_variable( 'slide-permalink', $d['post_permalink'] );

            if( empty( $d['post_featured_img'] ) )
            {
                add_variable( 'slide-featured-img-thumb', $nimg );
                add_variable( 'slide-featured-img', $nimg );
            }
            else
            {
                add_variable( 'slide-featured-img-thumb', optimized_images( HTSERVER . $d['post_featured_img'], 15 ) );
                add_variable( 'slide-featured-img', optimized_images( HTSERVER . $d['post_featured_img'], 832 ) );
            }

            parse_template( 'activity-loop-block', 'al-block', true );
        }

        add_variable( 'title', $post_title );
        add_variable( 'content', $post_brief );
        add_variable( 'permalink', $post_permalink );

        parse_template( 'activity-block', 'a-block' );
    }

    add_variable( 'archive-activities-link', get_archive_link( 'activities' ) );
}

/*
| -----------------------------------------------------------------------------
| Homepage - Set Map Locations
| -----------------------------------------------------------------------------
*/
function set_homepage_location_content()
{
    add_block( 'locations-category-loop-block', 'lcl-block', 'home-template' );
    add_block( 'locations-loop-block', 'll-block', 'home-template' );

    $data = get_archive_list( 'locations' );
    $rule = array();

    if( empty( $data['items'] ) === false )
    {
        foreach( $data['items'] as $idx => $d )
        {
            if( empty( $d['post_category'] ) === false )
            {
                $rule[ $d['post_category']['categories'][0]['order'] ] = $d['post_category']['categories'][0];

                add_variable( 'loc-name', $d['post_title'] );
                add_variable( 'loc-type', $d['post_category']['categories'][0]['sef'] );
                add_variable( 'loc-cordinate', $d['post_additional_field']['loc_cordinate'] );

                parse_template( 'locations-loop-block', 'll-block', true );
            }
        }
    }

    if( empty( $rule ) === false )
    {
        ksort( $rule );

        foreach( $rule as $d )
        {
            add_variable( 'type', $d['sef'] );
            add_variable( 'name', $d['name'] );

            parse_template( 'locations-category-loop-block', 'lcl-block', true );
        }
    }
}

/*
| -----------------------------------------------------------------------------
| Homepage - Set Reviews Content
| -----------------------------------------------------------------------------
*/
function set_homepage_review_content()
{
    add_block( 'reviews-loop-block', 'rvl-block', 'home-template' );
    add_block( 'reviews-block', 'rv-block', 'home-template' );

    $data = get_archive_list( 'reviews' );

    if( empty( $data['items'] ) === false )
    {
        foreach( $data['items'] as $idx => $d )
        {
            add_variable( 'review-title', $d['post_title'] );
            add_variable( 'review-rate', get_rating_star( $d['post_additional_field']['review_rate'] ) );
            add_variable( 'review-by', empty( $d['post_additional_field']['review_by'] ) ? '' : sprintf( '<p class="review-by"><span>Reviewed By</span> %s</p>', $d['post_additional_field']['review_by'] ) );
            add_variable( 'review-content', post_review_brief( $d['post_content'], $d['post_additional_field']['review_link'], $d['post_additional_field']['review_target'] ) );

            parse_template( 'reviews-loop-block', 'rvl-block', true );
        }
        
        parse_template( 'reviews-block', 'rv-block' );
    }
}

function get_rating_star( $rate = 0 )
{
    return '
    <span class="rating">
        <label class="' . ( $rate > 0 ? ( $rate == 0.5 ? 'rating-half' : 'rating-fill' ) : 'rating-empty' ) . '"></label>
        <label class="' . ( $rate > 1 ? ( $rate == 1.5 ? 'rating-half' : 'rating-fill' ) : 'rating-empty' ) . '"></label>
        <label class="' . ( $rate > 2 ? ( $rate == 2.5 ? 'rating-half' : 'rating-fill' ) : 'rating-empty' ) . '"></label>
        <label class="' . ( $rate > 3 ? ( $rate == 3.5 ? 'rating-half' : 'rating-fill' ) : 'rating-empty' ) . '"></label>
        <label class="' . ( $rate > 4 ? ( $rate == 4.5 ? 'rating-half' : 'rating-fill' ) : 'rating-empty' ) . '"></label>
    </span>';
}

/*
| -----------------------------------------------------------------------------
| Archive - Rooms Content
| -----------------------------------------------------------------------------
*/
function rooms_archive_content()
{
    set_template( TEMPLATE_PATH . '/template/archive.html', 'archive-template' );
    
    //-- Set Metadata
    set_archive_metadata( 'rooms' );

    //-- Set Archive List
    set_archive_list( 'rooms', 3 );

    add_block( 'archive-block', 'a-block', 'archive-template' );
    add_variable( 'sticky-note', sticky_notes() );
    add_variable( 'image-url', get_theme_img() );
    add_variable( 'archive-btn-text', 'Book Now' );
    add_variable( 'archive-btn-link', 'javascript:;' );
    add_variable( 'archive-btn-class', 'book-now-btn' );
    add_variable( 'archive-anchor-text', 'Discover More' );
    add_variable( 'archive-hero-img', get_archive_hero_image( 'rooms' ) );
    add_variable( 'archive-title', get_meta_data( 'setting_title', 'rooms' ) );
    add_variable( 'archive-hero-img-thumb', get_archive_hero_image( 'rooms', true ) );
    add_variable( 'archive-description', get_meta_data( 'setting_description', 'rooms' ) );
        
    add_actions( 'body-class-actions', 'get_body_class', 'rooms' );

    parse_template( 'archive-block', 'a-block', false );

    return return_template( 'archive-template' );
}

/*
| -----------------------------------------------------------------------------
| Archive - Activities Content
| -----------------------------------------------------------------------------
*/
function activities_archive_content()
{
    set_template( TEMPLATE_PATH . '/template/archive-activities.html', 'archive-template' );

    //-- Set Metadata
    set_archive_metadata( 'activities' );

    //-- Set Archive List
    set_archive_list( 'activities', 99 );

    add_block( 'archive-block', 'a-block', 'archive-template' );
    add_variable( 'sticky-note', sticky_notes() );
    add_variable( 'image-url', get_theme_img() );
    add_variable( 'archive-btn-text', 'Reserve' );
    add_variable( 'archive-btn-link', 'javascript:;' );
    add_variable( 'archive-btn-class', 'reserve-btn' );
    add_variable( 'archive-anchor-text', 'Sell All Activities' );
    add_variable( 'archive-hero-img', get_archive_hero_image( 'activities' ) );
    add_variable( 'archive-title', get_meta_data( 'setting_title', 'activities' ) );
    add_variable( 'archive-hero-img-thumb', get_archive_hero_image( 'activities', true ) );
    add_variable( 'archive-description', get_meta_data( 'setting_description', 'activities' ) );

    add_variable( 'page-recaptcha-public-key', get_meta_data( 'r_public_key' ) );
    add_actions( 'footer-actions', 'get_custom_javascript', '//www.google.com/recaptcha/api.js' );


    add_variable( 'site-url', site_url() );

    add_actions( 'header-actions', 'get_custom_css', '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css' );
    add_actions( 'footer-actions', 'get_custom_javascript', '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js' );
    add_actions( 'body-class-actions', 'get_body_class', 'activities' );

    parse_template( 'archive-block', 'a-block', false );

    return return_template( 'archive-template' );
}

/*
| -----------------------------------------------------------------------------
| Archive - Special Offers Content
| -----------------------------------------------------------------------------
*/
function offers_archive_content()
{
    set_template( TEMPLATE_PATH . '/template/archive.html', 'archive-template' );

    //-- Set Metadata
    set_archive_metadata( 'offers' );

    //-- Set Archive List
    set_archive_list( 'offers', 0 );

    add_block( 'archive-block', 'a-block', 'archive-template' );
    add_variable( 'sticky-note', sticky_notes() );
    add_variable( 'image-url', get_theme_img() );
    add_variable( 'archive-btn-text', 'Book Now' );
    add_variable( 'archive-btn-link', 'javascript:;' );
    add_variable( 'archive-btn-class', 'book-now-btn' );
    add_variable( 'archive-anchor-text', 'See All Promotions' );
    add_variable( 'archive-hero-img', get_archive_hero_image( 'offers' ) );
    add_variable( 'archive-title', get_meta_data( 'setting_title', 'offers' ) );
    add_variable( 'archive-hero-img-thumb', get_archive_hero_image( 'offers', true ) );
    add_variable( 'archive-description', get_meta_data( 'setting_description', 'offers' ) );
        
    add_actions( 'body-class-actions', 'get_body_class', 'offers' );

    parse_template( 'archive-block', 'a-block', false );

    return return_template( 'archive-template' );
}

/*
| -----------------------------------------------------------------------------
| Archive - Galleries Content
| -----------------------------------------------------------------------------
*/
function galleries_archive_content()
{
    set_template( TEMPLATE_PATH . '/template/archive-gallery.html', 'archive-template' );

    //-- Set Metadata
    set_archive_metadata( 'gallery' );

    //-- Set Archive List
    set_archive_gallery_list( 4 );

    add_block( 'archive-block', 'a-block', 'archive-template' );
    add_variable( 'sticky-note', sticky_notes() );
    add_variable( 'image-url', get_theme_img() );
    add_variable( 'archive-anchor-text', 'Discover Gallery' );
    add_variable( 'archive-hero-img', get_archive_hero_image( 'gallery' ) );
    add_variable( 'archive-title', get_meta_data( 'setting_title', 'gallery' ) );
    add_variable( 'archive-hero-img-thumb', get_archive_hero_image( 'gallery', true ) );
    add_variable( 'archive-description', get_meta_data( 'setting_description', 'gallery' ) );
        
    add_actions( 'body-class-actions', 'get_body_class', 'gallery' );

    add_actions( 'footer-actions', 'get_custom_javascript', '//cdnjs.cloudflare.com/ajax/libs/jquery.isotope/3.0.6/isotope.pkgd.min.js' );
    add_actions( 'footer-actions', 'get_custom_javascript', '//cdnjs.cloudflare.com/ajax/libs/jquery-infinitescroll/3.0.5/infinite-scroll.pkgd.min.js' );

    parse_template( 'archive-block', 'a-block', false );

    return return_template( 'archive-template' );
}

/*
| -----------------------------------------------------------------------------
| Archive - Galleries Content
| -----------------------------------------------------------------------------
*/
function locations_archive_content()
{    
    set_template( TEMPLATE_PATH . '/template/archive-locations.html', 'archive-template' );

    //-- Set Metadata
    set_archive_metadata( 'locations' );

    //-- Set Archive List
    set_archive_locations_list();
    add_variable( 'sticky-note', sticky_notes() );
    add_block( 'archive-block', 'a-block', 'archive-template' );

    add_variable( 'image-url', get_theme_img() );

    add_actions( 'body-class-actions', 'get_body_class', 'locations' );

    add_actions( 'footer-actions', 'get_custom_javascript', '//cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js' );
    add_actions( 'footer-actions', 'get_custom_javascript', '//maps.googleapis.com/maps/api/js?key=AIzaSyC9mKKb71I104S_XsT8jhw1DQ2FGqtLkds&callback=init_map_location', true, true );

    parse_template( 'archive-block', 'a-block', false );

    return return_template( 'archive-template' );
}

/*
| -----------------------------------------------------------------------------
| Archive - Galleries Content
| -----------------------------------------------------------------------------
*/
function services_archive_content()
{    
    set_template( TEMPLATE_PATH . '/template/archive-services.html', 'archive-template' );

    //-- Set Metadata
    set_archive_metadata( 'services' );

    //-- Set Archive List
    set_archive_services_list();

    add_block( 'archive-block', 'a-block', 'archive-template' );
    add_variable( 'sticky-note', sticky_notes() );
    add_variable( 'image-url', get_theme_img() );
    add_variable( 'archive-anchor-text', 'See All Services' );
    add_variable( 'archive-hero-img', get_archive_hero_image( 'services' ) );
    add_variable( 'archive-title', get_meta_data( 'setting_title', 'services' ) );
    add_variable( 'archive-hero-img-thumb', get_archive_hero_image( 'services', true ) );
    add_variable( 'archive-description', get_meta_data( 'setting_description', 'services' ) );

    add_actions( 'body-class-actions', 'get_body_class', 'services' );

    parse_template( 'archive-block', 'a-block', false );

    return return_template( 'archive-template' );
}

/*
| -----------------------------------------------------------------------------
| Archive - Item List
| -----------------------------------------------------------------------------
*/
function set_archive_list( $app_name = 'blogs', $limit = 0 )
{    
    add_block( 'archive-loop-block', 'al-block', 'archive-template' );

    $data = get_archive_list( $app_name, $limit );

    if( empty( $data['items'] ) === false )
    {
        foreach( $data['items'] as $idx => $d )
        {
            add_variable( 'post-title', $d['post_title'] );
            add_variable( 'post-brief', $d['post_brief'] );
            add_variable( 'post-thumb-img', optimized_images( HTSERVER . $d['post_featured_img_medium'], 15 ) );
            add_variable( 'post-medium-img', optimized_images( HTSERVER . $d['post_featured_img_medium'], null, 600 ) );

            if( $app_name == 'rooms' )
            {
                add_variable( 'post-subtitle', 'Room Type' );
                add_variable( 'post-link-text', 'View Room' );
                add_variable( 'post-permalink', $d['post_permalink'] );
            }
            elseif( $app_name == 'offers' )
            {
                add_variable( 'post-subtitle', 'Special Promotion' );
                add_variable( 'post-link-text', 'See More' );
                
                // $direct_link = get_additional_field( $d['post_id'], 'booking_link_add', 'offers' );
                // if (empty($direct_link)) {
                  
                // }

                $direct_link = $d['post_permalink'];
                add_variable( 'post-permalink', $direct_link );
            }
            elseif( $app_name == 'activities' )
            {
                add_variable( 'post-subtitle', 'Activity' );
                add_variable( 'post-link-text', 'Learn More' );
                add_variable( 'post-permalink', $d['post_permalink'] );
            }

            parse_template( 'archive-loop-block', 'al-block', true );
        }

        add_variable( 'archive-paging', get_pagination( $data['page'], $data['rows'], $limit, 1, '//' . site_url() . '/' . $app_name, '/' ) );
    }
}

/*
| -----------------------------------------------------------------------------
| Archive - Gallery Item List
| -----------------------------------------------------------------------------
*/
function set_archive_gallery_list( $limit = 0 )
{    
    add_block( 'archive-filter-loop-block', 'afl-block', 'archive-template' );
    add_block( 'archive-loop-block', 'al-block', 'archive-template' );

    $data = get_archive_list( 'gallery' );

    if( empty( $data['items'] ) === false )
    {
        $gallery = array();

        foreach( $data['items'] as $idx => $d )
        {
            if( empty( $d['post_gallery'] ) === false )
            {
                foreach( $d['post_gallery'] as $key => $post_gallery )
                {
                    $d['post_gallery'][ $key ]['img_group'] = $d['post_sef'];

                    $gallery[] = $d['post_gallery'][ $key ];
                }
            }

            add_variable( 'filter-sef', $d['post_sef'] );
            add_variable( 'filter-name', $d['post_title'] );

            parse_template( 'archive-filter-loop-block', 'afl-block', true );
        }

        if( empty( $gallery ) === false )
        {
            $gallery_page   = isset( $_GET[ 'page' ] ) ? $_GET[ 'page' ] : 1;
            $gallery_paging = get_archive_paging_array( $gallery, $gallery_page, $limit );
            $gallery_number = count( $gallery );

            foreach( $gallery_paging as $d )
            {
                add_variable( 'gallery-class', $d['img_group'] );
                add_variable( 'gallery-img-alt', $d['img_alt'] );
                add_variable( 'gallery-img-thumb', $d['img_medium'] );
                add_variable( 'gallery-img-caption', $d['img_caption'] );
                add_variable( 'gallery-img-fullscreen', $d['img_large'] );

                parse_template( 'archive-loop-block', 'al-block', true );
            }

            add_variable( 'archive-paging', get_pagination( $gallery_page, $gallery_number, $limit, 1, '//' . SITE_URL . '/gallery/' ) );
        }
    }
}

/*
| -----------------------------------------------------------------------------
| Archive - Locations Item List
| -----------------------------------------------------------------------------
*/
function set_archive_locations_list()
{
    add_block( 'archive-category-loop-block', 'acl-block', 'archive-template' );
    add_block( 'archive-loop-block', 'al-block', 'archive-template' );

    $data = get_archive_list( 'locations' );
    $rule = array();

    if( empty( $data['items'] ) === false )
    {
        foreach( $data['items'] as $idx => $d )
        {
            if( empty( $d['post_category'] ) === false )
            {
                $rule[ $d['post_category']['categories'][0]['order'] ] = $d['post_category']['categories'][0];

                add_variable( 'loc-name', $d['post_title'] );
                add_variable( 'loc-type', $d['post_category']['categories'][0]['sef'] );
                add_variable( 'loc-cordinate', $d['post_additional_field']['loc_cordinate'] );

                parse_template( 'archive-loop-block', 'al-block', true );
            }
        }
    }

    if( empty( $rule ) === false )
    {
        ksort( $rule );

        foreach( $rule as $d )
        {
            add_variable( 'type', $d['sef'] );
            add_variable( 'name', $d['name'] );

            parse_template( 'archive-category-loop-block', 'acl-block', true );
        }
    }
}

function set_archive_services_list()
{
    add_block( 'archive-loop-block', 'al-block', 'archive-template' );

    $data = get_archive_list( 'services' );
    $surl = SITE_URL;
    $rule = array();

    if( empty( $data['items'] ) === false )
    {
        foreach( $data['items'] as $idx => $d )
        {
            $iconpicker = isset( $d['post_additional_field']['iconpicker'] ) ? $d['post_additional_field']['iconpicker'] : '';

            if( empty( $iconpicker ) === false )
            {
                if( strpos( $iconpicker, 'ico-' ) === false )
                {
                    $iconpicker = '<i class="' . $iconpicker . '"></i>';
                }
                else
                {
                    $iconpicker = '<img src="//' . $surl . '/lumonata-plugins/additional/icons/' . str_replace( 'ico-', '', $iconpicker ) . '.svg" alt="" />';
                }
            }
            
            add_variable( 'services-icon', $iconpicker );
            add_variable( 'services-name', $d['post_title'] );

            parse_template( 'archive-loop-block', 'al-block', true );
        }

        add_actions( 'header-actions', 'get_custom_css', '//use.fontawesome.com/releases/v5.5.0/css/all.css' );
    }
}

/*
| -----------------------------------------------------------------------------
| Archive - Set Metadata
| -----------------------------------------------------------------------------
*/
function set_archive_metadata( $app_name = 'blogs' )
{
    $title  = get_meta_data( 'setting_title', $app_name );
    $desc   = get_meta_data( 'setting_description', $app_name );

    $mtitle = get_meta_data( 'setting_meta_title', $app_name );
    $mkey   = get_meta_data( 'setting_meta_keywords', $app_name );
    $mdesc  = get_meta_data( 'setting_meta_description', $app_name) ;

    if( empty( $mtitle ) )
    {
        if( empty( $title ) )
        {
            $mtitle = ucfirst( $app_name ) . ' - ' . trim( web_title() );
        }
        else
        {
            $mtitle = $title;
        }
    }

    add_meta_data_value( $mtitle, $mkey, $mdesc );
}

/*
| -----------------------------------------------------------------------------
| Archive - Get Hero Image
| -----------------------------------------------------------------------------
*/
function get_archive_hero_image( $app_name = 'blogs', $use_thumb = false )
{
    $data = get_additional_setting_hero_image( $app_name );

    if( empty( $data ) )
    {
        return 'https://dummyimage.com/900x900/F0F0F0/F0F0F0&text=No+Image';
    }
    else
    {
        if( $use_thumb )
        {
            return optimized_images( HTSERVER . $data['img_url'], 15 );
        }
        else
        {
            return optimized_images( HTSERVER . $data['img_url'], null, 900 );
        }
    }
}

/*
| -----------------------------------------------------------------------------
| Archive - Get Paging Array
| -----------------------------------------------------------------------------
*/
function get_archive_paging_array( array $data, $page, $limit )
{
    $start = $limit * ( $page - 1 );
    $end   = $start + $limit;
    $count = count( $data );

    //-- Conditionally return results
    if( $start < 0 || $count <= $start )
    {
        //-- Page is out of range
        return array();
    }
    elseif( $count <= $end )
    {
        //-- Partially-filled page
        return array_slice( $data, $start );
    }
    else
    {
        //-- Full page 
        return array_slice( $data, $start, $end - $start );
    }
}

/*
| -----------------------------------------------------------------------------
| Details - Restaurant
| -----------------------------------------------------------------------------
*/
function restaurant_detail_content()
{
    $sef  = get_uri_sef();
    $data = get_detail_post( $sef, 'restaurant' );

    if( empty( $data ) )
    {
        return page_404_content();
    }
    else
    {
        extract( $data );

        set_template( TEMPLATE_PATH . '/template/detail-restaurant.html', 'detail-template' );

        //-- Set Metadata
        set_detail_metadata( $data );

        //-- Set Hero Image
        set_detail_hero_image( $data );

        //-- Set Masking Image
        set_detail_masking_image( $data );

        //-- Set Food & Drink Menu
        set_detail_food_and_drink_menu( $data );

        add_block( 'detail-block', 'd-block', 'detail-template' );
        add_variable( 'sticky-note', sticky_notes() );
        add_variable( 'post-sef', $post_sef );
        add_variable( 'post-type', $post_type );
        add_variable( 'post-date', $post_date );
        add_variable( 'post-title', $post_title );
        add_variable( 'post-subtitle', 'Our Restaurant' );
        add_variable( 'post-content', do_shortcode( $post_content ) );
        add_variable( 'post-date-format', date( 'd F Y', strtotime( $post_date ) ) );
        add_variable( 'page-recaptcha-public-key', get_meta_data( 'r_public_key' ) );
        add_actions( 'footer-actions', 'get_custom_javascript', '//www.google.com/recaptcha/api.js' );

        $menu_pdf           = get_additional_field( $post_id, 'menu_pdf', 'restaurant' );
        $menu_pdf_text_link = get_additional_field( $post_id, 'menu_pdf_text_link', 'restaurant' );

        add_variable( 'menu_pdf', empty( $menu_pdf ) ? '' : '<a href="//'.SITE_URL . '/lumonata-plugins/additional/pdf/' . $menu_pdf.'" target="_blank" class="resto-pdf-btn">DOWNLOAD OUR MENU</a>' );
        add_variable( 'text_pdf', $menu_pdf_text_link );


        add_variable( 'site-url', site_url() );

        add_actions( 'header-actions', 'get_custom_css', '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css' );
        add_actions( 'footer-actions', 'get_custom_javascript', '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js' );

        add_variable( 'image-url', get_theme_img() );

        add_actions( 'body-class-actions', 'get_body_class', 'restaurant' );

        parse_template( 'detail-block', 'd-block', false );

        return return_template( 'detail-template' );
    }
}

/*
| -----------------------------------------------------------------------------
| Details - Spa
| -----------------------------------------------------------------------------
*/
function spa_detail_content()
{
    $sef  = get_uri_sef();
    $data = get_detail_post( $sef, 'spa' );

    if( empty( $data ) )
    {
        return page_404_content();
    }
    else
    {
        extract( $data );

        set_template( TEMPLATE_PATH . '/template/detail-spa.html', 'detail-template' );

        //-- Set Metadata
        set_detail_metadata( $data );

        //-- Set Hero Image
        set_detail_hero_image( $data );

        //-- Set Masking Image
        set_detail_masking_image( $data );

        //-- Set Detail Packages List
        set_detail_packages_list( $data );

        add_block( 'detail-block', 'd-block', 'detail-template' );

        add_variable( 'post-sef', $post_sef );
        add_variable( 'post-type', $post_type );
        add_variable( 'post-date', $post_date );
        add_variable( 'site-url', site_url() );
        add_variable( 'post-title', $post_title );
        add_variable( 'post-subtitle', 'Relax &amp; Enjoy' );
        add_variable( 'post-content', do_shortcode( $post_content ) );
        add_variable( 'post-date-format', date( 'd F Y', strtotime( $post_date ) ) );

        $menu_pdf           = get_additional_field( $post_id, 'menu_pdf', 'spa' );
        $menu_pdf_text_link = get_additional_field( $post_id, 'menu_pdf_text_link', 'spa' );

        add_variable( 'menu_pdf', empty( $menu_pdf ) ? '' : '<a href="//'.SITE_URL . '/lumonata-plugins/additional/pdf/' . $menu_pdf.'" target="_blank" class="resto-pdf-btn">DOWNLOAD OUR MENU</a>' );
        add_variable( 'text_pdf', $menu_pdf_text_link );
        
        add_variable( 'page-recaptcha-public-key', get_meta_data( 'r_public_key' ) );
        add_actions( 'footer-actions', 'get_custom_javascript', '//www.google.com/recaptcha/api.js' );

        add_actions( 'header-actions', 'get_custom_css', '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css' );
        add_actions( 'footer-actions', 'get_custom_javascript', '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js' );

        add_variable( 'image-url', get_theme_img() );
        // add_actions( 'header-actions', 'get_custom_css', '//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css' );
        // add_actions( 'footer-actions', 'get_custom_javascript', '//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js' );
        add_actions( 'body-class-actions', 'get_body_class', 'spa' );

        parse_template( 'detail-block', 'd-block', false );

        return return_template( 'detail-template' );
    }
}

/*
| -----------------------------------------------------------------------------
| Details - Rooms
| -----------------------------------------------------------------------------
*/
function rooms_detail_content()
{
    $sef  = get_uri_sef();
    $data = get_detail_post( $sef, 'rooms' );

    if( empty( $data ) )
    {
        return page_404_content();
    }
    else
    {
        extract( $data );

        set_template( TEMPLATE_PATH . '/template/detail-rooms.html', 'detail-template' );

        //-- Set Metadata
        set_detail_metadata( $data );

        //-- Set Detail Amenities
        set_detail_amenities( $data );

        //-- Set Detail Amenities
        set_detail_galleries( $data );

        //-- Set Other Rooms
        set_detail_others( $data );

        add_block( 'detail-block', 'd-block', 'detail-template' );           
        add_variable( 'sticky-note', sticky_notes() );
        add_variable( 'post-sef', $post_sef );
        add_variable( 'post-type', $post_type );
        add_variable( 'post-date', $post_date );
        add_variable( 'post-title', $post_title );
        add_variable( 'post-subtitle', $post_subtitle );
        add_variable( 'post-anchor-text', 'Discover More' );
        add_variable( 'post-content', do_shortcode( $post_content ) );
        add_variable( 'post-rooms-image', post_rooms_img( $post_id ) );
        add_variable( 'post-rooms-thumb-image', post_rooms_img( $post_id, true ) );
        add_variable( 'post-date-format', date( 'd F Y', strtotime( $post_date ) ) );
        add_variable( 'post-image', optimized_images( HTSERVER . $post_featured_img, null, 900 ) );
        add_variable( 'post-thumb-image', optimized_images( HTSERVER . $post_featured_img_thumb, 15 ) );

        add_variable( 'image-url', get_theme_img() );

        add_actions( 'body-class-actions', 'get_body_class', 'rooms rooms-detail' );

        parse_template( 'detail-block', 'd-block', false );

        return return_template( 'detail-template' );
    }
}

/*
| -----------------------------------------------------------------------------
| Details - Activities
| -----------------------------------------------------------------------------
*/
function activities_detail_content()
{
    $sef  = get_uri_sef();
    $data = get_detail_post( $sef, 'activities' );

    if( empty( $data ) )
    {
        return page_404_content();
    }
    else
    {
        extract( $data );

        set_template( TEMPLATE_PATH . '/template/detail-activities.html', 'detail-template' );

        //-- Set Metadata
        set_detail_metadata( $data );

        //-- Set Detail Amenities
        set_detail_galleries( $data );

        //-- Set Other Rooms
        set_detail_others( $data );

        add_block( 'detail-block', 'd-block', 'detail-template' );           
        add_variable( 'sticky-note', sticky_notes() );
        add_variable( 'post-sef', $post_sef );
        add_variable( 'post-type', $post_type );
        add_variable( 'post-date', $post_date );
        add_variable( 'post-title', $post_title );
        add_variable( 'post-subtitle', $post_subtitle );
        add_variable( 'post-anchor-text', 'Discover More' );
        add_variable( 'post-content', do_shortcode( $post_content ) );
        add_variable( 'post-date-format', date( 'd F Y', strtotime( $post_date ) ) );
        add_variable( 'post-image', optimized_images( HTSERVER . $post_featured_img, null, 900 ) );
        add_variable( 'post-thumb-image', optimized_images( HTSERVER . $post_featured_img_thumb, 15 ) );

        add_variable( 'image-url', get_theme_img() );

        add_actions( 'body-class-actions', 'get_body_class', 'rooms rooms-detail' );

        parse_template( 'detail-block', 'd-block', false );

        return return_template( 'detail-template' );
    }
}

/*
| -----------------------------------------------------------------------------
| Details - Offers (if link empty)
| -----------------------------------------------------------------------------
*/
function offers_detail_content()
{

    $sef  = get_uri_sef();
    $data = get_detail_post( $sef, 'offers' );

    if( empty( $data ) )
    {
        return page_404_content();
    }
    else
    {
        extract( $data );

        set_template( TEMPLATE_PATH . '/template/offers-detail.html', 'detail-template' );

        //-- Set Metadata
        set_detail_metadata( $data );

        add_block( 'detail-block', 'd-block', 'detail-template' );

        $supercontent = do_shortcode($post_content);
        $supercontent = str_replace('src="../', 'src="'.HTSERVER.'//'.site_url().'/', $supercontent);
        add_variable( 'page-content', $supercontent );

        add_variable( 'post-sef', $post_sef );
        add_variable( 'post-type', $post_type );
        add_variable( 'post-date', $post_date );
        add_variable( 'post-title', $post_title );
        add_variable( 'post-subtitle', $post_subtitle );
        add_variable( 'post-anchor-text', 'Discover More' );
        add_variable( 'post-content', do_shortcode( $post_content ) );
        add_variable( 'post-date-format', date( 'd F Y', strtotime( $post_date ) ) );
        add_variable( 'post-image', optimized_images( HTSERVER . $post_featured_img, null, 900 ) );
        add_variable( 'post-thumb-image', optimized_images( HTSERVER . $post_featured_img_thumb, 15 ) );

        add_variable( 'image-url', get_theme_img() );

        $imjnURL = HTSERVER.'//'.site_url().'/'.$sef.'/';
        $imjnTitle = $post_title;
        // Konstruksi URL untuk sharing
        $facebookURL  = 'https://www.facebook.com/sharer/sharer.php?u='.$imjnURL;
        $twitterURL   = 'https://twitter.com/intent/tweet?text='.$imjnTitle.'&amp;url='.$imjnURL.'&amp;';
        $whatsappURL  = 'https://wa.me/?text='.$imjnTitle . ' ' . $imjnURL;
        $pinterestURL = 'https://pinterest.com/pin/create/button/?url='.$imjnURL.'&amp;media='.$imjnURL.'&amp;description='.$imjnTitle;

        add_variable( 'facebookURL', $facebookURL );
        add_variable( 'twitterURL', $twitterURL );
        add_variable( 'whatsappURL', $whatsappURL );
        add_variable( 'pinterestURL', $pinterestURL );

        $direct_offers = 'https://secure.staah.com/common-cgi/package/packagebooking.pl?propertyId=12695';
        $the_link_o = get_additional_field( $post_id, 'booking_link_add', 'offers' );
        if (!empty($the_link_o)) {
          $direct_offers = $the_link_o;
        }
        add_variable( 'direct_offers_link', $direct_offers );
        
        parse_template( 'detail-block', 'd-block', false );

        return return_template( 'detail-template' );
    }
}


/*
| -----------------------------------------------------------------------------
| Details - Hero Image
| -----------------------------------------------------------------------------
*/
function set_detail_hero_image( $data )
{
    extract( $data );

    add_block( 'hero-slide-loop-block', 'hsl-block', 'detail-template' );
    add_block( 'hero-slide-block', 'hs-block', 'detail-template' );

    $nimg  = 'https://dummyimage.com/1920x1080/F0F0F0/F0F0F0&text=No+Image';
    $slide = post_gallery( $post_id, $post_type );

    if( empty( $slide ) )
    {
        if( empty( $post_featured_img_large ) )
        {
            add_variable( 'post-hero-img', $nimg );
            add_variable( 'post-hero-img-thumb', $nimg );
        }
        else
        {
            add_variable( 'post-hero-img', optimized_images( HTSERVER . $post_featured_img_large, null, 900 ) );
            add_variable( 'post-hero-img-thumb', optimized_images( HTSERVER . $post_featured_img_thumb, 15 ) );
        }

        parse_template( 'hero-slide-loop-block', 'hsl-block', false );
    }
    else
    {
        foreach( $slide as $dt )
        {
            add_variable( 'post-hero-img', optimized_images( HTSERVER . $dt['img_large'], null, 900 ) );
            add_variable( 'post-hero-img-thumb', optimized_images( HTSERVER . $dt['img_thumb'], 15 ) );

            parse_template( 'hero-slide-loop-block', 'hsl-block', true );
        }
    }

    parse_template( 'hero-slide-block', 'hs-block', false );
}

/*
| -----------------------------------------------------------------------------
| Details - Masking Image
| -----------------------------------------------------------------------------
*/
function set_detail_masking_image( $data )
{
    extract( $data );

    $masking_img_1 = get_masking_image( $post_id, $post_type . '-masking-img-1' );
    $masking_img_2 = get_masking_image( $post_id, $post_type . '-masking-img-2' );
    $masking_img_3 = get_masking_image( $post_id, $post_type . '-masking-img-3' );
    $masking_img_4 = get_masking_image( $post_id, $post_type . '-masking-img-4' );

    if( empty( $masking_img_1 ) )
    {
        add_variable( 'mask-1-img', '' );
        add_variable( 'mask-1-css', 'sr-only' );
    }
    else
    {
        add_variable( 'mask-1-img-thumb', optimized_images( HTSERVER . $masking_img_1['img_url'], 15 ) );
        add_variable( 'mask-1-img', $masking_img_1['img_url'] );
        add_variable( 'mask-1-css', '' );
    }

    if( empty( $masking_img_2 ) )
    {
        add_variable( 'mask-2-img-thumb', '' );
        add_variable( 'mask-2-img', '' );
        add_variable( 'mask-2-css', 'sr-only' );
    }
    else
    {
        add_variable( 'mask-2-img-thumb', optimized_images( HTSERVER . $masking_img_2['img_url'], 15 ) );
        add_variable( 'mask-2-img', $masking_img_2['img_url'] );
        add_variable( 'mask-2-css', '' );
    }

    if( empty( $masking_img_3 ) )
    {
        add_variable( 'mask-3-img-thumb', '' );
        add_variable( 'mask-3-img', '' );
        add_variable( 'mask-3-css', 'sr-only' );
    }
    else
    {
        add_variable( 'mask-3-img-thumb', optimized_images( HTSERVER . $masking_img_3['img_url'], 15 ) );
        add_variable( 'mask-3-img', $masking_img_3['img_url'] );
        add_variable( 'mask-3-css', '' );
    }

    if( empty( $masking_img_4 ) )
    {
        add_variable( 'mask-4-img-thumb', '' );
        add_variable( 'mask-4-img', '' );
        add_variable( 'mask-4-css', 'sr-only' );
    }
    else
    {
        add_variable( 'mask-4-img-thumb', optimized_images( HTSERVER . $masking_img_4['img_url'], 15 ) );
        add_variable( 'mask-4-img', $masking_img_4['img_url'] );
        add_variable( 'mask-4-css', '' );
    }
}

/*
| -----------------------------------------------------------------------------
| Details - Packages List
| -----------------------------------------------------------------------------
*/
function set_detail_packages_list( $data )
{
    extract( $data );

    add_block( 'packages-loop-block', 'pl-block', 'detail-template' );
    add_block( 'packages-block', 'p-block', 'detail-template' );

    $packages = get_additional_field( $post_id, 'packages_list', $post_type );
    $packages = json_decode( $packages, true );
    $site_url = site_url();

    if( empty( $packages ) === false )
    {
        foreach( $packages as $rule_id )
        {
            $d = fetch_rule( 'rule_id=' . $rule_id );
            $f = get_rule_featured_img( $rule_id, $post_type, false );

            if( empty( $f ) )
            {
                add_variable( 'package-featured-img', 'https://dummyimage.com/851x500/f0f0f0/f0f0f0&text=No+Image' );
            }
            else
            {
                add_variable( 'package-featured-img', optimized_images( HTSERVER . '//' . $site_url . $f['large'], null, 500 ) );
                add_variable( 'package-featured-img-thumb', optimized_images( HTSERVER . '//' . $site_url . $f['thumb'], 15 ) );
            }

            add_variable( 'package-title', $d['lname'] );
            add_variable( 'package-content', $d['ldescription'] );

            parse_template( 'packages-loop-block', 'pl-block', true );
        }

        parse_template( 'packages-block', 'p-block', true );
    }
}

/*
| -----------------------------------------------------------------------------
| Details - Amenities List
| -----------------------------------------------------------------------------
*/
function set_detail_amenities( $data )
{
    extract( $data );

    add_block( 'amenities-loop-block', 'pl-block', 'detail-template' );
    add_block( 'amenities-block', 'p-block', 'detail-template' );

    $amenities = get_additional_field( $post_id, 'amenities_list', $post_type );
    $amenities = json_decode( $amenities, true );

    if( empty( $amenities ) === false )
    {
        foreach( $amenities as $rule_id )
        {
            $d = fetch_rule( 'rule_id=' . $rule_id );

            add_variable( 'amenities-title', $d['lname'] );

            parse_template( 'amenities-loop-block', 'pl-block', true );
        }

        parse_template( 'amenities-block', 'p-block', true );
    }
}

/*
| -----------------------------------------------------------------------------
| Details - Other List
| -----------------------------------------------------------------------------
*/
function set_detail_others( $data )
{
    extract( $data );

    add_block( 'other-loop-block', 'ol-block', 'detail-template' );
    add_block( 'other-block', 'o-block', 'detail-template' );

    if( empty( $post_related ) === false )
    {
        foreach( $post_related as $d )
        {
            add_variable( 'other-post-title', $d['post_title'] );
            add_variable( 'other-permalink', $d['post_permalink'] );
            add_variable( 'other-featured-thumb-image', optimized_images( HTSERVER . $d['post_featured_img_medium'], 15 ) );
            add_variable( 'other-featured-image', optimized_images( HTSERVER . $d['post_featured_img_medium'], null, 530 ) );

            parse_template( 'other-loop-block', 'ol-block', true );
        }

        parse_template( 'other-block', 'o-block', true );
    }
}

/*
| -----------------------------------------------------------------------------
| Details - Gallery List
| -----------------------------------------------------------------------------
*/
function set_detail_galleries( $data )
{
    extract( $data );

    add_block( 'gallery-loop-block', 'gl-block', 'detail-template' );
    add_block( 'gallery-block', 'g-block', 'detail-template' );

    if( empty( $post_gallery ) === false )
    {
        foreach( $post_gallery as $d )
        {
            add_variable( 'gallery-img-alt', $d['img_alt'] );
            add_variable( 'gallery-img-caption', $d['img_caption'] );
            add_variable( 'gallery-img-fullscreen', $d['img_large'] );
            add_variable( 'gallery-img-thumb', optimized_images( HTSERVER . $d['img_medium'], 15 ) );
            add_variable( 'gallery-img', optimized_images( HTSERVER . $d['img_medium'], null, 700 ) );

            parse_template( 'gallery-loop-block', 'gl-block', true );
        }

        parse_template( 'gallery-block', 'g-block', true );
    }
}

/*
| -----------------------------------------------------------------------------
| Details - Food & Drink Menu
| -----------------------------------------------------------------------------
*/
function set_detail_food_and_drink_menu( $data )
{
    extract( $data );

    add_block( 'menus-loop-block', 'ml-block', 'detail-template' );
    add_block( 'menus-block', 'm-block', 'detail-template' );

    $menus = get_category_menu( $post_id );
    $list  = get_main_menu( $post_id );

    if( empty( $menus ) === false )
    {
        foreach( $menus as $obj )
        {
            $menu_list = '';

            if( isset( $obj['child'] ) )
            {
                foreach( $obj['child'] as $d )
                {
                    $menu_list .= '
                    <div class="col-md-12">';

                        $mlist = set_menu_data( $d['name'], $list );
    
                        if( isset( $mlist[ $d['name'] ] ) )
                        {        
                            if( empty( $mlist[ $d['name'] ] ) === false && is_array( $mlist[ $d['name'] ] ) )
                            {
                                $menu_list .= '
                                <small>' . $d['name'] . '</small>
                                <div class="row row-menu">';

                                    foreach( $mlist[ $d['name'] ] as $i => $val )
                                    {
                                        foreach( $val as $v )
                                        {
                                            $menu_list .= '
                                            <div class="col-item col-md-6">
                                                <h3>' . $v[ 'name' ] . '</h3>
                                                <p>' . ( $v['desc'] == '' ? '' : nl2br( $v['desc'] ) ) . '</p>
                                                <span>' . ( $v['price'] == 0 ? '' : $v[ 'price' ] ) . 'K</span>
                                            </div>';
                                        }
                                    }
                                    
                                    $menu_list .= '
                                </div>';
                            }
                        }

                        $menu_list .= '
                    </div>';
                }
            }
            else
            {
                $menu_list .= '
                <div class="col-md-12">';

                    $mlist = set_menu_data( $obj['name'], $list );

                    if( isset( $mlist[ $obj['name'] ] ) )
                    {        
                        if( empty( $mlist[ $obj['name'] ] ) === false && is_array( $mlist[ $obj['name'] ] ) )
                        {
                            $menu_list .= '
                            <div class="row row-menu">';

                                foreach( $mlist[ $obj['name'] ] as $i => $val )
                                {
                                    foreach( $val as $v )
                                    {
                                        $menu_list .= '
                                        <div class="col-item col-md-6">
                                            <h3>' . $v[ 'name' ] . '</h3>
                                            <p>' . ( $v['desc'] == '' ? '' : nl2br( $v['desc'] ) ) . '</p>
                                            <span>' . ( $v['price'] == 0 ? '' : $v[ 'price' ] ) . 'K</span>
                                        </div>';
                                    }
                                }
                                
                                $menu_list .= '
                            </div>';
                        }
                    }

                    $menu_list .= '
                </div>';
            }

            add_variable( 'menu-list', $menu_list );
            add_variable( 'menu-title', $obj['name'] );

            parse_template( 'menus-loop-block', 'ml-block', true );
        }

        parse_template( 'menus-block', 'm-block', true );
    }
}

/*
| -----------------------------------------------------------------------------
| Details - Set Metadata
| -----------------------------------------------------------------------------
*/
function set_detail_metadata( $data )
{
    extract( $data );

    $mtitle = empty( $mtitle ) ? $post_title . ' - ' . trim( web_title() ) : $mtitle;

    add_meta_data_value( $mtitle, $mkey, $mdesc );
}

/*
| -----------------------------------------------------------------------------
| Footer - Set Awards Slide Content
| -----------------------------------------------------------------------------
*/
function set_footer_awards_content()
{
    add_block( 'awards-loop-block', 'awl-block' );
    add_block( 'awards-block', 'aw-block' );

    $nimg = '//' . TEMPLATE_URL . '/images/tripadvisor.png';
    $data = get_archive_list( 'awards', 5 );

    if( $data['rows'] > 0 )
    {
        foreach( $data['items'] as $d )
        {
            add_variable( 'awards-title', $d['post_title'] );
            add_variable( 'awards-url', empty( $d['post_featured_img_opt_2'] ) ? $nimg : $d['post_featured_img_opt_2'] );

            parse_template( 'awards-loop-block', 'awl-block', true );
        }
        
        parse_template( 'awards-block', 'aw-block', false );
    }
}

/*
| -----------------------------------------------------------------------------
| Page Content
| -----------------------------------------------------------------------------
*/
function page_content()
{
    $sef  = get_uri_sef();
    $data = get_page_detail( $sef );

    if( empty( $data ) )
    {
        return page_404_content();
    }
    else
    {
        extract( $data );

        set_template( TEMPLATE_PATH . '/template/page.html', 'page-template' );

        add_block( 'page-block', 'p-block', 'page-template' );

        $supercontent = do_shortcode($content);
        $supercontent = str_replace('src="../', 'src="'.HTSERVER.'//'.site_url().'/', $supercontent);
        //-- Set Meta Data
        set_page_metadata( $id, $title );

        add_variable( 'page-sef', $sef );
        add_variable( 'page-title', $title );

        add_variable( 'page-content', $supercontent );
        add_variable( 'page-date', date( 'd F Y', strtotime( $date ) ) );

        add_variable( 'image-url', get_theme_img() );

        add_actions( 'body-class-actions', 'get_body_class', 'page ' . $sef );

        $imjnURL = HTSERVER.'//'.site_url().'/'.$sef.'/';
        $imjnTitle = $title;
        // Konstruksi URL untuk sharing
        $facebookURL  = 'https://www.facebook.com/sharer/sharer.php?u='.$imjnURL;
        $twitterURL   = 'https://twitter.com/intent/tweet?text='.$imjnTitle.'&amp;url='.$imjnURL.'&amp;';
        $whatsappURL  = 'https://wa.me/?text='.$imjnTitle . ' ' . $imjnURL;
        $pinterestURL = 'https://pinterest.com/pin/create/button/?url='.$imjnURL.'&amp;media='.$imjnURL.'&amp;description='.$imjnTitle;

        add_variable( 'facebookURL', $facebookURL );
        add_variable( 'twitterURL', $twitterURL );
        add_variable( 'whatsappURL', $whatsappURL );
        add_variable( 'pinterestURL', $pinterestURL );

        parse_template( 'page-block', 'p-block', false );

        return return_template( 'page-template' );
    }
}

/*
| -----------------------------------------------------------------------------
| Contact Content
| -----------------------------------------------------------------------------
*/
function contact_content()
{
    $sef  = get_uri_sef();
    $data = get_page_detail( $sef );

    if( empty( $data ) )
    {
        return page_404_content();
    }
    else
    {
        extract( $data );

        //-- Featured Image
        $featured     = post_featured_image( $id, 'pages', '', true );
        $featured_alt = post_featured_image_alt( $id, 'pages' );

        set_template( TEMPLATE_PATH . '/template/contact.html', 'page-template' );

        add_block( 'page-block', 'p-block', 'page-template' );

        //-- Set Meta Data
        set_page_metadata( $id, $title );
        add_variable( 'sticky-note', sticky_notes() );
        add_variable( 'page-sef', $sef );
        add_variable( 'page-title', $title );
        add_variable( 'page-brief', $brief );
        add_variable( 'page-anchor-text', 'Contact Us Now' );

        add_variable( 'page-hero-image-alt', $featured_image_alt );
        add_variable( 'page-recaptcha-public-key', get_meta_data( 'r_public_key' ) );
        add_variable( 'page-hero-image-thumb', optimized_images( HTSERVER . $featured_image, 15 ) );
        add_variable( 'page-hero-image', optimized_images( HTSERVER . $featured_image, null, 900 ) );

        add_variable( 'site-url', site_url() );
        add_variable( 'image-url', get_theme_img() );

        add_actions( 'body-class-actions', 'get_body_class', 'contact' );

        add_actions( 'footer-actions', 'get_custom_javascript', '//www.google.com/recaptcha/api.js' );
        add_actions( 'footer-actions', 'get_custom_javascript', '//maps.googleapis.com/maps/api/js?key=AIzaSyC9mKKb71I104S_XsT8jhw1DQ2FGqtLkds&callback=init_map', true, true );

        parse_template( 'page-block', 'p-block', false );

        return return_template( 'page-template' );
    }
}

/*
| -----------------------------------------------------------------------------
| Page - Set Metadata
| -----------------------------------------------------------------------------
*/
function set_page_metadata( $id, $title )
{
    $mtitle = strip_tags( get_additional_field( $id, 'meta_title', 'pages' ) );
    $mkey   = strip_tags( get_additional_field( $id, 'meta_keywords', 'pages' ) );
    $mdesc  = strip_tags( get_additional_field( $id, 'meta_description', 'pages' ) );
    $mtitle = empty( $mtitle ) ? $title . ' - ' . trim( web_title() ) : $mtitle;

    add_meta_data_value( $mtitle, $mkey, $mdesc );
}

/*
| -----------------------------------------------------------------------------
| Page Not Found Content
| -----------------------------------------------------------------------------
*/
function page_404_content()
{
    set_template( TEMPLATE_PATH . '/template/404.html', 'not-found-template' );

    add_block( 'not-found-block', 'nf-block', 'not-found-template' );

    //-- Set Meta Data
    add_meta_data_value( 'Page Not Found - ' . trim( web_title() ) );

    add_variable( 'site-url', site_url() );
    add_variable( 'image-url', get_theme_img() );
    
    add_actions( '404_page', true );

    add_actions( 'body-class-actions', 'get_body_class', 'not-found' );

    parse_template( 'not-found-block', 'nf-block', false );

    return return_template( 'not-found-block' );
}

/*
| -----------------------------------------------------------------------------
| Page Thank You Content
| -----------------------------------------------------------------------------
*/
function thanks_content()
{
    $apps = get_appname();
    
    set_template( TEMPLATE_PATH . '/template/thanks.html', 'page-template' );

    add_block( 'page-block', 'p-block', 'page-template' );

    //-- Set Meta Data
    set_thanks_metadata( $apps );

    add_variable( 'page-title', get_meta_data( 'thanks_' . $apps . '_title' ) );
    add_variable( 'page-content',  get_meta_data( 'thanks_' . $apps . '_content' ) );

    add_variable( 'site-url', site_url() );
    add_variable( 'image-url', get_theme_img() );

    add_actions( 'body-class-actions', 'get_body_class', 'thanks' );

    parse_template( 'page-block', 'p-block', false );

    return return_template( 'page-template' );
}

/*
| -----------------------------------------------------------------------------
| Page - Set Thanks Metadata
| -----------------------------------------------------------------------------
*/
function set_thanks_metadata( $apps )
{
    $title  = get_meta_data( 'thanks_' . $apps . '_title' );
    $mtitle = strip_tags( get_meta_data( 'thanks_' . $apps . '_meta_title' ) );
    $mkey   = strip_tags( get_meta_data( 'thanks_' . $apps . '_meta_keywords' ) );
    $mdesc  = strip_tags( get_meta_data( 'thanks_' . $apps . '_meta_description' ) );
    $mtitle = empty( $mtitle ) ? $title . ' - ' . trim( web_title() ) : $mtitle;

    add_meta_data_value( $mtitle, $mkey, $mdesc );
}

/*
| -----------------------------------------------------------------------------
| Get Page Detail
| -----------------------------------------------------------------------------
*/
function get_page_detail( $sef )
{
    $result = array();

    $d = fetch_articles( 'sef=' . $sef );

    if( empty( $d ) )
    {
        return $result;
    }

    
    $result = array(
        'id' => $d['larticle_id'],
        'date' => $d['lpost_date'],
        'title' => $d['larticle_title'],
        'content' => $d['larticle_content'],
        'featured_image_alt' => post_featured_image_alt( $d['larticle_id'], 'pages' ),
        'featured_image' => post_featured_image( $d['larticle_id'], 'pages', '', true ),
        'brief' => post_brief( $d[ 'larticle_id' ], 'pages', $d[ 'larticle_content' ] ),
        'lstatus' => $d['larticle_status']
    );

    return $result;
}

/*
| -----------------------------------------------------------------------------
| Get Post Archive
| -----------------------------------------------------------------------------
*/
function get_archive_list( $group = 'blogs', $limit = 0, $rule = '', $rule_sef = '' )
{
    global $db;

    $n      = 0;
    $items  = array();
    $page   = isset( $_GET[ 'page' ] ) ? $_GET[ 'page' ] : 1;
    $viewed = $limit * ( $page - 1 );
    
    if( empty( $rule_sef ) )
    {
        $cek_url = cek_url();
        
        if( $cek_url[ 0 ] == $group && isset( $cek_url[ 1 ] ) )
        {
            $rule_sef = $cek_url[ 1 ];
        }
    }

    if( empty( $rule ) )
    {
        $s = 'SELECT a.* FROM lumonata_articles a 
              WHERE a.larticle_type = %s AND a.larticle_status = %s 
              ORDER BY a.lorder';
        $q = $db->prepare_query( $s, $group, 'publish' );
        $r = $db->do_query( $q );
        $n = $db->num_rows( $r );

        if( empty( $limit ) === false )
        {
            $s = $s . ' ASC LIMIT %d, %d';
            $q = $db->prepare_query( $s, $group, 'publish', $viewed, $limit );    
            $r = $db->do_query( $q );
        }
    }
    else
    {
        if( empty( $rule_sef ) )
        {
            $s = 'SELECT c.* FROM 
                    lumonata_rules a, 
                    lumonata_rule_relationship b, 
                    lumonata_articles c 
                  WHERE a.lrule=%s AND 
                    a.lgroup=%s AND 
                    a.lrule_id=b.lrule_id AND
                    b.lapp_id=c.larticle_id AND
                    c.larticle_status=%s
                  GROUP BY c.larticle_id
                  ORDER BY c.lorder';
            $q = $db->prepare_query( $s, $rule, $group, 'publish' );
            $r = $db->do_query( $q );
            $n = $db->num_rows( $r );

            if( empty( $limit ) === false )
            {
                $s = $s . ' ASC LIMIT %d, %d';
                $q = $db->prepare_query( $s, $rule, $group, 'publish', $viewed, $limit );    
                $r = $db->do_query( $q );
            }
        }
        else
        {
            $s = 'SELECT c.* FROM 
                    lumonata_rules a, 
                    lumonata_rule_relationship b, 
                    lumonata_articles c 
                  WHERE a.lrule=%s AND 
                    a.lgroup=%s AND 
                    a.lrule_id=b.lrule_id AND
                    b.lapp_id=c.larticle_id AND
                    a.lsef=%s AND 
                    c.larticle_status=%s
                  GROUP BY c.larticle_id
                  ORDER BY c.lorder';
            $q = $db->prepare_query( $s, $rule, $group, $rule_sef, 'publish' );
            $r = $db->do_query( $q );
            $n = $db->num_rows( $r );

            if( empty( $limit ) === false )
            {
                $s = $s . ' ASC LIMIT %d, %d';
                $q = $db->prepare_query( $s,  $rule, $group, $rule_sef, 'publish', $viewed, $limit );    
                $r = $db->do_query( $q );
            }
        }
    }
    
    if( $db->num_rows( $r ) > 0 )
    {
        $i = 0;
        
        while( $d = $db->fetch_array( $r ) )
        {
            $items[] = array(
                'post_sef'                 => $d[ 'lsef' ],
                'post_date'                => $d[ 'lpost_date' ],
                'post_id'                  => $d[ 'larticle_id' ],
                'post_title'               => $d[ 'larticle_title' ],
                'post_content'             => $d[ 'larticle_content' ],
                // 'post_permalink'           => permalink( $d[ 'larticle_id' ] ),
                'post_permalink'           => HTSERVER.'//'.SITE_URL.'/'.$d['larticle_type'].'/'.$d['lsef'].'.html',
                'post_category'            => post_category( $d[ 'larticle_id' ] ),
                'post_gallery'             => post_gallery( $d[ 'larticle_id' ], $group ),
                'post_featured_img'        => post_featured_image( $d[ 'larticle_id' ], $group ),
                'post_additional_field'    => post_additional_field( $d[ 'larticle_id' ], $group ),
                'post_featured_img_alt'    => post_featured_image_alt( $d[ 'larticle_id' ], $group ),
                'post_featured_img_title'  => post_featured_image_title( $d[ 'larticle_id' ], $group ),
                'post_featured_img_thumb'  => post_featured_image( $d[ 'larticle_id' ], $group, '_thumb' ),
                'post_featured_img_large'  => post_featured_image( $d[ 'larticle_id' ], $group, '_large' ),
                'post_featured_img_medium' => post_featured_image( $d[ 'larticle_id' ], $group, '_medium' ),
                'post_featured_img_opt_2'  => post_featured_image( $d[ 'larticle_id' ], $group, null, true ),
                'post_brief'               => post_brief( $d[ 'larticle_id' ], $group, $d[ 'larticle_content' ] )
            );
        }
    }
    
    return array( 'page' => $page, 'rows' => $n, 'items' => $items );
}

function post_additional_field( $app_id, $app_name )
{
    $field = get_additional_field_by_id( $app_id, $app_name );
    $data  = array();

    if( empty( $field ) === false )
    {
        foreach( $field as $d )
        {
            $data[ $d['lkey'] ] = $d['lvalue'];
        }
    }

    return $data;
}

/*
| -----------------------------------------------------------------------------
| Get Detail Post
| -----------------------------------------------------------------------------
*/
function get_detail_post( $sef, $group = 'blogs' )
{
    global $db;
    
    $d = fetch_articles( 'sef=' . $sef . '&type=' . $group );
    
    $items = array();
    
    if( empty( $d ) === false )
    {
        $items = array(
            'post_sef'                 => $d[ 'lsef' ],
            'post_date'                => $d[ 'lpost_date' ],
            'post_id'                  => $d[ 'larticle_id' ],
            'post_type'                => $d[ 'larticle_type' ],
            'post_title'               => $d[ 'larticle_title' ],
            'post_content'             => $d[ 'larticle_content' ],
            'post_permalink'           => permalink( $d[ 'larticle_id' ] ),
            'post_category'            => post_category( $d[ 'larticle_id' ] ),
            'post_related'             => post_related( $d[ 'larticle_id' ], $group ),
            'post_gallery'             => post_gallery( $d[ 'larticle_id' ], $group ),
            'post_subtitle'            => post_subtitle( $d[ 'larticle_id' ], $group ),
            'post_featured_img'        => post_featured_image( $d[ 'larticle_id' ], $group ),
            'post_featured_img_alt'    => post_featured_image_alt( $d[ 'larticle_id' ], $group ),
            'post_featured_img_title'  => post_featured_image_title( $d[ 'larticle_id' ], $group ),
            'post_featured_img_thumb'  => post_featured_image( $d[ 'larticle_id' ], $group, '_thumb' ),
            'post_featured_img_medium' => post_featured_image( $d[ 'larticle_id' ], $group, '_medium' ),
            'post_featured_img_large'  => post_featured_image( $d[ 'larticle_id' ], $group, '_large', true ),
            'post_brief'               => post_brief( $d[ 'larticle_id' ], 'blogs', $d[ 'larticle_content' ] ),
            'mtitle'                   => get_post_meta( $d[ 'larticle_id' ], 'meta_title' ),
            'mdesc'                    => get_post_meta( $d[ 'larticle_id' ], 'meta_description' ),
            'mkey'                     => get_post_meta( $d[ 'larticle_id' ], 'meta_keywords' )

        );
    }
    
    return $items;
}

/*
| -----------------------------------------------------------------------------
| Get Gallery Post
| -----------------------------------------------------------------------------
*/
function post_gallery( $post_id, $group = 'blogs' )
{
    global $db;

    $s = 'SELECT * FROM lumonata_attachment WHERE larticle_id = %d AND lapp_name = %s AND mime_type LIKE %s ORDER BY lorder';
    $q = $db->prepare_query( $s, $post_id, 'gallery-' . $group, '%image%' );
    $r = $db->do_query( $q );
    
    $data = array();
    
    if( $db->num_rows( $r ) > 0 )
    {
        $site_url = site_url();

        while( $d = $db->fetch_array( $r ) )
        {
            $data[] = array(
                'img_title'   => $d['ltitle'],
                'img_caption' => $d['lcaption'],
                'img_content' => $d['lcontent'],
                'img_alt'     => $d['lalt_text'],
                'img_id'      => $d['lattach_id'],
                'img_ori'     => '//' . $site_url . $d['lattach_loc'],
                'img_thumb'   => '//' . $site_url . $d['lattach_loc_thumb'],
                'img_large'   => '//' . $site_url . $d['lattach_loc_large'],
                'img_medium'  => '//' . $site_url . $d['lattach_loc_medium']
            );
        }
    }

    return $data;
}

/*
| -----------------------------------------------------------------------------
| Get Post Rooms Image
| -----------------------------------------------------------------------------
*/
function post_rooms_img( $post_id, $use_thumb = false )
{
    $data = get_rooms_image( $post_id, 'rooms-content-image' );

    if( empty( $data ) )
    {
        return 'https://dummyimage.com/900x900/F0F0F0/F0F0F0&text=No+Image';
    }
    else
    {
        if( $use_thumb )
        {
            return optimized_images( HTSERVER . $data['img_url'], 15 );
        }
        else
        {
            return optimized_images( HTSERVER . $data['img_url'], null, 900 );
        }
    }
} 

/*
| -----------------------------------------------------------------------------
| Get Related Post
| -----------------------------------------------------------------------------
*/
function post_related( $post_id, $group = 'blogs' )
{
    $related = get_additional_field( $post_id, 'related_post', $group );
    $related = json_decode( $related, true );
    $data    = array();

    if( empty( $related ) === false && is_array( $related ) )
    {
        foreach( $related as $id )
        {
            $d = fetch_articles( 'id=' . $id . '&type=' . $group );

            if( empty( $d ) === false )
            {
                $data[] = array(
                    'post_sef'                 => $d[ 'lsef' ],
                    'post_date'                => $d[ 'lpost_date' ],
                    'post_id'                  => $d[ 'larticle_id' ],
                    'post_type'                => $d[ 'larticle_type' ],
                    'post_title'               => $d[ 'larticle_title' ],
                    'post_content'             => $d[ 'larticle_content' ],
                    'post_permalink'           => permalink( $d[ 'larticle_id' ] ),
                    'post_category'            => post_category( $d[ 'larticle_id' ] ),
                    'post_gallery'             => post_gallery( $d[ 'larticle_id' ], $group ),
                    'post_subtitle'            => post_subtitle( $d[ 'larticle_id' ], $group ),
                    'post_featured_img'        => post_featured_image( $d[ 'larticle_id' ], $group ),
                    'post_featured_img_alt'    => post_featured_image_alt( $d[ 'larticle_id' ], $group ),
                    'post_featured_img_title'  => post_featured_image_title( $d[ 'larticle_id' ], $group ),
                    'post_featured_img_thumb'  => post_featured_image( $d[ 'larticle_id' ], $group, '_thumb' ),
                    'post_featured_img_medium' => post_featured_image( $d[ 'larticle_id' ], $group, '_medium' ),
                    'post_featured_img_large'  => post_featured_image( $d[ 'larticle_id' ], $group, '_large', true ),
                    'post_brief'               => post_brief( $d[ 'larticle_id' ], 'blogs', $d[ 'larticle_content' ] )
                );
            }
        }
    }

    return $data;
}

/*
| -----------------------------------------------------------------------------
| Get Additional Post
| -----------------------------------------------------------------------------
*/
function get_additional_post( $post_id, $group = 'blogs' )
{
    global $db;

    $s = 'SELECT lvalue FROM lumonata_additional_fields WHERE lapp_id=%d AND lkey=%s AND lapp_name=%s';
    $q = $db->prepare_query( $s, $post_id, 'custom_field', $group );
    $r = $db->do_query( $q );
    $d = $db->fetch_array( $r );
    
    if( empty( $d['lvalue'] ) === false )
    {
        return json_decode( $d['lvalue'], true );
    }
}

/*
| -----------------------------------------------------------------------------
| Post Rule Permalink
| -----------------------------------------------------------------------------
*/
function post_rule_permalink( $sef = '', $app_name = 'blogs' )
{
    if( empty( $sef ) )
    {
        return '#';
    }
    else
    {
        return '//' . site_url() . '/' . $app_name . '/' . $sef;
    }
}

/*
| -----------------------------------------------------------------------------
| Post Brief
| -----------------------------------------------------------------------------
*/
function post_brief( $id, $app_name = 'blogs', $content = '' )
{
    $brief = get_additional_field( $id, 'brief', $app_name );
    
    if( empty( $brief ) && empty( $content ) === false )
    {
        $cont_arr = explode( '<!-- pagebreak -->', $content );

        if( is_array( $cont_arr ) && count( $cont_arr ) > 1 )
        {
            $brief = strip_tags( $cont_arr[0], '<a>' );
        }
        else
        {
            $brief = limit_words( strip_tags( $content ), 20 );
        }
    }
    else
    {
        $brief = nl2br( $brief );
    }
    
    return $brief;
}

/*
| -----------------------------------------------------------------------------
| Post Review Brief
| -----------------------------------------------------------------------------
*/
function post_review_brief( $content = '', $link = '', $link_target = '_self', $link_text = 'Read More' )
{
    if( empty( $content ) === false )
    {
        $cont_arr = explode( '<!-- pagebreak -->', $content );

        if( is_array( $cont_arr ) && count( $cont_arr ) > 1 )
        {
            $brief = strip_tags( $cont_arr[0], '<a>' );
        }
        else
        {
            $brief = limit_words( strip_tags( $content ), 150, $link, true, $link_text, $link_target );
        }
    }
    
    return $brief;
}

/*
| -----------------------------------------------------------------------------
| Post Subtitle
| -----------------------------------------------------------------------------
*/
function post_subtitle( $id, $app_name = 'blogs' )
{
    return get_additional_field( $id, 'subtitle', $app_name );
}

/*
| -----------------------------------------------------------------------------
| Post Category
| -----------------------------------------------------------------------------
*/
function post_category( $id )
{
    global $db;
    
    $d = fetch_rule_relationship( 'app_id=' . $id );
    
    $category = array();
    
    foreach( $d as $id )
    {
        $rule = fetch_rule( 'rule_id=' . $id );

        $category[ $rule[ 'lrule' ] ][] = array(
            'sef'   => $rule[ 'lsef' ],
            'name'  => $rule[ 'lname' ],
            'order' => $rule[ 'lorder' ]
        );
    }
    
    return $category;
}

/*
| -----------------------------------------------------------------------------
| Post meta title
| -----------------------------------------------------------------------------
*/
function get_post_meta( $id, $appname = 'meta_title' )
{
    return get_additional_field_by_id_and_lkey( $id, $appname);

}

/*
| -----------------------------------------------------------------------------
| Post Feature Image
| -----------------------------------------------------------------------------
*/
function post_featured_image( $id, $group = 'blogs', $size = '', $return_empty = false )
{
    $d = get_featured_img( $id, $group, false );

    if( isset( $d['lattach_loc' . $size] ) && empty( $d['lattach_loc' . $size] ) === false )
    {
        return '//' . site_url() . $d['lattach_loc' . $size];
    }
    else
    {
        if( $return_empty )
        {
            return '';
        }
        else
        {
            return 'https://dummyimage.com/330x330/dddddd/333333&text=No+Image';
        }
    }
}

/*
| -----------------------------------------------------------------------------
| Post Feature Image Alt
| -----------------------------------------------------------------------------
*/
function post_featured_image_alt( $id, $group = 'blogs' )
{
    return get_additional_field( $id, 'featured_img_alt', $group );
}

/*
| -----------------------------------------------------------------------------
| Post Feature Image Alt
| -----------------------------------------------------------------------------
*/
function post_featured_image_title( $id, $group = 'blogs' )
{
    return get_additional_field( $id, 'featured_img_title', $group );
}

/*
| -----------------------------------------------------------------------------
| Add Meta Data Value
| -----------------------------------------------------------------------------
*/
function add_meta_data_value( $mtitle = '', $mkey = '', $mdesc = '' )
{
    global $actions;
    
    if( isset( $actions->action['meta_title'] ) )
    {
        unset( $actions->action['meta_title'] );
    }
    
    if( isset( $actions->action['meta_keywords'] ) )
    {
        unset( $actions->action['meta_keywords'] );
    }
    
    if( isset( $actions->action['meta_description'] ) )
    {
        unset( $actions->action['meta_description'] );
    }

    add_actions( 'meta_title', get_meta_title( $mtitle ) );
    add_actions( 'meta_keywords', get_meta_keywords( $mkey ) );
    add_actions( 'meta_description', get_meta_description( $mdesc ) );
}

/*
| -----------------------------------------------------------------------------
| Optimized Images
| -----------------------------------------------------------------------------
*/
function optimized_images( $url = '', $width = null, $height = null )
{
    if( empty( $url ) === false && ( is_null( $width ) === false || is_null( $height ) === false ) )
    {
        $surl = site_url();

        if( is_null( $width ) )
        {
            return '//' . $surl . '/lumonata-functions/mthumb.php?src=' . $url . '&q=80&h=' . $height;
        }
        elseif( is_null( $height ) )
        {
            return '//' . $surl . '/lumonata-functions/mthumb.php?src=' . $url . '&q=80&w=' . $width;
        }
        else
        {            
            return '//' . $surl . '/lumonata-functions/mthumb.php?src=' . $url . '&q=80&w=' . $width . '&h=' . $height;
        }
    }
}

/*
| -----------------------------------------------------------------------------
| Body Class
| -----------------------------------------------------------------------------
*/
function get_body_class( $class = '' )
{
    return $class;
}

/*
| -----------------------------------------------------------------------------
| Ajax Permalink
| -----------------------------------------------------------------------------
*/
function ajax_permalink()
{
    return '//' . site_url() . '/ajax-function/';
}

/*
| -----------------------------------------------------------------------------
| Get Archive Link
| -----------------------------------------------------------------------------
*/
function get_archive_link( $app_name = 'blogs' )
{
    return '//' . site_url() . '/' . $app_name . '/';
}

/*
| -----------------------------------------------------------------------------
| Is Archive
| -----------------------------------------------------------------------------
*/
function is_archive( $app_name = 'blogs' )
{
    $sef = get_uri_sef();

    if( $sef == $app_name )
    {
        return true;
    }
    else
    {
        return false;
    }
}

/*
| -----------------------------------------------------------------------------
| Is Thanks Page
| -----------------------------------------------------------------------------
*/
function is_thanks()
{
    $sef = get_uri_sef();

    if( $sef == 'thank-you' )
    {
        return true;
    }
    else
    {
        return false;
    }
}

/*
| -----------------------------------------------------------------------------
| Subscribe Newsletter
| -----------------------------------------------------------------------------
*/
require( ADMIN_PATH . '/includes/mailchimp/vendor/autoload.php' );

use \DrewM\MailChimp\MailChimp;

function subscribe_newsletter()
{    
    $api_key = get_meta_data( 'mailchimp_api_key' );
    $list_id = get_meta_data( 'mailchimp_list_id' );

    if( empty( $api_key ) || empty( $list_id ) )
    {
        return array( 'result' => 'failed', 'message' => 'Something wrong, please try again later' );
    }
    else
    {
        extract( $_POST );

        $MailChimp = new MailChimp( $api_key );

        $MailChimp->post( 'lists/' . $list_id . '/members', array( 
            'status' => 'pending',
            'email_address' => $subscribe_email
        ));

        if( $MailChimp->success() === false )
        {
            $error = $MailChimp->getLastResponse();
            $error = json_decode( $error['body'] );

            if( $error->status == 400 && $error->title == 'Member Exists' )
            {
                return array( 'result' => 'failed', 'message' => 'The email address has already been subscribed' );
            }
            else
            {
                return array( 'result' => 'failed', 'message' => $error->detail );
            }
        }
        else
        {
            return array( 'result' => 'success', 'message' => 'Thank you for subscribing to our newsletter.'  );
        }
    }
}

/*
| -----------------------------------------------------------------------------
| Send Enquiry Function
| -----------------------------------------------------------------------------
*/
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

function send_contact_enquiry()
{
    if( isset( $_POST[ 'g-recaptcha-response' ] ) && empty( $_POST[ 'g-recaptcha-response' ] ) === false )
    {
        $curl  = curl_init();
        $param = http_build_query( array( 
            'remoteip' => $_SERVER[ 'REMOTE_ADDR' ],
            'secret'   => get_meta_data( 'r_secret_key' ),
            'response' => urlencode( $_POST[ 'g-recaptcha-response' ] )
        ));

        curl_setopt_array( $curl, array(
            CURLOPT_URL => 'https://www.google.com/recaptcha/api/siteverify?' . $param,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_RETURNTRANSFER => 1,
        ));
        
        $data = curl_exec( $curl );

        curl_close( $curl );
        
        $response = json_decode( $data );

        if( $response === null && json_last_error() !== JSON_ERROR_NONE )
        {
            return array( 'result' => 'failed', 'message' => 'Something wrong, please try again later' );
        }
        else
        {        
            if( $response->success )
            {
                require ADMIN_PATH . '/includes/php-mailer/vendor/autoload.php';

                $mail = new PHPMailer( true) ;

                try
                {
                    extract( $_POST );

                    $web_name  = trim( web_title() );
                    $mail_from = get_meta_data( 'smtp_email_from' );
                    $mail_to   = get_meta_data( 'contact_info_email' );
                    // $mail_to   = 'arya@lumonata.com';

                    $mail->isSMTP();
                    $mail->isHTML( true );

                    $mail->Host        = get_meta_data( 'smtp' );
                    $mail->Password    = get_meta_data( 'smtp_email_pwd' );
                    $mail->Port        = get_meta_data( 'smtp_email_port' );
                    $mail->Username    = get_meta_data( 'smtp_email_address' );
                    $mail->SMTPSecure  = 'tls';
                    $mail->SMTPAuth    = true;

                    $mail->setFrom( $mail_from, $web_name );
                    $mail->addAddress( $mail_to, $web_name );
                    $mail->addReplyTo( $email, $name );

                    $mail->Subject = 'Contact From ' . $name;
                    $mail->Body    = set_contact_enquiry_content( $name, $email, $subject, $message, $web_name );

                    $mail->send();

                    return array( 'result' => 'success', 'redirect' => '//' . site_url() . '/contact/thank-you.html' );
                }
                catch( Exception $e )
                {
                    return array( 'result' => 'failed', 'message' => 'Message could not be sent. Mailer Error: ' . $mail->ErrorInfo );
                }
            }
            else
            {
                return array( 'result' => 'failed', 'message' => 'Invalid captcha code' );
            }
        }
    }
    else
    {
        return array( 'result' => 'failed', 'message' => 'Please check captcha code' );
    }
}

/*
| -----------------------------------------------------------------------------
| Contact - Set Enquiry Email Content
| -----------------------------------------------------------------------------
*/
function set_contact_enquiry_content( $name, $email, $subject, $message, $web_name )
{    
    set_template( TEMPLATE_PATH . '/template/enquiry-email.html', 'email-template' );

    add_block( 'email-block', 'e-block', 'email-template' );

    add_variable( 'name', $name );
    add_variable( 'email', $email );
    add_variable( 'subject', $subject );
    add_variable( 'web-name', $web_name );
    add_variable( 'message', nl2br( $message ) );

    parse_template( 'email-block', 'e-block', false );

    return return_template( 'email-block' );
}

function send_spa_enquiry()
{
    if( isset( $_POST[ 'g_recaptcha_response' ] ) && empty( $_POST[ 'g_recaptcha_response' ] ) === false )
    {
        $curl  = curl_init();
        $param = http_build_query( array(
            'remoteip' => $_SERVER[ 'REMOTE_ADDR' ],
            'secret'   => get_meta_data( 'r_secret_key' ),
            'response' => urlencode( $_POST[ 'g_recaptcha_response' ] )
        ));

        curl_setopt_array( $curl, array(
            CURLOPT_URL => 'https://www.google.com/recaptcha/api/siteverify?' . $param,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_RETURNTRANSFER => 1,
        ));

        $data = curl_exec( $curl );

        curl_close( $curl );

        $response = json_decode( $data );

        if( $response === null && json_last_error() !== JSON_ERROR_NONE )
        {
            return array( 'result' => 'failed', 'message' => 'Something wrong, please try again later' );
        }
        else
        {
            if( $response->success )
            {
                require ADMIN_PATH . '/includes/php-mailer/vendor/autoload.php';

                $mail = new PHPMailer( true) ;

                // print_r($_POST);
                // exit;

                try
                {
                    extract( $_POST );

                    $web_name  = trim( web_title() );
                    $mail_from = get_meta_data( 'smtp_email_from' );
                    $mail_to   = get_meta_data( 'contact_info_email' );
                    // $mail_to   = 'arya@lumonata.com';

                    $mail->isSMTP();
                    $mail->isHTML( true );

                    //helper phpmailer
                    // $mail->SMTPOptions = array(
                    //     'ssl' => array(
                    //         'verify_peer' => false,
                    //         'verify_peer_name' => false,
                    //         'allow_self_signed' => true
                    //     )
                    // );

                    $mail->Host        = get_meta_data( 'smtp' );
                    $mail->Password    = get_meta_data( 'smtp_email_pwd' );
                    $mail->Port        = get_meta_data( 'smtp_email_port' );
                    $mail->Username    = get_meta_data( 'smtp_email_address' );
                    $mail->SMTPSecure  = 'tls';
                    $mail->SMTPAuth    = true;

                    $mail->setFrom( $mail_from, $web_name );
                    $mail->addAddress( $mail_to, $web_name );
                    $mail->addReplyTo( $_POST['email'], $_POST['guest_name'] );
                    // $mail->addReplyTo( 'aryasutrisnap@gmail.com', 'arya in the house yo' );

                    $mail->Subject = 'Spa Inquiry From ' . $_POST['guest_name'];
                    $mail->Body    = set_spa_enquiry_content( $_POST );
                    // $mail->Body    = 'mashhoookk';

                    $mail->send();

                    return array( 'result' => 'success', 'redirect' => '//' . site_url() . '/contact/thank-you.html' );
                }
                catch( Exception $e )
                {
                    return array( 'result' => 'failed', 'message' => 'Message could not be sent. Mailer Error: ' . $mail->ErrorInfo );
                }
            }
            else
            {
                return array( 'result' => 'failed', 'message' => 'Invalid captcha code' );
            }
        }
    }
    else
    {
        return array( 'result' => 'failed', 'message' => 'Please check captcha code' );
    }
}


function set_spa_enquiry_content( $dt )
{
    set_template( TEMPLATE_PATH . '/template/enquiry-spa.html', 'email-template' );

    add_block( 'email-block', 'e-block', 'email-template' );

    $spa_type               = $dt['spa_type'];
    $guest                    = $dt['guest'];
    $promo_code           = $dt['promo_code'];
    $date                         = $dt['date'];
    $time                       = $dt['time'];
    $hour                       = $dt['hour'];
    $current_stay         = $dt['current_stay'];
    $special_request    = $dt['special_request'];
    $guest_name           = $dt['guest_name'];
    $nationality            = $dt['nationality'];
    $email                    = $dt['email'];
    $phone                    = $dt['phone'];
    $radio_prefer_      = $dt['radio_prefer_'];

    add_variable( 'spa_type', $spa_type );
    add_variable( 'guest', $guest );
    add_variable( 'promo_code', $promo_code );
    add_variable( 'date', $date );
    add_variable( 'time', $time );
    add_variable( 'hour', $hour );
    add_variable( 'current_stay', $current_stay );
    add_variable( 'special_request', $special_request );
    add_variable( 'guest_name', $guest_name );
    add_variable( 'nationality', $nationality );
    add_variable( 'email', $email );
    add_variable( 'phone', $phone );
    add_variable( 'radio_prefer_', $radio_prefer_ );

    parse_template( 'email-block', 'e-block', false );
    return return_template( 'email-block' );
}

// NOTE: FOR RESTAURANT AND ACTIVITIES
function send_restact_enquiry()
{
    if( isset( $_POST[ 'g_recaptcha_response' ] ) && empty( $_POST[ 'g_recaptcha_response' ] ) === false )
    {
        $curl  = curl_init();
        $param = http_build_query( array(
            'remoteip' => $_SERVER[ 'REMOTE_ADDR' ],
            'secret'   => get_meta_data( 'r_secret_key' ),
            'response' => urlencode( $_POST[ 'g_recaptcha_response' ] )
        ));

        curl_setopt_array( $curl, array(
            CURLOPT_URL => 'https://www.google.com/recaptcha/api/siteverify?' . $param,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_RETURNTRANSFER => 1,
        ));

        $data = curl_exec( $curl );

        curl_close( $curl );

        $response = json_decode( $data );

        if( $response === null && json_last_error() !== JSON_ERROR_NONE )
        {
            return array( 'result' => 'failed', 'message' => 'Something wrong, please try again later' );
        }
        else
        {
            if( $response->success )
            {
                require ADMIN_PATH . '/includes/php-mailer/vendor/autoload.php';

                $mail = new PHPMailer( true) ;

                // print_r($_POST);
                // exit;

                try
                {
                    extract( $_POST );

                    $web_name  = trim( web_title() );
                    $mail_from = get_meta_data( 'smtp_email_from' );
                    $mail_to   = get_meta_data( 'contact_info_email' );
                    $mail_to   = 'arya@lumonata.com';

                    $mail->isSMTP();
                    $mail->isHTML( true );

                    //helper phpmailer
                    // $mail->SMTPOptions = array(
                    //     'ssl' => array(
                    //         'verify_peer' => false,
                    //         'verify_peer_name' => false,
                    //         'allow_self_signed' => true
                    //     )
                    // );

                    $mail->Host        = get_meta_data( 'smtp' );
                    $mail->Password    = get_meta_data( 'smtp_email_pwd' );
                    $mail->Port        = get_meta_data( 'smtp_email_port' );
                    $mail->Username    = get_meta_data( 'smtp_email_address' );
                    $mail->SMTPSecure  = 'tls';
                    $mail->SMTPAuth    = true;

                    $mail->setFrom( $mail_from, $web_name );
                    $mail->addAddress( $mail_to, $web_name );
                    $mail->addReplyTo( $_POST['email'], $_POST['guest_name'] );
                    // $mail->addReplyTo( 'aryasutrisnap@gmail.com', 'arya in the house yo' );

                    $mail->Subject = $_POST['titz'].'Reservation Inquiry From ' . $_POST['guest_name'];
                    $mail->Body    = set_restact_enquiry_content( $_POST );
                    // $mail->Body    = 'mashhoookk';

                    $mail->send();

                    return array( 'result' => 'success', 'redirect' => '//' . site_url() . '/contact/thank-you.html' );
                }
                catch( Exception $e )
                {
                    return array( 'result' => 'failed', 'message' => 'Message could not be sent. Mailer Error: ' . $mail->ErrorInfo );
                }
            }
            else
            {
                return array( 'result' => 'failed', 'message' => 'Invalid captcha code' );
            }
        }
    }
    else
    {
        return array( 'result' => 'failed', 'message' => 'Please check captcha code' );
    }
}


function set_restact_enquiry_content( $dt )
{
    set_template( TEMPLATE_PATH . '/template/enquiry-rest-act.html', 'email-template' );

    add_block( 'email-block', 'e-block', 'email-template' );

    // $spa_type                = $dt['spa_type'];
    $guest                    = $dt['guest'];
    $promo_code           = $dt['promo_code'];
    $date                         = $dt['date'];
    $time                       = $dt['time'];
    $hour                       = $dt['hour'];
    $current_stay         = $dt['current_stay'];
    $special_request    = $dt['special_request'];
    $guest_name           = $dt['guest_name'];
    $nationality            = $dt['nationality'];
    $email                    = $dt['email'];
    $phone                    = $dt['phone'];
    $radio_prefer_      = $dt['radio_prefer_'];

    // add_variable( 'spa_type', $spa_type );

    add_variable( 'titz_', $dt['titz'] );
    add_variable( 'guest', $guest );
    add_variable( 'promo_code', $promo_code );
    add_variable( 'date', $date );
    add_variable( 'time', $time );
    add_variable( 'hour', $hour );
    add_variable( 'current_stay', $current_stay );
    add_variable( 'special_request', $special_request );
    add_variable( 'guest_name', $guest_name );
    add_variable( 'nationality', $nationality );
    add_variable( 'email', $email );
    add_variable( 'phone', $phone );
    add_variable( 'radio_prefer_', $radio_prefer_ );

    parse_template( 'email-block', 'e-block', false );
    return return_template( 'email-block' );
}

/*
| -----------------------------------------------------------------------------
| Ajax Function
| -----------------------------------------------------------------------------
*/
function ajax_content()
{
    global $db;

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'send-contact' )
    {
        echo json_encode( send_contact_enquiry() );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'subscribe-newsletter' )
    {
        echo json_encode( subscribe_newsletter() );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'inquiry-spa' )
    {
        echo json_encode( send_spa_enquiry() );
    }

    if( isset( $_POST['pkey'] ) && $_POST['pkey'] == 'inquiry-rest-and-activity' )
    {
        echo json_encode( send_restact_enquiry() );
    }

    exit;
}
function sticky_notes(){
  $wbd_title = get_meta_data( 'wbd_title' );
  $wbd_content = get_meta_data( 'wbd_content' );
  $link = get_meta_data('hm_link_promo');

  $html = '<section class="why-book-direct actv" >
            <div class="wbd-con actv" id="promo-content" style="display:none">
              <a class="cc_"></a>
              <div class="lo_">
                <h2>'.$wbd_title.'</h2>
                '.$wbd_content.'
              </div>
              <a class="bk_" href="'.$link.'" target="_blank">BOOK NOW</a>
            </div>
          </section>
          <section class="why-book-direct-button" style="display:block;visibility:visible">
            <div class="button-display-promo">
              <button id="button-display-promo">Why book direct with us?
              </button>
            </div>
          </section>';

    return $html;
}
?>