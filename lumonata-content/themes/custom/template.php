<?php

/*
	Title: Custom
	Preview: template.png
	Author: Ngurah Rai
	Author Url: http://www.lumonata.com
	Description: Custom template for bidadari
*/
require_once 'functions.php';

$thecontent = set_frontend_content();

set_frontend_template();

add_block( 'main-block', 'm-block' );

add_variable( 'version', '1.0.0' );
add_variable( 'year', date( 'Y' ) );
add_variable( 'site-url', site_url() );
add_variable( 'content-area', $thecontent );
add_variable( 'template-url', TEMPLATE_URL );
add_variable( 'image-url', get_theme_img() );
add_variable( 'header_method',get_meta_data( 'header_method' ));
add_variable( 'link_booking_engine',get_meta_data( 'link_booking_engine' ));
add_variable( 'wa_link',get_meta_data( 'contact_info_wa_link' ));
add_variable( 'header-menu', the_menus( 'menuset=Header&show_title=false&addClass=header-menu' ) );
add_variable( 'mobile-header-menu', the_menus( 'menuset=Header&show_title=false&addClass=header-menu' ) );
add_variable( 'footer-menu', the_menus( 'menuset=Footer&show_title=false&addClass=footer-menu clearfix' ) );

add_variable( 'contact-info-map-addrs', get_meta_data( 'contact_info_map_addrs' ) );
add_variable( 'contact-info-wa-link', get_meta_data( 'contact_info_wa_link' ) );
add_variable( 'contact-info-addrs', get_meta_data( 'contact_info_addrs' ) );
add_variable( 'contact-info-email', get_meta_data( 'contact_info_email' ) );
add_variable( 'contact-info-phone', get_meta_data( 'contact_info_phone' ) );
add_variable( 'contact-info-fax', get_meta_data( 'contact_info_fax' ) );
add_variable( 'contact-info-wa', get_meta_data( 'contact_info_wa' ) );

add_variable( 'contact-info-phone-str', str_replace( ' ', '', get_meta_data( 'contact_info_phone' ) ) );
add_variable( 'contact-info-fax-str', str_replace( ' ', '', get_meta_data( 'contact_info_fax' ) ) );

add_variable( 'other-prop-1-url', optimized_images( HTSERVER . '//' . TEMPLATE_URL . '/images/logo-the-light-villas.png', 100 ) );
add_variable( 'other-prop-2-url', optimized_images( HTSERVER . '//' . TEMPLATE_URL . '/images/logo-yubi.png', 120 ) );
add_variable( 'other-prop-3-url', optimized_images( HTSERVER . '//' . TEMPLATE_URL . '/images/logo-thesunhotel-black.png', 180 ) );

add_variable( 'footer-text-title', get_meta_data( 'footer_text_title' ) );
add_variable( 'footer-text-content', get_meta_data( 'footer_text_content' ) );

add_variable( 'meta-title', run_actions( 'meta_title' ) );
add_variable( 'meta-keywords', run_actions( 'meta_keywords' ) );
add_variable( 'meta-description', run_actions( 'meta_description' ) );

add_variable( 'og-url', run_actions( 'og_url' ) );
add_variable( 'og-title', run_actions( 'og_title' ) );
add_variable( 'og-image', run_actions( 'og_image' ) );
add_variable( 'og-site-name', run_actions( 'og_site_name' ) );
add_variable( 'og-description', run_actions( 'og_description' ) );

add_variable( 'body-class', run_actions( 'body-class-actions' ) );

add_variable( 'header-actions', attemp_actions( 'header-actions' ) );
add_variable( 'footer-actions', attemp_actions( 'footer-actions' ) );

$ig = get_meta_data( 'social_media_instagram' );
$fb = get_meta_data( 'social_media_facebook' );

$tagsos = '';
if (!empty($fb)) {
  $tagsos .= '
  <li class="sosmed_">
    <a href="'.$fb.'" target="_blank">
      <img src="'.get_theme_img().'/facebook.svg">
      The Bidadari Villas & Spa
    </a>
  </li>
  ';
}

if (!empty($ig)) {
  $tagsos .= '
  <li class="sosmed_">
    <a href="'.$ig.'" target="_blank">
      <img src="'.get_theme_img().'/instagram.svg">
      @thebidadarivillasandspa
    </a>
  </li>
  ';
}

add_variable( 'wbd_title', get_meta_data( 'wbd_title' ) );
add_variable( 'wbd_content', get_meta_data( 'wbd_content' ) );

add_variable( 'sosmed', $tagsos );

// NOTE: GOOGLE TAG MANAGER
add_variable( 'gtm_head', get_meta_data( 'g_analytic' ) );
add_variable( 'gtm_body', get_meta_data( 'g_tag_manager' ) );

// NOTE: SOJERN PIXELS !
$sojern = 'sojern_script_tracking';
$url    = cek_url();

if (isset($url[0])) {
  if ($url[0] == 'rooms' || $url[0] == 'spa') {
    $sojern = 'sojern_script_product';
  }
}

if (is_home()) {
  $sojern = 'sojern_script';
}

add_variable( 'sojern', get_meta_data( $sojern ) );

parse_template( 'main-block', 'm-block' );
print_template();

?>