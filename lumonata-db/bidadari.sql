/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50621
 Source Host           : 127.0.0.1:3306
 Source Schema         : bidadari

 Target Server Type    : MySQL
 Target Server Version : 50621
 File Encoding         : 65001

 Date: 15/03/2019 01:53:21
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for lumonata_additional_fields
-- ----------------------------
DROP TABLE IF EXISTS `lumonata_additional_fields`;
CREATE TABLE `lumonata_additional_fields`  (
  `lapp_id` bigint(20) NOT NULL,
  `lkey` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `lvalue` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `lapp_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  PRIMARY KEY (`lapp_id`, `lkey`) USING BTREE,
  INDEX `key`(`lkey`) USING BTREE,
  INDEX `app_name`(`lapp_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of lumonata_additional_fields
-- ----------------------------
INSERT INTO `lumonata_additional_fields` VALUES (1, 'amenities_list', '[\"27\",\"26\",\"25\",\"23\",\"20\",\"19\",\"18\",\"15\",\"14\",\"13\"]', 'rooms');
INSERT INTO `lumonata_additional_fields` VALUES (1, 'brief', 'Each villa enjoys the utmost privacy, screened from outside by a Balinese wall and access from the private path-way to the entrance court yard.', 'rooms');
INSERT INTO `lumonata_additional_fields` VALUES (1, 'featured_img_alt', '', 'rooms');
INSERT INTO `lumonata_additional_fields` VALUES (1, 'featured_img_copyright', '', 'rooms');
INSERT INTO `lumonata_additional_fields` VALUES (1, 'featured_img_title', '', 'rooms');
INSERT INTO `lumonata_additional_fields` VALUES (1, 'meta_description', 'Each villa enjoys the utmost privacy, screened from outside by a Balinese wall and access from the private path-way to the entrance court yard.', 'rooms');
INSERT INTO `lumonata_additional_fields` VALUES (1, 'meta_keywords', 'villa bali, villa pool, one bedroom', 'rooms');
INSERT INTO `lumonata_additional_fields` VALUES (1, 'meta_title', 'Rooms - One Bedroom Pool Villas', 'rooms');
INSERT INTO `lumonata_additional_fields` VALUES (1, 'related_post', '[\"2\",\"3\"]', 'rooms');
INSERT INTO `lumonata_additional_fields` VALUES (1, 'subtitle', 'The Bidadari Luxuri Villas &amp; Spa', 'rooms');
INSERT INTO `lumonata_additional_fields` VALUES (2, 'amenities_list', '[\"27\",\"25\",\"20\",\"18\",\"16\",\"14\",\"13\"]', 'rooms');
INSERT INTO `lumonata_additional_fields` VALUES (2, 'brief', 'Private pool villa with two king size bedrooms, separate bathrooms and shower with large living area pavilion, dining are, kitchenette, a lush tropical garden surrounding the villa, mild and tranquility for two couples or family.', 'rooms');
INSERT INTO `lumonata_additional_fields` VALUES (2, 'featured_img_alt', '', 'rooms');
INSERT INTO `lumonata_additional_fields` VALUES (2, 'featured_img_copyright', '', 'rooms');
INSERT INTO `lumonata_additional_fields` VALUES (2, 'featured_img_title', '', 'rooms');
INSERT INTO `lumonata_additional_fields` VALUES (2, 'meta_description', '', 'rooms');
INSERT INTO `lumonata_additional_fields` VALUES (2, 'meta_keywords', '', 'rooms');
INSERT INTO `lumonata_additional_fields` VALUES (2, 'meta_title', '', 'rooms');
INSERT INTO `lumonata_additional_fields` VALUES (2, 'related_post', '[\"1\",\"3\"]', 'rooms');
INSERT INTO `lumonata_additional_fields` VALUES (2, 'subtitle', 'The Bidadari Luxuri Villas &amp; Spa', 'rooms');
INSERT INTO `lumonata_additional_fields` VALUES (3, 'amenities_list', '[\"27\",\"26\",\"25\",\"24\",\"23\",\"22\",\"21\",\"20\",\"19\",\"18\",\"17\",\"16\",\"15\",\"14\",\"13\"]', 'rooms');
INSERT INTO `lumonata_additional_fields` VALUES (3, 'brief', 'The villa has a two-storey, the roof pavilion with soaring ceilings with separate bathrooms, indoor bath-tub and showers.', 'rooms');
INSERT INTO `lumonata_additional_fields` VALUES (3, 'featured_img_alt', '', 'rooms');
INSERT INTO `lumonata_additional_fields` VALUES (3, 'featured_img_copyright', '', 'rooms');
INSERT INTO `lumonata_additional_fields` VALUES (3, 'featured_img_title', '', 'rooms');
INSERT INTO `lumonata_additional_fields` VALUES (3, 'meta_description', '', 'rooms');
INSERT INTO `lumonata_additional_fields` VALUES (3, 'meta_keywords', '', 'rooms');
INSERT INTO `lumonata_additional_fields` VALUES (3, 'meta_title', '', 'rooms');
INSERT INTO `lumonata_additional_fields` VALUES (3, 'related_post', '[\"1\",\"2\"]', 'rooms');
INSERT INTO `lumonata_additional_fields` VALUES (3, 'subtitle', 'The Bidadari Luxuri Villas &amp; Spa', 'rooms');
INSERT INTO `lumonata_additional_fields` VALUES (4, 'brief', 'Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Donec sollicitudin molestie malesuada. Vivamus suscipit tortor eget felis porttitor volutpat. Donec rutrum congue leo eget malesuada.', 'offers');
INSERT INTO `lumonata_additional_fields` VALUES (4, 'featured_img_alt', '', 'offers');
INSERT INTO `lumonata_additional_fields` VALUES (4, 'featured_img_copyright', '', 'offers');
INSERT INTO `lumonata_additional_fields` VALUES (4, 'featured_img_title', '', 'offers');
INSERT INTO `lumonata_additional_fields` VALUES (4, 'meta_description', '', 'offers');
INSERT INTO `lumonata_additional_fields` VALUES (4, 'meta_keywords', '', 'offers');
INSERT INTO `lumonata_additional_fields` VALUES (4, 'meta_title', '', 'offers');
INSERT INTO `lumonata_additional_fields` VALUES (5, 'brief', '', 'offers');
INSERT INTO `lumonata_additional_fields` VALUES (5, 'featured_img_alt', '', 'offers');
INSERT INTO `lumonata_additional_fields` VALUES (5, 'featured_img_copyright', '', 'offers');
INSERT INTO `lumonata_additional_fields` VALUES (5, 'featured_img_title', '', 'offers');
INSERT INTO `lumonata_additional_fields` VALUES (5, 'meta_description', '', 'offers');
INSERT INTO `lumonata_additional_fields` VALUES (5, 'meta_keywords', '', 'offers');
INSERT INTO `lumonata_additional_fields` VALUES (5, 'meta_title', '', 'offers');
INSERT INTO `lumonata_additional_fields` VALUES (6, 'brief', '', 'offers');
INSERT INTO `lumonata_additional_fields` VALUES (6, 'featured_img_alt', '', 'offers');
INSERT INTO `lumonata_additional_fields` VALUES (6, 'featured_img_copyright', '', 'offers');
INSERT INTO `lumonata_additional_fields` VALUES (6, 'featured_img_title', '', 'offers');
INSERT INTO `lumonata_additional_fields` VALUES (6, 'meta_description', '', 'offers');
INSERT INTO `lumonata_additional_fields` VALUES (6, 'meta_keywords', '', 'offers');
INSERT INTO `lumonata_additional_fields` VALUES (6, 'meta_title', '', 'offers');
INSERT INTO `lumonata_additional_fields` VALUES (7, 'brief', 'Most yoga practices focus on physical postures called â€œasanas,â€ breathing exercises called â€œpranayama,â€ and meditation to bring your body and mind together through slow, careful movements. But, thereâ€™s more to it than that! Yoga leads to improved physical fitness, increased ability to concentrate, and decreased stress. Yoga is an activity that helps both your body and mind work a little better.', 'activities');
INSERT INTO `lumonata_additional_fields` VALUES (7, 'featured_img_alt', '', 'activities');
INSERT INTO `lumonata_additional_fields` VALUES (7, 'featured_img_copyright', '', 'activities');
INSERT INTO `lumonata_additional_fields` VALUES (7, 'featured_img_title', '', 'activities');
INSERT INTO `lumonata_additional_fields` VALUES (7, 'meta_description', '', 'activities');
INSERT INTO `lumonata_additional_fields` VALUES (7, 'meta_keywords', '', 'activities');
INSERT INTO `lumonata_additional_fields` VALUES (7, 'meta_title', '', 'activities');
INSERT INTO `lumonata_additional_fields` VALUES (7, 'subtitle', '', 'activities');
INSERT INTO `lumonata_additional_fields` VALUES (8, 'brief', 'Discover the spiced flavors of Baliâ€™s virtually unknown cuisine. Classes offer a fascinating introduction in to the exotic ingredients and unique culinary heritage of Bali. They provide a valuable insight into the various techniques, traditional and modern, of food preparation and the cooking style used in homes across the island.', 'activities');
INSERT INTO `lumonata_additional_fields` VALUES (8, 'featured_img_alt', '', 'activities');
INSERT INTO `lumonata_additional_fields` VALUES (8, 'featured_img_copyright', '', 'activities');
INSERT INTO `lumonata_additional_fields` VALUES (8, 'featured_img_title', '', 'activities');
INSERT INTO `lumonata_additional_fields` VALUES (8, 'meta_description', '', 'activities');
INSERT INTO `lumonata_additional_fields` VALUES (8, 'meta_keywords', '', 'activities');
INSERT INTO `lumonata_additional_fields` VALUES (8, 'meta_title', '', 'activities');
INSERT INTO `lumonata_additional_fields` VALUES (9, 'brief', 'Find indoor and outdoor games and activities that will challenge and develop your child\'s creativity, imagination, thinking skills, and social skills. Search our Activities Center for more great ideas for all ages.', 'activities');
INSERT INTO `lumonata_additional_fields` VALUES (9, 'featured_img_alt', '', 'activities');
INSERT INTO `lumonata_additional_fields` VALUES (9, 'featured_img_copyright', '', 'activities');
INSERT INTO `lumonata_additional_fields` VALUES (9, 'featured_img_title', '', 'activities');
INSERT INTO `lumonata_additional_fields` VALUES (9, 'meta_description', '', 'activities');
INSERT INTO `lumonata_additional_fields` VALUES (9, 'meta_keywords', '', 'activities');
INSERT INTO `lumonata_additional_fields` VALUES (9, 'meta_title', '', 'activities');
INSERT INTO `lumonata_additional_fields` VALUES (10, 'brief', 'Balinese and Indonesia herbs and spices blended from traditional recipes into modern-day beauty rituals.', 'spa');
INSERT INTO `lumonata_additional_fields` VALUES (10, 'featured_img_alt', '', 'spa');
INSERT INTO `lumonata_additional_fields` VALUES (10, 'featured_img_copyright', '', 'spa');
INSERT INTO `lumonata_additional_fields` VALUES (10, 'featured_img_title', '', 'spa');
INSERT INTO `lumonata_additional_fields` VALUES (10, 'meta_description', '', 'spa');
INSERT INTO `lumonata_additional_fields` VALUES (10, 'meta_keywords', '', 'spa');
INSERT INTO `lumonata_additional_fields` VALUES (10, 'meta_title', '', 'spa');
INSERT INTO `lumonata_additional_fields` VALUES (10, 'packages_list', '[\"7\",\"6\",\"5\"]', 'spa');
INSERT INTO `lumonata_additional_fields` VALUES (11, 'brief', 'Two storey restaurant and magnificent view of rice field from the second floor. The talented, experienced chefâ€™s creation International cuisine, with 45 seats, serving breakfast, lunch and dinner.', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES (11, 'featured_img_alt', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES (11, 'featured_img_copyright', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES (11, 'featured_img_title', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES (11, 'meta_description', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES (11, 'meta_keywords', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES (11, 'meta_title', '', 'restaurant');
INSERT INTO `lumonata_additional_fields` VALUES (12, 'brief', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES (12, 'featured_img_alt', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES (12, 'featured_img_copyright', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES (12, 'featured_img_title', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES (12, 'meta_description', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES (12, 'meta_keywords', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES (12, 'meta_title', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES (13, 'brief', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES (13, 'featured_img_alt', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES (13, 'featured_img_copyright', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES (13, 'featured_img_title', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES (13, 'meta_description', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES (13, 'meta_keywords', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES (13, 'meta_title', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES (14, 'brief', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES (14, 'featured_img_alt', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES (14, 'featured_img_copyright', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES (14, 'featured_img_title', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES (14, 'meta_description', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES (14, 'meta_keywords', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES (14, 'meta_title', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES (15, 'brief', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES (15, 'featured_img_alt', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES (15, 'featured_img_copyright', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES (15, 'featured_img_title', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES (15, 'meta_description', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES (15, 'meta_keywords', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES (15, 'meta_title', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES (16, 'brief', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES (16, 'featured_img_alt', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES (16, 'featured_img_copyright', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES (16, 'featured_img_title', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES (16, 'meta_description', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES (16, 'meta_keywords', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES (16, 'meta_title', '', 'awards');
INSERT INTO `lumonata_additional_fields` VALUES (17, 'brief', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES (17, 'featured_img_alt', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES (17, 'featured_img_copyright', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES (17, 'featured_img_title', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES (17, 'meta_description', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES (17, 'meta_keywords', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES (17, 'meta_title', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES (18, 'brief', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES (18, 'featured_img_alt', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES (18, 'featured_img_copyright', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES (18, 'featured_img_title', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES (18, 'meta_description', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES (18, 'meta_keywords', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES (18, 'meta_title', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES (19, 'brief', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES (19, 'featured_img_alt', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES (19, 'featured_img_copyright', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES (19, 'featured_img_title', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES (19, 'meta_description', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES (19, 'meta_keywords', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES (19, 'meta_title', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES (20, 'brief', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES (20, 'featured_img_alt', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES (20, 'featured_img_copyright', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES (20, 'featured_img_title', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES (20, 'meta_description', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES (20, 'meta_keywords', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES (20, 'meta_title', '', 'gallery');
INSERT INTO `lumonata_additional_fields` VALUES (23, 'brief', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (23, 'featured_img_alt', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (23, 'featured_img_copyright', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (23, 'featured_img_title', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (23, 'loc_addr_1', 'Cannot determine address at this location.', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (23, 'loc_addr_2', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (23, 'loc_cordinate', '-8.654776,115.1511393', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (23, 'meta_description', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (23, 'meta_keywords', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (23, 'meta_title', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (24, 'brief', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (24, 'featured_img_alt', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (24, 'featured_img_copyright', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (24, 'featured_img_title', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (24, 'loc_addr_1', 'Cannot determine address at this location.', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (24, 'loc_addr_2', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (24, 'loc_cordinate', '-8.658237,115.1505733', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (24, 'meta_description', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (24, 'meta_keywords', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (24, 'meta_title', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (25, 'brief', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (25, 'featured_img_alt', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (25, 'featured_img_copyright', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (25, 'featured_img_title', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (25, 'loc_addr_1', 'Cannot determine address at this location.', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (25, 'loc_addr_2', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (25, 'loc_cordinate', '-8.6610555,115.1503492', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (25, 'meta_description', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (25, 'meta_keywords', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (25, 'meta_title', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (26, 'brief', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (26, 'featured_img_alt', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (26, 'featured_img_copyright', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (26, 'featured_img_title', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (26, 'loc_addr_1', 'Cannot determine address at this location.', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (26, 'loc_addr_2', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (26, 'loc_cordinate', '-8.6468341,115.1304461', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (26, 'meta_description', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (26, 'meta_keywords', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (26, 'meta_title', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (27, 'brief', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (27, 'featured_img_alt', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (27, 'featured_img_copyright', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (27, 'featured_img_title', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (27, 'loc_addr_1', 'Cannot determine address at this location.', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (27, 'loc_addr_2', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (27, 'loc_cordinate', '-8.6617537,115.1450708', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (27, 'meta_description', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (27, 'meta_keywords', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (27, 'meta_title', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (28, 'brief', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (28, 'featured_img_alt', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (28, 'featured_img_copyright', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (28, 'featured_img_title', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (28, 'loc_addr_1', 'Cannot determine address at this location.', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (28, 'loc_addr_2', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (28, 'loc_cordinate', '-8.6624618,115.1449431', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (28, 'meta_description', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (28, 'meta_keywords', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (28, 'meta_title', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (29, 'brief', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (29, 'featured_img_alt', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (29, 'featured_img_copyright', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (29, 'featured_img_title', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (29, 'loc_addr_1', 'Cannot determine address at this location.', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (29, 'loc_addr_2', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (29, 'loc_cordinate', '-8.6628773,115.1442514', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (29, 'meta_description', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (29, 'meta_keywords', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (29, 'meta_title', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (30, 'brief', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (30, 'featured_img_alt', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (30, 'featured_img_copyright', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (30, 'featured_img_title', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (30, 'loc_addr_1', 'Cannot determine address at this location.', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (30, 'loc_addr_2', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (30, 'loc_cordinate', '-8.6657281,115.1455224', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (30, 'meta_description', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (30, 'meta_keywords', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (30, 'meta_title', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (31, 'brief', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (31, 'featured_img_alt', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (31, 'featured_img_copyright', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (31, 'featured_img_title', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (31, 'loc_addr_1', 'Cannot determine address at this location.', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (31, 'loc_addr_2', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (31, 'loc_cordinate', '-8.66599,115.1412673', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (31, 'meta_description', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (31, 'meta_keywords', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (31, 'meta_title', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (32, 'brief', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (32, 'featured_img_alt', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (32, 'featured_img_copyright', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (32, 'featured_img_title', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (32, 'loc_addr_1', 'Cannot determine address at this location.', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (32, 'loc_addr_2', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (32, 'loc_cordinate', '-8.6559592,115.1513084', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (32, 'meta_description', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (32, 'meta_keywords', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (32, 'meta_title', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (33, 'brief', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (33, 'featured_img_alt', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (33, 'featured_img_copyright', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (33, 'featured_img_title', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (33, 'loc_addr_1', 'Cannot determine address at this location.', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (33, 'loc_addr_2', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (33, 'loc_cordinate', '-8.6539832,115.1536386', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (33, 'meta_description', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (33, 'meta_keywords', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (33, 'meta_title', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (34, 'brief', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (34, 'featured_img_alt', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (34, 'featured_img_copyright', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (34, 'featured_img_title', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (34, 'loc_addr_1', 'Cannot determine address at this location.', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (34, 'loc_addr_2', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (34, 'loc_cordinate', '-8.6546302,115.1556771', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (34, 'meta_description', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (34, 'meta_keywords', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (34, 'meta_title', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (35, 'brief', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (35, 'featured_img_alt', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (35, 'featured_img_copyright', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (35, 'featured_img_title', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (35, 'loc_addr_1', 'Cannot determine address at this location.', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (35, 'loc_addr_2', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (35, 'loc_cordinate', '-8.6568364,115.1478558', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (35, 'meta_description', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (35, 'meta_keywords', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (35, 'meta_title', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (36, 'brief', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (36, 'featured_img_alt', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (36, 'featured_img_copyright', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (36, 'featured_img_title', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (36, 'loc_addr_1', 'Cannot determine address at this location.', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (36, 'loc_addr_2', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (36, 'loc_cordinate', '-8.662723,115.1482956', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (36, 'meta_description', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (36, 'meta_keywords', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (36, 'meta_title', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (37, 'brief', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (37, 'featured_img_alt', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (37, 'featured_img_copyright', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (37, 'featured_img_title', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (37, 'loc_addr_1', 'Cannot determine address at this location.', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (37, 'loc_addr_2', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (37, 'loc_cordinate', '-8.668355,115.1452057', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (37, 'meta_description', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (37, 'meta_keywords', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (37, 'meta_title', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (38, 'brief', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (38, 'featured_img_alt', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (38, 'featured_img_copyright', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (38, 'featured_img_title', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (38, 'loc_addr_1', 'Cannot determine address at this location.', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (38, 'loc_addr_2', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (38, 'loc_cordinate', '-8.6644519,115.1377707', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (38, 'meta_description', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (38, 'meta_keywords', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (38, 'meta_title', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (39, 'brief', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (39, 'featured_img_alt', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (39, 'featured_img_copyright', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (39, 'featured_img_title', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (39, 'loc_addr_1', 'Cannot determine address at this location.', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (39, 'loc_addr_2', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (39, 'loc_cordinate', '-8.6723429,115.1464396', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (39, 'meta_description', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (39, 'meta_keywords', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (39, 'meta_title', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (40, 'brief', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (40, 'featured_img_alt', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (40, 'featured_img_copyright', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (40, 'featured_img_title', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (40, 'loc_addr_1', 'Cannot determine address at this location.', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (40, 'loc_addr_2', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (40, 'loc_cordinate', '-8.6568364,115.1478558', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (40, 'meta_description', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (40, 'meta_keywords', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (40, 'meta_title', '', 'locations');
INSERT INTO `lumonata_additional_fields` VALUES (41, 'brief', 'Located in a relaxing place of Umalas area, around 30 minutes from Ngurah Rai Bali International airport and 15 minutes away to popular tourist sport in Seminyak or Canggu- Brawa.', 'pages');
INSERT INTO `lumonata_additional_fields` VALUES (41, 'featured_img_alt', '', 'pages');
INSERT INTO `lumonata_additional_fields` VALUES (41, 'featured_img_copyright', '', 'pages');
INSERT INTO `lumonata_additional_fields` VALUES (41, 'featured_img_title', '', 'pages');
INSERT INTO `lumonata_additional_fields` VALUES (41, 'meta_description', '', 'pages');
INSERT INTO `lumonata_additional_fields` VALUES (41, 'meta_keywords', '', 'pages');
INSERT INTO `lumonata_additional_fields` VALUES (41, 'meta_title', '', 'pages');
INSERT INTO `lumonata_additional_fields` VALUES (42, 'brief', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum in eros vel lorem rhoncus cursus porttitor et lorem.', 'pages');
INSERT INTO `lumonata_additional_fields` VALUES (42, 'featured_img_alt', '', 'pages');
INSERT INTO `lumonata_additional_fields` VALUES (42, 'featured_img_copyright', '', 'pages');
INSERT INTO `lumonata_additional_fields` VALUES (42, 'featured_img_title', '', 'pages');
INSERT INTO `lumonata_additional_fields` VALUES (42, 'meta_description', '', 'pages');
INSERT INTO `lumonata_additional_fields` VALUES (42, 'meta_keywords', '', 'pages');
INSERT INTO `lumonata_additional_fields` VALUES (42, 'meta_title', '', 'pages');
INSERT INTO `lumonata_additional_fields` VALUES (42, 'subtitle', '', 'pages');
INSERT INTO `lumonata_additional_fields` VALUES (43, 'brief', 'Aenean sollicitudin gravida vestibulum. Aliquam vel libero non nunc sodales bibendum. Duis accumsan quam eu eros suscipit, a vulputate nibh pharetra.', 'pages');
INSERT INTO `lumonata_additional_fields` VALUES (43, 'featured_img_alt', '', 'pages');
INSERT INTO `lumonata_additional_fields` VALUES (43, 'featured_img_copyright', '', 'pages');
INSERT INTO `lumonata_additional_fields` VALUES (43, 'featured_img_title', '', 'pages');
INSERT INTO `lumonata_additional_fields` VALUES (43, 'meta_description', '', 'pages');
INSERT INTO `lumonata_additional_fields` VALUES (43, 'meta_keywords', '', 'pages');
INSERT INTO `lumonata_additional_fields` VALUES (43, 'meta_title', '', 'pages');
INSERT INTO `lumonata_additional_fields` VALUES (44, 'brief', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (44, 'featured_img_alt', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (44, 'featured_img_copyright', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (44, 'featured_img_title', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (44, 'iconpicker', 'ico-travel', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (44, 'meta_description', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (44, 'meta_keywords', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (44, 'meta_title', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (44, 'subtitle', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (45, 'brief', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (45, 'featured_img_alt', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (45, 'featured_img_copyright', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (45, 'featured_img_title', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (45, 'iconpicker', 'ico-barbeque', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (45, 'meta_description', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (45, 'meta_keywords', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (45, 'meta_title', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (45, 'subtitle', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (46, 'brief', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (46, 'featured_img_alt', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (46, 'featured_img_copyright', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (46, 'featured_img_title', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (46, 'iconpicker', 'ico-chef', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (46, 'meta_description', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (46, 'meta_keywords', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (46, 'meta_title', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (46, 'subtitle', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (47, 'brief', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (47, 'featured_img_alt', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (47, 'featured_img_copyright', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (47, 'featured_img_title', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (47, 'iconpicker', 'ico-toasts', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (47, 'meta_description', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (47, 'meta_keywords', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (47, 'meta_title', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (47, 'subtitle', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (48, 'brief', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (48, 'featured_img_alt', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (48, 'featured_img_copyright', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (48, 'featured_img_title', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (48, 'iconpicker', 'ico-dinning-table', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (48, 'meta_description', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (48, 'meta_keywords', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (48, 'meta_title', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (48, 'subtitle', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (49, 'brief', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (49, 'featured_img_alt', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (49, 'featured_img_copyright', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (49, 'featured_img_title', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (49, 'iconpicker', 'ico-exchange', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (49, 'meta_description', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (49, 'meta_keywords', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (49, 'meta_title', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (49, 'subtitle', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (50, 'brief', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (50, 'featured_img_alt', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (50, 'featured_img_copyright', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (50, 'featured_img_title', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (50, 'iconpicker', 'ico-airplane', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (50, 'meta_description', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (50, 'meta_keywords', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (50, 'meta_title', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (50, 'subtitle', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (51, 'brief', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (51, 'featured_img_alt', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (51, 'featured_img_copyright', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (51, 'featured_img_title', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (51, 'iconpicker', 'ico-wifi', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (51, 'meta_description', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (51, 'meta_keywords', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (51, 'meta_title', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (51, 'subtitle', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (52, 'brief', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (52, 'featured_img_alt', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (52, 'featured_img_copyright', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (52, 'featured_img_title', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (52, 'iconpicker', 'ico-car', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (52, 'meta_description', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (52, 'meta_keywords', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (52, 'meta_title', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (52, 'subtitle', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (53, 'brief', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (53, 'featured_img_alt', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (53, 'featured_img_copyright', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (53, 'featured_img_title', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (53, 'iconpicker', 'ico-tour-arrangement', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (53, 'meta_description', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (53, 'meta_keywords', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (53, 'meta_title', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (53, 'subtitle', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (54, 'brief', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (54, 'featured_img_alt', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (54, 'featured_img_copyright', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (54, 'featured_img_title', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (54, 'iconpicker', 'ico-doctor-stethoscope', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (54, 'meta_description', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (54, 'meta_keywords', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (54, 'meta_title', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (54, 'subtitle', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (55, 'brief', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (55, 'featured_img_alt', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (55, 'featured_img_copyright', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (55, 'featured_img_title', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (55, 'iconpicker', 'ico-washing-machine', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (55, 'meta_description', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (55, 'meta_keywords', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (55, 'meta_title', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (55, 'subtitle', '', 'services');
INSERT INTO `lumonata_additional_fields` VALUES (56, 'brief', '', 'reviews');
INSERT INTO `lumonata_additional_fields` VALUES (56, 'featured_img_alt', '', 'reviews');
INSERT INTO `lumonata_additional_fields` VALUES (56, 'featured_img_copyright', '', 'reviews');
INSERT INTO `lumonata_additional_fields` VALUES (56, 'featured_img_title', '', 'reviews');
INSERT INTO `lumonata_additional_fields` VALUES (56, 'meta_description', '', 'reviews');
INSERT INTO `lumonata_additional_fields` VALUES (56, 'meta_keywords', '', 'reviews');
INSERT INTO `lumonata_additional_fields` VALUES (56, 'meta_title', '', 'reviews');
INSERT INTO `lumonata_additional_fields` VALUES (56, 'review_by', 'ChuckHay', 'reviews');
INSERT INTO `lumonata_additional_fields` VALUES (56, 'review_link', '#', 'reviews');
INSERT INTO `lumonata_additional_fields` VALUES (56, 'review_rate', '5', 'reviews');
INSERT INTO `lumonata_additional_fields` VALUES (56, 'review_target', '_self', 'reviews');
INSERT INTO `lumonata_additional_fields` VALUES (56, 'subtitle', '', 'reviews');
INSERT INTO `lumonata_additional_fields` VALUES (57, 'brief', '', 'reviews');
INSERT INTO `lumonata_additional_fields` VALUES (57, 'featured_img_alt', '', 'reviews');
INSERT INTO `lumonata_additional_fields` VALUES (57, 'featured_img_copyright', '', 'reviews');
INSERT INTO `lumonata_additional_fields` VALUES (57, 'featured_img_title', '', 'reviews');
INSERT INTO `lumonata_additional_fields` VALUES (57, 'meta_description', '', 'reviews');
INSERT INTO `lumonata_additional_fields` VALUES (57, 'meta_keywords', '', 'reviews');
INSERT INTO `lumonata_additional_fields` VALUES (57, 'meta_title', '', 'reviews');
INSERT INTO `lumonata_additional_fields` VALUES (57, 'review_by', 'Ross L', 'reviews');
INSERT INTO `lumonata_additional_fields` VALUES (57, 'review_link', 'http://thebidadarivillasandspa.com/', 'reviews');
INSERT INTO `lumonata_additional_fields` VALUES (57, 'review_rate', '3.5', 'reviews');
INSERT INTO `lumonata_additional_fields` VALUES (57, 'review_target', '_blank', 'reviews');
INSERT INTO `lumonata_additional_fields` VALUES (57, 'subtitle', '', 'reviews');

-- ----------------------------
-- Table structure for lumonata_additional_rule_fields
-- ----------------------------
DROP TABLE IF EXISTS `lumonata_additional_rule_fields`;
CREATE TABLE `lumonata_additional_rule_fields`  (
  `lrule_id` bigint(20) NOT NULL,
  `lkey` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `lvalue` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `lapp_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`lrule_id`, `lkey`) USING BTREE,
  INDEX `key`(`lkey`) USING BTREE,
  INDEX `app_name`(`lapp_name`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = latin1 COLLATE = latin1_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lumonata_additional_rule_fields
-- ----------------------------
INSERT INTO `lumonata_additional_rule_fields` VALUES (7, 'featured_img', '{\"thumb\":\"\\/lumonata-content\\/files\\/201903\\/spa-package-1-1551871042-thumbnail.jpg\",\"medium\":\"\\/lumonata-content\\/files\\/201903\\/spa-package-1-1551871042-medium.jpg\",\"large\":\"\\/lumonata-content\\/files\\/201903\\/spa-package-1-1551871042-large.jpg\",\"original\":\"\\/lumonata-content\\/files\\/201903\\/spa-package-1-1551871042.jpg\"}', 'spa');
INSERT INTO `lumonata_additional_rule_fields` VALUES (6, 'featured_img', '{\"thumb\":\"\\/lumonata-content\\/files\\/201903\\/spa-package-2-1551871063-thumbnail.jpg\",\"medium\":\"\\/lumonata-content\\/files\\/201903\\/spa-package-2-1551871063-medium.jpg\",\"large\":\"\\/lumonata-content\\/files\\/201903\\/spa-package-2-1551871063-large.jpg\",\"original\":\"\\/lumonata-content\\/files\\/201903\\/spa-package-2-1551871063.jpg\"}', 'spa');
INSERT INTO `lumonata_additional_rule_fields` VALUES (5, 'featured_img', '{\"thumb\":\"\\/lumonata-content\\/files\\/201903\\/spa-package-3-1551871076-thumbnail.jpg\",\"medium\":\"\\/lumonata-content\\/files\\/201903\\/spa-package-3-1551871076-medium.jpg\",\"large\":\"\\/lumonata-content\\/files\\/201903\\/spa-package-3-1551871076-large.jpg\",\"original\":\"\\/lumonata-content\\/files\\/201903\\/spa-package-3-1551871076.jpg\"}', 'spa');

-- ----------------------------
-- Table structure for lumonata_articles
-- ----------------------------
DROP TABLE IF EXISTS `lumonata_articles`;
CREATE TABLE `lumonata_articles`  (
  `larticle_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `larticle_title` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `larticle_brief` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `larticle_content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `larticle_status` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `larticle_type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `lcomment_status` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `lcomment_count` bigint(20) NULL DEFAULT 0,
  `lcount_like` bigint(20) NULL DEFAULT 0,
  `lsef` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `lorder` bigint(20) NULL DEFAULT 1,
  `lpost_by` bigint(20) NULL DEFAULT 0,
  `lpost_date` datetime(0) NULL DEFAULT NULL,
  `lupdated_by` bigint(20) NULL DEFAULT 0,
  `ldlu` datetime(0) NULL DEFAULT NULL,
  `lshare_to` bigint(20) NULL DEFAULT 0,
  PRIMARY KEY (`larticle_id`) USING BTREE,
  INDEX `article_title`(`larticle_title`(255)) USING BTREE,
  INDEX `type_status_date_by`(`larticle_type`, `larticle_status`, `lpost_date`, `lpost_by`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 58 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of lumonata_articles
-- ----------------------------
INSERT INTO `lumonata_articles` VALUES (1, 'One Bedroom Pool Villas', NULL, '<p>Each villa enjoys the utmost privacy, screened from outside by a Balinese wall and access from the private path-way to the entrance court yard.<br /><br />Adjacent to the living area pavilion with DVD &amp; TV cable and it is overlooking to the pool.</p>', 'publish', 'rooms', 'not_allowed', 0, 0, 'one-bedroom-pool-villas', 59, 1, '2019-03-01 00:00:00', 1, '2019-03-13 23:31:07', 0);
INSERT INTO `lumonata_articles` VALUES (2, 'Two Bedrooms Pool Villas', NULL, '<p>Private pool villa with two king size bedrooms, separate bathrooms and shower with large living area pavilion, dining are, kitchenette, a lush tropical garden surrounding the villa, mild and tranquility for two couples or family.</p>', 'publish', 'rooms', 'not_allowed', 0, 0, 'two-bedrooms-pool-villas', 60, 1, '2019-03-01 00:00:00', 1, '2019-03-10 01:13:10', 0);
INSERT INTO `lumonata_articles` VALUES (3, 'Three Bedroom Pool Villas', NULL, '<p>The villa has a two-storey, the roof pavilion with soaring ceilings with separate bathrooms, indoor bath-tub and showers.<br /><br />The pavilion with adjacent to the living area pavilion, dining area, kitchenette, the villa features a similar design, lay out and facilities, adjacent to the living area pavilion comfortable for family.</p>', 'publish', 'rooms', 'not_allowed', 0, 0, 'three-bedroom-pool-villas', 61, 1, '2019-03-01 00:00:00', 1, '2019-03-13 23:32:01', 0);
INSERT INTO `lumonata_articles` VALUES (4, 'Bidadari Celebration', NULL, '<p>Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Donec sollicitudin molestie malesuada. Vivamus suscipit tortor eget felis porttitor volutpat. Donec rutrum congue leo eget malesuada.</p>', 'publish', 'offers', 'not_allowed', 0, 0, 'bidadari-celebration', 56, 1, '2019-03-01 00:00:00', 1, '2019-03-01 11:14:41', 0);
INSERT INTO `lumonata_articles` VALUES (5, 'Bidadari Romantic', NULL, '<p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Praesent sapien massa, convallis a nec, egestas non nisi. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus.</p>', 'publish', 'offers', 'not_allowed', 0, 0, 'bidadari-romantic', 57, 1, '2019-03-01 00:00:00', 1, '2019-03-01 11:14:43', 0);
INSERT INTO `lumonata_articles` VALUES (6, 'Bidadari Experience ', NULL, '<p>Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Proin eget tortor risus. Quisque velit nisi, pretium ut lacinia in, elementum id enim.</p>', 'publish', 'offers', 'not_allowed', 0, 0, 'bidadari-experience', 58, 1, '2019-03-01 00:00:00', 1, '2019-03-01 11:14:43', 0);
INSERT INTO `lumonata_articles` VALUES (7, 'Yoga Activity', NULL, '<p>Most yoga practices focus on physical postures called &ldquo;asanas,&rdquo; breathing exercises called &ldquo;pranayama,&rdquo; and meditation to bring your body and mind together through slow, careful movements. But, there&rsquo;s more to it than that! Yoga leads to improved physical fitness, increased ability to concentrate, and decreased stress. Yoga is an activity that helps both your body and mind work a little better.</p>', 'publish', 'activities', 'not_allowed', 0, 0, 'yoga-activity', 53, 1, '2019-03-01 00:00:00', 1, '2019-03-14 01:09:10', 0);
INSERT INTO `lumonata_articles` VALUES (8, 'Cooking Class', NULL, '<p>Discover the spiced flavors of Bali&rsquo;s virtually unknown cuisine. Classes offer a fascinating introduction in to the exotic ingredients and unique culinary heritage of Bali. They provide a valuable insight into the various techniques, traditional and modern, of food preparation and the cooking style used in homes across the island.</p>', 'publish', 'activities', 'not_allowed', 0, 0, 'cooking-class', 54, 1, '2019-03-01 00:00:00', 1, '2019-03-01 14:50:03', 0);
INSERT INTO `lumonata_articles` VALUES (9, 'Children  Activity', NULL, '<p>Find indoor and outdoor games and activities that will challenge and develop your child\'s creativity, imagination, thinking skills, and social skills. Search our Activities Center for more great ideas for all ages.</p>', 'publish', 'activities', 'not_allowed', 0, 0, 'children-activity', 55, 1, '2019-03-01 00:00:00', 1, '2019-03-01 14:50:03', 0);
INSERT INTO `lumonata_articles` VALUES (10, 'The Yubi Spa', NULL, '<p>Balinese and Indonesia herbs and spices blended from traditional recipes into modern-day beauty rituals.</p>', 'publish', 'spa', 'not_allowed', 0, 0, 'the-yubi-spa', 53, 1, '2019-03-01 00:00:00', 1, '2019-03-06 20:13:14', 0);
INSERT INTO `lumonata_articles` VALUES (11, 'The Yubi Restaurant', NULL, '<p>Two storey restaurant and magnificent view of rice field from the second floor. The talented, experienced chef&rsquo;s creation International cuisine, with 45 seats, serving breakfast, lunch and dinner.</p>', 'publish', 'restaurant', 'not_allowed', 0, 0, 'the-yubi-restaurant', 52, 1, '2019-03-01 00:00:00', 1, '2019-03-01 00:00:00', 0);
INSERT INTO `lumonata_articles` VALUES (12, 'Recommended On Tripadvisor', NULL, '', 'publish', 'awards', 'not_allowed', 0, 0, 'recommended-on-tripadvisor', 47, 1, '2019-03-04 00:00:00', 1, '2019-03-05 00:06:13', 0);
INSERT INTO `lumonata_articles` VALUES (13, 'Certificate of Excellence 2017', NULL, '', 'publish', 'awards', 'not_allowed', 0, 0, 'certificate-of-excellence-2017', 47, 1, '2019-03-04 00:00:00', 1, '2019-03-04 23:44:09', 0);
INSERT INTO `lumonata_articles` VALUES (14, 'Certificate of Excellence 2016', NULL, '', 'publish', 'awards', 'not_allowed', 0, 0, 'certificate-of-excellence-2016', 48, 1, '2019-03-04 00:00:00', 1, '2019-03-04 23:44:09', 0);
INSERT INTO `lumonata_articles` VALUES (15, 'Certificate of Excellence 2015', NULL, '', 'publish', 'awards', 'not_allowed', 0, 0, 'certificate-of-excellence-2015', 49, 1, '2019-03-04 00:00:00', 1, '2019-03-04 23:44:09', 0);
INSERT INTO `lumonata_articles` VALUES (16, 'The Bidadari Luxury Villas and Spa Rated \"Excellent\" by 85 Traveller', NULL, '', 'publish', 'awards', 'not_allowed', 0, 0, 'the-bidadari-luxury-villas-and-spa-rated-excellent-by-85-traveller', 50, 1, '2019-03-04 00:00:00', 1, '2019-03-04 23:44:09', 0);
INSERT INTO `lumonata_articles` VALUES (17, 'Others', NULL, '', 'publish', 'gallery', 'not_allowed', 0, 0, 'others', 42, 1, '2019-03-08 00:00:00', 1, '2019-03-08 00:00:00', 0);
INSERT INTO `lumonata_articles` VALUES (18, 'Spa', NULL, '', 'publish', 'gallery', 'not_allowed', 0, 0, 'spa', 41, 1, '2019-03-08 00:00:00', 1, '2019-03-08 00:00:00', 0);
INSERT INTO `lumonata_articles` VALUES (19, 'Restaurant', NULL, '', 'publish', 'gallery', 'not_allowed', 0, 0, 'restaurant', 40, 1, '2019-03-08 00:00:00', 1, '2019-03-08 00:00:00', 0);
INSERT INTO `lumonata_articles` VALUES (20, 'Villas', NULL, '', 'publish', 'gallery', 'not_allowed', 0, 0, 'villas', 39, 1, '2019-03-08 00:00:00', 1, '2019-03-08 00:00:00', 0);
INSERT INTO `lumonata_articles` VALUES (23, 'CafÃ© Cous Cous', NULL, '', 'publish', 'locations', 'not_allowed', 0, 0, 'caf-cous-cous', 36, 1, '2019-03-08 00:00:00', 1, '2019-03-09 10:17:44', 0);
INSERT INTO `lumonata_articles` VALUES (24, 'Souphoria', NULL, '', 'publish', 'locations', 'not_allowed', 0, 0, 'souphoria', 35, 1, '2019-03-08 00:00:00', 1, '2019-03-09 10:17:39', 0);
INSERT INTO `lumonata_articles` VALUES (25, 'Bumbak Coffee', NULL, '', 'publish', 'locations', 'not_allowed', 0, 0, 'bumbak-coffee', 34, 1, '2019-03-08 00:00:00', 1, '2019-03-09 10:17:34', 0);
INSERT INTO `lumonata_articles` VALUES (26, 'Bali Protestant Church', NULL, '', 'publish', 'locations', 'not_allowed', 0, 0, 'bali-protestant-church', 33, 1, '2019-03-08 00:00:00', 1, '2019-03-09 10:17:29', 0);
INSERT INTO `lumonata_articles` VALUES (27, 'Splash Waterpark Bali', NULL, '', 'publish', 'locations', 'not_allowed', 0, 0, 'splash-waterpark-bali', 32, 1, '2019-03-08 00:00:00', 1, '2019-03-09 10:17:20', 0);
INSERT INTO `lumonata_articles` VALUES (28, 'Finns Recreation Club', NULL, '', 'publish', 'locations', 'not_allowed', 0, 0, 'finns-recreation-club', 31, 1, '2019-03-08 00:00:00', 1, '2019-03-09 10:17:15', 0);
INSERT INTO `lumonata_articles` VALUES (29, 'Strike Bowling', NULL, '', 'publish', 'locations', 'not_allowed', 0, 0, 'strike-bowling', 30, 1, '2019-03-08 00:00:00', 1, '2019-03-09 10:17:07', 0);
INSERT INTO `lumonata_articles` VALUES (30, 'MyWarung Berawa Canggu', NULL, '', 'publish', 'locations', 'not_allowed', 0, 0, 'mywarung-berawa-canggu', 29, 1, '2019-03-08 00:00:00', 1, '2019-03-09 10:16:21', 0);
INSERT INTO `lumonata_articles` VALUES (31, 'Wave House', NULL, '', 'publish', 'locations', 'not_allowed', 0, 0, 'wave-house', 28, 1, '2019-03-08 00:00:00', 1, '2019-03-09 10:16:13', 0);
INSERT INTO `lumonata_articles` VALUES (32, 'Primo Chocolate Factory', NULL, '', 'publish', 'locations', 'not_allowed', 0, 0, 'primo-chocolate-factory', 27, 1, '2019-03-08 00:00:00', 1, '2019-03-09 10:16:07', 0);
INSERT INTO `lumonata_articles` VALUES (33, 'Thai Spice', NULL, '', 'publish', 'locations', 'not_allowed', 0, 0, 'thai-spice', 26, 1, '2019-03-08 00:00:00', 1, '2019-03-09 10:16:03', 0);
INSERT INTO `lumonata_articles` VALUES (34, 'One Bean Coffee House', NULL, '', 'publish', 'locations', 'not_allowed', 0, 0, 'one-bean-coffee-house', 25, 1, '2019-03-08 00:00:00', 1, '2019-03-09 10:15:20', 0);
INSERT INTO `lumonata_articles` VALUES (35, 'Maidenlove Store', NULL, '', 'publish', 'locations', 'not_allowed', 0, 0, 'maidenlove-store', 24, 1, '2019-03-08 00:00:00', 1, '2019-03-09 10:15:15', 0);
INSERT INTO `lumonata_articles` VALUES (36, 'Canggu Plaza', NULL, '', 'publish', 'locations', 'not_allowed', 0, 0, 'canggu-plaza', 23, 1, '2019-03-08 00:00:00', 1, '2019-03-09 10:15:10', 0);
INSERT INTO `lumonata_articles` VALUES (37, 'Pantai Kayu Putih', NULL, '', 'publish', 'locations', 'not_allowed', 0, 0, 'pantai-kayu-putih', 22, 1, '2019-03-08 00:00:00', 1, '2019-03-09 10:14:29', 0);
INSERT INTO `lumonata_articles` VALUES (38, 'Pantai Perancak', NULL, '', 'publish', 'locations', 'not_allowed', 0, 0, 'pantai-perancak', 21, 1, '2019-03-08 00:00:00', 1, '2019-03-09 10:14:34', 0);
INSERT INTO `lumonata_articles` VALUES (39, 'Pantai Batu Belig', NULL, '', 'publish', 'locations', 'not_allowed', 0, 0, 'pantai-batu-belig', 20, 1, '2019-03-08 00:00:00', 1, '2019-03-09 10:14:08', 0);
INSERT INTO `lumonata_articles` VALUES (40, 'Green Ginger Noodle House', NULL, '', 'publish', 'locations', 'not_allowed', 0, 0, 'green-ginger-noodle-house', 19, 1, '2019-03-08 00:00:00', 1, '2019-03-09 10:13:55', 0);
INSERT INTO `lumonata_articles` VALUES (41, 'Contact Us for More Information', NULL, '', 'publish', 'pages', 'not_allowed', 0, 0, 'contact-us', 18, 1, '2019-03-09 14:16:29', 1, '2019-03-09 14:16:29', 0);
INSERT INTO `lumonata_articles` VALUES (42, 'FAQ', NULL, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum in eros vel lorem rhoncus cursus porttitor et lorem. Fusce est ligula, pretium eu magna eu, sagittis condimentum nulla. Nullam ut lectus nec risus tristique congue. Proin lacinia dignissim ligula, non rhoncus nisi facilisis at. Donec dapibus at libero a venenatis. Fusce vestibulum tellus sem, non laoreet erat commodo vitae. Nam suscipit dolor risus, id maximus urna faucibus ac. Integer sollicitudin vel justo a sagittis. Nunc semper dapibus ex a placerat. Cras non elit nulla. Aenean dolor sem, luctus a semper a, imperdiet vitae lacus. Vivamus imperdiet leo sit amet purus tempus, faucibus lacinia nisi placerat. Aliquam vestibulum, sem sed semper efficitur, leo nisi placerat lacus, eget tempor dolor magna in odio.</p>\r\n<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec nisi elit, elementum ac efficitur quis, semper at turpis. Ut vitae urna lorem. Donec iaculis pulvinar metus luctus feugiat. Aliquam at orci eget eros placerat ultrices. Maecenas ac lorem magna. Aliquam neque turpis, hendrerit non nunc a, suscipit porta arcu. Curabitur elementum molestie sem non varius. Phasellus molestie scelerisque dui laoreet lobortis. Aliquam justo lorem, pharetra non diam ac, rutrum porta sem. Integer tempor ac nisi id blandit. Nunc convallis nulla odio. Vivamus faucibus augue in ex mattis, id tincidunt arcu sagittis.</p>\r\n<p>Proin hendrerit, nibh ac semper malesuada, tellus massa sagittis dui, sit amet egestas nulla turpis at velit. Mauris pretium feugiat facilisis. Nulla ut felis ante. Nam imperdiet quam sed est sagittis, id viverra nisl viverra. Curabitur lectus justo, interdum ut mi nec, viverra aliquam urna. Integer quis lacinia sem. Etiam volutpat diam at nunc lobortis, ac finibus leo suscipit. Nam a nulla varius dolor blandit facilisis. Sed blandit, eros at volutpat semper, elit velit viverra mauris, eu convallis purus neque sit amet nisl. Nunc sollicitudin, purus nec pretium iaculis, mi sem vestibulum lacus, tempus placerat nisl orci mattis tellus. Mauris in augue justo. Fusce molestie felis at commodo ullamcorper. Pellentesque in porta mauris. Quisque pellentesque arcu quis ipsum semper pulvinar. Nunc interdum sem magna, a tristique sapien efficitur ac.</p>', 'publish', 'pages', 'not_allowed', 0, 0, 'faq', 17, 1, '2019-03-15 01:37:41', 1, '2019-03-15 01:37:41', 0);
INSERT INTO `lumonata_articles` VALUES (43, 'Term &amp; Conditions', NULL, '<p>Proin hendrerit, nibh ac semper malesuada, tellus massa sagittis dui, sit amet egestas nulla turpis at velit. Mauris pretium feugiat facilisis. Nulla ut felis ante. Nam imperdiet quam sed est sagittis, id viverra nisl viverra. Curabitur lectus justo, interdum ut mi nec, viverra aliquam urna. Integer quis lacinia sem. Etiam volutpat diam at nunc lobortis, ac finibus leo suscipit. Nam a nulla varius dolor blandit facilisis. Sed blandit, eros at volutpat semper, elit velit viverra mauris, eu convallis purus neque sit amet nisl. Nunc sollicitudin, purus nec pretium iaculis, mi sem vestibulum lacus, tempus placerat nisl orci mattis tellus. Mauris in augue justo. Fusce molestie felis at commodo ullamcorper. Pellentesque in porta mauris. Quisque pellentesque arcu quis ipsum semper pulvinar. Nunc interdum sem magna, a tristique sapien efficitur ac.</p>\r\n<p>Aenean sollicitudin gravida vestibulum. Aliquam vel libero non nunc sodales bibendum. Duis accumsan quam eu eros suscipit, a vulputate nibh pharetra. Etiam tempus facilisis diam, non aliquam est varius quis. Nam a metus in risus finibus rutrum. Donec finibus, orci et efficitur ultrices, libero nisl tristique ligula, malesuada condimentum metus odio in libero. Donec non urna vel augue imperdiet volutpat. Pellentesque quis molestie lorem. Nulla volutpat, massa eu tincidunt posuere, metus eros dapibus est, vel efficitur neque ligula eu dui. Duis eget ligula quis urna vehicula accumsan.</p>\r\n<p>Fusce vestibulum eros ac facilisis rhoncus. In hac habitasse platea dictumst. Morbi nec ante dolor. Duis ante nisi, placerat quis turpis quis, venenatis hendrerit elit. Cras ac ultricies risus. Sed convallis maximus velit eget congue. Maecenas ultricies porttitor dui, eu commodo libero varius at. In pharetra, libero vel congue posuere, dui mi maximus quam, non pretium leo orci ut risus.</p>', 'publish', 'pages', 'not_allowed', 0, 0, 'term-conditions', 16, 1, '2019-03-09 13:54:58', 1, '2019-03-09 13:54:58', 0);
INSERT INTO `lumonata_articles` VALUES (44, 'Free shuttle service to Seminyak and Canggu / Brawa', NULL, '', 'publish', 'services', 'not_allowed', 0, 0, 'free-shuttle-service-to-seminyak-and-canggu-brawa', 2, 1, '2019-03-10 00:00:00', 1, '2019-03-10 15:41:51', 0);
INSERT INTO `lumonata_articles` VALUES (45, 'BBQ', NULL, '', 'publish', 'services', 'not_allowed', 0, 0, 'bbq', 13, 1, '2019-03-10 00:00:00', 1, '2019-03-10 16:17:34', 0);
INSERT INTO `lumonata_articles` VALUES (46, 'Cooking Class', NULL, '', 'publish', 'services', 'not_allowed', 0, 0, 'cooking-class', 12, 1, '2019-03-10 00:00:00', 1, '2019-03-10 15:46:01', 0);
INSERT INTO `lumonata_articles` VALUES (47, 'Floating Breakfast', NULL, '', 'publish', 'services', 'not_allowed', 0, 0, 'floating-breakfast', 11, 1, '2019-03-10 00:00:00', 1, '2019-03-10 15:45:41', 0);
INSERT INTO `lumonata_articles` VALUES (48, 'In villa dinning', NULL, '', 'publish', 'services', 'not_allowed', 0, 0, 'in-villa-dinning', 10, 1, '2019-03-10 00:00:00', 1, '2019-03-10 15:45:24', 0);
INSERT INTO `lumonata_articles` VALUES (49, 'Currency exchange', NULL, '', 'publish', 'services', 'not_allowed', 0, 0, 'currency-exchange', 9, 1, '2019-03-10 00:00:00', 1, '2019-03-10 15:45:03', 0);
INSERT INTO `lumonata_articles` VALUES (50, 'Air port transfers', NULL, '', 'publish', 'services', 'not_allowed', 0, 0, 'air-port-transfers', 8, 1, '2019-03-10 00:00:00', 1, '2019-03-12 10:54:16', 0);
INSERT INTO `lumonata_articles` VALUES (51, 'Internet facilities', NULL, '', 'publish', 'services', 'not_allowed', 0, 0, 'internet-facilities', 7, 1, '2019-03-10 00:00:00', 1, '2019-03-10 15:44:30', 0);
INSERT INTO `lumonata_articles` VALUES (52, 'Car rental', NULL, '', 'publish', 'services', 'not_allowed', 0, 0, 'car-rental', 6, 1, '2019-03-10 00:00:00', 1, '2019-03-10 15:44:15', 0);
INSERT INTO `lumonata_articles` VALUES (53, 'Tour arrangement', NULL, '', 'publish', 'services', 'not_allowed', 0, 0, 'tour-arrangement', 5, 1, '2019-03-10 00:00:00', 1, '2019-03-10 15:42:51', 0);
INSERT INTO `lumonata_articles` VALUES (54, 'Doctor on call', NULL, '', 'publish', 'services', 'not_allowed', 0, 0, 'doctor-on-call', 4, 1, '2019-03-10 00:00:00', 1, '2019-03-10 15:42:34', 0);
INSERT INTO `lumonata_articles` VALUES (55, 'Laundry and dry cleaning service', NULL, '', 'publish', 'services', 'not_allowed', 0, 0, 'laundry-and-dry-cleaning-service', 3, 1, '2019-03-10 00:00:00', 1, '2019-03-10 15:42:14', 0);
INSERT INTO `lumonata_articles` VALUES (56, 'Amazing place for the price', NULL, '<p>We had an amazing stay at the Bidadari. Our villa was immaculate and we spent hours of every day in the pool or around it relaxing. The staff are the backbone of this place and we&rsquo;re all so helpful, they would go above and beyond to help you, they even remembered our names after</p>', 'publish', 'reviews', 'not_allowed', 0, 0, 'amazing-place-for-the-price', 2, 1, '2019-03-14 00:00:00', 1, '2019-03-14 16:15:58', 0);
INSERT INTO `lumonata_articles` VALUES (57, 'Favourite place in bali', NULL, '<p>Thankyou Bidadari villas staff!! We spent a big chunk of time staying here and absolutely loved it. It was my pregnant wife, me and my two year old. We loved every minute of it. Staff were incredible including management. We felt looked after and left feeling like family. The villa was quiet and</p>', 'publish', 'reviews', 'not_allowed', 0, 0, 'favourite-place-in-bali', 1, 1, '2019-03-14 00:00:00', 1, '2019-03-14 16:16:38', 0);

-- ----------------------------
-- Table structure for lumonata_attachment
-- ----------------------------
DROP TABLE IF EXISTS `lumonata_attachment`;
CREATE TABLE `lumonata_attachment`  (
  `lattach_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `larticle_id` bigint(20) NOT NULL,
  `lattach_loc` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `lattach_loc_thumb` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `lattach_loc_medium` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `lattach_loc_large` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `lapp_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ltitle` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `lcontent` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `lalt_text` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `lcaption` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `mime_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `lorder` bigint(20) NULL DEFAULT 1,
  `upload_date` datetime(0) NULL DEFAULT NULL,
  `date_last_update` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`lattach_id`) USING BTREE,
  INDEX `article_id`(`larticle_id`) USING BTREE,
  INDEX `attachment_title`(`ltitle`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 82 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of lumonata_attachment
-- ----------------------------
INSERT INTO `lumonata_attachment` VALUES (1, 1, '/lumonata-content/files/201903/room-type-1-1551409146.jpg', '/lumonata-content/files/201903/room-type-1-1551409146-thumbnail.jpg', '/lumonata-content/files/201903/room-type-1-1551409146-medium.jpg', '/lumonata-content/files/201903/room-type-1-1551409146-large.jpg', 'featured-image-rooms', 'room-type-1-1551409146', '', '', '', 'image/jpeg', 84, '2019-03-01 10:59:07', '2019-03-01 10:59:07');
INSERT INTO `lumonata_attachment` VALUES (2, 1, '/lumonata-content/files/201903/room-gallery-1-1551409159.jpg', '/lumonata-content/files/201903/room-gallery-1-1551409159-thumbnail.jpg', '/lumonata-content/files/201903/room-gallery-1-1551409159-medium.jpg', '/lumonata-content/files/201903/room-gallery-1-1551409159-large.jpg', 'gallery-rooms', 'room-gallery-1.jpg', '', '', '', 'image/jpeg', 83, '2019-03-01 10:59:20', '2019-03-01 10:59:20');
INSERT INTO `lumonata_attachment` VALUES (3, 1, '/lumonata-content/files/201903/room-gallery-2-1551409160.jpg', '/lumonata-content/files/201903/room-gallery-2-1551409160-thumbnail.jpg', '/lumonata-content/files/201903/room-gallery-2-1551409160-medium.jpg', '/lumonata-content/files/201903/room-gallery-2-1551409160-large.jpg', 'gallery-rooms', 'room-gallery-2.jpg', '', '', '', 'image/jpeg', 82, '2019-03-01 10:59:20', '2019-03-01 10:59:20');
INSERT INTO `lumonata_attachment` VALUES (4, 1, '/lumonata-content/files/201903/room-gallery-3-1551409160.jpg', '/lumonata-content/files/201903/room-gallery-3-1551409160-thumbnail.jpg', '/lumonata-content/files/201903/room-gallery-3-1551409160-medium.jpg', '/lumonata-content/files/201903/room-gallery-3-1551409160-large.jpg', 'gallery-rooms', 'room-gallery-3.jpg', '', '', '', 'image/jpeg', 81, '2019-03-01 10:59:20', '2019-03-01 10:59:20');
INSERT INTO `lumonata_attachment` VALUES (5, 1, '/lumonata-content/files/201903/room-gallery-4-1551409160.jpg', '/lumonata-content/files/201903/room-gallery-4-1551409160-thumbnail.jpg', '/lumonata-content/files/201903/room-gallery-4-1551409160-medium.jpg', '/lumonata-content/files/201903/room-gallery-4-1551409160-large.jpg', 'gallery-rooms', 'room-gallery-4.jpg', '', '', '', 'image/jpeg', 80, '2019-03-01 10:59:21', '2019-03-01 10:59:21');
INSERT INTO `lumonata_attachment` VALUES (11, 2, '/lumonata-content/files/201903/room-type-2-1551409608.jpg', '/lumonata-content/files/201903/room-type-2-1551409608-thumbnail.jpg', '/lumonata-content/files/201903/room-type-2-1551409608-medium.jpg', '/lumonata-content/files/201903/room-type-2-1551409608-large.jpg', 'featured-image-rooms', 'room-type-2-1551409608', '', '', '', 'image/jpeg', 74, '2019-03-01 11:06:48', '2019-03-01 11:06:48');
INSERT INTO `lumonata_attachment` VALUES (12, 2, '/lumonata-content/files/201903/room-gallery-1-1551409617.jpg', '/lumonata-content/files/201903/room-gallery-1-1551409617-thumbnail.jpg', '/lumonata-content/files/201903/room-gallery-1-1551409617-medium.jpg', '/lumonata-content/files/201903/room-gallery-1-1551409617-large.jpg', 'gallery-rooms', 'room-gallery-1.jpg', '', '', '', 'image/jpeg', 33, '2019-03-01 11:06:57', '2019-03-01 11:06:57');
INSERT INTO `lumonata_attachment` VALUES (13, 2, '/lumonata-content/files/201903/room-gallery-2-1551409617.jpg', '/lumonata-content/files/201903/room-gallery-2-1551409617-thumbnail.jpg', '/lumonata-content/files/201903/room-gallery-2-1551409617-medium.jpg', '/lumonata-content/files/201903/room-gallery-2-1551409617-large.jpg', 'gallery-rooms', 'room-gallery-2.jpg', '', '', '', 'image/jpeg', 31, '2019-03-01 11:06:58', '2019-03-01 11:06:58');
INSERT INTO `lumonata_attachment` VALUES (14, 2, '/lumonata-content/files/201903/room-gallery-3-1551409618.jpg', '/lumonata-content/files/201903/room-gallery-3-1551409618-thumbnail.jpg', '/lumonata-content/files/201903/room-gallery-3-1551409618-medium.jpg', '/lumonata-content/files/201903/room-gallery-3-1551409618-large.jpg', 'gallery-rooms', 'room-gallery-3.jpg', '', '', '', 'image/jpeg', 34, '2019-03-01 11:06:58', '2019-03-01 11:06:58');
INSERT INTO `lumonata_attachment` VALUES (15, 2, '/lumonata-content/files/201903/room-gallery-4-1551409618.jpg', '/lumonata-content/files/201903/room-gallery-4-1551409618-thumbnail.jpg', '/lumonata-content/files/201903/room-gallery-4-1551409618-medium.jpg', '/lumonata-content/files/201903/room-gallery-4-1551409618-large.jpg', 'gallery-rooms', 'room-gallery-4.jpg', '', '', '', 'image/jpeg', 32, '2019-03-01 11:06:58', '2019-03-01 11:06:58');
INSERT INTO `lumonata_attachment` VALUES (16, 3, '/lumonata-content/files/201903/room-type-3-1551409682.jpg', '/lumonata-content/files/201903/room-type-3-1551409682-thumbnail.jpg', '/lumonata-content/files/201903/room-type-3-1551409682-medium.jpg', '/lumonata-content/files/201903/room-type-3-1551409682-large.jpg', 'featured-image-rooms', 'room-type-3-1551409682', '', '', '', 'image/jpeg', 69, '2019-03-01 11:08:02', '2019-03-01 11:08:02');
INSERT INTO `lumonata_attachment` VALUES (17, 3, '/lumonata-content/files/201903/room-gallery-1-1551409689.jpg', '/lumonata-content/files/201903/room-gallery-1-1551409689-thumbnail.jpg', '/lumonata-content/files/201903/room-gallery-1-1551409689-medium.jpg', '/lumonata-content/files/201903/room-gallery-1-1551409689-large.jpg', 'gallery-rooms', 'room-gallery-1.jpg', '', '', '', 'image/jpeg', 31, '2019-03-01 11:08:09', '2019-03-01 11:08:09');
INSERT INTO `lumonata_attachment` VALUES (18, 3, '/lumonata-content/files/201903/room-gallery-2-1551409689.jpg', '/lumonata-content/files/201903/room-gallery-2-1551409689-thumbnail.jpg', '/lumonata-content/files/201903/room-gallery-2-1551409689-medium.jpg', '/lumonata-content/files/201903/room-gallery-2-1551409689-large.jpg', 'gallery-rooms', 'room-gallery-2.jpg', '', '', '', 'image/jpeg', 32, '2019-03-01 11:08:10', '2019-03-01 11:08:10');
INSERT INTO `lumonata_attachment` VALUES (19, 3, '/lumonata-content/files/201903/room-gallery-3-1551409690.jpg', '/lumonata-content/files/201903/room-gallery-3-1551409690-thumbnail.jpg', '/lumonata-content/files/201903/room-gallery-3-1551409690-medium.jpg', '/lumonata-content/files/201903/room-gallery-3-1551409690-large.jpg', 'gallery-rooms', 'room-gallery-3.jpg', '', '', '', 'image/jpeg', 34, '2019-03-01 11:08:10', '2019-03-01 11:08:10');
INSERT INTO `lumonata_attachment` VALUES (20, 3, '/lumonata-content/files/201903/room-gallery-4-1551409690.jpg', '/lumonata-content/files/201903/room-gallery-4-1551409690-thumbnail.jpg', '/lumonata-content/files/201903/room-gallery-4-1551409690-medium.jpg', '/lumonata-content/files/201903/room-gallery-4-1551409690-large.jpg', 'gallery-rooms', 'room-gallery-4.jpg', '', '', '', 'image/jpeg', 33, '2019-03-01 11:08:10', '2019-03-01 11:08:10');
INSERT INTO `lumonata_attachment` VALUES (21, 4, '/lumonata-content/files/201903/offers-1-1551409972.jpg', '/lumonata-content/files/201903/offers-1-1551409972-thumbnail.jpg', '/lumonata-content/files/201903/offers-1-1551409972-medium.jpg', '/lumonata-content/files/201903/offers-1-1551409972-large.jpg', 'featured-image-offers', 'offers-1-1551409972', '', '', '', 'image/jpeg', 64, '2019-03-01 11:12:52', '2019-03-01 11:12:52');
INSERT INTO `lumonata_attachment` VALUES (22, 5, '/lumonata-content/files/201903/offers-2-1551410029.jpg', '/lumonata-content/files/201903/offers-2-1551410029-thumbnail.jpg', '/lumonata-content/files/201903/offers-2-1551410029-medium.jpg', '/lumonata-content/files/201903/offers-2-1551410029-large.jpg', 'featured-image-offers', 'offers-2-1551410029', '', '', '', 'image/jpeg', 63, '2019-03-01 11:13:49', '2019-03-01 11:13:49');
INSERT INTO `lumonata_attachment` VALUES (23, 6, '/lumonata-content/files/201903/offers-3-1551410071.jpg', '/lumonata-content/files/201903/offers-3-1551410071-thumbnail.jpg', '/lumonata-content/files/201903/offers-3-1551410071-medium.jpg', '/lumonata-content/files/201903/offers-3-1551410071-large.jpg', 'featured-image-offers', 'offers-3-1551410071', '', '', '', 'image/jpeg', 62, '2019-03-01 11:14:31', '2019-03-01 11:14:31');
INSERT INTO `lumonata_attachment` VALUES (24, 7, '/lumonata-content/files/201903/activity-1551410188.jpg', '/lumonata-content/files/201903/activity-1551410188-thumbnail.jpg', '/lumonata-content/files/201903/activity-1551410188-medium.jpg', '/lumonata-content/files/201903/activity-1551410188-large.jpg', 'featured-image-activities', 'activity-1551410188', '', '', '', 'image/jpeg', 61, '2019-03-01 11:16:28', '2019-03-01 11:16:28');
INSERT INTO `lumonata_attachment` VALUES (25, 8, '/lumonata-content/files/201903/activity-2-1551410229.jpg', '/lumonata-content/files/201903/activity-2-1551410229-thumbnail.jpg', '/lumonata-content/files/201903/activity-2-1551410229-medium.jpg', '/lumonata-content/files/201903/activity-2-1551410229-large.jpg', 'featured-image-activities', 'activity-2-1551410229', '', '', '', 'image/jpeg', 60, '2019-03-01 11:17:10', '2019-03-01 11:17:10');
INSERT INTO `lumonata_attachment` VALUES (26, 9, '/lumonata-content/files/201903/activity-3-1551410259.jpg', '/lumonata-content/files/201903/activity-3-1551410259-thumbnail.jpg', '/lumonata-content/files/201903/activity-3-1551410259-medium.jpg', '/lumonata-content/files/201903/activity-3-1551410259-large.jpg', 'featured-image-activities', 'activity-3-1551410259', '', '', '', 'image/jpeg', 59, '2019-03-01 11:17:39', '2019-03-01 11:17:39');
INSERT INTO `lumonata_attachment` VALUES (27, 10, '/lumonata-content/files/201903/spa-hero-1551423173.jpg', '/lumonata-content/files/201903/spa-hero-1551423173-thumbnail.jpg', '/lumonata-content/files/201903/spa-hero-1551423173-medium.jpg', '/lumonata-content/files/201903/spa-hero-1551423173-large.jpg', 'gallery-spa', 'spa-hero.jpg', '', '', '', 'image/jpeg', 58, '2019-03-01 14:52:54', '2019-03-01 14:52:54');
INSERT INTO `lumonata_attachment` VALUES (28, 10, '/lumonata-content/files/201903/spa-hero-2-1551423174.jpg', '/lumonata-content/files/201903/spa-hero-2-1551423174-thumbnail.jpg', '/lumonata-content/files/201903/spa-hero-2-1551423174-medium.jpg', '/lumonata-content/files/201903/spa-hero-2-1551423174-large.jpg', 'gallery-spa', 'spa-hero-2.jpg', '', '', '', 'image/jpeg', 57, '2019-03-01 14:52:54', '2019-03-01 14:52:54');
INSERT INTO `lumonata_attachment` VALUES (29, 10, '/lumonata-content/files/201903/spa-hero-3-1551423175.jpg', '/lumonata-content/files/201903/spa-hero-3-1551423175-thumbnail.jpg', '/lumonata-content/files/201903/spa-hero-3-1551423175-medium.jpg', '/lumonata-content/files/201903/spa-hero-3-1551423175-large.jpg', 'gallery-spa', 'spa-hero-3.jpg', '', '', '', 'image/jpeg', 56, '2019-03-01 14:52:55', '2019-03-01 14:52:55');
INSERT INTO `lumonata_attachment` VALUES (30, 11, '/lumonata-content/files/201903/restaurant-hero-1551423348.jpg', '/lumonata-content/files/201903/restaurant-hero-1551423348-thumbnail.jpg', '/lumonata-content/files/201903/restaurant-hero-1551423348-medium.jpg', '/lumonata-content/files/201903/restaurant-hero-1551423348-large.jpg', 'gallery-restaurant', 'restaurant-hero.jpg', '', '', '', 'image/jpeg', 55, '2019-03-01 14:55:49', '2019-03-01 14:55:49');
INSERT INTO `lumonata_attachment` VALUES (31, 11, '/lumonata-content/files/201903/restaurant-hero-2-1551423349.jpg', '/lumonata-content/files/201903/restaurant-hero-2-1551423349-thumbnail.jpg', '/lumonata-content/files/201903/restaurant-hero-2-1551423349-medium.jpg', '/lumonata-content/files/201903/restaurant-hero-2-1551423349-large.jpg', 'gallery-restaurant', 'restaurant-hero-2.jpg', '', '', '', 'image/jpeg', 54, '2019-03-01 14:55:50', '2019-03-01 14:55:50');
INSERT INTO `lumonata_attachment` VALUES (32, 11, '/lumonata-content/files/201903/restaurant-hero-3-1551423350.jpg', '/lumonata-content/files/201903/restaurant-hero-3-1551423350-thumbnail.jpg', '/lumonata-content/files/201903/restaurant-hero-3-1551423350-medium.jpg', '/lumonata-content/files/201903/restaurant-hero-3-1551423350-large.jpg', 'gallery-restaurant', 'restaurant-hero-3.jpg', '', '', '', 'image/jpeg', 53, '2019-03-01 14:55:50', '2019-03-01 14:55:50');
INSERT INTO `lumonata_attachment` VALUES (33, 17, '/lumonata-content/files/201903/gallery-6-1552031641.jpg', '/lumonata-content/files/201903/gallery-6-1552031641-thumbnail.jpg', '/lumonata-content/files/201903/gallery-6-1552031641-medium.jpg', '/lumonata-content/files/201903/gallery-6-1552031641-large.jpg', 'gallery-gallery', 'gallery-6.jpg', '', '', '', 'image/jpeg', 50, '2019-03-08 15:54:03', '2019-03-08 15:54:03');
INSERT INTO `lumonata_attachment` VALUES (34, 17, '/lumonata-content/files/201903/gallery-5-1552031643.jpg', '/lumonata-content/files/201903/gallery-5-1552031643-thumbnail.jpg', '/lumonata-content/files/201903/gallery-5-1552031643-medium.jpg', '/lumonata-content/files/201903/gallery-5-1552031643-large.jpg', 'gallery-gallery', 'gallery-5.jpg', '', '', '', 'image/jpeg', 49, '2019-03-08 15:54:05', '2019-03-08 15:54:05');
INSERT INTO `lumonata_attachment` VALUES (35, 18, '/lumonata-content/files/201903/gallery-1-1552031669.jpg', '/lumonata-content/files/201903/gallery-1-1552031669-thumbnail.jpg', '/lumonata-content/files/201903/gallery-1-1552031669-medium.jpg', '/lumonata-content/files/201903/gallery-1-1552031669-large.jpg', 'gallery-gallery', 'gallery-1.jpg', '', '', '', 'image/jpeg', 48, '2019-03-08 15:54:32', '2019-03-08 15:54:32');
INSERT INTO `lumonata_attachment` VALUES (36, 18, '/lumonata-content/files/201903/gallery-2-1552031672.jpg', '/lumonata-content/files/201903/gallery-2-1552031672-thumbnail.jpg', '/lumonata-content/files/201903/gallery-2-1552031672-medium.jpg', '/lumonata-content/files/201903/gallery-2-1552031672-large.jpg', 'gallery-gallery', 'gallery-2.jpg', '', '', '', 'image/jpeg', 47, '2019-03-08 15:54:34', '2019-03-08 15:54:34');
INSERT INTO `lumonata_attachment` VALUES (37, 18, '/lumonata-content/files/201903/gallery-3-1552031674.jpg', '/lumonata-content/files/201903/gallery-3-1552031674-thumbnail.jpg', '/lumonata-content/files/201903/gallery-3-1552031674-medium.jpg', '/lumonata-content/files/201903/gallery-3-1552031674-large.jpg', 'gallery-gallery', 'gallery-3.jpg', '', '', '', 'image/jpeg', 46, '2019-03-08 15:54:36', '2019-03-08 15:54:36');
INSERT INTO `lumonata_attachment` VALUES (38, 18, '/lumonata-content/files/201903/gallery-4-1552031676.jpg', '/lumonata-content/files/201903/gallery-4-1552031676-thumbnail.jpg', '/lumonata-content/files/201903/gallery-4-1552031676-medium.jpg', '/lumonata-content/files/201903/gallery-4-1552031676-large.jpg', 'gallery-gallery', 'gallery-4.jpg', '', '', '', 'image/jpeg', 45, '2019-03-08 15:54:38', '2019-03-08 15:54:38');
INSERT INTO `lumonata_attachment` VALUES (39, 19, '/lumonata-content/files/201903/gallery-14-1552031703.jpg', '/lumonata-content/files/201903/gallery-14-1552031703-thumbnail.jpg', '/lumonata-content/files/201903/gallery-14-1552031703-medium.jpg', '/lumonata-content/files/201903/gallery-14-1552031703-large.jpg', 'gallery-gallery', 'gallery-14.jpg', '', '', '', 'image/jpeg', 44, '2019-03-08 15:55:05', '2019-03-08 15:55:05');
INSERT INTO `lumonata_attachment` VALUES (40, 19, '/lumonata-content/files/201903/gallery-15-1552031706.jpg', '/lumonata-content/files/201903/gallery-15-1552031706-thumbnail.jpg', '/lumonata-content/files/201903/gallery-15-1552031706-medium.jpg', '/lumonata-content/files/201903/gallery-15-1552031706-large.jpg', 'gallery-gallery', 'gallery-15.jpg', '', '', '', 'image/jpeg', 43, '2019-03-08 15:55:08', '2019-03-08 15:55:08');
INSERT INTO `lumonata_attachment` VALUES (41, 20, '/lumonata-content/files/201903/gallery-8-1552031777.jpg', '/lumonata-content/files/201903/gallery-8-1552031777-thumbnail.jpg', '/lumonata-content/files/201903/gallery-8-1552031777-medium.jpg', '/lumonata-content/files/201903/gallery-8-1552031777-large.jpg', 'gallery-gallery', 'gallery-8.jpg', '', '', '', 'image/jpeg', 42, '2019-03-08 15:56:19', '2019-03-08 15:56:19');
INSERT INTO `lumonata_attachment` VALUES (42, 20, '/lumonata-content/files/201903/gallery-7-1552031779.jpg', '/lumonata-content/files/201903/gallery-7-1552031779-thumbnail.jpg', '/lumonata-content/files/201903/gallery-7-1552031779-medium.jpg', '/lumonata-content/files/201903/gallery-7-1552031779-large.jpg', 'gallery-gallery', 'gallery-7.jpg', '', '', '', 'image/jpeg', 41, '2019-03-08 15:56:21', '2019-03-08 15:56:21');
INSERT INTO `lumonata_attachment` VALUES (43, 20, '/lumonata-content/files/201903/gallery-9-1552031781.jpg', '/lumonata-content/files/201903/gallery-9-1552031781-thumbnail.jpg', '/lumonata-content/files/201903/gallery-9-1552031781-medium.jpg', '/lumonata-content/files/201903/gallery-9-1552031781-large.jpg', 'gallery-gallery', 'gallery-9.jpg', '', '', '', 'image/jpeg', 40, '2019-03-08 15:56:24', '2019-03-08 15:56:24');
INSERT INTO `lumonata_attachment` VALUES (44, 20, '/lumonata-content/files/201903/gallery-10-1552031784.jpg', '/lumonata-content/files/201903/gallery-10-1552031784-thumbnail.jpg', '/lumonata-content/files/201903/gallery-10-1552031784-medium.jpg', '/lumonata-content/files/201903/gallery-10-1552031784-large.jpg', 'gallery-gallery', 'gallery-10.jpg', '', '', '', 'image/jpeg', 39, '2019-03-08 15:56:26', '2019-03-08 15:56:26');
INSERT INTO `lumonata_attachment` VALUES (45, 20, '/lumonata-content/files/201903/gallery-11-1552031786.jpg', '/lumonata-content/files/201903/gallery-11-1552031786-thumbnail.jpg', '/lumonata-content/files/201903/gallery-11-1552031786-medium.jpg', '/lumonata-content/files/201903/gallery-11-1552031786-large.jpg', 'gallery-gallery', 'gallery-11.jpg', '', '', '', 'image/jpeg', 38, '2019-03-08 15:56:29', '2019-03-08 15:56:29');
INSERT INTO `lumonata_attachment` VALUES (46, 20, '/lumonata-content/files/201903/gallery-12-1552031789.jpg', '/lumonata-content/files/201903/gallery-12-1552031789-thumbnail.jpg', '/lumonata-content/files/201903/gallery-12-1552031789-medium.jpg', '/lumonata-content/files/201903/gallery-12-1552031789-large.jpg', 'gallery-gallery', 'gallery-12.jpg', '', '', '', 'image/jpeg', 37, '2019-03-08 15:56:32', '2019-03-08 15:56:32');
INSERT INTO `lumonata_attachment` VALUES (47, 20, '/lumonata-content/files/201903/gallery-13-1552031792.jpg', '/lumonata-content/files/201903/gallery-13-1552031792-thumbnail.jpg', '/lumonata-content/files/201903/gallery-13-1552031792-medium.jpg', '/lumonata-content/files/201903/gallery-13-1552031792-large.jpg', 'gallery-gallery', 'gallery-13.jpg', '', '', '', 'image/jpeg', 36, '2019-03-08 15:56:34', '2019-03-08 15:56:34');
INSERT INTO `lumonata_attachment` VALUES (48, 41, '/lumonata-content/files/201903/contact-hero-1552110584.jpg', '/lumonata-content/files/201903/contact-hero-1552110584-thumbnail.jpg', '/lumonata-content/files/201903/contact-hero-1552110584-medium.jpg', '/lumonata-content/files/201903/contact-hero-1552110584-large.jpg', 'featured-image-pages', 'contact-hero-1552110584', '', '', '', 'image/jpeg', 35, '2019-03-09 13:49:46', '2019-03-09 13:49:46');
INSERT INTO `lumonata_attachment` VALUES (51, 7, '/lumonata-content/files/201903/9243-1552496935.jpg', '/lumonata-content/files/201903/9243-1552496935-thumbnail.jpg', '/lumonata-content/files/201903/9243-1552496935-medium.jpg', '/lumonata-content/files/201903/9243-1552496935-large.jpg', 'gallery-activities', '9243.jpg', '', '', '', 'image/jpeg', 30, '2019-03-14 01:08:57', '2019-03-14 01:08:57');
INSERT INTO `lumonata_attachment` VALUES (52, 7, '/lumonata-content/files/201903/25032-1552496937.jpg', '/lumonata-content/files/201903/25032-1552496937-thumbnail.jpg', '/lumonata-content/files/201903/25032-1552496937-medium.jpg', '/lumonata-content/files/201903/25032-1552496937-large.jpg', 'gallery-activities', '25032.jpg', '', '', '', 'image/jpeg', 27, '2019-03-14 01:08:59', '2019-03-14 01:08:59');
INSERT INTO `lumonata_attachment` VALUES (53, 7, '/lumonata-content/files/201903/o6kg9q0-1552496940.jpg', '/lumonata-content/files/201903/o6kg9q0-1552496940-thumbnail.jpg', '/lumonata-content/files/201903/o6kg9q0-1552496940-medium.jpg', '/lumonata-content/files/201903/o6kg9q0-1552496940-large.jpg', 'gallery-activities', 'O6KG9Q0.jpg', '', '', '', 'image/jpeg', 29, '2019-03-14 01:09:02', '2019-03-14 01:09:02');
INSERT INTO `lumonata_attachment` VALUES (54, 7, '/lumonata-content/files/201903/oih97w0-1552496942.jpg', '/lumonata-content/files/201903/oih97w0-1552496942-thumbnail.jpg', '/lumonata-content/files/201903/oih97w0-1552496942-medium.jpg', '/lumonata-content/files/201903/oih97w0-1552496942-large.jpg', 'gallery-activities', 'OIH97W0.jpg', '', '', '', 'image/jpeg', 28, '2019-03-14 01:09:04', '2019-03-14 01:09:04');
INSERT INTO `lumonata_attachment` VALUES (66, 11, '/lumonata-content/files/201903/resto-mask-2-1552578367.png', '', '', '', 'restaurant-masking-img-1', 'resto-mask-2-1552578367.png', '', '', '', 'image/png', 16, '2019-03-14 23:46:09', '2019-03-14 23:46:09');
INSERT INTO `lumonata_attachment` VALUES (67, 11, '/lumonata-content/files/201903/resto-mask-1552578375.png', '', '', '', 'restaurant-masking-img-2', 'resto-mask-1552578375.png', '', '', '', 'image/png', 15, '2019-03-14 23:46:19', '2019-03-14 23:46:19');
INSERT INTO `lumonata_attachment` VALUES (68, 11, '/lumonata-content/files/201903/resto-mask-3-1552578393.png', '', '', '', 'restaurant-masking-img-3', 'resto-mask-3-1552578393.png', '', '', '', 'image/png', 14, '2019-03-14 23:46:34', '2019-03-14 23:46:34');
INSERT INTO `lumonata_attachment` VALUES (69, 11, '/lumonata-content/files/201903/resto-mask-4-1552578399.png', '', '', '', 'restaurant-masking-img-4', 'resto-mask-4-1552578399.png', '', '', '', 'image/png', 13, '2019-03-14 23:46:42', '2019-03-14 23:46:42');
INSERT INTO `lumonata_attachment` VALUES (71, 10, '/lumonata-content/files/201903/spa-mask-1552578442.png', '', '', '', 'spa-masking-img-2', 'spa-mask-1552578442.png', '', '', '', 'image/png', 11, '2019-03-14 23:47:24', '2019-03-14 23:47:24');
INSERT INTO `lumonata_attachment` VALUES (72, 10, '/lumonata-content/files/201903/spa-mask-3-1552578450.png', '', '', '', 'spa-masking-img-3', 'spa-mask-3-1552578450.png', '', '', '', 'image/png', 10, '2019-03-14 23:47:30', '2019-03-14 23:47:30');
INSERT INTO `lumonata_attachment` VALUES (73, 10, '/lumonata-content/files/201903/spa-mask-4-1552578456.png', '', '', '', 'spa-masking-img-4', 'spa-mask-4-1552578456.png', '', '', '', 'image/png', 9, '2019-03-14 23:47:36', '2019-03-14 23:47:36');
INSERT INTO `lumonata_attachment` VALUES (75, 10, '/lumonata-content/files/201903/spa-mask-2-1552579427.png', '', '', '', 'spa-masking-img-1', 'spa-mask-2-1552579427.png', '', '', '', 'image/png', 7, '2019-03-15 00:03:47', '2019-03-15 00:03:47');
INSERT INTO `lumonata_attachment` VALUES (79, 1, '/lumonata-content/files/201903/detail-room-1552580297.jpg', '', '', '', 'rooms-content-image', 'detail-room-1552580297.jpg', '', '', '', 'image/jpeg', 3, '2019-03-15 00:18:18', '2019-03-15 00:18:18');
INSERT INTO `lumonata_attachment` VALUES (80, 2, '/lumonata-content/files/201903/detail-room-1552580324.jpg', '', '', '', 'rooms-content-image', 'detail-room-1552580324.jpg', '', '', '', 'image/jpeg', 2, '2019-03-15 00:18:47', '2019-03-15 00:18:47');
INSERT INTO `lumonata_attachment` VALUES (81, 3, '/lumonata-content/files/201903/detail-room-1552580335.jpg', '', '', '', 'rooms-content-image', 'detail-room-1552580335.jpg', '', '', '', 'image/jpeg', 1, '2019-03-15 00:18:58', '2019-03-15 00:18:58');

-- ----------------------------
-- Table structure for lumonata_comments
-- ----------------------------
DROP TABLE IF EXISTS `lumonata_comments`;
CREATE TABLE `lumonata_comments`  (
  `lcomment_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `larticle_id` bigint(20) NOT NULL,
  `lcomment_parent` bigint(20) NULL DEFAULT 0,
  `lcomentator_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `lcomentator_email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `lcomentator_url` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `lcomentator_ip` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `lcomment_date` datetime(0) NULL DEFAULT NULL,
  `lcomment` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `lcomment_status` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `lcomment_like` bigint(20) NULL DEFAULT 0,
  `luser_id` bigint(20) NOT NULL,
  `lcomment_type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT 'like,comment,like_comment',
  PRIMARY KEY (`lcomment_id`) USING BTREE,
  INDEX `lcomment_status`(`lcomment_status`) USING BTREE,
  INDEX `lcomment_userid`(`luser_id`) USING BTREE,
  INDEX `lcomment_type`(`lcomment_type`) USING BTREE,
  INDEX `larticle_id`(`larticle_id`) USING BTREE,
  CONSTRAINT `lumonata_comments_ibfk_1` FOREIGN KEY (`larticle_id`) REFERENCES `lumonata_articles` (`larticle_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `lumonata_comments_ibfk_2` FOREIGN KEY (`luser_id`) REFERENCES `lumonata_users` (`luser_id`) ON DELETE RESTRICT ON UPDATE NO ACTION
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for lumonata_friends_list
-- ----------------------------
DROP TABLE IF EXISTS `lumonata_friends_list`;
CREATE TABLE `lumonata_friends_list`  (
  `lfriends_list_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `luser_id` bigint(20) NOT NULL,
  `llist_name` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `lorder` bigint(20) NULL DEFAULT 1,
  PRIMARY KEY (`lfriends_list_id`) USING BTREE,
  INDEX `luser_id`(`luser_id`) USING BTREE,
  CONSTRAINT `lumonata_friends_list_ibfk_1` FOREIGN KEY (`luser_id`) REFERENCES `lumonata_users` (`luser_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for lumonata_friends_list_rel
-- ----------------------------
DROP TABLE IF EXISTS `lumonata_friends_list_rel`;
CREATE TABLE `lumonata_friends_list_rel`  (
  `lfriendship_id` bigint(20) NOT NULL,
  `lfriends_list_id` bigint(20) NOT NULL,
  PRIMARY KEY (`lfriendship_id`, `lfriends_list_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Table structure for lumonata_friendship
-- ----------------------------
DROP TABLE IF EXISTS `lumonata_friendship`;
CREATE TABLE `lumonata_friendship`  (
  `lfriendship_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `luser_id` bigint(20) NOT NULL,
  `lfriend_id` bigint(20) NOT NULL,
  `lstatus` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'connected, onrequest, pending, unfollow',
  PRIMARY KEY (`lfriendship_id`) USING BTREE,
  INDEX `luser_id`(`luser_id`) USING BTREE,
  CONSTRAINT `lumonata_friendship_ibfk_1` FOREIGN KEY (`luser_id`) REFERENCES `lumonata_users` (`luser_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for lumonata_language
-- ----------------------------
DROP TABLE IF EXISTS `lumonata_language`;
CREATE TABLE `lumonata_language`  (
  `llang_id` int(11) NOT NULL AUTO_INCREMENT,
  `llanguage` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `llanguage_code` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `lstatus` smallint(3) NOT NULL DEFAULT 0 COMMENT '0=Non Active;1=Active',
  `ldefault` smallint(3) NOT NULL COMMENT '0=Non;1=Yes',
  `lflag` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `lorder` bigint(20) NOT NULL,
  `lpost_by` bigint(20) NOT NULL,
  `lpost_date` datetime(0) NOT NULL,
  `lupdated_by` bigint(20) NOT NULL,
  `ldlu` datetime(0) NOT NULL,
  PRIMARY KEY (`llang_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for lumonata_menu
-- ----------------------------
DROP TABLE IF EXISTS `lumonata_menu`;
CREATE TABLE `lumonata_menu`  (
  `lmenu_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `lcmenu_id` bigint(20) NOT NULL,
  `lmenu_name` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `lmenu_desc` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `lmenu_price` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `lorder` bigint(20) NOT NULL DEFAULT 0,
  PRIMARY KEY (`lmenu_id`) USING BTREE,
  INDEX `lcmenu_id`(`lcmenu_id`) USING BTREE,
  CONSTRAINT `lumonata_menu_ibfk_1` FOREIGN KEY (`lcmenu_id`) REFERENCES `lumonata_menu_category` (`lcmenu_id`) ON DELETE RESTRICT ON UPDATE NO ACTION
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of lumonata_menu
-- ----------------------------
INSERT INTO `lumonata_menu` VALUES (2, 4, 'Spicy Watermelon Gazpacho', 'crab, peppers, mint', '55', 4);
INSERT INTO `lumonata_menu` VALUES (3, 4, 'Soup Buntut', 'aromatic Javanese oxtail and vegetable broth', '65', 5);
INSERT INTO `lumonata_menu` VALUES (4, 4, 'Salad of Fennel & Orange', 'olives, citrus, rocket, labneh, Mandarin sea salt', '60', 6);
INSERT INTO `lumonata_menu` VALUES (5, 4, 'Fresh Fish', 'reef fish, aromatic lemongrass, turmeric glass noodle broth', '65', 7);
INSERT INTO `lumonata_menu` VALUES (6, 4, 'Vietnamese Fresh Spring Rolls', '3 per serving prawn with nuoc cham sauce', '65', 8);
INSERT INTO `lumonata_menu` VALUES (7, 5, 'Spaghetti', 'red wine beef Bolognese, baby tomatoes, shaved parmesan', '130', 3);
INSERT INTO `lumonata_menu` VALUES (8, 5, 'Salmon Sashimi Linguine', 'lemon, capers, chilli, basil, rocket and extra virgin olive oil', '130', 2);
INSERT INTO `lumonata_menu` VALUES (9, 5, 'Fettuccine Marinara', 'of prawns, scampi, squid and mussels', '130', 1);
INSERT INTO `lumonata_menu` VALUES (10, 5, 'Angel Hair Pasta', 'with crab, peas, asparagus, cream', '140', 0);

-- ----------------------------
-- Table structure for lumonata_menu_category
-- ----------------------------
DROP TABLE IF EXISTS `lumonata_menu_category`;
CREATE TABLE `lumonata_menu_category`  (
  `lcmenu_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `larticle_id` bigint(20) NOT NULL,
  `lcmenu_parent` bigint(20) NULL DEFAULT NULL,
  `lcmenu_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `lorder` bigint(20) NULL DEFAULT 0,
  PRIMARY KEY (`lcmenu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of lumonata_menu_category
-- ----------------------------
INSERT INTO `lumonata_menu_category` VALUES (1, 11, 0, 'Lunch & Dinner Menu', 0);
INSERT INTO `lumonata_menu_category` VALUES (4, 11, 1, 'Starters & Salads', 0);
INSERT INTO `lumonata_menu_category` VALUES (5, 11, 1, 'Pasta', 1);

-- ----------------------------
-- Table structure for lumonata_meta_data
-- ----------------------------
DROP TABLE IF EXISTS `lumonata_meta_data`;
CREATE TABLE `lumonata_meta_data`  (
  `lmeta_id` int(11) NOT NULL AUTO_INCREMENT,
  `lmeta_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `lmeta_value` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `lapp_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `lapp_id` int(11) NULL DEFAULT 0,
  PRIMARY KEY (`lmeta_id`) USING BTREE,
  INDEX `meta_name`(`lmeta_name`) USING BTREE,
  INDEX `app_name`(`lapp_name`) USING BTREE,
  INDEX `app_id`(`lapp_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 382 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of lumonata_meta_data
-- ----------------------------
INSERT INTO `lumonata_meta_data` VALUES (1, 'front_theme', 'custom', 'themes', 0);
INSERT INTO `lumonata_meta_data` VALUES (2, 'admin_theme', 'default', 'themes', 0);
INSERT INTO `lumonata_meta_data` VALUES (3, 'custome_bg_color', 'FFF', 'themes', 0);
INSERT INTO `lumonata_meta_data` VALUES (4, 'active_plugins', '{\"lumonata-additional-data\":\"\\/additional\\/additional.php\",\"lumonata-meta-data\":\"\\/metadata\\/metadata.php\",\"global-setting\":\"\\/setting\\/global.php\",\"lumonata-restaurant-menus\":\"\\/menus\\/menus.php\"}', 'plugins', 0);
INSERT INTO `lumonata_meta_data` VALUES (5, 'time_zone', 'Asia/Singapore', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (6, 'site_url', 'localhost/bidadari', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (7, 'web_title', 'The Bidadari', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (8, 'smtp_server', 'localhost', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (9, 'smtp', 'bidadari.lumonatalabs.com', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (10, 'email', 'dev@lumonata.com', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (11, 'web_tagline', '', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (12, 'invitation_limit', '10', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (13, 'date_format', 'F j, Y', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (14, 'time_format', 'H:i', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (15, 'post_viewed', '30', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (16, 'rss_viewed', '15', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (17, 'rss_view_format', 'full_text', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (18, 'list_viewed', '50', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (19, 'email_format', 'html', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (20, 'text_editor', 'tiny_mce', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (21, 'thumbnail_image_size', '300:300', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (22, 'large_image_size', '1024:1024', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (23, 'medium_image_size', '700:700', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (24, 'is_allow_comment', '1', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (25, 'is_login_to_comment', '1', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (26, 'is_auto_close_comment', '0', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (27, 'days_auto_close_comment', '15', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (28, 'is_break_comment', '1', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (29, 'comment_page_displayed', 'last', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (30, 'comment_per_page', '3', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (31, 'save_changes', 'Save Changes', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (32, 'is_rewrite', 'yes', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (33, 'is_allow_post_like', '1', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (34, 'is_allow_comment_like', '1', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (35, 'alert_on_register', '1', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (36, 'alert_on_comment', '1', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (37, 'alert_on_comment_reply', '1', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (38, 'alert_on_liked_post', '1', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (39, 'alert_on_liked_comment', '1', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (40, 'web_name', 'The Bidadari', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (41, 'meta_description', '', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (42, 'meta_keywords', '', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (43, 'meta_title', 'The Bidadari Luxury Villas & Spa', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (44, 'status_viewed', '4', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (45, 'update', 'true', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (46, 'the_date_format', 'F j, Y', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (47, 'the_time_format', 'H:i', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (48, 'thumbnail_image_width', '300', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (49, 'thumbnail_image_height', '300', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (50, 'medium_image_width', '700', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (51, 'medium_image_height', '700', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (52, 'large_image_width', '1024', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (53, 'large_image_height', '1024', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (54, 'author', '{\"name\":\"Author\",\"privileges\":[\"dashboard\",\"status\",\"applications\",\"articles\",\"pages\",\"friends\",\"notifications\",\"people\"]}', 'user_privileges', 0);
INSERT INTO `lumonata_meta_data` VALUES (55, 'standard', '{\"name\":\"Standard\",\"privileges\":[\"dashboard\",\"status\",\"friends\",\"notifications\",\"people\"]}', 'user_privileges', 0);
INSERT INTO `lumonata_meta_data` VALUES (56, 'contributor', '{\"name\":\"Contributor\",\"privileges\":[\"dashboard\",\"status\",\"applications\",\"comments\",\"articles\",\"pages\",\"friends\",\"notifications\",\"people\"]}', 'user_privileges', 0);
INSERT INTO `lumonata_meta_data` VALUES (57, 'editor', '{\"name\":\"Editor\",\"privileges\":[\"dashboard\",\"status\",\"applications\",\"comments\",\"articles\",\"blogs\",\"pages\",\"categories\",\"tags\",\"friends\",\"notifications\",\"people\"]}', 'user_privileges', 0);
INSERT INTO `lumonata_meta_data` VALUES (58, 'administrator', '{\"name\":\"Administrator\",\"privileges\":[\"dashboard\",\"status\",\"global_settings\",\"menus\",\"applications\",\"plugins\",\"themes\",\"comments\",\"articles\",\"blogs\",\"media\",\"pages\",\"categories\",\"tags\",\"users\",\"friends\",\"notifications\",\"people\",\"metadata\"]}', 'user_privileges', 0);
INSERT INTO `lumonata_meta_data` VALUES (59, 'analytic_view_id', '', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (60, 'multi_language', '0', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (61, 'setting_title', 'The Bidadari Luxury Villas & Spa', 'rooms', 0);
INSERT INTO `lumonata_meta_data` VALUES (62, 'setting_description', '<p>The resort comprises of 19 Units villas with 7 units of One bedroom pool villa, 11 units of Two bedroom pool villa and 1 unit of Three bedroom pool villa.</p>', 'rooms', 0);
INSERT INTO `lumonata_meta_data` VALUES (63, 'setting_meta_title', 'Rooms - The Bidadari Luxury Villas & Spa', 'rooms', 0);
INSERT INTO `lumonata_meta_data` VALUES (64, 'setting_meta_description', '', 'rooms', 0);
INSERT INTO `lumonata_meta_data` VALUES (65, 'setting_meta_keywords', '', 'rooms', 0);
INSERT INTO `lumonata_meta_data` VALUES (66, 'setting_title', 'The Bidadari Villas & Spa Special Promotions', 'offers', 0);
INSERT INTO `lumonata_meta_data` VALUES (67, 'setting_description', '<p>Sed porttitor lectus nibh. Vivamus suscipit tortor eget felis porttitor volutpat. Nulla porttitor accumsan tincidunt. Curabitur aliquet quam id dui posuere blandit.</p>', 'offers', 0);
INSERT INTO `lumonata_meta_data` VALUES (68, 'setting_meta_title', '', 'offers', 0);
INSERT INTO `lumonata_meta_data` VALUES (69, 'setting_meta_description', '', 'offers', 0);
INSERT INTO `lumonata_meta_data` VALUES (70, 'setting_meta_keywords', '', 'offers', 0);
INSERT INTO `lumonata_meta_data` VALUES (71, 'setting_title', 'Activities  at The Bidadari Luxury Villas & Spa', 'activities', 0);
INSERT INTO `lumonata_meta_data` VALUES (72, 'setting_description', '<p>Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Cras ultricies ligula sed magna dictum porta.</p>', 'activities', 0);
INSERT INTO `lumonata_meta_data` VALUES (73, 'setting_meta_title', '', 'activities', 0);
INSERT INTO `lumonata_meta_data` VALUES (74, 'setting_meta_description', '', 'activities', 0);
INSERT INTO `lumonata_meta_data` VALUES (75, 'setting_meta_keywords', '', 'activities', 0);
INSERT INTO `lumonata_meta_data` VALUES (76, 'landing_page_section_1_title', 'The Bidadari Luxury Villas & Spa', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (77, 'landing_page_section_1_subtitle', 'Welcome To', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (78, 'landing_page_section_1_content', '<p>Tranquility surrounding, warm &amp; friendly hospitality. Located in a relaxing place of Umalas area, around 30 minutes from Ngurah Rai Bali International airport and 15 minutes away to popular tourist sport in Seminyak or Canggu-Brawa.</p>\r\n<p>The resort comprises of 19 Units villas with 7 units of One bedroom pool villa, 11 units of Two bedroom pool villa and 1 unit of Three bedroom pool villa.</p>', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (79, 'smtp_email_port', '587', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (80, 'smtp_email_address', 'developer@bidadari.lumonatalabs.com', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (81, 'smtp_email_pwd', 'x48kdtybhy0c', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (82, 'smtp_email_from', 'noreply@bidadari.lumonatalabs.com', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (83, 'r_public_key', '6LeUp5EUAAAAAI7CazgF7cfxwqkdbov956-kRFwZ', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (84, 'r_secret_key', '6LeUp5EUAAAAAEvTCY_jneHAt0IYcSLnQAMFqv9X', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (85, 'contact_info_email', 'info@thebidadarivillasandspa.com', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (86, 'contact_info_phone', '(+62 361) 847-5566', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (87, 'contact_info_desc', '<p>Located in a relaxing place of Umalas area, around 30 minutes from Ngurah Rai Bali International airport and 15 minutes away to popular tourist sport in Seminyak or Canggu- Brawa.</p>', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (88, 'social_media_facebook', '', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (89, 'social_media_instagram', '', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (90, 'g_analytic', '', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (91, 'g_tag_manager', '', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (92, 'landing_page_section_2_title', 'Room Types', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (93, 'landing_page_section_2_subtitle', 'The Bidadari Luxury Villas & Spa', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (94, 'landing_page_section_2_link_text', 'View All Room Types', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (95, 'landing_page_section_3_title', 'Special Promotions', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (96, 'landing_page_section_3_link_text', 'See All Promotions', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (97, 'landing_page_section_5_button_text', 'Learn More', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (98, 'landing_page_section_5_link_text', 'See All Activities', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (99, 'landing_page_section_6_title', 'The Bidadari Luxury Villas & Spa', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (100, 'landing_page_section_6_content', '<p>Located in Umalas, Seminyak, set amidst a tropical garden, approximately 10-minutes drive from Petitenget Beach, Seminyak and easy to go to others destination around villa.</p>', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (121, 'triple_box_image', '/lumonata-content/files/201903/about-2-1551692886.jpg', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (122, 'triple_box_image_order', '1', 'global_setting', 121);
INSERT INTO `lumonata_meta_data` VALUES (123, 'triple_box_image', '/lumonata-content/files/201903/about-1-1551692886.jpg', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (124, 'triple_box_image_order', '0', 'global_setting', 123);
INSERT INTO `lumonata_meta_data` VALUES (125, 'triple_box_image', '/lumonata-content/files/201903/about-3-1551692886.jpg', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (126, 'triple_box_image_order', '2', 'global_setting', 125);
INSERT INTO `lumonata_meta_data` VALUES (127, 'contact_info_fax', '(+62 361) 847-5566', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (128, 'contact_info_map_addrs', 'https://goo.gl/maps/2aqoFrMaEm72', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (129, 'contact_info_addrs', 'Jl. Bumbak No. 27 Banjar Anyar Kelod - Kerobokan, Kuta Utara - Bali - Indonesia', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (130, 'state_url', 'http://localhost/bidadari/lumonata-admin/?state=global_settings&tab=landing', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (131, 'contact_info_title', 'Contact Us For More Information', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (132, 'contact_info_wa', '(+62) 8786-0634-896', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (133, 'contact_info_wa_link', 'https://api.whatsapp.com/send?phone=087860634896', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (134, 'footer_text_title', 'The Bidadari Luxury Villas & Spa', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (135, 'footer_text_content', '<p>Located in Umalas, Seminyak, set amidst a tropical garden, approximately 10-minutes drive from Petitenget Beach, Seminyak. It features 19 villas with a private pavilion and swimming pool.</p>', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (142, 'homepage_slideshow', '/lumonata-content/files/201903/home-hero-1551713612.jpg', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (143, 'homepage_slideshow_order', '0', 'global_setting', 142);
INSERT INTO `lumonata_meta_data` VALUES (144, 'homepage_slideshow', '/lumonata-content/files/201903/home-hero-2-1551713613.jpg', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (145, 'homepage_slideshow_order', '1', 'global_setting', 144);
INSERT INTO `lumonata_meta_data` VALUES (146, 'homepage_slideshow', '/lumonata-content/files/201903/home-hero-3-1551713615.jpg', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (147, 'homepage_slideshow_order', '2', 'global_setting', 146);
INSERT INTO `lumonata_meta_data` VALUES (148, 'menu_set', '{\"header\":\"Header\",\"footer\":\"Footer\"}', 'menus', 0);
INSERT INTO `lumonata_meta_data` VALUES (159, 'menu_items_header', '[{\"id\":0,\"label\":\"Rooms\",\"target\":\"_self\",\"link\":\"rooms\\/\",\"permalink\":\"rooms\\/\"},{\"id\":1,\"label\":\"Activities\",\"target\":\"_self\",\"link\":\"activities\\/\",\"permalink\":\"activities\\/\"},{\"id\":2,\"label\":\"Offers\",\"target\":\"_self\",\"link\":\"offers\\/\",\"permalink\":\"offers\\/\"},{\"id\":3,\"label\":\"Restaurant\",\"target\":\"_self\",\"link\":\"\\/?page_id=11\",\"permalink\":\"restaurant\\/the-yubi-restaurant.html\"},{\"id\":4,\"label\":\"Spa\",\"target\":\"_self\",\"link\":\"\\/?page_id=10\",\"permalink\":\"spa\\/the-yubi-spa.html\"},{\"id\":5,\"label\":\"Gallery\",\"target\":\"_self\",\"link\":\"gallery\\/\",\"permalink\":\"gallery\\/\"},{\"id\":6,\"label\":\"Locations\",\"target\":\"_self\",\"link\":\"locations\\/\",\"permalink\":\"locations\\/\"},{\"id\":7,\"label\":\"Services & Facilities\",\"target\":\"_self\",\"link\":\"services\\/\",\"permalink\":\"services\\/\"}]', 'menus', 0);
INSERT INTO `lumonata_meta_data` VALUES (160, 'menu_order_header', '[{\"id\":\"0\"},{\"id\":\"7\"},{\"id\":\"3\"},{\"id\":\"4\"},{\"id\":\"1\"},{\"id\":\"5\"},{\"id\":\"6\"},{\"id\":\"2\"}]', 'menus', 0);
INSERT INTO `lumonata_meta_data` VALUES (163, 'setting_hero_image', '/lumonata-content/files/201903/room-type-3-1551845069.jpg', 'rooms', 0);
INSERT INTO `lumonata_meta_data` VALUES (166, 'setting_hero_image', '/lumonata-content/files/201903/offers-hero-1551845986.jpg', 'offers', 0);
INSERT INTO `lumonata_meta_data` VALUES (171, 'setting_hero_image', '/lumonata-content/files/201903/gallery-6-1551846248.jpg', 'activities', 0);
INSERT INTO `lumonata_meta_data` VALUES (172, 'setting_hero_image', '/lumonata-content/files/201903/gallery-hero-1552033947.jpg', 'gallery', 0);
INSERT INTO `lumonata_meta_data` VALUES (173, 'setting_title', 'The Bidadari Villas & Spa Photo Gallery', 'gallery', 0);
INSERT INTO `lumonata_meta_data` VALUES (174, 'setting_description', '', 'gallery', 0);
INSERT INTO `lumonata_meta_data` VALUES (175, 'setting_meta_title', '', 'gallery', 0);
INSERT INTO `lumonata_meta_data` VALUES (176, 'setting_meta_description', '', 'gallery', 0);
INSERT INTO `lumonata_meta_data` VALUES (177, 'setting_meta_keywords', '', 'gallery', 0);
INSERT INTO `lumonata_meta_data` VALUES (178, 'menu_items_footer', '[{\"id\":0,\"label\":\"Contact\",\"target\":\"_self\",\"link\":\"\\/?page_id=41\",\"permalink\":\"contact-us.html\"},{\"id\":1,\"label\":\"FAQ\",\"target\":\"_self\",\"link\":\"\\/?page_id=42\",\"permalink\":\"faq.html\"},{\"id\":2,\"label\":\"Term & Conditions\",\"target\":\"_self\",\"link\":\"\\/?page_id=43\",\"permalink\":\"term-conditions.html\"}]', 'menus', 0);
INSERT INTO `lumonata_meta_data` VALUES (179, 'menu_order_footer', '[{\"id\":0},{\"id\":1},{\"id\":2}]', 'menus', 0);
INSERT INTO `lumonata_meta_data` VALUES (180, 'setting_hero_image', '/lumonata-content/files/201903/service-hero-1552151899.jpg', 'services', 0);
INSERT INTO `lumonata_meta_data` VALUES (181, 'setting_title', 'The Bidadari Villas & Spa Hospitality Services', 'services', 0);
INSERT INTO `lumonata_meta_data` VALUES (182, 'setting_description', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent nec enim eros. Nulla aliquet fringilla arcu sit amet bibendum. Nunc elit dolor, tempor vel sapien non, placerat egestas leo.</p>', 'services', 0);
INSERT INTO `lumonata_meta_data` VALUES (183, 'setting_meta_title', '', 'services', 0);
INSERT INTO `lumonata_meta_data` VALUES (184, 'setting_meta_description', '', 'services', 0);
INSERT INTO `lumonata_meta_data` VALUES (185, 'setting_meta_keywords', '', 'services', 0);
INSERT INTO `lumonata_meta_data` VALUES (186, 'mailchimp_api_key', '630ba13cbb56d9418531be1f4fc053f1-us14', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (187, 'mailchimp_list_id', '2832126e40', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (188, 'landing_page_section_4_title', 'Why Stay with Us', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (189, 'landing_page_section_4_content', '<p>Pellentesque in ipsum id orci porta dapibus. Curabitur aliquet quam id dui posuere blandit. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem.</p>', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (352, 'homepage_slide', '/lumonata-content/files/201903/berawa-beach-bali-1552540785.jpg', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (353, 'homepage_slide_order', '1', 'global_setting', 352);
INSERT INTO `lumonata_meta_data` VALUES (354, 'homepage_slide_title', 'Near with Canggu, Brawa, Seminyak', 'global_setting', 352);
INSERT INTO `lumonata_meta_data` VALUES (355, 'homepage_slide_subtitle', 'Near To Others Destination', 'global_setting', 352);
INSERT INTO `lumonata_meta_data` VALUES (356, 'homepage_slide_use_link', '1', 'global_setting', 352);
INSERT INTO `lumonata_meta_data` VALUES (357, 'homepage_slide_link_url', '//localhost/bidadari/activities/yoga-activity.html', 'global_setting', 352);
INSERT INTO `lumonata_meta_data` VALUES (358, 'homepage_slide_link_type', 'detail', 'global_setting', 352);
INSERT INTO `lumonata_meta_data` VALUES (359, 'homepage_slide_link_target', '_self', 'global_setting', 352);
INSERT INTO `lumonata_meta_data` VALUES (360, 'homepage_slide_post_link_id', '7', 'global_setting', 352);
INSERT INTO `lumonata_meta_data` VALUES (361, 'homepage_slide', '/lumonata-content/files/201903/bulungdaya09131488-1552540786.jpg', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (362, 'homepage_slide_order', '0', 'global_setting', 361);
INSERT INTO `lumonata_meta_data` VALUES (363, 'homepage_slide_title', 'Feel the Beauty of Tropical Place', 'global_setting', 361);
INSERT INTO `lumonata_meta_data` VALUES (364, 'homepage_slide_subtitle', 'Natural Place', 'global_setting', 361);
INSERT INTO `lumonata_meta_data` VALUES (365, 'homepage_slide_use_link', '1', 'global_setting', 361);
INSERT INTO `lumonata_meta_data` VALUES (366, 'homepage_slide_link_url', 'http://thebidadarivillasandspa.com/', 'global_setting', 361);
INSERT INTO `lumonata_meta_data` VALUES (367, 'homepage_slide_link_type', 'custom', 'global_setting', 361);
INSERT INTO `lumonata_meta_data` VALUES (368, 'homepage_slide_link_target', '_blank', 'global_setting', 361);
INSERT INTO `lumonata_meta_data` VALUES (369, 'homepage_slide_post_link_id', 'custom', 'global_setting', 361);
INSERT INTO `lumonata_meta_data` VALUES (370, 'homepage_slide', '/lumonata-content/files/201903/wp-image-430545318-1552540786.jpg', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (371, 'homepage_slide_order', '2', 'global_setting', 370);
INSERT INTO `lumonata_meta_data` VALUES (372, 'homepage_slide_title', 'Interact with friendly Balinese people', 'global_setting', 370);
INSERT INTO `lumonata_meta_data` VALUES (373, 'homepage_slide_subtitle', 'Cozy Place & Friendly People', 'global_setting', 370);
INSERT INTO `lumonata_meta_data` VALUES (374, 'homepage_slide_use_link', '0', 'global_setting', 370);
INSERT INTO `lumonata_meta_data` VALUES (375, 'homepage_slide_link_url', '#', 'global_setting', 370);
INSERT INTO `lumonata_meta_data` VALUES (376, 'homepage_slide_link_type', 'custom', 'global_setting', 370);
INSERT INTO `lumonata_meta_data` VALUES (377, 'homepage_slide_link_target', '_self', 'global_setting', 370);
INSERT INTO `lumonata_meta_data` VALUES (378, 'homepage_slide_post_link_id', 'custom', 'global_setting', 370);
INSERT INTO `lumonata_meta_data` VALUES (379, 'landing_page_section_7_title', 'Guest Reviews', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (380, 'landing_page_section_7_link_text', 'See All Reviews', 'global_setting', 0);
INSERT INTO `lumonata_meta_data` VALUES (381, 'landing_page_section_7_link', 'http://thebidadarivillasandspa.com/', 'global_setting', 0);

-- ----------------------------
-- Table structure for lumonata_notifications
-- ----------------------------
DROP TABLE IF EXISTS `lumonata_notifications`;
CREATE TABLE `lumonata_notifications`  (
  `lnotification_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `lpost_id` bigint(20) NOT NULL,
  `lpost_owner` bigint(20) NOT NULL,
  `luser_id` bigint(20) NOT NULL,
  `laffected_user` bigint(20) NOT NULL,
  `laction_name` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `laction_date` date NOT NULL,
  `lstatus` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `lshare_to` bigint(20) NOT NULL,
  PRIMARY KEY (`lnotification_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for lumonata_rule_relationship
-- ----------------------------
DROP TABLE IF EXISTS `lumonata_rule_relationship`;
CREATE TABLE `lumonata_rule_relationship`  (
  `lapp_id` bigint(20) NOT NULL,
  `lrule_id` bigint(20) NOT NULL,
  `lorder_id` bigint(20) NULL DEFAULT 1,
  PRIMARY KEY (`lapp_id`, `lrule_id`) USING BTREE,
  INDEX `taxonomy_id`(`lrule_id`) USING BTREE,
  CONSTRAINT `lumonata_rule_relationship_ibfk_1` FOREIGN KEY (`lrule_id`) REFERENCES `lumonata_rules` (`lrule_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of lumonata_rule_relationship
-- ----------------------------
INSERT INTO `lumonata_rule_relationship` VALUES (1, 1, 1);
INSERT INTO `lumonata_rule_relationship` VALUES (2, 1, 1);
INSERT INTO `lumonata_rule_relationship` VALUES (3, 1, 1);
INSERT INTO `lumonata_rule_relationship` VALUES (4, 1, 1);
INSERT INTO `lumonata_rule_relationship` VALUES (5, 1, 1);
INSERT INTO `lumonata_rule_relationship` VALUES (6, 1, 1);
INSERT INTO `lumonata_rule_relationship` VALUES (7, 1, 1);
INSERT INTO `lumonata_rule_relationship` VALUES (8, 1, 1);
INSERT INTO `lumonata_rule_relationship` VALUES (9, 1, 1);
INSERT INTO `lumonata_rule_relationship` VALUES (10, 1, 1);
INSERT INTO `lumonata_rule_relationship` VALUES (11, 1, 1);
INSERT INTO `lumonata_rule_relationship` VALUES (12, 1, 1);
INSERT INTO `lumonata_rule_relationship` VALUES (13, 1, 1);
INSERT INTO `lumonata_rule_relationship` VALUES (14, 1, 1);
INSERT INTO `lumonata_rule_relationship` VALUES (15, 1, 1);
INSERT INTO `lumonata_rule_relationship` VALUES (16, 1, 1);
INSERT INTO `lumonata_rule_relationship` VALUES (17, 1, 1);
INSERT INTO `lumonata_rule_relationship` VALUES (18, 1, 1);
INSERT INTO `lumonata_rule_relationship` VALUES (19, 1, 1);
INSERT INTO `lumonata_rule_relationship` VALUES (20, 1, 1);
INSERT INTO `lumonata_rule_relationship` VALUES (23, 8, 1);
INSERT INTO `lumonata_rule_relationship` VALUES (24, 8, 1);
INSERT INTO `lumonata_rule_relationship` VALUES (25, 8, 1);
INSERT INTO `lumonata_rule_relationship` VALUES (26, 9, 1);
INSERT INTO `lumonata_rule_relationship` VALUES (27, 10, 1);
INSERT INTO `lumonata_rule_relationship` VALUES (28, 10, 1);
INSERT INTO `lumonata_rule_relationship` VALUES (29, 10, 1);
INSERT INTO `lumonata_rule_relationship` VALUES (30, 8, 1);
INSERT INTO `lumonata_rule_relationship` VALUES (31, 10, 1);
INSERT INTO `lumonata_rule_relationship` VALUES (32, 8, 1);
INSERT INTO `lumonata_rule_relationship` VALUES (33, 8, 1);
INSERT INTO `lumonata_rule_relationship` VALUES (34, 8, 1);
INSERT INTO `lumonata_rule_relationship` VALUES (35, 11, 1);
INSERT INTO `lumonata_rule_relationship` VALUES (36, 11, 1);
INSERT INTO `lumonata_rule_relationship` VALUES (37, 12, 1);
INSERT INTO `lumonata_rule_relationship` VALUES (38, 12, 1);
INSERT INTO `lumonata_rule_relationship` VALUES (39, 12, 1);
INSERT INTO `lumonata_rule_relationship` VALUES (40, 11, 1);
INSERT INTO `lumonata_rule_relationship` VALUES (44, 1, 1);
INSERT INTO `lumonata_rule_relationship` VALUES (45, 1, 1);
INSERT INTO `lumonata_rule_relationship` VALUES (46, 1, 1);
INSERT INTO `lumonata_rule_relationship` VALUES (47, 1, 1);
INSERT INTO `lumonata_rule_relationship` VALUES (48, 1, 1);
INSERT INTO `lumonata_rule_relationship` VALUES (49, 1, 1);
INSERT INTO `lumonata_rule_relationship` VALUES (50, 1, 1);
INSERT INTO `lumonata_rule_relationship` VALUES (51, 1, 1);
INSERT INTO `lumonata_rule_relationship` VALUES (52, 1, 1);
INSERT INTO `lumonata_rule_relationship` VALUES (53, 1, 1);
INSERT INTO `lumonata_rule_relationship` VALUES (54, 1, 1);
INSERT INTO `lumonata_rule_relationship` VALUES (55, 1, 1);
INSERT INTO `lumonata_rule_relationship` VALUES (56, 1, 1);
INSERT INTO `lumonata_rule_relationship` VALUES (57, 1, 1);

-- ----------------------------
-- Table structure for lumonata_rules
-- ----------------------------
DROP TABLE IF EXISTS `lumonata_rules`;
CREATE TABLE `lumonata_rules`  (
  `lrule_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `lparent` bigint(20) NULL DEFAULT 0,
  `lname` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `lsef` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ldescription` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `lrule` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `lgroup` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `lcount` bigint(20) NULL DEFAULT 0,
  `lorder` bigint(20) NULL DEFAULT 1,
  `lsubsite` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT 'arunna',
  PRIMARY KEY (`lrule_id`) USING BTREE,
  INDEX `rules_name`(`lname`) USING BTREE,
  INDEX `sef`(`lsef`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of lumonata_rules
-- ----------------------------
INSERT INTO `lumonata_rules` VALUES (1, 0, 'Uncategorized', 'uncategorized', '', 'category', 'default', 34, 43, 'arunna');
INSERT INTO `lumonata_rules` VALUES (5, 0, 'Package III (1.5 hours)', 'package-iii-15-hours', '<ul>\r\n<li>Welcome drink</li>\r\n<li>Foot Massage (30 Minutes)</li>\r\n<li>Facial (1 Hour)</li>\r\n</ul>', 'packages', 'spa', 0, 23, 'arunna');
INSERT INTO `lumonata_rules` VALUES (6, 0, 'Package II (2 hours)', 'package-ii-2-hours', '<ul>\r\n<li>Welcome drink</li>\r\n<li>Balinese Massage (1 hour)</li>\r\n<li>Mask (body) 25 minutes</li>\r\n<li>Milk Bath (35 Minutes)</li>\r\n</ul>', 'packages', 'spa', 0, 22, 'arunna');
INSERT INTO `lumonata_rules` VALUES (7, 0, 'Package I (2 hours)', 'package-i-2-hours', '<ul>\r\n<li>Welcome drink</li>\r\n<li>Balinese Massage (1 hour)</li>\r\n<li>Lulur (25 minutes)</li>\r\n<li>Flower bath (35 minutes)</li>\r\n</ul>', 'packages', 'spa', 0, 21, 'arunna');
INSERT INTO `lumonata_rules` VALUES (8, 0, 'Restaurants', 'restaurants', '', 'categories', 'locations', 7, 16, 'arunna');
INSERT INTO `lumonata_rules` VALUES (9, 0, 'Worships', 'worships', '', 'categories', 'locations', 1, 18, 'arunna');
INSERT INTO `lumonata_rules` VALUES (10, 0, 'Attractions', 'attractions', '', 'categories', 'locations', 4, 19, 'arunna');
INSERT INTO `lumonata_rules` VALUES (11, 0, 'Shops', 'shops', '', 'categories', 'locations', 3, 15, 'arunna');
INSERT INTO `lumonata_rules` VALUES (12, 0, 'Beachs', 'beachs', '', 'categories', 'locations', 3, 17, 'arunna');
INSERT INTO `lumonata_rules` VALUES (13, 0, 'Parking area', 'parking-area', '', 'amenities', 'rooms', 0, 15, 'arunna');
INSERT INTO `lumonata_rules` VALUES (14, 0, 'Room Service', 'room-service', '', 'amenities', 'rooms', 0, 14, 'arunna');
INSERT INTO `lumonata_rules` VALUES (15, 0, 'CCTV', 'cctv', '', 'amenities', 'rooms', 0, 13, 'arunna');
INSERT INTO `lumonata_rules` VALUES (16, 0, '24 hours Reception and Security', '24-hours-reception-and-security', '', 'amenities', 'rooms', 0, 12, 'arunna');
INSERT INTO `lumonata_rules` VALUES (17, 0, 'Shuttle service', 'shuttle-service', '', 'amenities', 'rooms', 0, 11, 'arunna');
INSERT INTO `lumonata_rules` VALUES (18, 0, 'Kitchenette', 'kitchenette', '', 'amenities', 'rooms', 0, 10, 'arunna');
INSERT INTO `lumonata_rules` VALUES (19, 0, 'Mini Bar', 'mini-bar', '', 'amenities', 'rooms', 0, 9, 'arunna');
INSERT INTO `lumonata_rules` VALUES (20, 0, 'Wi-fi access', 'wi-fi-access', '', 'amenities', 'rooms', 0, 8, 'arunna');
INSERT INTO `lumonata_rules` VALUES (21, 0, 'Money Exchange', 'money-exchange', '', 'amenities', 'rooms', 0, 7, 'arunna');
INSERT INTO `lumonata_rules` VALUES (22, 0, 'Motorbike rent', 'motorbike-rent', '', 'amenities', 'rooms', 0, 6, 'arunna');
INSERT INTO `lumonata_rules` VALUES (23, 0, 'Tour Service', 'tour-service', '', 'amenities', 'rooms', 0, 5, 'arunna');
INSERT INTO `lumonata_rules` VALUES (24, 0, 'Meeting Room', 'meeting-room', '', 'amenities', 'rooms', 0, 4, 'arunna');
INSERT INTO `lumonata_rules` VALUES (25, 0, 'Laundry service', 'laundry-service', '', 'amenities', 'rooms', 0, 3, 'arunna');
INSERT INTO `lumonata_rules` VALUES (26, 0, 'Spa', 'spa', '', 'amenities', 'rooms', 0, 2, 'arunna');
INSERT INTO `lumonata_rules` VALUES (27, 0, 'Restaurant & Bar', 'restaurant-bar', '', 'amenities', 'rooms', 0, 1, 'arunna');

-- ----------------------------
-- Table structure for lumonata_users
-- ----------------------------
DROP TABLE IF EXISTS `lumonata_users`;
CREATE TABLE `lumonata_users`  (
  `luser_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `lusername` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ldisplay_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `lpassword` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `lemail` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `lregistration_date` datetime(0) NULL DEFAULT NULL,
  `luser_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `lactivation_key` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `lavatar` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `lsex` int(11) NULL DEFAULT 1 COMMENT '1=male,2=female',
  `lbirthday` date NULL DEFAULT NULL,
  `lstatus` int(11) NULL DEFAULT 0 COMMENT '0=pendding activation, 1=active,2=blocked',
  `ldlu` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`luser_id`) USING BTREE,
  INDEX `username`(`lusername`) USING BTREE,
  INDEX `display_name`(`ldisplay_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of lumonata_users
-- ----------------------------
INSERT INTO `lumonata_users` VALUES (1, 'admin', 'Administrator', 'fcea920f7412b5da7be0cf42b8c93759', 'dev@lumonata.com', '0000-00-00 00:00:00', 'administrator', '', '', 1, '2011-03-19', 1, '2015-03-09 15:52:57');

SET FOREIGN_KEY_CHECKS = 1;
