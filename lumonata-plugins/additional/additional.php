<?php

/*
| -------------------------------------------------------------------------------------------------------------------------
Plugin Name: Lumonata Additional Data
Plugin URL: http://lumonata.com/
Description: This plugin is use for adding addtional field in each post and page.
Author: Ngurah Rai
Author URL: http://lumonata.com/
Version: 1.0.0
| -------------------------------------------------------------------------------------------------------------------------
*/

//-- Add Actions

$apps_array = array(
    'spa'        => 'Spa',
    'rooms'      => 'Rooms',
    'awards'     => 'Awards',
    'offers'     => 'Offers',
    'reviews'    => 'Reviews',
    'gallery'    => 'Gallery',
    'services'   => 'Services',
    'locations'  => 'Locations',
    'restaurant' => 'Restaurant',
    'activities' => 'Activities',
);

add_actions( 'css_elements', 'get_custom_css', '//' . SITE_URL . '/lumonata-plugins/additional/css/menus.css' );
add_actions( 'plugin_menu_post_type', 'set_plugin_menu_blog', $apps_array );
add_actions( 'additional-ajax_page', 'run_additional_ajax_request' );
add_actions( 'page_additional_data', 'additional_data_page_func' );

foreach( $apps_array as $apps => $name )
{
    if( $apps == 'spa' )
    {
        $tabs = array( 'blogs' => $name, 'categories' => 'Categories', 'packages' => 'Packages', 'setting' => 'Setting' );
    }
    elseif( $apps == 'rooms' )
    {
        $tabs = array( 'blogs' => $name, 'categories' => 'Categories', 'amenities' => 'Amenities', 'setting' => 'Setting' );
    }
    else
    {
        $tabs = array( 'blogs' => $name, 'categories' => 'Categories', 'setting' => 'Setting' );
    }

    add_main_menu( array( $apps => $name ) );

    add_privileges( 'administrator', $apps, 'request' );
    add_privileges( 'administrator', $apps, 'insert' );
    add_privileges( 'administrator', $apps, 'update' );
    add_privileges( 'administrator', $apps, 'delete' );

    add_actions( $apps . '_additional_field', 'additional_data_apps_func', $apps );
    add_actions( $apps . '_additional_delete', 'additional_delete_apps_func', $apps );
    add_actions( $apps . '_rule_additional_field', 'rule_additional_data_apps_func', $apps );

    add_actions( $apps, 'get_admin_article', $apps, $name . '|' . $name, $tabs, true, false);
    add_actions( $apps . '_admin_page', 'run_additional_ajax_request' );

    add_actions( 'plugin_menu_set', '<option value="' . $apps . '">' . $name . '</option>' );

    add_actions( 'setting_article_extends', 'additional_setting_form', $tabs );
    add_actions( 'packages_article_extends', 'additional_packages_list', $tabs );
    add_actions( 'amenities_article_extends', 'additional_amenities_list', $tabs );
}

function additional_packages_list( $tabs )
{
    return get_admin_rule( 'packages', 'spa', 'Spa|Spa', $tabs );
}

function additional_amenities_list( $tabs )
{
    return get_admin_rule( 'amenities', 'rooms', 'Room|Rooms', $tabs );
}

function additional_data_page_func()
{
    global $thepost, $db;

    $i    = $thepost->post_index;
    $id   = isset( $_GET['id'] ) ? $_GET['id'] : time();
    $surl = SITE_URL;

    set_template( PLUGINS_PATH . '/additional/additional-form.html', 'additional-template-' . $i );
    add_block( 'additional-block', 'adt', 'additional-template-' . $i );
    
    add_variable( 'i', $i );
    add_variable( 'post_id', $id );
    add_variable( 'app_name', 'pages' );
    add_variable( 'site_url', SITE_URL );
    add_variable( 'plugin_url', $surl . '/lumonata-plugins/additional' );

    add_variable( 'featured_img', get_featured_img( $id, 'pages' ) );
    add_variable( 'featured_img_title', get_additional_field( $id, 'featured_img_title', 'pages' ) );
    add_variable( 'featured_img_alt', get_additional_field( $id, 'featured_img_alt', 'pages' ) );
    add_variable( 'featured_img_copyright', get_additional_field( $id, 'featured_img_copyright', 'pages' ) );

    add_variable( 'brief', get_additional_field( $id, 'brief', 'pages' ) );
    add_variable( 'booking_link_add', get_additional_field( $id, 'booking_link_add', 'pages' ) );
    add_variable( 'subtitle', get_additional_field( $id, 'subtitle', 'pages' ) );
    add_variable( 'custom_field', get_custom_field_content( $i, $id, 'pages' ) );
    add_variable( 'additional_field', get_additional_field_content( $i, $id, 'pages' ) );
    add_variable( 'action_type', $_GET['prc'] );

    add_actions( 'css_elements', 'get_custom_css', '//' . $surl . '/lumonata-plugins/additional/css/dropzone.min.css' );
    add_actions( 'css_elements', 'get_custom_css', '//' . $surl . '/lumonata-plugins/additional/css/chosen.min.css' );
    add_actions( 'css_elements', 'get_custom_css', '//' . $surl . '/lumonata-plugins/additional/css/style.css' );

    add_actions( 'js_elements', 'get_custom_javascript', '//' . $surl . '/lumonata-plugins/additional/js/jquery.mjs.nestedSortable.js' );
    add_actions( 'js_elements', 'get_custom_javascript', '//' . $surl . '/lumonata-plugins/additional/js/chosen.jquery.min.js' );
    add_actions( 'js_elements', 'get_custom_javascript', '//' . $surl . '/lumonata-plugins/additional/js/dropzone.min.js' );
    add_actions( 'js_elements', 'get_custom_javascript', '//' . $surl . '/lumonata-plugins/additional/js/scripts.js' );

    parse_template( 'additional-block', 'adt', false );

    return return_template( 'additional-template-' . $i );
}

function additional_data_func( $apps )
{
    global $thepost;

    $i       = $thepost->post_index;
    $post_id = $thepost->post_id;

    if( is_publish() )
    {
        // set_menu_pdf( $post_id, $apps );
        set_main_images( $post_id, $apps );
        set_article_pdf( $post_id, $apps );
    }

    set_template( PLUGINS_PATH . '/additional/additional-form.html', 'additional_template_' . $i );

    add_block( 'additional_block', 'adt', 'additional_template_' . $i );
    add_variable( 'i', $i );
    add_variable( 'app_name', $apps );
    add_variable( 'post_id', $post_id );
    add_variable( 'site_url', SITE_URL );
    add_variable( 'admin_url', SITE_URL . '/lumonata-admin' );
    add_variable( 'ajax_url', SITE_URL . '/additional-ajax/' );
    add_variable( 'plugin_url', SITE_URL . '/lumonata-plugins/additional' );

    add_variable( 'action_type', $_GET[ 'prc' ] );
    add_variable( 'parent', get_parent( $i, $post_id, $apps ) );
    add_variable( 'pdf_file', get_pdf_file( $i, $post_id, $apps ) );
    add_variable( 'amenities', get_amenities( $i, $post_id, $apps ) );
    add_variable( 'main_image', get_main_image( $i, $post_id, $apps ) );
    add_variable( 'floor_plan', get_floor_plan( $i, $post_id, $apps ) );
    add_variable( 'website_link', get_website_link( $i, $post_id, $apps ) );
    add_variable( 'brief', get_additional_field( $post_id, 'brief', $apps ) );
    add_variable( 'destination_opt', get_destination_option( $post_id, $apps ) );
    add_variable( 'featured_content', get_featured_content( $i, $post_id, $apps ) );
    add_variable( 'accomodation_detail', get_accomodation_detail( $i, $post_id, $apps ) );
    add_variable( 'special_offer_subcontent', get_special_offer_subcontent( $i, $post_id, $apps ) );

    parse_template( 'additional_block', 'adt', false );

    return return_template( 'additional_template_' . $i );
}


function additional_data_apps_func( $apps )
{
    global $thepost;

    $i    = $thepost->post_index;
    $id   = isset( $_GET['id'] ) ? $_GET['id'] : time();
    $surl = SITE_URL;
    
    set_template( PLUGINS_PATH . '/additional/additional-form.html', 'additional-template-' . $i );
    add_block( 'additional-block', 'adt', 'additional-template-' . $i );

    add_variable( 'i', $i );
    add_variable( 'post_id', $id );
    add_variable( 'app_name', $apps );
    add_variable( 'site_url', $surl );
    add_variable( 'plugin_url', $surl . '/lumonata-plugins/additional' );

    add_variable( 'featured_img', get_featured_img( $id, $apps ) );
    add_variable( 'featured_img_title', get_additional_field( $id, 'featured_img_title', $apps ) );
    add_variable( 'featured_img_alt', get_additional_field( $id, 'featured_img_alt', $apps ) );
    add_variable( 'featured_img_copyright', get_additional_field( $id, 'featured_img_copyright', $apps ) );

    add_variable( 'brief', get_additional_field( $id, 'brief', $apps ) );
    add_variable( 'booking_link_add', get_additional_field( $id, 'booking_link_add', $apps ) );
    add_variable( 'related', get_related_post_content( $id, $id, $apps ) );
    add_variable( 'subtitle', get_additional_field( $id, 'subtitle', $apps ) );
    add_variable( 'custom_field', get_custom_field_content( $i, $id, $apps ) );
    add_variable( 'additional_field', get_additional_field_content( $i, $id, $apps ) );
    add_variable( 'action_type', $_GET['prc'] );

    add_actions( 'css_elements', 'get_custom_css', '//' . $surl . '/lumonata-plugins/additional/css/dropzone.min.css' );
    add_actions( 'css_elements', 'get_custom_css', '//' . $surl . '/lumonata-plugins/additional/css/chosen.min.css' );
    add_actions( 'css_elements', 'get_custom_css', '//' . $surl . '/lumonata-plugins/additional/css/style.css' );

    add_actions( 'js_elements', 'get_custom_javascript', '//' . $surl . '/lumonata-plugins/additional/js/jquery.mjs.nestedSortable.js' );
    add_actions( 'js_elements', 'get_custom_javascript', '//' . $surl . '/lumonata-plugins/additional/js/chosen.jquery.min.js' );
    add_actions( 'js_elements', 'get_custom_javascript', '//' . $surl . '/lumonata-plugins/additional/js/dropzone.min.js' );
    add_actions( 'js_elements', 'get_custom_javascript', '//' . $surl . '/lumonata-plugins/additional/js/scripts.js' );

    parse_template( 'additional-block', 'adt', false );

    return return_template( 'additional-template-' . $i );
}

function additional_delete_apps_func( $apps )
{
    global $thepost;

    //-- If you want to trigger some function on delete application,
    //-- add that script here
}

function set_menu_pdf( $post_id, $apps )
{
    if( empty( $post_id ) )
    {
        return;
    }

    $old_pdf = get_additional_field( $post_id, 'menu_pdf', $apps );

    if( isset( $_FILES[ 'menu_pdf' ] ) && $_FILES[ 'menu_pdf' ][ 'error' ] == 0 )
    {
        //-- CHECK FOLDER EXIST
        if( !file_exists( PLUGINS_PATH . '/additional/pdf/' ) )
        {
            mkdir( PLUGINS_PATH . '/additional/pdf/' );
        }

        $file_name   = $_FILES[ 'menu_pdf' ][ 'name' ];
        $file_size   = $_FILES[ 'menu_pdf' ][ 'size' ];
        $file_type   = $_FILES[ 'menu_pdf' ][ 'type' ];
        $file_source = $_FILES[ 'menu_pdf' ][ 'tmp_name' ];

        $pdf = '';

        if( is_allow_file_size( $file_size ) )
        {
            if( is_allow_file_type( $file_type, 'pdf' ) )
            {
                $destination = PLUGINS_PATH . '/additional/pdf/' . $file_name;

                if( upload( $file_source, $destination ) )
                {
                    $pdf = $file_name;

                    if( !empty( $old_pdf ) && file_exists( PLUGINS_PATH . '/additional/pdf/' . $old_pdf ) )
                    {
                        unlink( PLUGINS_PATH . '/additional/pdf/' . $old_pdf );
                    }
                }
            }
        }
    }
    else
    {
        $pdf = $old_pdf;
    }

    edit_additional_field( $post_id, 'menu_pdf', $pdf, $apps );
}

function rule_additional_data_apps_func( $apps )
{
    global $thepost;

    $i    = $thepost->post_index;
    $id   = isset( $_GET['id'] ) ? $_GET['id'] : time();
    $surl = SITE_URL;

    set_template( PLUGINS_PATH . '/additional/rule-additional-form.html', 'rule-additional-template-' . $i );
    add_block( 'rule-additional-block', 'radt', 'rule-additional-template-' . $i );

    add_variable( 'i', $i );
    add_variable( 'app_name', $apps );
    add_variable( 'rule_id', $id );
    add_variable( 'site_url', $surl );
    add_variable( 'plugin_url', $surl . '/lumonata-plugins/additional' );

    add_variable( 'featured_img', get_rule_featured_img( $id, $apps ) );
    add_variable( 'action_type', $_GET['prc'] );

    add_actions( 'css_elements', 'get_custom_css', '//' . $surl . '/lumonata-plugins/additional/css/dropzone.min.css' );
    add_actions( 'css_elements', 'get_custom_css', '//' . $surl . '/lumonata-plugins/additional/css/chosen.min.css' );
    add_actions( 'css_elements', 'get_custom_css', '//' . $surl . '/lumonata-plugins/additional/css/style.css' );

    add_actions( 'js_elements', 'get_custom_javascript', '//' . $surl . '/lumonata-plugins/additional/js/jquery.mjs.nestedSortable.js' );
    add_actions( 'js_elements', 'get_custom_javascript', '//' . $surl . '/lumonata-plugins/additional/js/chosen.jquery.min.js' );
    add_actions( 'js_elements', 'get_custom_javascript', '//' . $surl . '/lumonata-plugins/additional/js/dropzone.min.js' );
    add_actions( 'js_elements', 'get_custom_javascript', '//' . $surl . '/lumonata-plugins/additional/js/scripts.js' );

    parse_template( 'rule-additional-block', 'radt', false );

    return return_template( 'rule-additional-template-' . $i );
}

function additional_setting_form( $tabs )
{
    extract( $_GET );

    $surl = SITE_URL;

    set_template( PLUGINS_PATH.'/additional/setting-form.html', 'setting-template' );

    add_block( 'setting-block', 's-block', 'setting-template' );

    add_variable( 'site_url', $surl );
    add_variable( 'app_name', $state );
    add_variable( 'tab', set_tabs( $tabs,'setting' ) );
    add_variable( 'app_title', $tabs['blogs'] .' Setting' );
    add_variable( 'ajax_url', get_additional_ajax_url( $state ) );
    add_variable( 'state_url', get_state_url( $state . '&tab=setting' ) );
    add_variable( 'btn_save', button( 'button=save_changes&label=Save' ) );
    add_variable( 'hero_img', get_additional_setting_hero_image_content( $state ) );
    add_variable( 'message', run_saving_additional_setting( $state, $tabs['blogs'] ) );
    add_variable( 'btn_cancel', button( 'button=cancel', get_state_url( $state . '&tab=blogs' ) ) );

    add_variable( 'title', get_meta_data( 'setting_title', $state ) );
    add_variable( 'meta_title', get_meta_data( 'setting_meta_title', $state ) );
    add_variable( 'meta_keywords', get_meta_data( 'setting_meta_keywords', $state ) );
    add_variable( 'meta_description', get_meta_data( 'setting_meta_description', $state ) );
    add_variable( 'textarea', textarea( 'additional_setting[description]', 0, get_meta_data( 'setting_description', $state ), 0, true, false ) );

    add_actions( 'section_title', $tabs['blogs'] );

    add_actions( 'css_elements', 'get_custom_css', '//' . $surl . '/lumonata-plugins/additional/css/style.css' );

    add_actions( 'js_elements', 'get_custom_javascript', '//' . $surl . '/lumonata-plugins/additional/js/setting.js' );
    add_actions( 'js_elements', 'get_javascript_inc', 'tinymce/tinymce.min.js' );
    add_actions( 'js_elements', 'get_javascript', 'tinymce' );

    parse_template( 'setting-block', 's-block', false );

    return return_template( 'setting-template' );
}

function run_saving_additional_setting( $apps, $tab_name )
{
    $is_edit_success = false;

    if( is_save_changes() )
    {
        foreach( $_POST['additional_setting'] as $name => $value )
        {
            if( update_meta_data( 'setting_' . $name , $value, $apps, 0 ) )
            {
                $is_edit_success = true;
            }
        }

        if( $is_edit_success )
        {
            return '
            <div class="message success">
                '. $tab_name .' setting data has been successfully edited.
            </div>';
        }
        else
        {
            return '
            <div class="message error">
                Something wrong, please try again.
            </div>';
        }
    }
}

function get_featured_img( $post_id, $apps, $html = true )
{
    $site_url = SITE_URL;
    $content  = $html ? '' : array();
    $featured = get_attachment_by_app_name( $post_id, 'featured-image-' . $apps, false );

    if( !empty( $featured ) )
    {
        if( $html )
        {
            $content .= '
            <div class="box">
                <img src="//' . $site_url . $featured['lattach_loc_medium'] . '" />
                <div class="overlay">
                    <a data-post-id="' . $post_id . '" class="img-delete" title="Delete">
                        <img src="//' . $site_url . '/lumonata-plugins/additional/images/delete.png">
                    </a>
                </div>
            </div>';
        }
        else
        {
            $content = $featured;
        }
    }

    return $content;
}

function get_rule_featured_img( $rule_id, $apps, $html = true )
{
    $content  = $html ? '' : array();
    $featured = get_rule_additional_field( $rule_id, 'featured_img', $apps );

    if( !empty( $featured ) )
    {
        $img = json_decode( $featured, true );

        if( !empty( $img ) )
        {
            $surl = SITE_URL;

            if( $html )
            {
                $content .= '
                <div class="box">
                    <img src="//' . $surl . $img['medium'] . '" />
                    <div class="overlay">
                        <a data-post-id="' . $rule_id . '" class="img-delete" title="Delete">
                            <img src="//' . $surl . '/lumonata-plugins/additional/images/delete.png">
                        </a>
                    </div>
                </div>';
            }
            else
            {
                $content = array(
                    'original' => $img['original'],
                    'medium' => $img['medium'],
                    'large' => $img['large'],
                    'thumb' => $img['thumb']
                );
            }
        }
    }

    return $content;
}

function get_additional_ajax_url( $app_name )
{
    return get_state_url( 'ajax&apps=' . $app_name );
}

function get_additional_field_content( $i, $post_id, $apps )
{
    global $db;

    $s = 'SELECT lvalue FROM lumonata_additional_fields WHERE lapp_id=%d AND lkey=%s AND lapp_name=%s';
    $q = $db->prepare_query( $s, $post_id, 'custom_field', $apps );
    $r = $db->do_query( $q );
    $d = $db->fetch_array( $r );

    $fields = json_decode( $d['lvalue'], true );
    $surl   = SITE_URL;

    $content = '
    <fieldset>
        <div class="additional_data post-additional-data-wrapp">
            <h2>Additional Data</h2>
            <div class="additional_content">
                <div class="boxs-wrapp">
                    <div class="boxs">
                        <div class="new-field-box clearfix">
                            <input type="text" class="field textbox" name="field_label" autocomplete="off" value="" placeholder="Field label" />
                            <input type="hidden" class="field textbox" name="field_name" autocomplete="off" value="" placeholder="Field name" />
                            <select class="field select-opt" name="field_type" autocomplete="off">
                                <option value="">Select Type Of Field</option>
                                <option value="0">Text Field</option>
                                <option value="1">Textarea</option>
                                <option value="2">Tinymce</option>
                            </select>
                            <a class="add-new-field">Add New Field</a>
                        </div>
                        <div class="new-field-list clearfix">
                            <ol>';

                            if( !empty( $fields ) && is_array( $fields ) )
                            {
                                foreach( $fields as $name => $d )
                                {
                                    if( $d['type'] == 0 )
                                    {
                                        $content .= '
                                        <li>
                                            <div class="field-wrapp">
                                                <p><a class="delete-field" data-name="' . $name . '"><img src="//' . $surl . '/lumonata-plugins/additional/images/delete.png" /></a></p>
                                                <div class="inner">
                                                    <span>Label</span>
                                                    <input type="text" class="textbox" name="additional_fields[custom_field][' . $i . '][label][]" autocomplete="off" value="' . $d['label'] . '" />
                                                    <span>Value</span>
                                                    <input type="text" class="textbox" name="additional_fields[custom_field][' . $i . '][value][]" autocomplete="off" value="' . $d['value'] . '" />
                                                    <input type="text" class="textbox hidden" name="additional_fields[custom_field][' . $i . '][type][]" autocomplete="off" value="' . $d['type'] . '" />
                                                </div>
                                            </div>
                                        </li>';
                                    }
                                    elseif( $d['type'] == 1 )
                                    {
                                        $content .= '
                                        <li>
                                            <div class="field-wrapp">
                                                <p><a class="delete-field" data-name="' . $name . '"><img src="//' . $surl . '/lumonata-plugins/additional/images/delete.png" /></a></p>
                                                <div class="inner">
                                                    <span>Label</span>
                                                    <input type="text" class="textbox" name="additional_fields[custom_field][' . $i . '][label][]" autocomplete="off" value="' . $d['label'] . '" />
                                                    <span>Value</span>
                                                    <textarea class="textarea" name="additional_fields[custom_field][' . $i . '][value][]" rows="10" autocomplete="off">' . $d['value'] . '</textarea>
                                                    <input type="text" class="textbox hidden" name="additional_fields[custom_field][' . $i . '][type][]" autocomplete="off" value="' . $d['type'] . '" />
                                                </div>
                                            </div>
                                        </li>';
                                    }
                                    elseif( $d['type'] == 2 )
                                    {
                                        $content .= '
                                        <li>
                                            <div class="field-wrapp">
                                                <p>' . $d['label'] . '<a class="delete-field" data-name="' . $name . '"><img src="//' . $surl . '/lumonata-plugins/additional/images/delete.png" /></a></p>
                                                <div class="inner">
                                                    <span>Label</span>
                                                    <input type="text" class="textbox" name="additional_fields[custom_field][' . $i . '][label][]" autocomplete="off" value="' . $d['label'] . '" />
                                                    <span>Value</span>
                                                    <textarea id="tinymce-' . $name . '-' . time() . '" class="textarea tinymce" name="additional_fields[custom_field][' . $i . '][value][]" autocomplete="off">' . $d['value'] . '</textarea>
                                                    <input type="text" class="textbox hidden" name="additional_fields[custom_field][' . $i . '][type][]" autocomplete="off" value="' . $d['type'] . '" />
                                                </div>
                                            </div>
                                        </li>';
                                    }
                                }
                            }

                            $content .= '
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>';

    return $content;
}

function get_related_post_content( $i, $post_id, $apps )
{
    global $db;

    $s = 'SELECT * FROM lumonata_articles WHERE larticle_status=%s AND larticle_type=%s AND larticle_id<>%d ORDER BY lorder';
    $q = $db->prepare_query( $s, 'publish', $apps, $post_id );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        if( isset( $_POST[ 'additional_fields' ][ 'related_post' ][ $i ] ) )
        {
            $value = $_POST[ 'additional_fields' ][ 'related_post' ][ $i ];
        }
        else
        {
            $value = get_additional_field( $post_id, 'related_post', $apps );
            $value = json_decode( $value, true );
        }

        $list  = '
        <select data-placeholder="Choose related post..." style="width:100%;" multiple class="chosen-select" name="additional_fields[related_post][' . $i . '][]" autocomplete="off">';
           while( $d = $db->fetch_array( $r ) )
            {

                $check = is_array( $value ) && in_array( $d[ 'larticle_id' ], $value ) ? 'selected="selected"' : '';
                $list .= '<option value="' . $d[ 'larticle_id' ] . '" ' . $check . '>' . $d[ 'larticle_title' ] . '</option>';
            }
            $list .= '
        </select>';

        return $list;
    }
}

function get_amenities_content( $i, $post_id, $apps )
{
    global $db;

    if( in_array( $apps, array( 'rooms' ) ) )
    {
        $content = '
        <fieldset>
            <div class="additional_data">
                <h2>Amenities List</h2>
                <div class="additional_content additional-amenities-list">
                    <div class="boxs-wrapp">
                        <div class="boxs">';

                            $s = 'SELECT * FROM lumonata_rules WHERE lrule = %s AND lgroup = %s ORDER BY lorder';
                            $q = $db->prepare_query( $s, 'amenities', $apps );
                            $r = $db->do_query( $q );

                            if( $db->num_rows( $r ) > 0 )
                            {
                                if( isset( $_POST[ 'additional_fields' ][ 'amenities_list' ][ $i ] ) )
                                {
                                    $value = $_POST[ 'additional_fields' ][ 'amenities_list' ][ $i ];
                                }
                                else
                                {
                                    $value = get_additional_field( $post_id, 'amenities_list', $apps );
                                    $value = json_decode( $value, true );
                                }

                                $content  .= '
                                <select data-placeholder="Choose related post..." style="width:100%;" multiple class="chosen-select" name="additional_fields[amenities_list][' . $i . '][]" autocomplete="off">';
                                   while( $d = $db->fetch_array( $r ) )
                                    {

                                        $check = is_array( $value ) && in_array( $d[ 'lrule_id' ], $value ) ? 'selected="selected"' : '';
                                        $content .= '<option value="' . $d[ 'lrule_id' ] . '" ' . $check . '>' . $d[ 'lname' ] . '</option>';
                                    }
                                    $content .= '
                                </select>';
                            }

                            $content .= '
                        </div>
                    </div>
                </div>
            </div>
        </fieldset>';

        return $content;
    }
}

function get_locations_content( $i, $post_id, $apps )
{
    global $db;

    if( $apps == 'locations' )
    {
        $loc_addr_1    = isset( $_POST[ 'additional_fields' ][ 'loc_addr_1' ][ $i ] ) ? $_POST[ 'additional_fields' ][ 'loc_addr_1' ][ $i ] : get_additional_field( $post_id, 'loc_addr_1', $apps );
        $loc_addr_2    = isset( $_POST[ 'additional_fields' ][ 'loc_addr_2' ][ $i ] ) ? $_POST[ 'additional_fields' ][ 'loc_addr_2' ][ $i ] : get_additional_field( $post_id, 'loc_addr_2', $apps );
        $loc_cordinate = isset( $_POST[ 'additional_fields' ][ 'loc_cordinate' ][ $i ] ) ? $_POST[ 'additional_fields' ][ 'loc_cordinate' ][ $i ] : get_additional_field( $post_id, 'loc_cordinate', $apps );

        $def_cordinate = empty( $loc_cordinate ) ? array( -8.6559592, 115.1513084 ) : explode( ',', trim( $loc_cordinate ) );
        $site_url      = SITE_URL;

        $content = '
        <fieldset>
            <div class="additional_data">
                <h2>Address</h2>
                <div class="additional_content">
                    <div class="boxs-wrapp">
                        <div class="boxs">
                            <p>Line 1 : </p>
                            <input type="text" class="textbox loc-addr-1" name="additional_fields[loc_addr_1][' . $i . ']" value="' . $loc_addr_1 . '">
                        </div>
                        <div class="boxs">
                            <p>Line 2 : </p>
                            <input type="text" class="textbox loc-addr-2" name="additional_fields[loc_addr_2][' . $i . ']" value="' . $loc_addr_2 . '">
                        </div>
                        <div class="boxs">
                            <p>Longitude / Latitude : </p>
                            <input type="text" class="textbox loc-cordinate" name="additional_fields[loc_cordinate][' . $i . ']" value="' . $loc_cordinate . '">
                        </div>
                        <div class="boxs">
                            <p>Exact Location On Map <i>( drag pointer to choose your location)</i></p>
                            <input type="hidden" class="textbox" name="default_latitude" value="' . $def_cordinate[0] . '">
                            <input type="hidden" class="textbox" name="default_longitude" value="' . $def_cordinate[1] . '">
                            <input type="hidden" class="textbox" name="default_pointer" value="//' . $site_url . '/lumonata-plugins/additional/images/pointer.png">
                            <div id="map" class="map"></div>
                        </div>
                    </div>
                </div>
            </div>
        </fieldset>';

        add_actions( 'other_elements', 'get_custom_javascript', '//maps.googleapis.com/maps/api/js?key=AIzaSyC9mKKb71I104S_XsT8jhw1DQ2FGqtLkds&callback=initmap', true, true );

        return $content;
    }
}

function get_packages_list_content( $i, $post_id, $apps )
{
    global $db;

    if( in_array( $apps, array( 'spa' ) ) )
    {
        $content = '
        <fieldset>
            <div class="additional_data">
                <h2>Packages List</h2>
                <div class="additional_content additional-packages-list">
                    <div class="boxs-wrapp">
                        <div class="boxs">';

                            $s = 'SELECT * FROM lumonata_rules WHERE lrule = %s AND lgroup = %s ORDER BY lorder';
                            $q = $db->prepare_query( $s, 'packages', $apps );
                            $r = $db->do_query( $q );

                            if( $db->num_rows( $r ) > 0 )
                            {
                                if( isset( $_POST[ 'additional_fields' ][ 'packages_list' ][ $i ] ) )
                                {
                                    $value = $_POST[ 'additional_fields' ][ 'packages_list' ][ $i ];
                                }
                                else
                                {
                                    $value = get_additional_field( $post_id, 'packages_list', $apps );
                                    $value = json_decode( $value, true );
                                }

                                $content  .= '
                                <select data-placeholder="Choose related post..." style="width:100%;" multiple class="chosen-select" name="additional_fields[packages_list][' . $i . '][]" autocomplete="off">';
                                   while( $d = $db->fetch_array( $r ) )
                                    {

                                        $check = is_array( $value ) && in_array( $d[ 'lrule_id' ], $value ) ? 'selected="selected"' : '';
                                        $content .= '<option value="' . $d[ 'lrule_id' ] . '" ' . $check . '>' . $d[ 'lname' ] . '</option>';
                                    }
                                    $content .= '
                                </select>';
                            }

                            $content .= '
                        </div>
                    </div>
                </div>
            </div>
        </fieldset>';

        return $content;
    }
}

function get_iconpicker_content( $i, $post_id, $apps )
{
    global $db;

    if( in_array( $apps, array( 'services' ) ) )
    {
        $value    = get_additional_field( $post_id, 'iconpicker', $apps );
        $plug_url = '//' . SITE_URL . '/lumonata-plugins/additional';
        $content  = '
        <fieldset>
            <div class="additional_data">
                <h2>Icon</h2>
                <div class="additional_content additional-iconpicker">
                    <div class="boxs-wrapp">
                        <div class="boxs">
                            <p>Default Icons</p>
                            <ul class="icon-default">
                                <li>
                                    <input type="radio" name="additional_fields[iconpicker][{i}]" class="iconpicker-input" value="ico-airplane" ' . ( $value == 'ico-airplane' ? 'checked' : '' ) . '>
                                    <img src="' . $plug_url . '/icons/airplane.svg">
                                </li>
                                <li>
                                    <input type="radio" name="additional_fields[iconpicker][{i}]" class="iconpicker-input" value="ico-barbeque" ' . ( $value == 'ico-barbeque' ? 'checked' : '' ) . '>
                                    <img src="' . $plug_url . '/icons/barbeque.svg">
                                </li>
                                <li>
                                    <input type="radio" name="additional_fields[iconpicker][{i}]" class="iconpicker-input" value="ico-car" ' . ( $value == 'ico-car' ? 'checked' : '' ) . '>
                                    <img src="' . $plug_url . '/icons/car.svg">
                                </li>
                                <li>
                                    <input type="radio" name="additional_fields[iconpicker][{i}]" class="iconpicker-input" value="ico-chef" ' . ( $value == 'ico-chef' ? 'checked' : '' ) . '>
                                    <img src="' . $plug_url . '/icons/chef.svg">
                                </li>
                                <li>
                                    <input type="radio" name="additional_fields[iconpicker][{i}]" class="iconpicker-input" value="ico-dinning-table" ' . ( $value == 'ico-dinning-table' ? 'checked' : '' ) . '>
                                    <img src="' . $plug_url . '/icons/dinning-table.svg">
                                </li>
                                <li>
                                    <input type="radio" name="additional_fields[iconpicker][{i}]" class="iconpicker-input" value="ico-doctor-stethoscope" ' . ( $value == 'ico-doctor-stethoscope' ? 'checked' : '' ) . '>
                                    <img src="' . $plug_url . '/icons/doctor-stethoscope.svg">
                                </li>
                                <li>
                                    <input type="radio" name="additional_fields[iconpicker][{i}]" class="iconpicker-input" value="ico-exchange" ' . ( $value == 'ico-exchange' ? 'checked' : '' ) . '>
                                    <img src="' . $plug_url . '/icons/exchange.svg">
                                </li>
                                <li>
                                    <input type="radio" name="additional_fields[iconpicker][{i}]" class="iconpicker-input" value="ico-key" ' . ( $value == 'ico-key' ? 'checked' : '' ) . '>
                                    <img src="' . $plug_url . '/icons/key.svg">
                                </li>
                                <li>
                                    <input type="radio" name="additional_fields[iconpicker][{i}]" class="iconpicker-input" value="ico-toasts" ' . ( $value == 'ico-toasts' ? 'checked' : '' ) . '>
                                    <img src="' . $plug_url . '/icons/toasts.svg">
                                </li>
                                <li>
                                    <input type="radio" name="additional_fields[iconpicker][{i}]" class="iconpicker-input" value="ico-tour-arrangement" ' . ( $value == 'ico-tour-arrangement' ? 'checked' : '' ) . '>
                                    <img src="' . $plug_url . '/icons/tour-arrangement.svg">
                                </li>
                                <li>
                                    <input type="radio" name="additional_fields[iconpicker][{i}]" class="iconpicker-input" value="ico-travel" ' . ( $value == 'ico-travel' ? 'checked' : '' ) . '>
                                    <img src="' . $plug_url . '/icons/travel.svg">
                                </li>
                                <li>
                                    <input type="radio" name="additional_fields[iconpicker][{i}]" class="iconpicker-input" value="ico-washing-machine" ' . ( $value == 'ico-washing-machine' ? 'checked' : '' ) . '>
                                    <img src="' . $plug_url . '/icons/washing-machine.svg">
                                </li>
                                <li>
                                    <input type="radio" name="additional_fields[iconpicker][{i}]" class="iconpicker-input" value="ico-wifi" ' . ( $value == 'ico-wifi' ? 'checked' : '' ) . '>
                                    <img src="' . $plug_url . '/icons/wifi.svg">
                                </li>
                            </ul>

                            <p>Use Another Icons</p>
                            <div class="btn-group">
                                <button data-selected="graduation-cap" type="button" class="icp icp-dd btn btn-default dropdown-toggle iconpicker-component" data-toggle="dropdown">
                                    Select the icon for your list <i class="fa fa-fw"></i>
                                    <span class="caret"></span>
                                </button>
                                <div class="dropdown-menu"></div>
                            </div>
                            <div class="clearfix">
                                <input type="radio" name="additional_fields[iconpicker][{i}]" class="sr-only iconpicker-input iconpicker-input-fa" value="' . ( strpos( $value, 'fa-' ) === false ? 'fa fa-graduation-cap' : $value ) . '" ' . ( strpos( $value, 'fa-' ) === false ? '' : 'checked' ) . '>
                                <p class="lead">
                                    <i class="' . ( strpos( $value, 'fa-' ) === false ? 'fa fa-graduation-cap' : $value ) . ' fa-2x picker-target"></i>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </fieldset>';

        add_actions( 'css_elements', 'get_custom_css', '//cdnjs.cloudflare.com/ajax/libs/fontawesome-iconpicker/3.2.0/css/fontawesome-iconpicker.min.css' );
        add_actions( 'css_elements', 'get_custom_css', '//stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css' );
        add_actions( 'css_elements', 'get_custom_css', '//use.fontawesome.com/releases/v5.5.0/css/all.css' );

        add_actions( 'js_elements', 'get_custom_javascript', '//cdnjs.cloudflare.com/ajax/libs/fontawesome-iconpicker/3.2.0/js/fontawesome-iconpicker.min.js' );
        add_actions( 'js_elements', 'get_custom_javascript', '//cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js' );
        add_actions( 'js_elements', 'get_custom_javascript', '//stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js' );

        return $content;
    }
}

function get_reviews_content( $i, $post_id, $apps )
{
    global $db;

    if( $apps == 'reviews' )
    {
        $review_by     = isset( $_POST[ 'additional_fields' ][ 'review_by' ][ $i ] ) ? $_POST[ 'additional_fields' ][ 'review_by' ][ $i ] : get_additional_field( $post_id, 'review_by', $apps );
        $review_rate   = isset( $_POST[ 'additional_fields' ][ 'review_rate' ][ $i ] ) ? $_POST[ 'additional_fields' ][ 'review_rate' ][ $i ] : get_additional_field( $post_id, 'review_rate', $apps );
        $review_link   = isset( $_POST[ 'additional_fields' ][ 'review_link' ][ $i ] ) ? $_POST[ 'additional_fields' ][ 'review_link' ][ $i ] : get_additional_field( $post_id, 'review_link', $apps );
        $review_target = isset( $_POST[ 'additional_fields' ][ 'review_target' ][ $i ] ) ? $_POST[ 'additional_fields' ][ 'review_target' ][ $i ] : get_additional_field( $post_id, 'review_target', $apps );

        $content = '
        <fieldset>
            <div class="additional_data">
                <h2>Tripadvisor Reviews</h2>
                <div class="additional_content additional-reviews">
                    <div class="boxs-wrapp">
                        <div class="boxs">
                            <p>Review By : </p>
                            <input type="text" class="textbox loc-review-by" name="additional_fields[review_by][' . $i . ']" value="' . $review_by . '">
                        </div>
                        <div class="boxs">
                            <p>Readmore Link : </p>
                            <input type="text" class="textbox loc-review-link" name="additional_fields[review_link][' . $i . ']" value="' . $review_link . '">
                        </div>
                        <div class="boxs">
                            <p>Link Target : </p>
                            <select name="additional_fields[review_target][' . $i . ']">
                                <option value="_self" ' . ( $review_target == '_self' ? 'checked' : '' ) . '>Same Window</option>
                                <option value="_blank" ' . ( $review_target == '_black' ? 'checked' : '' ) . '>New Window</option>
                            </select>
                        </div>
                        <div class="boxs clearfix">
                            <p>Star Rate : </p>
                            <div class="rating">
                                <input type="radio" id="star5" name="additional_fields[review_rate][' . $i . ']" value="5" ' . ( $review_rate == 5 ? 'checked' : '' ) . ' /><label class = "full" for="star5" title="Awesome - 5 stars"></label>
                                <input type="radio" id="star4half" name="additional_fields[review_rate][' . $i . ']" value="4.5" ' . ( $review_rate == 4.5 ? 'checked' : '' ) . ' /><label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
                                <input type="radio" id="star4" name="additional_fields[review_rate][' . $i . ']" value="4" ' . ( $review_rate == 4 ? 'checked' : '' ) . ' /><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
                                <input type="radio" id="star3half" name="additional_fields[review_rate][' . $i . ']" value="3.5" ' . ( $review_rate == 3.5 ? 'checked' : '' ) . ' /><label class="half" for="star3half" title="Meh - 3.5 stars"></label>
                                <input type="radio" id="star3" name="additional_fields[review_rate][' . $i . ']" value="3" ' . ( $review_rate == 3 ? 'checked' : '' ) . ' /><label class = "full" for="star3" title="Meh - 3 stars"></label>
                                <input type="radio" id="star2half" name="additional_fields[review_rate][' . $i . ']" value="2.5" ' . ( $review_rate == 2.5 ? 'checked' : '' ) . ' /><label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
                                <input type="radio" id="star2" name="additional_fields[review_rate][' . $i . ']" value="2" ' . ( $review_rate == 2 ? 'checked' : '' ) . ' /><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
                                <input type="radio" id="star1half" name="additional_fields[review_rate][' . $i . ']" value="1.5" ' . ( $review_rate == 1.5 ? 'checked' : '' ) . ' /><label class="half" for="star1half" title="Meh - 1.5 stars"></label>
                                <input type="radio" id="star1" name="additional_fields[review_rate][' . $i . ']" value="1" ' . ( $review_rate == 1 ? 'checked' : '' ) . ' /><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
                                <input type="radio" id="starhalf" name="additional_fields[review_rate][' . $i . ']" value="0.5" ' . ( $review_rate == 0.5 ? 'checked' : '' ) . ' /><label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </fieldset>';

        add_actions( 'css_elements', 'get_custom_css', '//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css' );

        return $content;
    }
}

function get_custom_field_content( $i, $post_id, $apps )
{
    global $db;

    $content  = get_reviews_content( $i, $post_id, $apps );
    $content .= get_amenities_content( $i, $post_id, $apps );
    $content .= get_locations_content( $i, $post_id, $apps );
    $content .= get_iconpicker_content( $i, $post_id, $apps );
    $content .= get_packages_list_content( $i, $post_id, $apps );
    $content .= get_gallery_image_content( $i, $post_id, $apps );
    $content .= get_masking_img_box_content( $i, $post_id, $apps );
    $content .= get_rooms_box_image_content( $i, $post_id, $apps );

    if( $apps == 'pages' )
    {
        //-- Parent Option List
        $option = array();
        $result = fetch_articles( 'type=pages', false );

        $option[0] = 'No Parent';

        while ( $d = $db->fetch_array( $result ) )
        {
            if( $d['larticle_id'] != $post_id )
            {
                $option[ $d['larticle_id'] ] = $d['larticle_title'];
            }
        }

        $param = array(
            'name' => 'parent',
            'label' => 'Parent Page',
            'multiple' => false,
            'option' => $option
        );
    }

    return $content;
}

function get_select_list_field( $i, $post_id, $apps, $param = array() )
{
    $data = get_additional_field( $post_id, $param['name'], $apps );

    $content = '
    <fieldset>
        <div class="additional_data additional-select-wrapp">
            <h2>' . $param['label'] . '</h2>
            <div class="additional_content">
                <div class="boxs-wrapp">
                    <div class="boxs">' .
                        ( isset( $param['desc'] ) ? '<p>' . $param['desc'] . '<p>' : '' );

                        if( $param['multiple'] )
                        {
                            $data     = json_decode( $data, true );
                            $data     = is_array( $data ) ? implode(',', $data) : $data;
                            $content .= '
                            <input id="' . $param['name'] . '_text_' .  $i . '" type="hidden" name="additional_fields[' . $param['name'] . '][' . $i . '][]" value="' . $data . '" />
                            <a id="' . $param['name'] . '_trigger_' .  $i . '"></a>
                            <select id="' . $param['name'] . '_' .  $i . '" class="select-opt multiple" autocomplete="off" multiple>';

                                foreach( $param['option'] as $val => $label )
                                {
                                   $content .= '<option value="' . $val . '">' . $label . '</option>';
                                }

                                $content .= '
                            </select>
                            <script type="text/javascript">
                                jQuery("#' . $param['name'] . '_' .  $i . '").chosen({ disable_search_threshold:10 });

                                var select  = $("#' . $param['name'] . '_' .  $i . '").get(0);
                                var textval = jQuery("#' . $param['name'] . '_text_' .  $i . '").val().split(",");

                                ChosenOrder.setSelectionOrder(select, textval, true);

                                jQuery("#' . $param['name'] . '_' .  $i . '").change(function(e){
                                    setTimeout(function(){
                                        var selection = ChosenOrder.getSelectionOrder(select);
                                        var dataval   = selection.join(",");

                                        jQuery("#' . $param['name'] . '_text_' .  $i . '").val(dataval);
                                    }, 100);
                                });
                            </script>';
                        }
                        else
                        {
                            $content .= '
                            <select id="' . $param['name'] . '_' .  $i . '" class="select-opt single" name="additional_fields[' . $param['name'] . '][' . $i . ']" autocomplete="off">';

                                foreach( $param['option'] as $val => $label )
                                {
                                   $content .= '<option value="' . $val . '" ' . ( $val == $data ? 'selected' : '' ) . ' >' . $label . '</option>';
                                }

                                $content .= '
                            </select>
                            <script type="text/javascript">
                                jQuery("#' . $param['name'] . '_' .  $i . '").chosen({ disable_search_threshold:10 });
                            </script>';
                        }

                        $content .= '
                    </div>
                </div>
            </div>
        </div>
    </fieldset>';

    return $content;
}

function get_textbox_field( $i, $post_id, $apps, $param = array() )
{
    foreach( $param as $name => $title )
    {
        $data = get_additional_field( $post_id, $name, $apps );

        return '
        <fieldset>
            <div class="additional_data">
                <h2>' . $title . '</h2>
                <div class="additional_content">
                    <div class="boxs-wrapp">
                        <div class="boxs">
                            <input type="text" class="textbox" name="additional_fields[' . $name . '][' . $i . ']" autocomplete="off" value="' . $data . '" />
                        </div>
                    </div>
                </div>
            </div>
        </fieldset>';
    }
}

function get_textarea_field( $i, $post_id, $apps, $param = array() )
{
    foreach( $param as $name => $title )
    {
        $data = get_additional_field( $post_id, $name, $apps );

        return '
        <fieldset>
            <div class="additional_data">
                <h2>' . $title . '</h2>
                <div class="additional_content">
                    <div class="boxs-wrapp">
                        <div class="boxs">
                            <textarea class="textarea" name="additional_fields[' . $name . '][' . $i . ']" autocomplete="off" rows="10">' . $data . '</textarea>
                        </div>
                    </div>
                </div>
            </div>
        </fieldset>';
    }
}

function get_tinymce_field( $i, $post_id, $apps, $param = array() )
{
    foreach( $param as $name => $title )
    {
        $data = get_additional_field( $post_id, $name, $apps );

        return '
        <fieldset>
            <div class="additional_data">
                <h2>' . $title . '</h2>
                <div class="additional_content">
                    <div class="boxs-wrapp">
                        <div class="boxs">
                            <textarea class="textarea tinymce" name="additional_fields[' . $name . '][' . $i . ']" autocomplete="off">' . $data . '</textarea>
                        </div>
                    </div>
                </div>
            </div>
        </fieldset>';
    }
}

function get_group_field( $i, $post_id, $apps, $param = array() )
{
    if( is_array( $param ) && !empty( $param ) )
    {
        $content = '
        <fieldset>
            <div class="additional_data">
                <h2>' . $param['group'] . '</h2>
                <div class="additional_content">
                    <div class="boxs-wrapp">
                        <div class="boxs">';

                        foreach( $param['field'] as $obj )
                        {
                            $data = get_additional_field( $post_id, $obj['name'], $apps );

                            if( $obj['type'] == 'textbox' )
                            {
                                $field = '<input type="text" class="textbox" name="additional_fields[' . $obj['name'] . '][' . $i . ']" autocomplete="off" value="' . $data . '" />';
                            }
                            else
                            {
                                $field = '<textarea class="textarea" name="additional_fields[' . $obj['name'] . '][' . $i . ']" autocomplete="off" rows="10">' . $data . '</textarea>';
                            }

                            $content .= '<p>' . $obj['label'] . '<br></br>' . $field . '</p>';
                        }

                        $content .= '
                        </div>
                    </div>
                </div>
            </div>
        </fieldset>';

        return $content;
    }
}

function get_rooms_box_image_content( $i, $post_id, $apps )
{
    if( in_array( $apps, array( 'rooms' ) ) )
    {
        return '
        <fieldset>
            <div class="additional_data">
                <h2>Content Image</h2>
                <div class="additional_content additional-rooms-img">
                    <h3>Select an image file on your computer (2MB max):</h3>
                    <div class="clearfix">
                        <div class="slider">
                            <div class="rooms-img clearfixed">' . get_rooms_image_content( $post_id, $apps . '-content-image' ) . '</div>
                            <input type="file" class="rooms-img-file" data-app-name="' . $apps . '-content-image" name="rooms_content_image" autocomplete="off" />
                            <span>Recommended Size : 1024px * 768px</span>
                        </div>
                    </div>
                </div>
            </div>
        </fieldset>';
    }
}

function get_rooms_image( $post_id, $app_name )
{
    global $db;

    $img  = get_attachment_by_app_name( $post_id, $app_name, false );
    $surl = SITE_URL;
    $data = array();

    if( empty( $img ) === false )
    {
        $data = array(
            'post_id'   => $post_id,
            'app_name'  => $app_name,
            'img_url'   => '//' . $surl . $img['lattach_loc'],
            'img_thumb' => '//' . $surl . '/lumonata-functions/mthumb.php?src=' . HTSERVER . '//' . $surl . $img['lattach_loc'] . '&w=300'
        );
    }

    return $data;
}

function get_rooms_image_content( $post_id, $app_name )
{
    $data = get_rooms_image( $post_id, $app_name );
    $surl = SITE_URL;

    if( empty( $data ) === false )
    {
        return '
        <div class="box">
            <img src="' . $data['img_thumb'] . '" />
            <div class="overlay">
                <a data-app-id="' . $data['post_id'] . '" data-app-name="' . $data['app_name'] . '" class="img-delete" title="Delete">
                    <img src="//' . $surl . '/lumonata-plugins/additional/images/delete.png">
                </a>
            </div>
        </div>';
    }
}

function get_gallery_image_content( $i, $post_id, $apps )
{
    $surl = SITE_URL;

    return '
    <fieldset>
        <div class="additional_data">
            <h2>Gallery Image</h2>
            <div class="additional_content additional-gallery-data">
                <div id="block-gallery" class="block-gallery">
                    <div id="dropzone">
                        <div class="dropzone needsclick dz-clickable" id="gallery-upload">
                            <div class="dz-message needsclick">
                                Drop files here or click to upload.<br>
                                <span class="note needsclick">( maximum size for upload image is 2MB )</span>
                            </div>
                        </div>
                    </div>

                    <div id="preview-template" class="dz-preview dz-file-preview" style="display:none;">
                        <div class="dz-preview dz-file-preview">
                            <div class="dz-image"><img data-dz-thumbnail data-dz-thumbnail-id/></div>
                            <div class="dz-details">
                                <a class="dz-edit">
                                    <img src="//' . $surl . '/lumonata-plugins/additional/images/edit.png" />
                                </a>
                                <a class="dz-delete">
                                    <img src="//' . $surl . '/lumonata-plugins/additional/images/delete.png" />
                                </a>
                            </div>
                            <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
                            <div class="dz-detail-box">
                                <div class="detail">
                                    <fieldset>
                                        <p>Image Title</p>
                                        <input type="text" name="gallery_title" value="" class="textbox" />
                                    </fieldset>
                                    <fieldset>
                                        <p>Image Link</p>
                                        <input type="text" name="gallery_alt" value="" class="textbox" />
                                    </fieldset>
                                    <fieldset>
                                        <p>Image Caption</p>
                                        <input type="text" name="gallery_caption" value="" class="textbox" />
                                    </fieldset>
                                    <fieldset>
                                        <p>Image Content</p>
                                        <textarea name="gallery_content" cols="95" rows="20" class="textbox gtiny_mce" autocomplete="off"></textarea>
                                    </fieldset>
                                    <fieldset>
                                        <input type="button" class="save-detail" value="Save"/>
                                        <input type="button" class="close-detail" value="Close"/>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <p class="note">Recommended resolution for sideshow image is <b>1024px * 768px</b>, with maximum upload size <b>2MB</b></p>
            </div>
        </div>
    </fieldset>';
}

function get_masking_img_box_content( $i, $post_id, $apps )
{
    if( in_array( $apps, array( 'restaurant', 'spa' ) ) )
    {
        $masking_img_1 = get_masking_image_content( $post_id, $apps . '-masking-img-1' );
        $masking_img_2 = get_masking_image_content( $post_id, $apps . '-masking-img-2' );
        $masking_img_3 = get_masking_image_content( $post_id, $apps . '-masking-img-3' );
        $masking_img_4 = get_masking_image_content( $post_id, $apps . '-masking-img-4' );

        return '
        <fieldset>
            <div class="additional_data">
                <h2>Masking Image Box</h2>
                <div class="additional_content additional-masking-img">
                    <h3>Select an image file on your computer (2MB max):</h3>
                    <div class="clearfix">
                        <div class="slider">
                            <h4>Masking I</h4>
                            <div class="masking-img clearfixed">' . $masking_img_1 . '</div>
                            <input type="file" class="masking-img-file" data-app-name="' . $apps . '-masking-img-1" name="masking_img_1" autocomplete="off" />
                            <span>Recommended Size : 260px * 215px</span>
                        </div>
                        <div class="slider">
                            <h4>Masking II</h4>
                            <div class="masking-img clearfixed">' . $masking_img_2 . '</div>
                            <input type="file" class="masking-img-file" data-app-name="' . $apps . '-masking-img-2" name="masking_img_2" autocomplete="off" />
                            <span>Recommended Height Size : 500px</span>
                        </div>
                        <div class="slider">
                            <h4>Masking III</h4>
                            <div class="masking-img clearfixed">' . $masking_img_3 . '</div>
                            <input type="file" class="masking-img-file" data-app-name="' . $apps . '-masking-img-3" name="masking_img_3" autocomplete="off" />
                            <span>Recommended Size : 203px * 135px</span>
                        </div>
                        <div class="slider">
                            <h4>Masking IV</h4>
                            <div class="masking-img clearfixed">' . $masking_img_4 . '</div>
                            <input type="file" class="masking-img-file" data-app-name="' . $apps . '-masking-img-4" name="masking_img_4" autocomplete="off" />
                            <span>Recommended Size : 227px * 150px</span>
                        </div>
                    </div>
                </div>
            </div>
        </fieldset>';
    }
}

function get_masking_image( $post_id, $app_name )
{
    global $db;

    $mask = get_attachment_by_app_name( $post_id, $app_name, false );
    $surl = SITE_URL;
    $data = array();

    if( empty( $mask ) === false )
    {
        $data = array(
            'post_id'   => $post_id,
            'app_name'  => $app_name,
            'img_url'   => '//' . $surl . $mask['lattach_loc'],
            'img_thumb' => '//' . $surl . '/lumonata-functions/mthumb.php?src=' . HTSERVER . '//' . $surl . $mask['lattach_loc'] . '&w=300'
        );
    }

    return $data;
}

function get_masking_image_content( $post_id, $app_name )
{
    $data = get_masking_image( $post_id, $app_name );
    $surl = SITE_URL;

    if( empty( $data ) === false )
    {
        return '
        <div class="box">
            <img src="' . $data['img_thumb'] . '" />
            <div class="overlay">
                <a data-app-id="' . $data['post_id'] . '" data-app-name="' . $data['app_name'] . '" class="img-delete" title="Delete">
                    <img src="//' . $surl . '/lumonata-plugins/additional/images/delete.png">
                </a>
            </div>
        </div>';
    }
}

function get_additional_setting_hero_image( $app_name )
{
    global $db;

    $s = 'SELECT lmeta_id, lmeta_value FROM lumonata_meta_data WHERE lmeta_name = %s AND lapp_name = %s AND lapp_id = %d ORDER BY lmeta_id';
    $q = $db->prepare_query( $s, 'setting_hero_image', $app_name, 0 );
    $r = $db->do_query( $q );

    $data = array();
    $surl = SITE_URL;

    if( $db->num_rows( $r ) > 0 )
    {
        $d = $db->fetch_array( $r );

        $data = array(
            'img_id'    => $d['lmeta_id'],
            'img_url'   => '//' . $surl . $d['lmeta_value'],
            'img_thumb' => '//' . $surl . '/lumonata-functions/mthumb.php?src=' . HTSERVER . '//' . $surl . $d['lmeta_value'] . '&w=300'
        );
    }

    return $data;
}

function get_additional_setting_hero_image_content( $app_name )
{
    $data = get_additional_setting_hero_image( $app_name );
    $surl = SITE_URL;

    if( empty( $data ) === false )
    {
        return '
        <div class="box">
            <img src="' . $data['img_thumb'] . '" />
            <div class="overlay">
                <a data-meta-id="' . $data['img_id'] . '" class="img-delete" title="Delete">
                    <img src="//' . $surl . '/lumonata-plugins/additional/images/delete.png">
                </a>
            </div>
        </div>';
    }
}

function upload_setting_hero_images()
{
    $data   = array();
    $folder = upload_folder_name();

    if( isset( $_FILES[ 'hero_img' ]['error'] ) && $_FILES[ 'hero_img' ]['error'] == 0 )
    {
        if( !defined( 'FILES_LOCATION' ) )
        {
            define( 'FILES_LOCATION', '/lumonata-content/files' );
        }

        if( is_dir( FILES_PATH .'/'. $folder ) === FALSE )
        {
            if( create_dir( FILES_PATH .'/'. $folder ) === FALSE )
            {
                return $data;
            }
        }

        $file_name   = $_FILES[ 'hero_img' ][ 'name' ];
        $file_size   = $_FILES[ 'hero_img' ][ 'size' ];
        $file_type   = $_FILES[ 'hero_img' ][ 'type' ];
        $file_source = $_FILES[ 'hero_img' ][ 'tmp_name' ];

        extract( $_POST );

        if( is_allow_file_type( $file_type, 'image' ) )
        {
            if( is_allow_file_size( $file_size ) )
            {
                $default_title   = file_name_filter( $file_name );
                $file_name       = character_filter( $file_name );
                $file_name       = file_name_filter( $file_name ) . '-' . time() . file_name_filter( $file_name, true );

                //-- Resize Image
                $file_location    = FILES_LOCATION . '/' . $folder . '/' . $file_name;
                $file_destination = FILES_PATH . '/' . $folder . '/' . $file_name;

                if( upload_resize( $file_source, $file_destination, $file_type, 1920, 1920 ) )
                {
                    $meta_id = set_meta_data( 'setting_hero_image', $file_location, $app_name );

                    if( $meta_id )
                    {
                        return $meta_id;
                    }
                }
            }
        }
    }
}

function delete_setting_hero_images( $meta_id )
{
    $image_loc = get_meta_data_by_id( $meta_id );

    if( file_exists( ROOT_PATH . $image_loc ) && !empty( $image_loc ) )
    {
        unlink( ROOT_PATH . $image_loc );
    }

    if( delete_meta_data_by_id( $meta_id ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function upload_masking_images()
{
    global $db;

    $folder = upload_folder_name();

    if( isset( $_FILES[ 'masking_img' ]['error'] ) && $_FILES[ 'masking_img' ]['error'] == 0 )
    {
        if( !defined( 'FILES_LOCATION' ) )
        {
            define( 'FILES_LOCATION', '/lumonata-content/files' );
        }

        if( is_dir( FILES_PATH .'/'. $folder ) === FALSE )
        {
            if( create_dir( FILES_PATH .'/'. $folder ) === FALSE )
            {
                return $data;
            }
        }

        $file_name   = $_FILES[ 'masking_img' ][ 'name' ];
        $file_size   = $_FILES[ 'masking_img' ][ 'size' ];
        $file_type   = $_FILES[ 'masking_img' ][ 'type' ];
        $file_source = $_FILES[ 'masking_img' ][ 'tmp_name' ];

        extract( $_POST );

        if( is_allow_file_type( $file_type, 'image' ) )
        {
            if( is_allow_file_size( $file_size ) )
            {
                $default_title   = file_name_filter( $file_name );
                $file_name       = character_filter( $file_name );
                $file_name       = file_name_filter( $file_name ) . '-' . time() . file_name_filter( $file_name, true );

                //-- Resize Image
                $file_location    = FILES_LOCATION . '/' . $folder . '/' . $file_name;
                $file_destination = FILES_PATH . '/' . $folder . '/' . $file_name;

                if( upload_resize( $file_source, $file_destination, $file_type, 500, 500 ) )
                {
                    if( insert_attachment( $post_id, $file_name, $file_type, $file_location, null, null, null, null, null, null, $app_name ) )
                    {
                        $att_id = $db->insert_id();

                        if( empty( $att_id ) )
                        {
                            //-- Delete File Image
                            if( file_exists( $file_destination ) )
                            {
                                unlink( $file_destination );
                            }
                        }

                        return true;
                    }
                }
            }
        }
    }
}

function delete_masking_images( $post_id, $app_name )
{
    global $db;

    $d = get_attachment_by_app_name( $post_id, $app_name, false );

    if( empty( $d['lattach_id'] ) === false )
    {
        if( delete_attachment( $d['lattach_id'] ) )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}

function upload_rooms_images()
{
    global $db;

    $folder = upload_folder_name();

    if( isset( $_FILES[ 'rooms_img' ]['error'] ) && $_FILES[ 'rooms_img' ]['error'] == 0 )
    {
        if( !defined( 'FILES_LOCATION' ) )
        {
            define( 'FILES_LOCATION', '/lumonata-content/files' );
        }

        if( is_dir( FILES_PATH .'/'. $folder ) === FALSE )
        {
            if( create_dir( FILES_PATH .'/'. $folder ) === FALSE )
            {
                return $data;
            }
        }

        $file_name   = $_FILES[ 'rooms_img' ][ 'name' ];
        $file_size   = $_FILES[ 'rooms_img' ][ 'size' ];
        $file_type   = $_FILES[ 'rooms_img' ][ 'type' ];
        $file_source = $_FILES[ 'rooms_img' ][ 'tmp_name' ];

        extract( $_POST );

        if( is_allow_file_type( $file_type, 'image' ) )
        {
            if( is_allow_file_size( $file_size ) )
            {
                $default_title   = file_name_filter( $file_name );
                $file_name       = character_filter( $file_name );
                $file_name       = file_name_filter( $file_name ) . '-' . time() . file_name_filter( $file_name, true );

                //-- Resize Image
                $file_location    = FILES_LOCATION . '/' . $folder . '/' . $file_name;
                $file_destination = FILES_PATH . '/' . $folder . '/' . $file_name;

                if( upload_resize( $file_source, $file_destination, $file_type, 1024, 1024 ) )
                {
                    if( insert_attachment( $post_id, $file_name, $file_type, $file_location, null, null, null, null, null, null, $app_name ) )
                    {
                        $att_id = $db->insert_id();

                        if( empty( $att_id ) )
                        {
                            //-- Delete File Image
                            if( file_exists( $file_destination ) )
                            {
                                unlink( $file_destination );
                            }
                        }

                        return true;
                    }
                }
            }
        }
    }
}

function delete_rooms_images( $post_id, $app_name )
{
    global $db;

    $d = get_attachment_by_app_name( $post_id, $app_name, false );

    if( empty( $d['lattach_id'] ) === false )
    {
        if( delete_attachment( $d['lattach_id'] ) )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}

function run_additional_ajax_request()
{
    global $db;

    if( isset( $_POST['ajax_key'] ) && $_POST['ajax_key'] == 'upload-setting-hero-images' )
    {
        $meta_id = upload_setting_hero_images();

        if( empty( $meta_id ) )
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'success', 'content' => get_additional_setting_hero_image_content( $_POST['app_name'] ) ) );
        }
    }

    if( $_POST[ 'ajax_key' ] == 'delete-setting-hero-images' )
    {
        if( delete_setting_hero_images( $_POST['meta_id'] ) )
        {
            echo json_encode( array( 'result' => 'success' ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
    }

    if( isset( $_POST['ajax_key'] ) && $_POST['ajax_key'] == 'upload-rooms-images' )
    {
        if( upload_rooms_images() )
        {
            echo json_encode( array( 'result' => 'success', 'content' => get_rooms_image_content( $_POST['post_id'], $_POST['app_name'] ) ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
    }

    if( $_POST[ 'ajax_key' ] == 'delete-rooms-images' )
    {
        if( delete_rooms_images( $_POST['post_id'], $_POST['app_name'] ) )
        {
            echo json_encode( array( 'result' => 'success' ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
    }

    if( isset( $_POST['ajax_key'] ) && $_POST['ajax_key'] == 'upload-masking-images' )
    {
        if( upload_masking_images() )
        {
            echo json_encode( array( 'result' => 'success', 'content' => get_masking_image_content( $_POST['post_id'], $_POST['app_name'] ) ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
    }

    if( $_POST[ 'ajax_key' ] == 'delete-masking-images' )
    {
        if( delete_masking_images( $_POST['post_id'], $_POST['app_name'] ) )
        {
            echo json_encode( array( 'result' => 'success' ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
    }

    if( isset( $_POST['ajax_key'] ) && $_POST['ajax_key'] == 'upload-featured-images' )
    {
        if( isset( $_FILES['featured_img'] ) && $_FILES['featured_img']['error']==0 )
        {
            $post_id     = $_POST['post_id'];
            $app_name    = $_POST['app_name'];
            $file_name   = $_FILES['featured_img']['name'];
            $file_size   = $_FILES['featured_img']['size'];
            $file_type   = $_FILES['featured_img']['type'];
            $file_source = $_FILES['featured_img']['tmp_name'];

            if( is_allow_file_size( $file_size ) )
            {
                if( is_allow_file_type( $file_type, 'image' ) )
                {
                    //-- CHECK FOLDER EXIST
                    $folder = upload_folder_name();

                    if( !is_dir( FILES_PATH . '/' . $folder ) )
                    {
                        create_dir( FILES_PATH . '/' . $folder );
                    }

                    $ext    = file_name_filter( $file_name, true );
                    $name   = file_name_filter( $file_name, false ) . '-' . time();
                    $name_t = $name . '-thumbnail' . $ext;
                    $name_m = $name . '-medium' . $ext;
                    $name_l = $name . '-large' . $ext;
                    $name_f = $name . $ext;

                    $ldestination = FILES_PATH . '/' . $folder . '/' . $name_l;
                    $mdestination = FILES_PATH . '/' . $folder . '/' . $name_m;
                    $tdestination = FILES_PATH . '/' . $folder . '/' . $name_t;
                    $fdestination = FILES_PATH . '/' . $folder . '/' . $name_f;

                    if( upload_resize( $file_source, $ldestination, $file_type, 1920, 1920 ) )
                    {
                        if( upload_resize( $file_source, $mdestination, $file_type, 1024, 1024 ) )
                        {
                            if( upload_crop( $file_source, $tdestination, $file_type, 550, 550 ) )
                            {
                                if( upload( $file_source, $fdestination ) )
                                {
                                    $loc        = '/lumonata-content/files/' . $folder . '/' . $name_f;
                                    $loc_thumb  = '/lumonata-content/files/' . $folder . '/' . $name_t;
                                    $loc_medium = '/lumonata-content/files/' . $folder . '/' . $name_m;
                                    $loc_large  = '/lumonata-content/files/' . $folder . '/' . $name_l;

                                    $d = get_attachment_by_app_name( $post_id, 'featured-image-' . $app_name, false );

                                    if( empty( $d ) )
                                    {
                                        if( insert_attachment( $post_id, $name, $file_type, $loc, $loc_thumb, $loc_medium, $loc_large, null, null, null, 'featured-image-' . $app_name ) )
                                        {
                                            echo json_encode( array( 'result' => 'success', 'post_id' => $post_id, 'filename' => $name_m, 'fileurl' => '//' . SITE_URL . $loc_medium ) );
                                        }
                                        else
                                        {
                                            //-- Delete Original Image
                                            if( file_exists( $fdestination ) )
                                            {
                                                unlink( $fdestination );
                                            }

                                            //-- Delete Thumb Image
                                            if( file_exists( $tdestination ) )
                                            {
                                                unlink( $tdestination );
                                            }

                                            //-- Delete Medium Image
                                            if( file_exists( $mdestination ) )
                                            {
                                                unlink( $mdestination );
                                            }

                                            //-- Delete Large Image
                                            if( file_exists( $ldestination ) )
                                            {
                                                unlink( $ldestination );
                                            }

                                            echo json_encode( array( 'result' => 'failed', 'error' => 'Error uploading file' ) );
                                        }
                                    }
                                    else
                                    {
                                        if( update_attachment( $d['lattach_id'], $post_id, $name, $file_type, $loc, $loc_thumb, $loc_medium, $loc_large, null, null, null, 'featured-image-' . $app_name ) )
                                        {
                                            //-- Delete Original Image
                                            if( !empty( $d[ 'lattach_loc' ] ) && file_exists( ROOT_PATH . $d[ 'lattach_loc' ] ) )
                                            {
                                                unlink( ROOT_PATH . $d[ 'lattach_loc' ] );
                                            }

                                            //-- Delete Thumb Image
                                            if( !empty( $d[ 'lattach_loc_thumb' ] ) && file_exists( ROOT_PATH . $d[ 'lattach_loc_thumb' ] ) )
                                            {
                                                unlink( ROOT_PATH . $d[ 'lattach_loc_thumb' ] );
                                            }

                                            //-- Delete Medium Image
                                            if( !empty( $d[ 'lattach_loc_medium' ] ) && file_exists( ROOT_PATH . $d[ 'lattach_loc_medium' ] ) )
                                            {
                                                unlink( ROOT_PATH . $d[ 'lattach_loc_medium' ] );
                                            }

                                            //-- Delete Large Image
                                            if( !empty( $d[ 'lattach_loc_large' ] ) && file_exists( ROOT_PATH . $d[ 'lattach_loc_large' ] ) )
                                            {
                                                unlink( ROOT_PATH . $d[ 'lattach_loc_large' ] );
                                            }

                                            echo json_encode( array( 'result' => 'success', 'post_id' => $post_id, 'filename' => $name_m, 'fileurl' => '//' . SITE_URL . $loc_medium ) );
                                        }
                                        else
                                        {
                                            echo json_encode( array( 'result' => 'success', 'post_id' => $post_id, 'filename' => $name_m, 'fileurl' => '//' . SITE_URL . $d[ 'lattach_loc_medium' ] ) );
                                        }
                                    }
                                }
                                else
                                {
                                    //-- Delete Thumb Image
                                    if( file_exists( $tdestination ) )
                                    {
                                        unlink( $tdestination );
                                    }

                                    //-- Delete Medium Image
                                    if( file_exists( $mdestination ) )
                                    {
                                        unlink( $mdestination );
                                    }

                                    //-- Delete Large Image
                                    if( file_exists( $ldestination ) )
                                    {
                                        unlink( $ldestination );
                                    }

                                    echo json_encode( array( 'result' => 'failed', 'error' => 'Error uploading file' ) );
                                }
                            }
                            else
                            {
                                //-- Delete Medium Image
                                if( file_exists( $mdestination ) )
                                {
                                    unlink( $mdestination );
                                }

                                //-- Delete Large Image
                                if( file_exists( $ldestination ) )
                                {
                                    unlink( $ldestination );
                                }

                                echo json_encode( array( 'result' => 'failed', 'error' => 'Error uploading file' ) );
                            }
                        }
                        else
                        {
                            //-- Delete Large Image
                            if( file_exists( $ldestination ) )
                            {
                                unlink( $ldestination );
                            }

                            echo json_encode( array( 'result' => 'failed', 'error' => 'Error uploading file' ) );
                        }
                    }
                    else
                    {
                        echo json_encode( array( 'result' => 'failed', 'error' => 'Error uploading file' ) );
                    }
                }
                else
                {
                    echo json_encode( array( 'result' => 'failed', 'error' => 'File is not an image' ) );
                }
            }
            else
            {
                echo json_encode( array( 'result' => 'failed', 'error' => 'Maximum file size is 2MB' ) );
            }
        }
        else
        {
            echo json_encode( array( 'result' => 'failed', 'error' => 'No image was found' ) );
        }
    }

    if( isset( $_POST['ajax_key'] ) && $_POST['ajax_key'] == 'delete-featured-images' )
    {
        if( empty( $_POST['post_id'] ) )
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
        else
        {
            $d = get_attachment_by_app_name( $_POST['post_id'], 'featured-image-' . $_POST['app_name'], false );

            if( delete_attachment( $d['lattach_id'] ) )
            {
                echo json_encode( array( 'result' => 'success' ) );
            }
            else
            {
                echo json_encode( array( 'result' => 'failed' ) );
            }
        }
    }

    if( isset( $_POST['ajax_key'] ) && $_POST['ajax_key'] == 'get-gallery-image' )
    {
        $s = 'SELECT * FROM lumonata_attachment WHERE larticle_id = %d AND lapp_name = %s AND mime_type LIKE %s ORDER BY lorder';
        $q = $db->prepare_query( $s, $_POST['post_id'], 'gallery-' . $_POST['app_name'], '%image%' );
        $r = $db->do_query( $q );

        $data = array();

        if( $db->num_rows( $r ) > 0 )
        {
            $site_url = SITE_URL;

            while( $d = $db->fetch_array( $r ) )
            {
                $filename = explode( '/', $d['lattach_loc_thumb'] );
                $filename = array_reverse( $filename );

                $data[] = array(
                    'img_id' => $d['lattach_id'],
                    'img_title' => $d['ltitle'],
                    'img_alt' => $d['lalt_text'],
                    'img_caption' => $d['lcaption'],
                    'img_content' => $d['lcontent'],
                    'img_thumb' => '//' . $site_url . $d['lattach_loc_thumb']
                );
            }
        }

        if( empty( $data ) )
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'success', 'data' => $data ) );
        }
    }
    
    if( isset( $_POST['ajax_key'] ) && $_POST['ajax_key'] == 'upload-gallery-image' )
    {
        if( isset( $_FILES['file']['name'] ) && $_FILES['file']['error'] == 0  )
        {
            $post_id     = $_POST['post_id'];
            $app_name    = $_POST['app_name'];
            $file_name   = $_FILES['file']['name'];
            $file_size   = $_FILES['file']['size'];
            $file_type   = $_FILES['file']['type'];
            $file_source = $_FILES['file']['tmp_name'];

            if( is_allow_file_size( $file_size ) )
            {
                if( is_allow_file_type( $file_type, 'image' ) )
                {
                    //-- CHECK FOLDER EXIST
                    $folder = upload_folder_name();

                    if( !is_dir( FILES_PATH . '/' . $folder ) )
                    {
                        create_dir( FILES_PATH . '/' . $folder );
                    }

                    $ext    = file_name_filter( $file_name, true );
                    $name   = file_name_filter( $file_name, false ) . '-' . time();
                    $name_t = $name . '-thumbnail' . $ext;
                    $name_m = $name . '-medium' . $ext;
                    $name_l = $name . '-large' . $ext;
                    $name_f = $name . $ext;

                    $ldestination = FILES_PATH . '/' . $folder . '/' . $name_l;
                    $mdestination = FILES_PATH . '/' . $folder . '/' . $name_m;
                    $tdestination = FILES_PATH . '/' . $folder . '/' . $name_t;
                    $fdestination = FILES_PATH . '/' . $folder . '/' . $name_f;

                    if( upload_resize( $file_source, $ldestination, $file_type, 1920, 1920 ) )
                    {
                        if( upload_resize( $file_source, $mdestination, $file_type, 1024, 1024 ) )
                        {
                            if( upload_crop( $file_source, $tdestination, $file_type, 550, 550 ) )
                            {
                                if( upload( $file_source, $fdestination ) )
                                {
                                    $loc        = '/lumonata-content/files/' . $folder . '/' . $name_f;
                                    $loc_thumb  = '/lumonata-content/files/' . $folder . '/' . $name_t;
                                    $loc_medium = '/lumonata-content/files/' . $folder . '/' . $name_m;
                                    $loc_large  = '/lumonata-content/files/' . $folder . '/' . $name_l;

                                    if( insert_attachment( $post_id, $file_name, $file_type, $loc, $loc_thumb, $loc_medium, $loc_large, null, null, null, 'gallery-' . $app_name ) )
                                    {
                                        $attach_id = $db->insert_id();

                                        if( empty( $attach_id ) )
                                        {
                                            //-- Delete Original Image
                                            if( file_exists( $fdestination ) )
                                            {
                                                unlink( $fdestination );
                                            }

                                            //-- Delete Thumb Image
                                            if( file_exists( $tdestination ) )
                                            {
                                                unlink( $tdestination );
                                            }

                                            //-- Delete Medium Image
                                            if( file_exists( $mdestination ) )
                                            {
                                                unlink( $mdestination );
                                            }

                                            //-- Delete Large Image
                                            if( file_exists( $ldestination ) )
                                            {
                                                unlink( $ldestination );
                                            }

                                            echo json_encode( array( 'result' => 'failed', 'message' => 'Something wrong!!' ) );
                                        }
                                        else
                                        {
                                            $s = 'SELECT * FROM lumonata_attachment WHERE lattach_id = %d';
                                            $q = $db->prepare_query( $s, $attach_id );
                                            $r = $db->do_query( $q );
                                            $d = $db->fetch_array( $r );

                                            echo json_encode(
                                                array(
                                                    'result'    => 'success',
                                                    'attach_id' => $attach_id,
                                                    'title'     => $d['ltitle'],
                                                    'content'   => $d['lcontent'],
                                                    'caption'   => $d['lcaption'],
                                                    'alt'       => $d['lalt_text']
                                                )
                                            );
                                        }
                                    }
                                    else
                                    {
                                        //-- Delete Original Image
                                        if( file_exists( $fdestination ) )
                                        {
                                            unlink( $fdestination );
                                        }

                                        //-- Delete Thumb Image
                                        if( file_exists( $tdestination ) )
                                        {
                                            unlink( $tdestination );
                                        }

                                        //-- Delete Medium Image
                                        if( file_exists( $mdestination ) )
                                        {
                                            unlink( $mdestination );
                                        }

                                        //-- Delete Large Image
                                        if( file_exists( $ldestination ) )
                                        {
                                            unlink( $ldestination );
                                        }

                                        echo json_encode( array( 'result' => 'failed', 'error' => 'Error uploading file' ) );
                                    }
                                }
                                else
                                {
                                    //-- Delete Thumb Image
                                    if( file_exists( $tdestination ) )
                                    {
                                        unlink( $tdestination );
                                    }

                                    //-- Delete Medium Image
                                    if( file_exists( $mdestination ) )
                                    {
                                        unlink( $mdestination );
                                    }

                                    //-- Delete Large Image
                                    if( file_exists( $ldestination ) )
                                    {
                                        unlink( $ldestination );
                                    }

                                    echo json_encode( array( 'result' => 'failed', 'error' => 'Error uploading file' ) );
                                }
                            }
                            else
                            {
                                //-- Delete Medium Image
                                if( file_exists( $mdestination ) )
                                {
                                    unlink( $mdestination );
                                }

                                //-- Delete Large Image
                                if( file_exists( $ldestination ) )
                                {
                                    unlink( $ldestination );
                                }

                                echo json_encode( array( 'result' => 'failed', 'error' => 'Error uploading file' ) );
                            }
                        }
                        else
                        {
                            //-- Delete Large Image
                            if( file_exists( $ldestination ) )
                            {
                                unlink( $ldestination );
                            }

                            echo json_encode( array( 'result' => 'failed', 'error' => 'Error uploading file' ) );
                        }
                    }
                    else
                    {
                        echo json_encode( array( 'result' => 'failed', 'error' => 'Error uploading file' ) );
                    }
                }
                else
                {
                    echo json_encode( array( 'result' => 'failed', 'message' => 'File is not an image' ) );
                }
            }
            else
            {
                echo json_encode( array( 'result' => 'failed', 'message' => 'Maximum file size is 2MB' ) );
            }
        }
        else
        {
            echo json_encode( array( 'result' => 'failed', 'error' => 'No image was found' ) );
        }
    }

    if( isset( $_POST['ajax_key'] ) && $_POST['ajax_key'] == 'edit-gallery-info' )
    {
        if( edit_attachment( $_POST['attach_id'], $_POST['title'], null, $_POST['alt'], $_POST['caption'], $_POST['content'] ) )
        {
            echo json_encode( array( 'result' => 'success' ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
    }

    if( isset( $_POST['ajax_key'] ) && $_POST['ajax_key'] =='delete-gallery-image' )
    {
        if( delete_attachment( $_POST['attach_id'] ) )
        {
            delete_additional_field( $_POST['attach_id'], 'attachment' );

            echo json_encode( array( 'result' => 'success' ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
    }

    if( isset( $_POST['ajax_key'] ) && $_POST['ajax_key'] =='reorder-gallery-image' )
    {
        if( isset( $_POST['value'] ) && !empty( $_POST['value'] ) )
        {
            $value = json_decode( $_POST['value'], true );

            foreach( $value as $i=>$val )
            {
                $s = 'UPDATE lumonata_attachment SET lorder=%d WHERE lattach_id=%d AND larticle_id=%d';
                $q = $db->prepare_query( $s, $i, $val, $_POST['id'] );
                $r = $db->do_query( $q );
            }

            echo json_encode( array( 'result' => 'success' ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
    }

    if( isset( $_POST['ajax_key'] ) && $_POST['ajax_key'] == 'upload-rule-featured-images' )
    {
        if( isset( $_FILES['featured_img'] ) && $_FILES['featured_img']['error']==0 )
        {
            $rule_id     = $_POST['rule_id'];
            $app_name    = $_POST['app_name'];
            $file_name   = $_FILES['featured_img']['name'];
            $file_size   = $_FILES['featured_img']['size'];
            $file_type   = $_FILES['featured_img']['type'];
            $file_source = $_FILES['featured_img']['tmp_name'];

            if( is_allow_file_size( $file_size ) )
            {
                if( is_allow_file_type( $file_type, 'image' ) )
                {
                    //-- CHECK FOLDER EXIST
                    $folder = upload_folder_name();

                    if( !is_dir( FILES_PATH . '/' . $folder ) )
                    {
                        create_dir( FILES_PATH . '/' . $folder );
                    }

                    $ext    = file_name_filter( $file_name, true );
                    $name   = file_name_filter( $file_name, false ) . '-' . time();
                    $name_t = $name . '-thumbnail' . $ext;
                    $name_m = $name . '-medium' . $ext;
                    $name_l = $name . '-large' . $ext;
                    $name_f = $name . $ext;

                    $ldestination = FILES_PATH . '/' . $folder . '/' . $name_l;
                    $mdestination = FILES_PATH . '/' . $folder . '/' . $name_m;
                    $tdestination = FILES_PATH . '/' . $folder . '/' . $name_t;
                    $fdestination = FILES_PATH . '/' . $folder . '/' . $name_f;

                    if( upload_resize( $file_source, $ldestination, $file_type, 1920, 1920 ) )
                    {
                        if( upload_resize( $file_source, $mdestination, $file_type, 1024, 1024 ) )
                        {
                            if( upload_crop( $file_source, $tdestination, $file_type, 550, 550 ) )
                            {
                                if( upload( $file_source, $fdestination ) )
                                {
                                    $loc        = '/lumonata-content/files/' . $folder . '/' . $name_f;
                                    $loc_thumb  = '/lumonata-content/files/' . $folder . '/' . $name_t;
                                    $loc_medium = '/lumonata-content/files/' . $folder . '/' . $name_m;
                                    $loc_large  = '/lumonata-content/files/' . $folder . '/' . $name_l;

                                    $cat = get_rule_additional_field( $rule_id, 'featured_img', $app_name );
                                    $arr = array(
                                        'thumb'    => $loc_thumb,
                                        'medium'   => $loc_medium,
                                        'large'    => $loc_large,
                                        'original' => $loc
                                    );

                                    if( empty( $cat ) )
                                    {
                                        if( add_rule_additional_field( $rule_id, 'featured_img', json_encode( $arr ), $app_name ) )
                                        {
                                            echo json_encode( array( 'result' => 'success', 'rule_id' => $rule_id, 'filename' => $name_m, 'fileurl' => '//' . SITE_URL . $loc_medium ) );
                                        }
                                        else
                                        {
                                            //-- Delete Original Image
                                            if( file_exists( $fdestination ) )
                                            {
                                                unlink( $fdestination );
                                            }

                                            //-- Delete Thumb Image
                                            if( file_exists( $tdestination ) )
                                            {
                                                unlink( $tdestination );
                                            }

                                            //-- Delete Medium Image
                                            if( file_exists( $mdestination ) )
                                            {
                                                unlink( $mdestination );
                                            }

                                            //-- Delete Large Image
                                            if( file_exists( $ldestination ) )
                                            {
                                                unlink( $ldestination );
                                            }

                                            echo json_encode( array( 'result' => 'failed', 'error' => 'Error uploading file' ) );
                                        }
                                    }
                                    else
                                    {
                                        if( edit_rule_additional_field( $rule_id, 'featured_img', json_encode( $arr ), $app_name ) )
                                        {
                                            $img = json_decode( $cat, true );

                                            if( !empty( $img ) )
                                            {
                                                if( file_exists( ROOT_PATH . $img['original'] ) )
                                                {
                                                    unlink( ROOT_PATH . $img['original'] );
                                                }

                                                if( file_exists( ROOT_PATH . $img['large'] ) )
                                                {
                                                    unlink( ROOT_PATH . $img['large'] );
                                                }

                                                if( file_exists( ROOT_PATH . $img['medium'] ) )
                                                {
                                                    unlink( ROOT_PATH . $img['medium'] );
                                                }

                                                if( file_exists( ROOT_PATH . $img['small'] ) )
                                                {
                                                    unlink( ROOT_PATH . $img['small'] );
                                                }

                                                if( file_exists( ROOT_PATH . $img['thumb'] ) )
                                                {
                                                    unlink( ROOT_PATH . $img['thumb'] );
                                                }
                                            }

                                            echo json_encode( array( 'result' => 'success', 'rule_id' => $rule_id, 'filename' => $name_m, 'fileurl' => '//' . SITE_URL . $loc_medium ) );
                                        }
                                        else
                                        {
                                            echo json_encode( array( 'result' => 'success', 'rule_id' => $rule_id, 'filename' => $name_m, 'fileurl' => '//' . SITE_URL . $img['medium'] ) );
                                        }
                                    }
                                }
                                else
                                {
                                    //-- Delete Thumb Image
                                    if( file_exists( $tdestination ) )
                                    {
                                        unlink( $tdestination );
                                    }

                                    //-- Delete Medium Image
                                    if( file_exists( $mdestination ) )
                                    {
                                        unlink( $mdestination );
                                    }

                                    //-- Delete Large Image
                                    if( file_exists( $ldestination ) )
                                    {
                                        unlink( $ldestination );
                                    }

                                    echo json_encode( array( 'result' => 'failed', 'error' => 'Error uploading file' ) );
                                }
                            }
                            else
                            {
                                //-- Delete Medium Image
                                if( file_exists( $mdestination ) )
                                {
                                    unlink( $mdestination );
                                }

                                //-- Delete Large Image
                                if( file_exists( $ldestination ) )
                                {
                                    unlink( $ldestination );
                                }

                                echo json_encode( array( 'result' => 'failed', 'error' => 'Error uploading file' ) );
                            }
                        }
                        else
                        {
                            //-- Delete Large Image
                            if( file_exists( $ldestination ) )
                            {
                                unlink( $ldestination );
                            }

                            echo json_encode( array( 'result' => 'failed', 'error' => 'Error uploading file' ) );
                        }
                    }
                    else
                    {
                        echo json_encode( array( 'result' => 'failed', 'error' => 'Error uploading file' ) );
                    }
                }
                else
                {
                    echo json_encode( array( 'result' => 'failed', 'error' => 'File is not an image' ) );
                }
            }
            else
            {
                echo json_encode( array( 'result' => 'failed', 'error' => 'Maximum file size is 2MB' ) );
            }
        }
    }

    if( isset( $_POST['ajax_key'] ) && $_POST['ajax_key'] == 'delete-rule-featured-images' )
    {
        if( empty( $_POST['rule_id'] ) )
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
        else
        {
            $featured = get_rule_additional_field( $_POST['rule_id'], 'featured_img', $_POST['app_name'] );

            if( !empty( $featured ) )
            {
                $img = json_decode( $featured, true );

                if( !empty( $img ) )
                {
                    if( isset( $img['original'] ) && !empty( $img['original'] ) && file_exists( ROOT_PATH . $img['original'] ) )
                    {
                        unlink( ROOT_PATH . $img['original'] );
                    }

                    if( isset( $img['large'] ) && !empty( $img['large'] ) && file_exists( ROOT_PATH . $img['large'] ) )
                    {
                        unlink( ROOT_PATH . $img['large'] );
                    }

                    if( isset( $img['medium'] ) && !empty( $img['medium'] ) && file_exists( ROOT_PATH . $img['medium'] ) )
                    {
                        unlink( ROOT_PATH . $img['medium'] );
                    }

                    if( isset( $img['thumb'] ) && !empty( $img['thumb'] ) && file_exists( ROOT_PATH . $img['thumb'] ) )
                    {
                        unlink( ROOT_PATH . $img['thumb'] );
                    }

                    if( delete_rule_additional_field_with_key( $_POST['rule_id'], 'featured_img', $_POST['app_name'] ) )
                    {
                        echo json_encode( array( 'result' => 'success' ) );
                    }
                    else
                    {
                        echo json_encode( array( 'result' => 'failed' ) );
                    }
                }
                else
                {
                    echo json_encode( array( 'result' => 'failed' ) );
                }
            }
            else
            {
                echo json_encode( array( 'result' => 'failed' ) );
            }
        }
    }

    if( isset( $_POST['ajax_key'] ) && $_POST['ajax_key'] == 'add-new-post-field' )
    {
        $content = '';
        $surl    = SITE_URL;
        $fname   = generate_field_name( $_POST['flabel'] );

        if( $_POST['ftype'] == 0 )
        {
            $content .= '
            <li>
                <div class="field-wrapp">
                    <p><a class="delete-field" data-name="' . $fname . '"><img src="//' . $surl . '/lumonata-plugins/additional/images/delete.png" /></a></p>
                    <div class="inner">
                        <span>Label</span>
                        <input type="text" class="textbox" name="additional_fields[custom_field][' . $_POST['index'] . '][label][]" autocomplete="off" value="' . $_POST['flabel'] . '" />
                        <span>Value</span>
                        <input type="text" class="textbox" name="additional_fields[custom_field][' . $_POST['index'] . '][value][]" autocomplete="off" value="" />
                        <input type="text" class="textbox hidden" name="additional_fields[custom_field][' . $_POST['index'] . '][type][]" autocomplete="off" value="' . $_POST['ftype'] . '" />
                    </div>
                </div>
            </li>';
        }
        elseif( $_POST['ftype'] == 1 )
        {
            $content .= '
            <li>
                <div class="field-wrapp">
                    <p><a class="delete-field" data-name="' . $fname . '"><img src="//' . $surl . '/lumonata-plugins/additional/images/delete.png" /></a></p>
                    <div class="inner">
                        <span>Label</span>
                        <input type="text" class="textbox" name="additional_fields[custom_field][' . $_POST['index'] . '][label][]" autocomplete="off" value="' . $_POST['flabel'] . '" />
                        <span>Value</span>
                        <textarea class="textarea" name="additional_fields[custom_field][' . $_POST['index'] . '][value][]" rows="10" autocomplete="off"></textarea>
                        <input type="text" class="textbox hidden" name="additional_fields[custom_field][' . $_POST['index'] . '][type][]" autocomplete="off" value="' . $_POST['ftype'] . '" />
                    </div>
                </div>
            </li>';
        }
        elseif( $_POST['ftype'] == 2 )
        {
            $content .= '
            <li>
                <div class="field-wrapp">
                    <p><a class="delete-field" data-name="' . $fname . '"><img src="//' . $surl . '/lumonata-plugins/additional/images/delete.png" /></a></p>
                    <div class="inner">
                        <span>Label</span>
                        <input type="text" class="textbox" name="additional_fields[custom_field][' . $_POST['index'] . '][label][]" autocomplete="off" value="' . $_POST['flabel'] . '" />
                        <span>Value</span>
                        <textarea id="tinymce-' . $fname . '-' . time() . '" class="textarea tinymce" name="additional_fields[custom_field][' . $_POST['index'] . '][value][]" autocomplete="off"></textarea>
                        <input type="text" class="textbox hidden" name="additional_fields[custom_field][' . $_POST['index'] . '][type][]" autocomplete="off" value="' . $_POST['ftype'] . '" />
                    </div>
                </div>
            </li>';
        }

        echo $content;
    }

    if( isset( $_POST['ajax_key'] ) && $_POST['ajax_key'] == 'delete-post-field' )
    {
        $s = 'SELECT lvalue FROM lumonata_additional_fields WHERE lapp_id=%d AND lkey=%s AND lapp_name=%s';
        $q = $db->prepare_query( $s, $_POST['post_id'], 'custom_field', $_POST['app_name'] );
        $r = $db->do_query( $q );
        $d = $db->fetch_array( $r );

        $fields = json_decode( $d['lvalue'], true );

        if( !empty( $fields ) && is_array( $fields ) )
        {
            if( isset( $fields[ $_POST['field'] ] ) )
            {
                unset( $fields[ $_POST['field'] ] );
            }

            if( edit_additional_field( $_POST['post_id'], 'custom_field', json_encode( $fields, JSON_HEX_QUOT | JSON_HEX_TAG ), $_POST['app_name'] ) )
            {
                echo json_encode( array( 'result' => 'success' ) );
            }
            else
            {
                echo json_encode( array( 'result' => 'failed' ) );
            }
        }
        else
        {
            echo json_encode( array( 'result' => 'success' ) );
        }
    }

    exit;
}

?>
