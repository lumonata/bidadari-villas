function initmap()
{
    var depoint = jQuery('[name=default_pointer]').val();
    var deflat  = jQuery('[name=default_latitude]').val();
    var deflong = jQuery('[name=default_longitude]').val();

    var geo = new google.maps.Geocoder();
    var map = new google.maps.Map( document.getElementById('map'), {
        center: { lat: Number( deflat ), lng: Number( deflong ) },
        zoom:12
    });

    var image = {
        url: depoint,
        size: new google.maps.Size(24, 24),
        scaledSize: new google.maps.Size(24, 24)
    };

    var marker = new google.maps.Marker({
        position: { lat: Number( deflat ), lng: Number( deflong ) },
        animation: google.maps.Animation.DROP,
        draggable: true,
        icon: image,
        map: map
    });

    google.maps.event.addListener( marker, 'dragend', function( evt ){
        var long = evt.latLng.lng().toFixed(6) ;   
        var lat  = evt.latLng.lat().toFixed(6);
        var coor = lat + ',' + long;

        jQuery('.loc-cordinate').val( coor );

        geo.geocode({ latLng: evt.latLng }, function( res ) {
            if( res && res.length > 0 )
            {
                jQuery('[name=loc_addr_1]').val( res[0].formatted_address );
            }
            else
            {
                jQuery('[name=loc_addr_1]').val( 'Cannot determine address at this location.' );
            }
        });

        map.setCenter( marker.position );
    });

    jQuery('.loc-cordinate').focusout(function(){
        var value = jQuery(this).val();
        var pos   = value.split( ',' );

        if( jQuery.isEmptyObject( pos ) === false && pos.length == 2 )
        {
            if( pos[0] == '' || pos[1] == '' || isNaN( Number( pos[0] ) ) || isNaN( Number( pos[1] ) ) )
            {
                jQuery('.loc-cordinate').val('').attr( 'placeholder', 'Invalid coordinate' ).addClass('error');
            }
            else
            {
                var cord = new Object;
                	cord.lat = Number( pos[0] );
                	cord.lng = Number( pos[1] );

                geo.geocode({ latLng: cord }, function( res ){
                    if( res && res.length > 0 )
                    {
                        jQuery('[name=loc_addr_1]').val( res[0].formatted_address );
                    }
                    else
                    {
                        jQuery('[name=loc_addr_1]').val( 'Cannot determine address at this location.' );
                    }
                });

                map.setCenter( cord );
                marker.setPosition( cord );
            }
        }
        else
        {
            jQuery('.loc-cordinate').val('').attr( 'placeholder', 'Invalid coordinate' ).addClass( 'error' );
        }
    });

    jQuery('.loc-cordinate').focusin(function(){
        jQuery(this).removeAttr( 'placeholder' ).removeClass( 'error' );
    });
}

/*
| -------------------------------------------------------------------------------------
| Upload Featured Image
| -------------------------------------------------------------------------------------
*/
Dropzone.autoDiscover = false;
		
function upload_featured_img( site_url, app_name ){
	jQuery('[name=featured_img]').change( function( el ){
        var data = new FormData();
            data.append( 'app_name', app_name );
            data.append( 'ajax_key', 'upload-featured-images' );
            data.append( 'post_id', jQuery('.post-id-field').val() );
            data.append( 'featured_img', el.currentTarget.files[0] );
        
        var xhrObject = new XMLHttpRequest();

        xhrObject.open('POST', '//' + site_url + '/additional-ajax');  
        xhrObject.onreadystatechange = function(){
            if( xhrObject.readyState == 4 && xhrObject.status == 200 )
            {
                var e = JSON.parse(xhrObject.responseText);

                if( e.result == 'success' )
                {
                    var images =
                    '<div class="box">'+
                        '<img src="' + e.fileurl + '" />'+
                        '<div class="overlay">'+
                            '<a data-post-id="' + e.post_id + '" class="img-delete" title="Delete">'+
                                '<img src="//' + site_url + '/lumonata-plugins/additional/images/delete.png">'+
                            '</a>'+
                        '</div>'+
                    '</div>';

                    jQuery('.featured-img').html( images );

                    delete_featured_img( site_url, app_name );
                }
                else
                {
                    alert( e.data );
                }
            }
        }

        xhrObject.send( data );
    });

    delete_featured_img( site_url, app_name );
}

/*
| -------------------------------------------------------------------------------------
| Delete Featured Image
| -------------------------------------------------------------------------------------
*/
function delete_featured_img( site_url, app_name ){
	jQuery('.featured-img .img-delete').click(function(){
    	var url = '//' + site_url + '/additional-ajax';
    	var prm = new Object;
    		prm.app_name = app_name;
    		prm.ajax_key = 'delete-featured-images';
    		prm.post_id  = jQuery('.post-id-field').val();

    	jQuery.post( url, prm, function( e ){
    		if( e.result == 'success' )
    		{
    			jQuery('.featured-img .box').remove();
    		}
    		else
    		{
    			alert('Failed to delete this image');
    		}
    	}, 'json');
    });
}

/*
| -------------------------------------------------------------------------------------
| Set Gallery Image Data
| -------------------------------------------------------------------------------------
*/
function set_gallery_img( site_url, app_name )
{
	if( jQuery('#preview-template').length > 0 )
	{
		var ajax_url   = '//' + site_url + '/additional-ajax';
	    var myDropzone = new Dropzone('#gallery-upload', { 
	        url: ajax_url,
	        previewTemplate: document.querySelector('#preview-template').innerHTML,
	        maxFilesize: 2,
	        init: function()
	        {
	        	var url = ajax_url;
			    var prm = new Object;
			        prm.app_name = app_name;
			        prm.ajax_key = 'get-gallery-image';
			        prm.post_id  = jQuery('.post-id-field').val();

			    jQuery.post(url, prm, function(e){
			        if( e.result=='success' )
			        {
			            jQuery.each(e.data, function(i, e){
			                var mockFile = { 
			                    url:e.url,
			                    id:e.img_id,
			                    title: e.img_title,
			                    alt: e.img_alt,
			                    caption: e.img_caption,
			                    content: e.img_content
			                };

			                myDropzone.emit('addedfile', mockFile);
			                myDropzone.emit('thumbnail', mockFile, e.img_thumb);
			                myDropzone.emit('complete', mockFile);
			                myDropzone.files.push(mockFile);
			            });
			        }
			    },'json');

			    this.on('addedfile', function(file) {			        
			        var sel     = jQuery(file.previewElement);
		        	var id      = typeof file.id != 'undefined' ? file.id : '';
		        	var title   = typeof file.title != 'undefined' ? file.title : '';
		        	var alt     = typeof file.alt != 'undefined' ? file.alt : '';
		        	var caption = typeof file.caption != 'undefined' ? file.caption : '';
		        	var content = typeof file.content != 'undefined' ? file.content : '';

			        sel.attr('id','gallery_'+id);
			        sel.find('.dz-detail-box [name=gallery_alt]').val(alt);
			        sel.find('.dz-detail-box [name=gallery_title]').val(title);
			        sel.find('.dz-detail-box [name=gallery_caption]').val(caption);

			        sel.find('.dz-delete').attr('rel', id);
		        	sel.find('.dz-detail-box .save-detail').attr('rel', id);
			        sel.find('.dz-detail-box [name=gallery_content]').attr('id', 'gtiny-mce-' + id).val(content);

			        initTinyMce( '#gtiny-mce-' + id );

			        sel.find('.dz-edit').unbind('click');
			        sel.find('.dz-edit').click(function(){
			            sel.toggleClass('opened');
			        });

			        sel.find('.dz-delete').unbind('click');
			        sel.find('.dz-delete').click(function(){
			            var url = ajax_url;
			            var prm = new Object;
			                prm.ajax_key  = 'delete-gallery-image';
			                prm.attach_id = jQuery(this).attr('rel');

			            jQuery.post(url, prm, function(e){

			                if(e.result=='success')
			                {
			                    myDropzone.removeFile(file);
			                }
			                else
			                {
			                    alert('Failed to delete this image');
			                }

			            },'json');
			        }); 

			        sel.find('.dz-detail-box .save-detail').unbind('click');
			        sel.find('.dz-detail-box .save-detail').click(function(){
			            var url = ajax_url;
			            var prm = new Object;
			                prm.ajax_key  = 'edit-gallery-info';
			                prm.attach_id = id;
			                prm.title     = sel.find('.dz-detail-box [name=gallery_title]').val();  
			                prm.alt       = sel.find('.dz-detail-box [name=gallery_alt]').val();  
			                prm.caption   = sel.find('.dz-detail-box [name=gallery_caption]').val(); 
			                prm.content   = tinymce.get('gtiny-mce-' + id).getContent();

			            jQuery.post(url, prm, function(e){
			                sel.removeClass('opened');
			            },'json');

			            return false;
			        });

			        sel.find('.dz-detail-box .close-detail').unbind('click');
			        sel.find('.dz-detail-box .close-detail').click(function(){
			            sel.removeClass('opened');
			        });
				});

				this.on('error', function(file, message) { 
					myDropzone.removeFile(file);
					alert(message);
				});

				this.on('success', function(file, responseText){
					var e = jQuery.parseJSON(responseText);

		            if( e.result = 'success' )
		            {
		                jQuery(file.previewElement).find('.dz-delete').attr('rel', e.attach_id);
		        		jQuery(file.previewElement).find('.dz-detail-box .save-detail').attr('rel', e.attach_id);
		                jQuery(file.previewElement).find('.dz-detail-box [name=gallery_title]').val(e.title);
		                jQuery(file.previewElement).find('.dz-detail-box [name=gallery_alt]').val(e.alt);
		                jQuery(file.previewElement).find('.dz-detail-box [name=gallery_caption]').val(e.caption);
				        jQuery(file.previewElement).find('.dz-detail-box [name=gallery_content]').attr('id', 'gtiny-mce-' + e.attach_id).val(e.content);

				        initTinyMce( '#gtiny-mce-' + e.attach_id );
		            }
		            else
		            {
		                myDropzone.removeFile(file);   
		            }
		        });

				this.on('sending', function(file, xhr, formData){
				    console.log(formData);
            		formData.append( 'app_name', app_name );
		            formData.append( 'ajax_key', 'upload-gallery-image' );
		            formData.append( 'post_id', jQuery('.post-id-field').val() );
				});
	        }
	    });
	}
}

/*
| -------------------------------------------------------------------------------------
| Reorder Gallery Image
| -------------------------------------------------------------------------------------
*/
function reorder_gallery_image( site_url )
{
	jQuery('#gallery-upload').nestedSortable({
		handle: 'div',
		items: '.dz-preview',
		listType:'div',
		opacity: .6,
		tolerance: 'pointer',
		maxLevels:0,
		stop: function(eve, ui){
			var url  = '//' + site_url + '/additional-ajax';
			var list = jQuery('#gallery-upload').nestedSortable('toArray');
			var objc = new Array;
				jQuery.each(list, function( i, e ){
					if( !jQuery.isEmptyObject(e.item_id) )
					{
						objc.push(e.item_id);
					}
				});
				
			var param = new Object;
				param.ajax_key	= 'reorder-gallery-image';
				param.id	    = jQuery('.post-id-field').val();
				param.value     = JSON.stringify(objc);

			jQuery.post(url, param, function( el ){				
				jQuery.each(objc, function( i, id ){
					if( tinymce.EditorManager.execCommand( 'mceRemoveEditor', true, 'gtiny-mce-' + id ) )
					{
						tinymce.EditorManager.execCommand( 'mceAddEditor', true, 'gtiny-mce-' + id );
					}
				});				
			},'json');
		}
	});
}

/*
| -------------------------------------------------------------------------------------
| Reorder Additional Field
| -------------------------------------------------------------------------------------
*/
function reorder_additional_field()
{
	jQuery('.new-field-list ol').nestedSortable({
        handle: 'div',
        items: 'li',
        maxLevels: 1,
        opacity: .6,
        forcePlaceholderSize: true,
        placeholder: 'placeholder',
        tolerance: 'pointer',
        stop: function( eve, ui ){
        	var sel = jQuery(ui.item[0]).find('textarea');

        	if( sel.hasClass('tinymce') )
        	{
            	var id  = sel.attr('id');
            	tinymce.get(id).remove();
            	initTinyMce( '#'+id );
            }
        }
    }); 
}

/*
| -------------------------------------------------------------------------------------
| Add New Post Additional Field
| -------------------------------------------------------------------------------------
*/
function post_additional_field( site_url, index, app_name )
{
    jQuery('[name=field_type]').chosen({ disable_search_threshold:10 });

	jQuery('.add-new-field').click(function(){
		var type  = jQuery('[name=field_type]').val();
		var name  = jQuery('[name=field_name]').val();
		var label = jQuery('[name=field_label]').val();

		if( label != '' && type != '' )
		{
	    	var url = '//' + site_url + '/additional-ajax';
	    	var prm = new Object;
	    		prm.ajax_key = 'add-new-post-field';
	    		prm.app_name = app_name;
	    		prm.index    = index;
	    		prm.ftype    = type;
	    		prm.flabel   = label;
	    		prm.fname    = name;

	    	jQuery.post( url, prm, function(e){
	    		jQuery('.new-field-list ol').append(e);
	    		jQuery('[name=field_type]').val('').trigger('chosen:updated');
	    		jQuery('[name=field_name]').val('');
	    		jQuery('[name=field_label]').val('');

	    		if( prm.ftype == 2 )
	    		{
	    			initTinyMce( '.tinymce' );
	    		}
	    		
	    		delete_post_additional_field( site_url, app_name );
	    		reorder_additional_field();
	    	});
		}
		else
		{
			if( label == '' && type == '' )
			{
				alert('Field label and type of field can\'t be empty');
			}
			else if( type == '' )
			{
				alert('Type of field can\'t be empty');
			}
			else
			{
				alert('Field label can\'t be empty');
			}
		}

		return false;
    });

    delete_post_additional_field( site_url, app_name );
    reorder_additional_field();
}

/*
| -------------------------------------------------------------------------------------
| Delete Post Additional Field
| -------------------------------------------------------------------------------------
*/
function delete_post_additional_field( site_url, app_name )
{	
	jQuery('.delete-field').unbind();
	jQuery('.delete-field').click(function(){
    	var sel = jQuery(this).parent().parent().parent();
    	var url = '//' + site_url + '/additional-ajax';
    	var prm = new Object;
    		prm.app_name = app_name;
    		prm.ajax_key = 'delete-post-field';
    		prm.field    = jQuery(this).attr('data-name');
    		prm.post_id  = jQuery('.post-id-field').val();

    	jQuery.post( url, prm, function( e ){
    		if( e.result == 'success' )
    		{
    			sel.fadeOut( 200, function(){ 
    				jQuery(this).remove(); 
    			});
    		}
			else
			{
				alert('Can\'t delete this field');
			}
    	}, 'json');
    });
}

/*
| -------------------------------------------------------------------------------------
| Upload Rule Featured Image
| -------------------------------------------------------------------------------------
*/
function upload_rule_featured_img( site_url, app_name ){
	jQuery('[name=rule_featured_img]').change( function( el ){
        var data = new FormData();
            data.append( 'app_name', app_name );
            data.append( 'rule_id', jQuery('.rule-text-id').val() );
            data.append( 'ajax_key', 'upload-rule-featured-images' );
            data.append( 'featured_img', el.currentTarget.files[0] ); 
        
        var xhrObject = new XMLHttpRequest();

        xhrObject.open('POST', '//' + site_url + '/additional-ajax');  
        xhrObject.onreadystatechange = function(){
            if( xhrObject.readyState == 4 && xhrObject.status == 200)
            {
                var e = JSON.parse( xhrObject.responseText );

                if( e.result == 'success' )
                {
                     var images =
                    '<div class="box">'+
                        '<img src="' + e.fileurl + '" />'+
                        '<div class="overlay">'+
                            '<a data-post-id="' + e.post_id + '" class="img-delete" title="Delete">'+
                                '<img src="//' + site_url + '/lumonata-plugins/additional/images/delete.png">'+
                            '</a>'+
                        '</div>'+
                    '</div>';

                    jQuery('.featured-img').html( images );

                    delete_rule_featured_img( site_url, app_name );
                }
                else
                {
                    alert( e.data );
                }
            }
        }

        xhrObject.send( data );
    });

    delete_rule_featured_img( site_url, app_name );
}

/*
| -------------------------------------------------------------------------------------
| Delete Rule Featured Image
| -------------------------------------------------------------------------------------
*/
function delete_rule_featured_img( site_url, app_name ){
	jQuery('.rule-featured-img .img-delete').click(function(){
    	var url = '//' + site_url + '/additional-ajax';
    	var prm = new Object;
    		prm.app_name = app_name;
    		prm.ajax_key = 'delete-rule-featured-images';
    		prm.rule_id  = jQuery('.rule-text-id').val();

    	jQuery.post( url, prm, function( e ){
    		if( e.result == 'success' )
    		{
    			jQuery('.rule-featured-img .box').remove();
    		}
    		else
    		{
    			alert('Failed to delete this image');
    		}
    	}, 'json');
    });
}

/*
| -------------------------------------------------------------------------------------
| Upload Rooms Content Image
| -------------------------------------------------------------------------------------
*/
function upload_rooms_img( site_url, app_name ){
	jQuery('.rooms-img-file').change( function( el ){
		var sel = jQuery(this).parent().find('.rooms-img');
    	var url = '//' + site_url + '/additional-ajax';
        var prm = new FormData();
            prm.append( 'ajax_key', 'upload-rooms-images' );
            prm.append( 'rooms_img', el.currentTarget.files[0] );
            prm.append( 'app_name', jQuery(this).data('app-name') );
            prm.append( 'post_id', jQuery('.post-id-field').val() );
        
        var xhrObject = new XMLHttpRequest();

        xhrObject.open('POST', url);
        xhrObject.onreadystatechange = function(){
            if( xhrObject.readyState == 4 && xhrObject.status == 200 )
            {
                var e = JSON.parse(xhrObject.responseText);

                if( e.result == 'success' )
                {
                    sel.html( e.content );

                    delete_rooms_img( site_url, app_name );
                }
                else
                {
                    alert( e.data );
                }
            }
        }

        xhrObject.send( prm );
    });

    delete_rooms_img( site_url, app_name );
}

/*
| -------------------------------------------------------------------------------------
| Delete Rooms Content Image
| -------------------------------------------------------------------------------------
*/
function delete_rooms_img( site_url, app_name ){
	jQuery('.additional-rooms-img .img-delete').click(function(){
		var sel = jQuery(this).parent().parent().parent().find('.box');
    	var url = '//' + site_url + '/additional-ajax';
    	var prm = new Object;
    		prm.ajax_key = 'delete-rooms-images';
    		prm.post_id  = jQuery(this).data('app-id');
    		prm.app_name = jQuery(this).data('app-name');

    	jQuery.post( url, prm, function( e ){
    		if( e.result == 'success' )
    		{
    			sel.remove();
    		}
    		else
    		{
    			alert('Failed to delete this image');
    		}
    	}, 'json');
    });
}

/*
| -------------------------------------------------------------------------------------
| Upload Masking Image
| -------------------------------------------------------------------------------------
*/
function upload_masking_img( site_url, app_name ){
	jQuery('.masking-img-file').change( function( el ){
		var sel = jQuery(this).parent().find('.masking-img');
    	var url = '//' + site_url + '/additional-ajax';
        var prm = new FormData();
            prm.append( 'ajax_key', 'upload-masking-images' );
            prm.append( 'masking_img', el.currentTarget.files[0] );
            prm.append( 'app_name', jQuery(this).data('app-name') );
            prm.append( 'post_id', jQuery('.post-id-field').val() );
        
        var xhrObject = new XMLHttpRequest();

        xhrObject.open('POST', url);
        xhrObject.onreadystatechange = function(){
            if( xhrObject.readyState == 4 && xhrObject.status == 200 )
            {
                var e = JSON.parse(xhrObject.responseText);

                if( e.result == 'success' )
                {
                    sel.html( e.content );

                    delete_masking_img( site_url, app_name );
                }
                else
                {
                    alert( e.data );
                }
            }
        }

        xhrObject.send( prm );
    });

    delete_masking_img( site_url, app_name );
}

/*
| -------------------------------------------------------------------------------------
| Delete Masking Image
| -------------------------------------------------------------------------------------
*/
function delete_masking_img( site_url, app_name ){
	jQuery('.additional-masking-img .img-delete').click(function(){
		var sel = jQuery(this).parent().parent().parent().find('.box');
    	var url = '//' + site_url + '/additional-ajax';
    	var prm = new Object;
    		prm.ajax_key = 'delete-masking-images';
    		prm.post_id  = jQuery(this).data('app-id');
    		prm.app_name = jQuery(this).data('app-name');

    	jQuery.post( url, prm, function( e ){
    		if( e.result == 'success' )
    		{
    			sel.remove();
    		}
    		else
    		{
    			alert('Failed to delete this image');
    		}
    	}, 'json');
    });
}

/*
| -------------------------------------------------------------------------------------
| Set Icon Picker
| -------------------------------------------------------------------------------------
*/
function init_icon_picker()
{
	if( jQuery('.icp-dd').length > 0 )
	{
	    jQuery('.icp-dd').iconpicker();
		jQuery('.icp-dd').on( 'iconpickerSelected', function( e ){
			jQuery('.iconpicker-input').prop('checked', false);
			jQuery('.iconpicker-input-fa').val( e.iconpickerValue ).prop('checked', true);
			jQuery('.lead .picker-target').get(0).className = 'picker-target fa-2x ' + e.iconpickerInstance.options.iconBaseClass + ' ' + e.iconpickerInstance.options.fullClassFormatter(e.iconpickerValue);
		});

		if( jQuery('.iconpicker-input-fa:checked').length > 0 )
		{
			var vals = jQuery('.iconpicker-input-fa:checked').val();

			jQuery('.icp-dd').data('iconpicker').update( vals, true );
		}
	}
}

/*
| -------------------------------------------------------------------------------------
| Set Chosen Options
| -------------------------------------------------------------------------------------
*/
function init_chosen()
{
	if( jQuery('.chosen-select').length > 0 )
	{
    	jQuery('.chosen-select').chosen({ disable_search_threshold:10 });
    }
}

jQuery(document).ready(function(){
	var index    = jQuery('[name=index]').val();
	var site_url = jQuery('[name=site_url]').val();
	var app_name = jQuery('[name=app_name]').val();

	init_chosen();
	init_icon_picker();

	reorder_gallery_image( site_url );
	set_gallery_img( site_url, app_name );
	upload_rooms_img( site_url, app_name );
	upload_masking_img( site_url, app_name );
	upload_featured_img( site_url, app_name );
    upload_rule_featured_img( site_url, app_name );
    post_additional_field( site_url, index, app_name );
});