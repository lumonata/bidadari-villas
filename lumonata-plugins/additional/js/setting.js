/*
| -------------------------------------------------------------------------------------
| Upload Hero Image
| -------------------------------------------------------------------------------------
*/
function upload_hero_img(){
	var site_url = jQuery('[name=site_url]').val();
	var ajax_url = jQuery('[name=ajax_url]').val();
	var app_name = jQuery('[name=app_name]').val();

	jQuery('[name=hero_img]').change( function( el ){
        var data = new FormData();
            data.append( 'app_name', app_name );
            data.append( 'ajax_key', 'upload-setting-hero-images' );
            data.append( 'hero_img', el.currentTarget.files[0] );
        
        var xhrObject = new XMLHttpRequest();

        xhrObject.open('POST', ajax_url);  
        xhrObject.onreadystatechange = function(){
            if( xhrObject.readyState == 4 && xhrObject.status == 200 )
            {
                var e = JSON.parse(xhrObject.responseText);

                if( e.result == 'success' )
                {
                    jQuery('.hero-img').html( e.content );

                    delete_hero_img();
                }
                else
                {
                    alert( e.data );
                }
            }
        }

        xhrObject.send( data );
    });

    delete_hero_img();
}

/*
| -------------------------------------------------------------------------------------
| Delete Hero Image
| -------------------------------------------------------------------------------------
*/
function delete_hero_img(){
	var ajax_url = jQuery('[name=ajax_url]').val();

	jQuery('.hero-img .img-delete').click(function(){
    	var prm = new Object;
    		prm.ajax_key = 'delete-setting-hero-images';
    		prm.meta_id  = jQuery(this).data('meta-id');

    	jQuery.post( ajax_url, prm, function( e ){
    		if( e.result == 'success' )
    		{
    			jQuery('.hero-img .box').remove();
    		}
    		else
    		{
    			alert('Failed to delete this image');
    		}
    	}, 'json');
    });
}

function additional_slide_toggle()
{
    jQuery('#hero_image_0').click(function(){
        jQuery('#hero_image_details_0').slideToggle( 100 );

        return false;
    });

    jQuery('#meta_data_0').click(function(){
        jQuery('#meta_data_details_0').slideToggle( 100 );

        return false;
    });
}

jQuery(document).ready(function(){
	additional_slide_toggle();
	upload_hero_img();
});