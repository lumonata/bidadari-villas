<?php

/*
| -------------------------------------------------------------------------------------------------------------------------
Plugin Name: Lumonata Restaurant Menus
Plugin URL: http://lumonata.com/
Description: This plugin is use for adding restaurant menus.
Author: Ngurah Rai
Author URL: http://lumonata.com/
Version: 1.0.0
| -------------------------------------------------------------------------------------------------------------------------
*/

add_actions( 'menus-ajax_page', 'execute_menus_ajax_request' );

//-- Add the meta data input interface
//-- into each proccess in administrator area. 
$apps_array = array( 'restaurant' );

if( is_edit_all() && !( is_save_draft() || is_publish() ) )
{
    foreach( $_POST[ 'select' ] as $index => $post_id )
    {
        foreach( $apps_array as $apps )
        {
            add_actions( $apps . '_additional_field' . $index, 'menus_data_func', $apps );
            add_actions( $apps . '_additional_delete_' . $index, 'menus_delete_func', $apps );
        }
    }
}
else
{
    foreach( $apps_array as $apps )
    {
        add_actions( $apps . '_additional_field', 'menus_data_func', $apps );
        add_actions( $apps . '_additional_delete', 'menus_delete_func', $apps );
    }
}

set_previous_menu_data();

function set_previous_menu_data()
{
    global $db;

    //-- Create Table lumonata_menu_category
    $q = 'CREATE TABLE IF NOT EXISTS `lumonata_menu_category` (
              `lcmenu_id` bigint(20) NOT NULL AUTO_INCREMENT,
              `larticle_id` bigint(20) NOT NULL,
              `lcmenu_parent` bigint(20) DEFAULT NULL,
              `lcmenu_name` varchar(100) DEFAULT NULL,
              `lorder` bigint(20) DEFAULT "0",
          PRIMARY KEY (`lcmenu_id`)
         ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8';
    $r = $db->do_query( $q );

    //-- Create Table lumonata_menu
    $q = 'CREATE TABLE IF NOT EXISTS `lumonata_menu` (
              `lmenu_id` bigint(20) NOT NULL AUTO_INCREMENT,
              `lcmenu_id` bigint(20) NOT NULL,
              `lmenu_name` varchar(150) DEFAULT NULL,
              `lmenu_desc` text,
              `lmenu_price` varchar(150) NOT NULL DEFAULT "0",
              `lorder` bigint(20) NOT NULL DEFAULT "0",
          PRIMARY KEY (`lmenu_id`), KEY `lcmenu_id` (`lcmenu_id`),
          CONSTRAINT `lumonata_menu_ibfk_1` FOREIGN KEY (`lcmenu_id`) REFERENCES `lumonata_menu_category` (`lcmenu_id`) ON UPDATE NO ACTION
        ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8';
    $r = $db->do_query( $q );

    //-- Count data on table lumonata_menu_category
    $q = 'SELECT COUNT(lcmenu_id) AS cmenu_count FROM lumonata_menu_category';
    $r = $db->do_query( $q );
    $d = $db->fetch_array( $r );

    //-- Count data on table lumonata_menu
    $q2 = 'SELECT COUNT(lmenu_id) AS menu_count FROM lumonata_menu';
    $r2 = $db->do_query( $q2 );
    $d2 = $db->fetch_array( $r2 );

    //-- If data empty run the script
    if( $d['cmenu_count'] == 0 && $d2['menu_count'] == 0 )
    {
        $s = 'SELECT
                b.lvalue,
                a.larticle_id,
                a.larticle_title
              FROM lumonata_articles AS a
              LEFT JOIN lumonata_additional_fields AS b ON a.larticle_id = b.lapp_id
              WHERE b.lkey = %s';
        $q = $db->prepare_query( $s, 'food_drink_menu_category' );
        $r = $db->do_query( $q );

        while( $d = $db->fetch_array( $r ) )
        {
            if( empty( $d['lvalue'] ) )
            {
                continue;
            }

            $objects = json_decode( $d['lvalue'], true );

            if ( $objects !== null && json_last_error() === JSON_ERROR_NONE )
            {
                $i = 0;

                foreach( $objects as $obj )
                {
                    $s2 = 'INSERT INTO lumonata_menu_category( larticle_id, lcmenu_parent, lcmenu_name, lorder ) VALUES( %d, %d, %s, %d )';
                    $q2 = $db->prepare_query( $s2, $d['larticle_id'], 0, $obj['name'], $i );
                    
                    if( $db->do_query( $q2 ) )
                    {
                        $lcmenu_id = mysql_insert_id();

                        if( isset( $obj['child'] ) && !empty( $obj['child'] ) )
                        {
                            $ii = 0;

                            foreach( $obj['child'] as $child )
                            {
                                $s3 = 'INSERT INTO lumonata_menu_category( larticle_id, lcmenu_parent, lcmenu_name, lorder ) VALUES( %d, %d, %s, %d )';
                                $q3 = $db->prepare_query( $s3, $d['larticle_id'], $lcmenu_id, $child['name'], $ii );

                                $db->do_query( $q3 );

                                $ii++;
                            }
                        }
                    }

                    $i++;
                }
            }
        }

        $s = 'SELECT
                b.lvalue,
                a.larticle_id,
                a.larticle_title
              FROM lumonata_articles AS a
              LEFT JOIN lumonata_additional_fields AS b ON a.larticle_id = b.lapp_id
              WHERE b.lkey = %s';
        $q = $db->prepare_query( $s, 'food_drink_menu' );
        $r = $db->do_query( $q );

        while( $d = $db->fetch_array( $r ) )
        {
            if( empty( $d['lvalue'] ) )
            {
                continue;
            }

            $objects = json_decode( $d['lvalue'], true );

            if ( $objects !== null && json_last_error() === JSON_ERROR_NONE )
            {
                $i = 0;

                foreach( $objects as $obj )
                {
                    $ct_arr = explode( '|', $obj['category'] );
                    $lcname = trim( end( $ct_arr ) );                

                    $s2 = 'SELECT lcmenu_id FROM lumonata_menu_category WHERE lcmenu_name = %s AND larticle_id = %d';
                    $q2 = $db->prepare_query( $s2, $lcname, $d['larticle_id'] );
                    $r2 = $db->do_query( $q2 );

                    if( $db->num_rows( $r2 ) )
                    {
                        $d2 = $db->fetch_array( $r2 );

                        $s3 = 'INSERT INTO lumonata_menu( lcmenu_id, lmenu_name, lmenu_desc, lmenu_price, lorder ) VALUES( %d, %s, %s, %s, %d )';
                        $q3 = $db->prepare_query( $s3, $d2['lcmenu_id'], $obj['name'], $obj['desc'], $obj['price'], $i );
                        
                        $db->do_query( $q3 );
                    }

                    $i++;
                }
            }
        }
    }
}

function menus_data_func( $apps )
{
    global $thepost;
    
    $i       = $thepost->post_index;
    $post_id = $thepost->post_id;
    
    if( is_publish() )
    {
    	set_menu_pdf( $post_id, $apps );
        menus_sync_func( $post_id, $i );
    }

    $menu_pdf           = get_additional_field( $post_id, 'menu_pdf', $apps );
    $menu_pdf_text_link = get_additional_field( $post_id, 'menu_pdf_text_link', $apps );

    set_template( PLUGINS_PATH . '/menus/menus-form.html', 'menus_template_' . $i );

    add_block( 'menus_block', 'adt', 'menus_template_' . $i );
    
    add_variable( 'i', $i );
    add_variable( 'app_name', $apps );
    add_variable( 'post_id', $post_id );
    add_variable( 'site_url', SITE_URL );
    add_variable( 'ajax_url', SITE_URL . '/menus-ajax/' );
    add_variable( 'admin_url', SITE_URL . '/lumonata-admin' );
    add_variable( 'plugin_url', SITE_URL . '/lumonata-plugins/menus' );

    add_variable( 'menu_pdf_text_link', $menu_pdf_text_link );
    add_variable( 'menu_pdf', empty( $menu_pdf ) ? '' : '<p class="pdf-file"> ' . $menu_pdf . '<a class="link">X</a></p>' );
    
    add_variable( 'action_type', $_GET[ 'prc' ] );

    add_actions( 'css_elements', 'get_custom_css', '//' . SITE_URL . '/lumonata-plugins/menus/style.css' );
    
    parse_template( 'menus_block', 'adt', false );

    return return_template( 'menus_template_' . $i );
}

function menus_sync_func( $post_id, $index )
{
    global $db;

    if( empty( $post_id ) )
    {
        return;
    }

    $s = 'UPDATE lumonata_menu_category SET larticle_id = %d WHERE larticle_id = %d';
    $q = $db->prepare_query( $s, $post_id, $_POST[ 'post_id' ][ $index ] );
    $r = $db->do_query( $q );
}

function menus_delete_func( $apps )
{
    global $db;
    
    if( isset( $_POST[ 'id' ] ) && !empty( $_POST[ 'id' ] ) )
    {
        if( is_array( $_POST[ 'id' ] ) )
        {
            foreach( $_POST[ 'id' ] as $post_id )
            {
                $s = 'SELECT lcmenu_id FROM lumonata_menu_category WHERE larticle_id = %d';
                $q = $db->prepare_query( $s, $post_id );
                $r = $db->do_query( $q );

                if( $db->num_rows( $r ) )
                {
                    while( $d = $db->fetch_array( $r ) )
                    {
                        $s2 = 'DELETE FROM lumonata_menu WHERE lcmenu_id = %d';
                        $q2 = $db->prepare_query( $s2, $d['lcmenu_id'] );
                        
                        if( $db->do_query( $q2 ) )
                        {
                            $s3 = 'DELETE FROM lumonata_menu_category WHERE larticle_id = %d';
                            $q3 = $db->prepare_query( $s3, $post_id );
                            
                            $db->do_query( $q3 );
                        }
                    }
                }
            }
        }
        else
        {
            $s = 'SELECT lcmenu_id FROM lumonata_menu_category WHERE larticle_id = %d';
            $q = $db->prepare_query( $s, $_POST[ 'id' ] );
            $r = $db->do_query( $q );

            if( $db->num_rows( $r ) )
            {
                while( $d = $db->fetch_array( $r ) )
                {
                    $s2 = 'DELETE FROM lumonata_menu WHERE lcmenu_id = %d';
                    $q2 = $db->prepare_query( $s2, $d['lcmenu_id'] );
                    
                    if( $db->do_query( $q2 ) )
                    {
                        $s3 = 'DELETE FROM lumonata_menu_category WHERE larticle_id = %d';
                        $q3 = $db->prepare_query( $s3, $_POST[ 'id' ] );
                        
                        $db->do_query( $q3 );
                    }
                }
            }
        }
    }
}

function get_menu_category_option( $post_id )
{
    global $db;

    $s = 'SELECT * FROM lumonata_menu_category WHERE larticle_id = %d ORDER BY lcmenu_parent, lorder ASC';
    $q = $db->prepare_query( $s, $post_id );
    $r = $db->do_query( $q );
    
    $options  = '';

    if( $db->num_rows( $r ) > 0 )
    {
        $dt = array();

        while( $d = $db->fetch_array( $r ) )
        {
            $dt[] = array( 
                'id'         => $d['lcmenu_id'], 
                'name'       => $d['lcmenu_name'],
                'article_id' => $d['larticle_id'],
                'parent_id'  => $d['lcmenu_parent'] 
            );
        }

        $data = build_tree( $dt );

        foreach( $data as $d )
        {
            $options .= '<option value="' . $d['id'] . '">' . $d['name'] . '</option>';

            if( isset( $d['child'] ) )
            {
                foreach( $d['child'] as $c )
                {
                    $options .= '<option class="with-tabs" value="' . $c['id'] . '">' . $c['name'] . '</option>';
                }
            }
        }
    }
    
    return $options;
}

function delete_pdf_menu_file( $post_id = '', $app_name = '' )
{
    if( empty( $post_id ) )
    {
        return true;
    }
    
    $pdf = get_additional_field( $post_id, 'menu_pdf', $app_name );
    
    if( empty( $pdf ) )
    {
        return true;
    }
    
    if( file_exists( PLUGINS_PATH . '/additional/pdf/' . $pdf ) )
    {
        unlink( PLUGINS_PATH . '/additional/pdf/' . $pdf );
    }
    
    if( edit_additional_field( $post_id, 'menu_pdf', '', $app_name ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function get_category_menu( $post_id )
{
    global $db;

    $s = 'SELECT * FROM lumonata_menu_category WHERE larticle_id = %d ORDER BY lorder ASC';
    $q = $db->prepare_query( $s, $post_id );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        $dt = array();

        while( $d = $db->fetch_array( $r ) )
        {
            $dt[] = array( 
                'id'         => $d['lcmenu_id'], 
                'name'       => $d['lcmenu_name'],
                'article_id' => $d['larticle_id'],
                'parent_id'  => $d['lcmenu_parent'] 
            );
        }

        $data = build_tree( $dt );

        return $data;
    }
}

function save_category_menu( $post_id, $cat_name )
{
    global $db;

    $s = 'INSERT INTO lumonata_menu_category( larticle_id, lcmenu_parent, lcmenu_name ) VALUES( %d, %d, %s )';
    $q = $db->prepare_query( $s, $post_id, 0, $cat_name );

    if( reset_order_id( 'lumonata_menu_category' ) )
    {
        if( $db->do_query( $q ) )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}

function update_category_menu( $cat_id, $cat_name )
{
    global $db;

    $s = 'UPDATE lumonata_menu_category SET lcmenu_name = %s WHERE lcmenu_id = %d';
    $q = $db->prepare_query( $s, $cat_name, $cat_id );
    
    if( $db->do_query( $q ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function reorder_category_menu( $list_arr = array() )
{
    global $db;

    if( empty( $list_arr ) )
    {
        return false;
    }
    else
    {
        foreach( $list_arr as $i => $obj )
        {
            $s = 'UPDATE lumonata_menu_category SET lorder = %d, lcmenu_parent = %d WHERE lcmenu_id = %d';
            $q = $db->prepare_query( $s, $i, 0, $obj['id'] );
            $r = $db->do_query( $q );

            if( isset( $obj['children'] ) )
            {
                foreach( $obj['children'] as $idx => $child )
                {
                    $s = 'UPDATE lumonata_menu_category SET lorder = %d, lcmenu_parent = %d WHERE lcmenu_id = %d';
                    $q = $db->prepare_query( $s, $idx, $obj['id'], $child['id'] );
                    $r = $db->do_query( $q );
                }
            }
        }

        return true;
    }
}

function delete_category_menu( $cat_id )
{
    global $db;

    $s = 'SELECT * FROM lumonata_menu_category WHERE lcmenu_id = %d OR lcmenu_parent = %d';
    $q = $db->prepare_query( $s, $cat_id, $cat_id );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        $id = array();

        while( $d = $db->fetch_array( $r ) )
        {
            $id[] = $d['lcmenu_id'];
        }

        $q2 = 'SELECT * FROM lumonata_menu WHERE lcmenu_id IN( ' . implode( ',', $id ) . ' )';
        $r2 = $db->do_query( $q2 );
        $n2 = $db->num_rows( $r2 );

        if( $n2 > 0 )
        {
            return false;
        }
        else
        {
            $s3 = 'DELETE FROM lumonata_menu_category WHERE lcmenu_id = %d';
            $q3 = $db->prepare_query( $s3, $cat_id );
            
            if( $db->do_query( $q3 ) )
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    else
    {
        $s2 = 'DELETE FROM lumonata_menu_category WHERE lcmenu_id = %d';
        $q2 = $db->prepare_query( $s2, $cat_id );
        
        if( $db->do_query( $q2 ) )
        {
            return true;
        }
        else
        {
            return false;
        } 
    }
}

function get_main_menu( $post_id )
{
    global $db;

    $s = 'SELECT *, ( 
                SELECT lcmenu_name 
                FROM lumonata_menu_category AS a2 
                WHERE a2.lcmenu_id = b.lcmenu_parent 
            ) AS lcmenu_parent_name
          FROM lumonata_menu AS a
          LEFT JOIN lumonata_menu_category AS b ON a.lcmenu_id = b.lcmenu_id
          WHERE b.larticle_id = %d ORDER BY a.lorder';
    $q = $db->prepare_query( $s, $post_id );

    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        $data = array();

        while( $d = $db->fetch_array( $r ) )
        {
            $data[] = array( 
                'lmenu_id'     => $d['lmenu_id'], 
                'lcmenu_id'    => $d['lcmenu_id'],
                'lmenu_name'   => $d['lmenu_name'],
                'lmenu_desc'   => $d['lmenu_desc'],
                'lmenu_price'  => $d['lmenu_price'],
                'lcmenu_name'  => $d['lcmenu_name'],
                'lmenu_type'   => empty( $d['lmenu_type'] ) ? '-' : json_decode( $d['lmenu_type'] ),
                'lcmenu_name2' => ( $d['lcmenu_parent_name'] != '' ? $d['lcmenu_parent_name'] . ' | ' . $d['lcmenu_name'] : $d['lcmenu_name'] )
            );
        }

        return $data;
    }
}

function set_menu_data( $cat, $menus )
{
    $i = 0;
    $r = array();

    if( !empty( $menus ) && is_array( $menus ) )
    {
        foreach( $menus as $menu )
        {
            $c = trim( $menu['lcmenu_name'] );

            if( !in_array( $c, $r ) )
            {
                if( isset( $r[ $c ][ $i ] ) )
                {
                    if( count( $r[ $c ][ $i ] ) < 2 )
                    {
                        $r[ $c ][ $i ][] = array(
                            'name'  => $menu['lmenu_name'],
                            'desc'  => $menu['lmenu_desc'],
                            'type'  => $menu['lmenu_type'],
                            'price' => $menu['lmenu_price']
                        );
                    }
                    else
                    {
                        $r[ $c ][ $i + 1 ][] = array(
                            'name'  => $menu['lmenu_name'],
                            'desc'  => $menu['lmenu_desc'],
                            'type'  => $menu['lmenu_type'],
                            'price' => $menu['lmenu_price']
                        );

                        $i = count( $r[ $c ][ $i ] ) == 2 && count( $r[ $c ][ $i + 1 ] ) == 2 ? $i + 1 : $i;
                    }
                }
                else
                {
                    $r[$c][$i][] = array(
                        'name'  => $menu['lmenu_name'],
                        'desc'  => $menu['lmenu_desc'],
                        'type'  => $menu['lmenu_type'],
                        'price' => $menu['lmenu_price']
                    );  
                }
            }
        }   
    }

    return $r;
}

function save_main_menu( $category, $name, $description = '', $type = array(), $price = 0 )
{
    global $db;

    if( empty( $type ) )
    {
        $s = 'INSERT INTO lumonata_menu( lcmenu_id, lmenu_name, lmenu_desc, lmenu_price ) VALUES( %d, %s, %s, %s )';
        $q = $db->prepare_query( $s, $category, $name, $description, $price );
    }
    else
    {
        $s = 'INSERT INTO lumonata_menu( lcmenu_id, lmenu_name, lmenu_desc, lmenu_type, lmenu_price ) VALUES( %d, %s, %s, %s, %s )';
        $q = $db->prepare_query( $s, $category, $name, $description, json_encode( $type ), $price );
    }
    
    if( reset_order_id( 'lumonata_menu' ) )
    {
        if( $db->do_query( $q ) )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}

function update_main_menu( $menu_id, $category, $name, $description = '', $type = array(), $price = 0 )
{
    global $db;

    if( empty( $type ) )
    {
        $s = 'UPDATE lumonata_menu SET 
                lcmenu_id = %s, 
                lmenu_name = %s,
                lmenu_desc = %s,
                lmenu_price = %s
              WHERE lmenu_id = %d';
        $q = $db->prepare_query( $s, $category, $name, $description, $price, $menu_id );
    }
    else
    {
        $s = 'UPDATE lumonata_menu SET 
                lcmenu_id = %s, 
                lmenu_name = %s,
                lmenu_desc = %s,
                lmenu_type = %s,
                lmenu_price = %s
              WHERE lmenu_id = %d';
        $q = $db->prepare_query( $s, $category, $name, $description, json_encode( $type ), $price, $menu_id );
    }
    
    if( $db->do_query( $q ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function reorder_main_menu( $list_arr = array() )
{
    global $db;

    if( empty( $list_arr ) )
    {
        return false;
    }
    else
    {
        foreach( $list_arr as $i => $obj )
        {
            $s = 'UPDATE lumonata_menu SET lorder = %d WHERE lmenu_id = %d';
            $q = $db->prepare_query( $s, $i, $obj['id'] );
            $r = $db->do_query( $q );
        }

        return true;
    }
}

function delete_main_menu( $menu_id )
{
    global $db;

    $s = 'DELETE FROM lumonata_menu WHERE lmenu_id = %d';
    $q = $db->prepare_query( $s, $menu_id );
    
    if( $db->do_query( $q ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function execute_menus_ajax_request()
{    
    if( is_ajax_request() )
    {
        if( $_POST[ 'ajax_key' ] == 'get-category-menu' )
        {
            $data = get_category_menu( $_POST['post_id'] );

            if( empty( $data ) )
            {
                echo json_encode( array( 'result' => 'failed' ) );
            }
            else
            {
                echo json_encode( array( 'result' => 'success', 'data' => $data ) );
            }
        }

        if( $_POST[ 'ajax_key' ] == 'delete-category-menu' )
        {
            if( delete_category_menu( $_POST['cat_id'] ) )
            {
                echo json_encode( array( 'result' => 'success' ) );
            }
            else
            {
                echo json_encode( array( 'result' => 'failed' ) ); 
            }
        }

        if( $_POST[ 'ajax_key' ] == 'save-category-menu' )
        {
            if( save_category_menu( $_POST['post_id'], $_POST['cat_name'] ) )
            {
                echo json_encode( array( 'result' => 'success' ) );
            }
            else
            {
                echo json_encode( array( 'result' => 'failed' ) ); 
            }
        }

        if( $_POST[ 'ajax_key' ] == 'update-category-menu' )
        {
            if( update_category_menu( $_POST['cat_id'], $_POST['cat_name'] ) )
            {
                echo json_encode( array( 'result' => 'success' ) );
            }
            else
            {
                echo json_encode( array( 'result' => 'failed' ) ); 
            }
        }

        if( $_POST[ 'ajax_key' ] == 'reorder-category-menu' )
        {
            if( reorder_category_menu( $_POST['list_arr'] ) )
            {
                echo json_encode( array( 'result' => 'success' ) );
            }
            else
            {
                echo json_encode( array( 'result' => 'failed' ) ); 
            }
        }

        if( $_POST[ 'ajax_key' ] == 'get-category-menu-option' )
        {
            $options = get_menu_category_option( $_POST['post_id'] );

            if( empty( $options ) )
            {
                echo json_encode( array( 'result' => 'failed' ) );
            }
            else
            {
                echo json_encode( array( 'result' => 'success', 'data' => $options ) );
            }
        }

        if( $_POST[ 'ajax_key' ] == 'get-main-menu' )
        {
            $data = get_main_menu( $_POST['post_id'] );

            if( empty( $data ) )
            {
                echo json_encode( array( 'result' => 'failed' ) );
            }
            else
            {
                echo json_encode( array( 'result' => 'success', 'data' => $data ) );
            }
        }

        if( $_POST[ 'ajax_key' ] == 'save-main-menu' )
        {
            $_POST['type'] = isset( $_POST['type'] ) ? $_POST['type'] : array();

            if( save_main_menu( $_POST['category'], $_POST['name'], $_POST['desc'], $_POST['type'], $_POST['price'] ) )
            {
                echo json_encode( array( 'result' => 'success' ) );
            }
            else
            {
                echo json_encode( array( 'result' => 'failed' ) ); 
            }
        }

        if( $_POST[ 'ajax_key' ] == 'update-main-menu' )
        {
            $_POST['type'] = isset( $_POST['type'] ) ? $_POST['type'] : array();

            if( update_main_menu( $_POST['menu_id'], $_POST['category'], $_POST['name'], $_POST['desc'], $_POST['type'], $_POST['price'] ) )
            {
                echo json_encode( array( 'result' => 'success' ) );
            }
            else
            {
                echo json_encode( array( 'result' => 'failed' ) ); 
            }
        }

        if( $_POST[ 'ajax_key' ] == 'reorder-main-menu' )
        {
            if( reorder_main_menu( $_POST['list_arr'] ) )
            {
                echo json_encode( array( 'result' => 'success' ) );
            }
            else
            {
                echo json_encode( array( 'result' => 'failed' ) ); 
            }
        }

        if( $_POST[ 'ajax_key' ] == 'delete-main-menu' )
        {
            if( delete_main_menu( $_POST['menu_id'] ) )
            {
                echo json_encode( array( 'result' => 'success' ) );
            }
            else
            {
                echo json_encode( array( 'result' => 'failed' ) ); 
            }
        }

        if( $_POST['ajax_key'] == 'delete-pdf-menu-file' )
        {
            if( delete_pdf_menu_file( $_POST['post_id'], $_POST['app_name'] ) )
            {
                echo json_encode( array( 'result' => 'success' ) );
            }
            else
            {
                echo json_encode( array( 'result' => 'failed' ) ); 
            }
        }
    }
    
    exit;
}

?>