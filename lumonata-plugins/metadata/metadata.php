<?php

/*
	Plugin Name: Lumonata Meta Data
	Plugin URL: http://lumonata.com/
	Description: This plugin is use for adding Meta Data in each post like Meta Title, Meta Keywords and Meta Description.
	Author: Wahya Biantara
	Author URL: http://wahya.biantara.com/
	Version: 1.0.1
*/

//-- Get Multi Language
$use_multi_lang = get_meta_data( 'multi_language' );
$lang_list_data = get_all_language();

if( $use_multi_lang == 1 && empty( $lang_list_data ) === FALSE )
{
    foreach( $lang_list_data as $d )
    {
        extract( $d );

		add_actions( 'page_additional_data_' . $llanguage_code, 'additional_data', 'Meta Data', 'meta_data', true, array( 'llanguage_code' => $llanguage_code ) );
		add_actions( 'articles_additional_data_' . $llanguage_code, 'additional_data', 'Meta Data', 'meta_data', true, array( 'llanguage_code' => $llanguage_code ) );
    }
}

add_apps_privileges( 'metadata', 'Meta Data' );

add_main_menu( array( 'metadata' => 'Meta Data' ) );

add_actions( 'metadata', 'set_home_meta_data' );
add_actions( 'page_additional_data', 'additional_data', 'Meta Data', 'meta_data' );
add_actions( 'articles_additional_data', 'additional_data', 'Meta Data', 'meta_data' );
add_actions( 'css_elements', 'get_custom_css', '//' . site_url() . '/lumonata-plugins/metadata/css/menus.css' );

function meta_data( $llanguage_code = '' )
{
    global $thepost;

    $index   = $thepost->post_index;
    $post_id = $thepost->post_id;
    
    $mtitle = '';
    $mkey   = '';
    $mdesc  = '';

    if( is_edit() )
    {    	
        $apps = $_GET['state'] == 'applications' ? $_GET['sub'] : $_GET['state'];

    	if( empty( $llanguage_code ) )
    	{
	        $mtitle  = get_additional_field( $post_id, 'meta_title', $apps );
	        $mkey    = get_additional_field( $post_id, 'meta_keywords', $apps );
	        $mdesc   = get_additional_field( $post_id, 'meta_description', $apps );

	        return '
	    	<div class="clearfix" style="padding:5px;">
	            <p>
	            	Title:<br /><br />
					<input type="text" class="textbox" name="additional_fields[meta_title][' . $index . ']" value="' . $mtitle . '" autocomplete="off" />
				</p>
				<p>
					Description:<br /><br />
					<textarea class="textarea" name="additional_fields[meta_description][' . $index . ']" autocomplete="off">' . $mdesc . '</textarea>
				</p>
				<p>
					Keywords:<br /><br />
					<input type="text" class="textbox" name="additional_fields[meta_keywords][' . $index . ']" value="' . $mkey . '" autocomplete="off" />
				</p>
			</div>';
		}
		else
		{
	        $mtitle  = get_additional_field( $post_id, 'meta_title_' . $llanguage_code, $apps );
	        $mkey    = get_additional_field( $post_id, 'meta_keywords_' . $llanguage_code, $apps );
	        $mdesc   = get_additional_field( $post_id, 'meta_description_' . $llanguage_code, $apps );

	        return '
	    	<div class="clearfix" style="padding:5px;">
	            <p>
	            	Title:<br /><br />
					<input type="text" class="textbox" name="additional_fields[meta_title_' . $llanguage_code . '][' . $index . ']" value="' . $mtitle . '" autocomplete="off" />
				</p>
				<p>
					Description:<br /><br />
					<textarea class="textarea" name="additional_fields[meta_description_' . $llanguage_code . '][' . $index . ']" autocomplete="off">' . $mdesc . '</textarea>
				</p>
				<p>
					Keywords:<br /><br />
					<input type="text" class="textbox" name="additional_fields[meta_keywords_' . $llanguage_code . '][' . $index . ']" value="' . $mkey . '" autocomplete="off" />
				</p>
			</div>';
		}
    }
    else
    {
    	if( empty( $llanguage_code ) )
    	{
	        return '
	    	<div class="clearfix" style="padding:5px;">
	            <p>
	            	Title:<br /><br />
					<input type="text" class="textbox" name="additional_fields[meta_title][' . $index . ']" autocomplete="off" />
				</p>
				<p>
					Description:<br /><br />
					<textarea class="textarea" name="additional_fields[meta_description][' . $index . ']" autocomplete="off"></textarea>
				</p>
				<p>
					Keywords:<br /><br />
					<input type="text" class="textbox" name="additional_fields[meta_keywords][' . $index . ']" autocomplete="off" />
				</p>
			</div>';
    	}
    	else
    	{
	        return '
	    	<div class="clearfix" style="padding:5px;">
	            <p>
	            	Title:<br /><br />
					<input type="text" class="textbox" name="additional_fields[meta_title_' . $llanguage_code . '][' . $index . ']" autocomplete="off" />
				</p>
				<p>
					Description:<br /><br />
					<textarea class="textarea" name="additional_fields[meta_description_' . $llanguage_code . '][' . $index . ']" autocomplete="off"></textarea>
				</p>
				<p>
					Keywords:<br /><br />
					<input type="text" class="textbox" name="additional_fields[meta_keywords_' . $llanguage_code . '][' . $index . ']" autocomplete="off" />
				</p>
			</div>';
    	}
    }
}

function get_meta_title( $title = '' )
{
	if( empty( $title ) )
	{
	    $id = post_to_id();

	    if( !empty( $id ) )
	    {
	    	$meta_title = strip_tags( get_additional_field( $id, 'meta_title', get_appname() ) );

	    	if( empty( $meta_title ) )
	        {
	        	$d = fetch_articles( 'id=' . $id );

	        	return $d['larticle_title'];
	        }
	        else
	        {
	        	return $meta_title;
	        }
	    }	
	}
	else
	{
		return strip_tags( $title );	
	}
}

function get_meta_keywords( $keywords='' )
{
	if( empty( $keywords ) )
	{
	    $id = post_to_id();

	    if( !empty( $id ) )
	    {
	        $meta_keywords = strip_tags( get_additional_field( $id, 'meta_keywords', get_appname() ) );

	        if( !empty( $meta_keywords ) )
	        {
	            return '<meta name="keywords" content="' . $meta_keywords . '" />';
	        }
	    }
	}
	else
	{
		return '<meta name="keywords" content="' . strip_tags( $keywords ) . '" />';
	}
}

function get_meta_description( $description='' )
{
	if( empty( $description ) )
	{
	    $id = post_to_id();

	    if( !empty( $id ) )
	    {
	        $meta_description = strip_tags( get_additional_field( $id, 'meta_description', get_appname() ) );

	        if( !empty( $meta_description ) )
	        {
	            return '<meta name="description" content="' . $meta_description . '" />';
	        }
	    }
	}
	else
	{
		return '<meta name="description" content="' . strip_tags( $description ) . '" />';
	}
}

function set_home_meta_data()
{
	$alert = '';

    if( isset( $_POST['save_changes'] ) )
    {
        foreach( $_POST as $key => $val )
        {
            if( $key != "save_changes" )
            {
                if( find_meta_data( $key ) )
                {
                    $update = update_meta_data( $key, $val );
                }
                else
                {
                    $update = set_meta_data( $key, $val );
                }
            }
        }

        if( $update )
        {
            $alert = '<div class="alert_green_form">Meta data has been updated.</div>';
        }
        else
        {
            $alert = '<div class="alert_green_form">Meta data failed to update.</div>';
        }
    }

    $meta_title       = get_meta_data( 'meta_title' );
    $meta_keywords    = get_meta_data( 'meta_keywords' );
    $meta_description = get_meta_data( 'meta_description' );

	$use_multi_lang   = get_meta_data( 'multi_language' );
	$lang_list_data   = get_all_language();

    add_actions( 'section_title', 'Meta Data' );

    $meta_content = '
    <h1>Meta Data</h1>
	<p>This settings will change your meta data on your home page</p>
	<div class="tab_container">
		<div class="single_content">
			' . $alert . '
			<form method="post" action="#">';				

				if( $use_multi_lang == 1 && empty( $lang_list_data ) === FALSE )
				{
        			$current_lang  = '';
    				$meta_content .= '
			        <div class="button-language {btn_cls}">
			            <ul class="clearfix">';
						    foreach( $lang_list_data as $d )
						    {					            
						        extract( $d );

					            if( $ldefault == 1 )
					            {
					                $current_lang = $llanguage_code;
					            }

    							$meta_content .= '
				                <li class="' . ( $ldefault == 1 ? 'active' : '' ) . '">
				                    <a href="#" data-filter="' . $llanguage_code . '" class="btn">
				                        <img src="//' . TEMPLATE_URL . '/flags/' . $llanguage_code . '.svg" alt="" width="24" />
				                        ' . $llanguage . '
				                    </a>
				                </li>';
						    }

    						$meta_content .= '
			            </ul>
			        </div>

	        		<div class="content-loop content-lang ' . $current_lang . ' active">
	                	<div class="list_edit">
							<fieldset>
								<p><label>Meta Title:</label></p>
								<input type="text" id="meta_title" name="meta_title" value="'.( empty( $meta_title ) ? '' : $meta_title ).'" class="textbox" />
						    </fieldset>
						    <fieldset>
								<p><label>Meta Keywords:</label></p>
								<input type="text" id="meta_keywords" name="meta_keywords" value="'.( empty( $meta_keywords ) ? '' : $meta_keywords ).'" class="textbox" />
						    </fieldset>
						    <fieldset>
								<p><label>Meta Description:</label></p>
								<textarea class="textarea" type="text" name="meta_description">'.( empty( $meta_description ) ? '' : $meta_description ).'</textarea>
						    </fieldset>
						</div>
					</div>';

				    foreach( $lang_list_data as $d )
				    {
						extract( $d );

			            if( $ldefault == 1 )
			            {
			                continue;
			            }

					    $llanguage_meta_title       = get_meta_data( 'meta_title_' . $llanguage_code );
					    $llanguage_meta_keywords    = get_meta_data( 'meta_keywords_' . $llanguage_code );
					    $llanguage_meta_description = get_meta_data( 'meta_description_' . $llanguage_code );

    					$meta_content .= '
        				<div class="content-loop content-lang ' . $llanguage_code . ' ' . ( $ldefault == 1 ? 'active' : '' ) . '">
                			<div class="list_edit">
								<fieldset>
									<p><label>Meta Title:</label></p>
									<input type="text" id="meta_title_' . $llanguage_code . '" name="meta_title_' . $llanguage_code . '" value="'. ( empty( $llanguage_meta_title ) ? '' : $llanguage_meta_title ) . '" class="textbox" />
							    </fieldset>
							    <fieldset>
									<p><label>Meta Keywords:</label></p>
									<input type="text" id="meta_keywords_' . $llanguage_code . '" name="meta_keywords_' . $llanguage_code . '" value="' . ( empty( $llanguage_meta_keywords ) ? '' : $llanguage_meta_keywords ) . '" class="textbox" />
							    </fieldset>
							    <fieldset>
									<p><label>Meta Description:</label></p>
									<textarea class="textarea" type="text" name="meta_description_' . $llanguage_code . '">' . ( empty( $llanguage_meta_description ) ? '' : $llanguage_meta_description ) . '</textarea>
							    </fieldset>
							</div>
        				</div>';
				    }
				}
				else
				{
    				$meta_content .= '
	        		<div class="content-loop active">
	                	<div class="list_edit">
							<fieldset>
								<p><label>Meta Title:</label></p>
								<input type="text" id="meta_title" name="meta_title" value="'.( empty( $meta_title ) ? '' : $meta_title ).'" class="textbox" />
						    </fieldset>
						    <fieldset>
								<p><label>Meta Keywords:</label></p>
								<input type="text" id="meta_keywords" name="meta_keywords" value="'.( empty( $meta_keywords ) ? '' : $meta_keywords ).'" class="textbox" />
						    </fieldset>
						    <fieldset>
								<p><label>Meta Description:</label></p>
								<textarea class="textarea" type="text" name="meta_description">'.( empty( $meta_description ) ? '' : $meta_description ).'</textarea>
						    </fieldset>
						</div>
					</div>';
				}
    			
    			$meta_content .= '
			    <div class="button_wrapper clearfix">
					<ul class="button_navigation">
						<li>' . save_changes_botton() . '</li>
					</ul>
			    </div>
			</form>
		</div>
	</div>';

	return $meta_content;
}

?>