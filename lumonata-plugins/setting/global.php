<?php
/*
    Plugin Name: Global Setting
    Plugin URL: http://lumonata.com/
    Description: -
    Author: Ngurah Rai
    Author URL: http://lumonata.com/
    Version: 1.0.1
*/

add_privileges( 'administrator', 'global', 'insert' );
add_privileges( 'administrator', 'global', 'update' );
add_privileges( 'administrator', 'global', 'delete' );

add_actions( 'tab_admin_aditional', 'view_global_setting', array( 'global' => 'Global' ), 'global' );
add_actions( 'tab_admin_aditional', 'view_global_setting', array( 'landing' => 'Landing Page' ), 'landing' );

function view_global_setting( $param, $tab )
{
    if( is_ajax_request() )
    {
        return execute_global_setting_ajax_request();
    }
    else
    {
        $alert = '';

        if( isset( $_POST[ 'save_changes' ] ) )
        {
            $update = manage_global_setting();

            if( $update )
            {
                $alert = '<div class="alert_green_form">Your global setting has been updated.</div>';
            }
            else
            {
                $alert = 'div class="alert_green_form">Your global setting failed to update.</div>';
            }
        }

        set_template( PLUGINS_PATH . '/setting/form.html', 'add-settings-template' );

        add_block( 'global-form', 'g-block', 'add-settings-template' );
        add_block( 'landing-form', 'l-block', 'add-settings-template' );
        add_block( 'settings-form', 's-block', 'add-settings-template' );

        add_actions( 'section_title', ' Setting' );

        add_variable( 'alert', $alert );
        add_variable( 'admin_tab', setting_admin_tabs() );

        if( $tab == 'global' )
        {
            $smtp_email_addr = get_meta_data( 'smtp_email_address' );
            $smtp_email_from = get_meta_data( 'smtp_email_from' );
            $smtp_email_port = get_meta_data( 'smtp_email_port' );
            $smtp_email_pwd  = get_meta_data( 'smtp_email_pwd' );

            $contact_info_map_addrs = get_meta_data( 'contact_info_map_addrs' );
            $contact_info_wa_link   = get_meta_data( 'contact_info_wa_link' );
            $contact_info_addrs     = get_meta_data( 'contact_info_addrs' );
            $contact_info_email     = get_meta_data( 'contact_info_email' );
            $contact_info_phone     = get_meta_data( 'contact_info_phone' );
            $contact_info_title     = get_meta_data( 'contact_info_title' );
            $contact_info_desc      = get_meta_data( 'contact_info_desc' );
            $contact_info_fax       = get_meta_data( 'contact_info_fax' );
            $contact_info_wa        = get_meta_data( 'contact_info_wa' );

            $r_public_key    = get_meta_data( 'r_public_key' );
            $r_secret_key    = get_meta_data( 'r_secret_key' );

            $g_tag_manager   = get_meta_data( 'g_tag_manager' );
            $g_analytic      = get_meta_data( 'g_analytic' );

            $sojern_script   = get_meta_data( 'sojern_script' );
            $sojern_script_product   = get_meta_data( 'sojern_script_product' );
            $sojern_script_tracking   = get_meta_data( 'sojern_script_tracking' );

            $footer_text_title   = get_meta_data( 'footer_text_title' );
            $footer_text_content = get_meta_data( 'footer_text_content' );

            $thanks_contact_title            = get_meta_data( 'thanks_contact_title' );
            $thanks_contact_content          = get_meta_data( 'thanks_contact_content' );
            $thanks_contact_meta_title       = get_meta_data( 'thanks_contact_meta_title' );
            $thanks_contact_meta_keywords    = get_meta_data( 'thanks_contact_meta_keywords' );
            $thanks_contact_meta_description = get_meta_data( 'thanks_contact_meta_description' );

            $thanks_booking_title            = get_meta_data( 'thanks_booking_title' );
            $thanks_booking_content          = get_meta_data( 'thanks_booking_content' );
            $thanks_booking_meta_title       = get_meta_data( 'thanks_booking_meta_title' );
            $thanks_booking_meta_keywords    = get_meta_data( 'thanks_booking_meta_keywords' );
            $thanks_booking_meta_description = get_meta_data( 'thanks_booking_meta_description' );

            $social_media_facebook  = get_meta_data( 'social_media_facebook' );
            $social_media_instagram = get_meta_data( 'social_media_instagram' );

            $mailchimp_api_key = get_meta_data( 'mailchimp_api_key' );
            $mailchimp_list_id = get_meta_data( 'mailchimp_list_id' );

            $ichronoz_api_key = get_meta_data( 'ichronoz_api_key' );

            add_variable( 'smtp_email_address', $smtp_email_addr );
            add_variable( 'smtp_email_from', $smtp_email_from );
            add_variable( 'smtp_email_port', $smtp_email_port );
            add_variable( 'smtp_email_pwd', $smtp_email_pwd );

            add_variable( 'contact_info_desc', textarea( 'contact_info_desc', 0, $contact_info_desc, null, false, false ) );
            add_variable( 'contact_info_map_addrs', $contact_info_map_addrs );
            add_variable( 'contact_info_wa_link', $contact_info_wa_link );
            add_variable( 'contact_info_addrs', $contact_info_addrs );
            add_variable( 'contact_info_email', $contact_info_email );
            add_variable( 'contact_info_phone', $contact_info_phone );
            add_variable( 'contact_info_title', $contact_info_title );
            add_variable( 'contact_info_fax', $contact_info_fax );
            add_variable( 'contact_info_wa', $contact_info_wa );

            add_variable( 'r_public_key', $r_public_key );
            add_variable( 'r_secret_key', $r_secret_key );

            add_variable( 'mailchimp_api_key', $mailchimp_api_key );
            add_variable( 'mailchimp_list_id', $mailchimp_list_id );

            add_variable( 'ichronoz_api_key', $ichronoz_api_key );

            add_variable( 'g_analytic', $g_analytic );
            add_variable( 'g_tag_manager', $g_tag_manager );

            add_variable( 'sojern_script', $sojern_script );
            add_variable( 'sojern_script_product', $sojern_script_product );
            add_variable( 'sojern_script_tracking', $sojern_script_tracking );

            add_variable( 'footer_text_title', $footer_text_title );
            add_variable( 'footer_text_content', textarea( 'footer_text_content', 0, $footer_text_content, null, false, false ) );

            add_variable( 'thanks_contact_title', $thanks_contact_title );
            add_variable( 'thanks_contact_meta_title', $thanks_contact_meta_title );
            add_variable( 'thanks_contact_meta_keywords', $thanks_contact_meta_keywords );
            add_variable( 'thanks_contact_meta_description', $thanks_contact_meta_description );
            add_variable( 'thanks_contact_content', textarea( 'thanks_contact_content', 0, $thanks_contact_content, null, false, false ) );

            add_variable( 'thanks_booking_title', $thanks_booking_title );
            add_variable( 'thanks_booking_meta_title', $thanks_booking_meta_title );
            add_variable( 'thanks_booking_meta_keywords', $thanks_booking_meta_keywords );
            add_variable( 'thanks_booking_meta_description', $thanks_booking_meta_description );
            add_variable( 'thanks_booking_content', textarea( 'thanks_booking_content', 0, $thanks_booking_content, null, false, false ) );

            add_variable( 'social_media_facebook', $social_media_facebook );
            add_variable( 'social_media_instagram', $social_media_instagram );

            parse_template( 'global-form', 'g-block', false );
        }
        elseif( $tab == 'landing' )
        {
            $imgt = get_meta_data('homepage_top_image','static_setting');
            $hm_link_promo = get_meta_data('hm_link_promo');

            $selected_ssl = get_meta_data('banner_option');
            $arr_ssl = array('No','Yes');
            $options_ssl = "";

            foreach($arr_ssl as $key_ssl =>$ssl){
              $options_ssl .= "<option ".($key_ssl==$selected_ssl?"selected":"")." value=\"$key_ssl\">$ssl</option>";
            }

            add_variable('banner_option',$options_ssl);
            add_variable('landing_link_video',get_meta_data( 'landing_link_video' ));
            add_variable('header_method',get_meta_data( 'header_method' ));
            add_variable('link_booking_engine',get_meta_data( 'link_booking_engine' ));

            $landing_page_section_1_title    = get_meta_data( 'landing_page_section_1_title' );
            $landing_page_section_1_content  = get_meta_data( 'landing_page_section_1_content' );
            $landing_page_section_1_subtitle = get_meta_data( 'landing_page_section_1_subtitle' );

            $landing_page_section_2_title     = get_meta_data( 'landing_page_section_2_title' );
            $landing_page_section_2_subtitle  = get_meta_data( 'landing_page_section_2_subtitle' );
            $landing_page_section_2_link_text = get_meta_data( 'landing_page_section_2_link_text' );

            $landing_page_section_3_title     = get_meta_data( 'landing_page_section_3_title' );
            $landing_page_section_3_link_text = get_meta_data( 'landing_page_section_3_link_text' );

            $landing_page_section_4_title   = get_meta_data( 'landing_page_section_4_title' );
            $landing_page_section_4_content = get_meta_data( 'landing_page_section_4_content' );
            $wbd_content = get_meta_data( 'wbd_content' );

            $landing_page_section_5_link_text   = get_meta_data( 'landing_page_section_5_link_text' );
            $landing_page_section_5_button_text = get_meta_data( 'landing_page_section_5_button_text' );

            $landing_page_section_6_title   = get_meta_data( 'landing_page_section_6_title' );
            $landing_page_section_6_content = get_meta_data( 'landing_page_section_6_content' );

            $landing_page_section_7_link      = get_meta_data( 'landing_page_section_7_link' );
            $landing_page_section_7_title     = get_meta_data( 'landing_page_section_7_title' );
            $wbd_title     = get_meta_data( 'wbd_title' );
            $landing_page_section_7_link_text = get_meta_data( 'landing_page_section_7_link_text' );

            add_variable( 'post_on_slide', set_post_list_option() );

            add_variable( 'landing_page_section_1_title', $landing_page_section_1_title );
            add_variable( 'landing_page_section_1_subtitle', $landing_page_section_1_subtitle );
            add_variable( 'landing_page_section_1_content', textarea( 'landing_page_section_1_content', 0, $landing_page_section_1_content, null, false, false ) );

            add_variable( 'landing_page_section_2_title', $landing_page_section_2_title );
            add_variable( 'landing_page_section_2_subtitle', $landing_page_section_2_subtitle );
            add_variable( 'landing_page_section_2_link_text', $landing_page_section_2_link_text );

            add_variable( 'landing_page_section_3_title', $landing_page_section_3_title );
            add_variable( 'landing_page_section_3_link_text', $landing_page_section_3_link_text );

            add_variable( 'landing_page_section_4_title', $landing_page_section_4_title );
            add_variable( 'landing_page_section_4_content', textarea( 'landing_page_section_4_content', 0, $landing_page_section_4_content, null, false, false ) );
            add_variable( 'wbd_content', textarea( 'wbd_content', 0, $wbd_content, null, false, false ) );

            add_variable( 'landing_page_section_5_link_text', $landing_page_section_5_link_text );
            add_variable( 'landing_page_section_5_button_text', $landing_page_section_5_button_text );

            add_variable( 'landing_page_section_6_title', $landing_page_section_6_title );
            add_variable( 'landing_page_section_6_content', textarea( 'landing_page_section_6_content', 0, $landing_page_section_6_content, null, false, false ) );

            add_variable( 'landing_page_section_7_link', $landing_page_section_7_link );
            add_variable( 'landing_page_section_7_title', $landing_page_section_7_title );
            add_variable( 'wbd_title', $wbd_title );
            add_variable( 'landing_page_section_7_link_text', $landing_page_section_7_link_text );

            add_actions( 'css_elements', 'get_css_inc', 'dropzone-5.5.0/dist/min/dropzone.min.css' );

            add_actions( 'js_elements', 'get_javascript', 'jquery.mjs.nestedSortable.js' );
            add_actions( 'js_elements', 'get_javascript_inc', 'dropzone-5.5.0/dist/min/dropzone.min.js' );
            add_actions( 'js_elements', 'get_custom_javascript', '//' . SITE_URL . '/lumonata-plugins/setting/script.js' );


            // NOTE: POPUP PROMO
            add_variable( 'hm_link_promo', $hm_link_promo );
            $background_top  = set_homepage_bg_top_content($imgt);
            add_variable('background_img_top', $background_top);


            parse_template( 'landing-form', 'l-block', false );
        }

        add_variable( 'plugin_url', SITE_URL . '/lumonata-plugins/setting' );
        add_variable( 'state_url', get_state_url( 'global_settings&tab=' . $tab ) );

        add_actions( 'js_elements', 'get_javascript_inc', 'tinymce/tinymce.min.js' );
        add_actions( 'js_elements', 'get_javascript', 'tinymce' );

        parse_template( 'settings-form', 's-block', false );

        return return_template( 'add-settings-template' );
    }
}

function set_homepage_bg_top_content($img)
{
    if (!empty($img)) {
        return '
      <div class="box">
          <img src="//' . SITE_URL . '/lumonata-plugins/setting/background/thumbnail/' . $img . '" />
          <div class="ioverlay" style="display: none;">
              <a data-bg-name="' . $img . '" class="top-delete-header">
                  <img src="//' . SITE_URL . '/lumonata-plugins/setting/images/delete.png">
              </a>
          </div>
      </div>';
    }
}

function is_num_global_setting( $key )
{
    global $db;

    $s = 'SELECT * FROM lumonata_meta_data WHERE lmeta_name=%s AND lapp_name=%s';
    $q = $db->prepare_query( $s, $key, 'global_setting' );
    $n = count_rows( $q );

    if( $n > 0 )
    {
        return true;
    }

    return false;
}

function manage_global_setting()
{
    global $db;

    foreach( $_POST as $key => $val )
    {
        $not_include = array( 'save_changes' );

        if( !in_array( $key, $not_include ) )
        {
            update_meta_data( $key, $val, 'global_setting' );
        }
    }

    return true;
}

function setting_admin_tabs()
{
    global $actions;

    $tabs = run_actions( 'tab_admin' );
    $tabs = $actions->action[ 'tab_admin' ][ 'args' ][ 0 ][ 0 ];

    if( is_contributor() || is_author() )
    {
        $tabs = $tabs[ 0 ];
    }
    else
    {
        $tabs = $tabs;
    }

    $tabb     = '';
    $tab_keys = array_keys( $tabs );

    if( empty( $_GET[ 'tab' ] ) )
    {
        $the_tab = $tab_keys[ 0 ];
    }
    else
    {
        $the_tab = $_GET[ 'tab' ];
    }

    $tabs = set_tabs( $tabs, $the_tab );

    return $tabs;
}

function set_post_list_option( $post_id = '' )
{
    global $db;

    $site_url = site_url();

    $s = 'SELECT DISTINCT larticle_type FROM lumonata_articles WHERE larticle_type NOT IN ("slideshow","banner")';
    $q = $db->prepare_query( $s );
    $r = $db->do_query( $q );
    $n = $db->num_rows( $r );

    $option = '
    <option value="homepage" data-type="homepage" data-link="//' . $site_url . '">Homepage</option>';

    if( $n > 0 )
    {
        $data = array();

        while( $d = $db->fetch_array( $r ) )
        {
            $data[]  = $d;
            $option .= '
            <option value="' . $d[ 'larticle_type' ] . '" data-type="archive" data-link="//' . $site_url . '/' . $d[ 'larticle_type' ] . '/">' . ucfirst( $d[ 'larticle_type' ] ) . ' Archive</option>';
        }

        foreach( $data as $d )
        {
            $s3 = 'SELECT * FROM lumonata_articles WHERE larticle_type=%s ORDER BY lorder';
            $q3 = $db->prepare_query( $s3, $d[ 'larticle_type' ] );
            $r3 = $db->do_query( $q3 );

            $option .= '<optgroup label="' . ucfirst( $d[ 'larticle_type' ] ) . '">';

            while( $d3 = $db->fetch_array( $r3 ) )
            {
                $permalink = permalink( $d3[ 'larticle_id' ] );

                if( $d3[ 'larticle_id' ] == $post_id )
                {
                    $option .= '<option selected value="' . $d3[ 'larticle_id' ] . '" data-type="detail" data-link="' . $permalink . '">' . $d3[ 'larticle_title' ] . '</option>';
                }
                else
                {
                    $option .= '<option value="' . $d3[ 'larticle_id' ] . '" data-type="detail" data-link="' . $permalink . '">' . $d3[ 'larticle_title' ] . '</option>';
                }
            }

            $option .= '</optgroup>';
        }
    }

    return $option;
}

function get_homepage_slideshow()
{
    global $db;

    $s = 'SELECT lmeta_id, lmeta_value FROM lumonata_meta_data WHERE lmeta_name = %s AND lapp_name = %s AND lapp_id = %d ORDER BY lmeta_id';
    $q = $db->prepare_query( $s, 'homepage_slideshow', 'global_setting', 0 );
    $r = $db->do_query( $q );

    $data     = array();
    $site_url = site_url();

    if( $db->num_rows( $r ) > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $idx = get_meta_data( 'homepage_slideshow_order', 'global_setting', $d['lmeta_id'] );
            $arr = explode( '/', $d['lmeta_value'] );
            $arr = array_reverse( $arr );

            $data[ $idx ] = array(
                'img_name' => $arr[0],
                'img_id' => $d['lmeta_id'],
                'img_path' => ROOT_PATH . $d['lmeta_value'],
                'img_url' => '//' . $site_url . $d['lmeta_value'],
                'img_size' => filesize( ROOT_PATH . $d['lmeta_value'] ),
                'img_low_res' => '//' . $site_url . '/lumonata-functions/mthumb.php?src=' . HTSERVER . '//' . $site_url . $d['lmeta_value'] . '&w=15',
                'img_thumb' => '//' . $site_url . '/lumonata-functions/mthumb.php?src=' . HTSERVER . '//' . $site_url . $d['lmeta_value'] . '&w=120&h=120'
            );
        }

        ksort( $data );

        return $data;
    }
}

function add_homepage_slideshow()
{
    global $db;

    $data   = array();
    $folder = upload_folder_name();

    if( isset( $_FILES[ 'file' ]['error'] ) && $_FILES[ 'file' ]['error'] == 0 )
    {
        if( !defined( 'FILES_LOCATION' ) )
        {
            define( 'FILES_LOCATION', '/lumonata-content/files' );
        }

        //-- Create destination folder if folder is not exist yet
        if( is_dir( FILES_PATH .'/'. $folder ) === FALSE )
        {
            if( create_dir( FILES_PATH .'/'. $folder ) === FALSE )
            {
                return $data;
            }
        }

        $file_name   = $_FILES[ 'file' ][ 'name' ];
        $file_size   = $_FILES[ 'file' ][ 'size' ];
        $file_type   = $_FILES[ 'file' ][ 'type' ];
        $file_source = $_FILES[ 'file' ][ 'tmp_name' ];

        extract( $_POST );

        if( is_allow_file_type( $file_type, 'image' ) )
        {
            if( is_allow_file_size( $file_size ) )
            {
                $default_title   = file_name_filter( $file_name );
                $file_name       = character_filter( $file_name );
                $file_name       = file_name_filter( $file_name ) . '-' . time() . file_name_filter( $file_name, true );

                //-- Resize Image
                $file_location    = FILES_LOCATION . '/' . $folder . '/' . $file_name;
                $file_destination = FILES_PATH . '/' . $folder . '/' . $file_name;

                if( upload_resize( $file_source, $file_destination, $file_type, 1920, 1920 ) )
                {
                    $meta_id = set_meta_data( 'homepage_slideshow', $file_location, 'global_setting' );

                    if( $meta_id )
                    {
                        update_homepage_slideshow_order( $meta_id );

                        return $meta_id;
                    }
                }
            }
        }
    }
}

function update_homepage_slideshow_order( $meta_id )
{
    global $db;

    $s = 'UPDATE lumonata_meta_data SET lmeta_value = lmeta_value + 1 WHERE lmeta_name = %s';
    $q = $db->prepare_query( $s, 'homepage_slideshow_order' );
    $r = $db->do_query( $q );

    if( is_array( $r ) )
    {
        return false;
    }
    else
    {
        update_meta_data( 'homepage_slideshow_order', 1, 'global_setting', $meta_id );
    }
}

function reorder_homepage_slideshow( $object )
{
    $object = json_decode( $object, true );

    foreach( $object as $meta_value => $meta_id )
    {
        update_meta_data( 'homepage_slideshow_order', $meta_value, 'global_setting', $meta_id );
    }
}

function delete_homepage_slideshow( $meta_id )
{
    $image_loc = get_meta_data_by_id( $meta_id );

    if( file_exists( ROOT_PATH . $image_loc ) && !empty( $image_loc ) )
    {
        unlink( ROOT_PATH . $image_loc );
    }

    if( delete_meta_data_by_id( $meta_id ) )
    {
        delete_meta_data( 'homepage_slideshow_order', 'global_setting', $meta_id );

        return true;
    }
    else
    {
        return false;
    }
}

function get_homepage_slide()
{
    global $db;

    $s = 'SELECT lmeta_id, lmeta_value FROM lumonata_meta_data WHERE lmeta_name = %s AND lapp_name = %s AND lapp_id = %d ORDER BY lmeta_id';
    $q = $db->prepare_query( $s, 'homepage_slide', 'global_setting', 0 );
    $r = $db->do_query( $q );

    $data     = array();
    $site_url = site_url();

    if( $db->num_rows( $r ) > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $title        = get_meta_data( 'homepage_slide_title', 'global_setting', $d['lmeta_id'] );
            $subtitle     = get_meta_data( 'homepage_slide_subtitle', 'global_setting', $d['lmeta_id'] );
            $use_link     = get_meta_data( 'homepage_slide_use_link', 'global_setting', $d['lmeta_id'] );
            $link_url     = get_meta_data( 'homepage_slide_link_url', 'global_setting', $d['lmeta_id'] );
            $link_type    = get_meta_data( 'homepage_slide_link_type', 'global_setting', $d['lmeta_id'] );
            $link_target  = get_meta_data( 'homepage_slide_link_target', 'global_setting', $d['lmeta_id'] );
            $post_link_id = get_meta_data( 'homepage_slide_post_link_id', 'global_setting', $d['lmeta_id'] );

            $idx = get_meta_data( 'homepage_slide_order', 'global_setting', $d['lmeta_id'] );
            $arr = explode( '/', $d['lmeta_value'] );
            $arr = array_reverse( $arr );

            $data[ $idx ] = array(
                'title'        => $title,
                'img_name'     => $arr[0],
                'subtitle'     => $subtitle,
                'use_link'     => $use_link,
                'link_url'     => $link_url,
                'link_type'    => $link_type,
                'link_target'  => $link_target,
                'post_link_id' => $post_link_id,
                'img_id'       => $d['lmeta_id'],
                'img_url'      => '//' . $site_url . $d['lmeta_value'],
                'img_size'     => filesize( ROOT_PATH . $d['lmeta_value'] ),
                'img_thumb'    => '//' . $site_url . '/lumonata-functions/mthumb.php?src=' . HTSERVER . '//' . $site_url . $d['lmeta_value'] . '&w=120&h=120'
            );
        }

        ksort( $data );

        return $data;
    }
}

function add_homepage_slide()
{
    global $db;

    $data   = array();
    $folder = upload_folder_name();

    if( isset( $_FILES[ 'file' ]['error'] ) && $_FILES[ 'file' ]['error'] == 0 )
    {
        if( !defined( 'FILES_LOCATION' ) )
        {
            define( 'FILES_LOCATION', '/lumonata-content/files' );
        }

        //-- Create destination folder if folder is not exist yet
        if( is_dir( FILES_PATH .'/'. $folder ) === FALSE )
        {
            if( create_dir( FILES_PATH .'/'. $folder ) === FALSE )
            {
                return $data;
            }
        }

        $file_name   = $_FILES[ 'file' ][ 'name' ];
        $file_size   = $_FILES[ 'file' ][ 'size' ];
        $file_type   = $_FILES[ 'file' ][ 'type' ];
        $file_source = $_FILES[ 'file' ][ 'tmp_name' ];

        extract( $_POST );

        if( is_allow_file_type( $file_type, 'image' ) )
        {
            if( is_allow_file_size( $file_size ) )
            {
                $default_title   = file_name_filter( $file_name );
                $file_name       = character_filter( $file_name );
                $file_name       = file_name_filter( $file_name ) . '-' . time() . file_name_filter( $file_name, true );

                //-- Resize Image
                $file_location    = FILES_LOCATION . '/' . $folder . '/' . $file_name;
                $file_destination = FILES_PATH . '/' . $folder . '/' . $file_name;

                if( upload_resize( $file_source, $file_destination, $file_type, 1920, 1920 ) )
                {
                    $meta_id = set_meta_data( 'homepage_slide', $file_location, 'global_setting' );

                    if( $meta_id )
                    {
                        update_homepage_slide_order( $meta_id );
                        update_homepage_slide_field( $meta_id );

                        return $meta_id;
                    }
                }
            }
        }
    }
}

function update_homepage_slide_order( $meta_id )
{
    global $db;

    $s = 'UPDATE lumonata_meta_data SET lmeta_value = lmeta_value + 1 WHERE lmeta_name = %s';
    $q = $db->prepare_query( $s, 'homepage_slide_order' );
    $r = $db->do_query( $q );

    if( is_array( $r ) )
    {
        return false;
    }
    else
    {
        update_meta_data( 'homepage_slide_order', 1, 'global_setting', $meta_id );
    }
}

function update_homepage_slide_field( $meta_id )
{
    $title        = isset( $_POST['title'] ) ? $_POST['title'] : '';
    $use_link     = isset( $_POST['use_link'] ) ? $_POST['use_link'] : 0;
    $subtitle     = isset( $_POST['subtitle'] ) ? $_POST['subtitle'] : '';
    $link_url     = isset( $_POST['link_url'] ) ? $_POST['link_url'] : '';
    $link_type    = isset( $_POST['link_type'] ) ? $_POST['link_type'] : '';
    $post_link_id = isset( $_POST['post_link_id'] ) ? $_POST['post_link_id'] : '';
    $link_target  = isset( $_POST['link_target'] ) ? $_POST['link_target'] : '_self';

    update_meta_data( 'homepage_slide_title', $title, 'global_setting', $meta_id );
    update_meta_data( 'homepage_slide_subtitle', $subtitle, 'global_setting', $meta_id );
    update_meta_data( 'homepage_slide_use_link', $use_link, 'global_setting', $meta_id );
    update_meta_data( 'homepage_slide_link_url', $link_url, 'global_setting', $meta_id );
    update_meta_data( 'homepage_slide_link_type', $link_type, 'global_setting', $meta_id );
    update_meta_data( 'homepage_slide_link_target', $link_target, 'global_setting', $meta_id );
    update_meta_data( 'homepage_slide_post_link_id', $post_link_id, 'global_setting', $meta_id );

    return true;
}

function reorder_homepage_slide( $object )
{
    $object = json_decode( $object, true );

    foreach( $object as $meta_value => $meta_id )
    {
        update_meta_data( 'homepage_slide_order', $meta_value, 'global_setting', $meta_id );
    }
}

function delete_homepage_slide( $meta_id )
{
    $image_loc = get_meta_data_by_id( $meta_id );

    if( file_exists( ROOT_PATH . $image_loc ) && !empty( $image_loc ) )
    {
        unlink( ROOT_PATH . $image_loc );
    }

    if( delete_meta_data_by_id( $meta_id ) )
    {
        delete_meta_data( 'homepage_slide_order', 'global_setting', $meta_id );
        delete_meta_data( 'homepage_slide_title', 'global_setting', $meta_id );
        delete_meta_data( 'homepage_slide_subtitle', 'global_setting', $meta_id );
        delete_meta_data( 'homepage_slide_use_link', 'global_setting', $meta_id );
        delete_meta_data( 'homepage_slide_link_url', 'global_setting', $meta_id );
        delete_meta_data( 'homepage_slide_link_type', 'global_setting', $meta_id );
        delete_meta_data( 'homepage_slide_link_target', 'global_setting', $meta_id );
        delete_meta_data( 'homepage_slide_post_link_id', 'global_setting', $meta_id );

        return true;
    }
    else
    {
        return false;
    }
}

function get_triple_box_media()
{
    global $db;

    $s = 'SELECT lmeta_id, lmeta_value FROM lumonata_meta_data WHERE lmeta_name = %s AND lapp_name = %s AND lapp_id = %d ORDER BY lmeta_id';
    $q = $db->prepare_query( $s, 'triple_box_image', 'global_setting', 0 );
    $r = $db->do_query( $q );

    $data     = array();
    $site_url = site_url();

    if( $db->num_rows( $r ) > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $idx = get_meta_data( 'triple_box_image_order', 'global_setting', $d['lmeta_id'] );
            $arr = explode( '/', $d['lmeta_value'] );
            $arr = array_reverse( $arr );

            $data[ $idx ] = array(
                'img_name' => $arr[0],
                'img_id' => $d['lmeta_id'],
                'img_url' => '//' . $site_url . $d['lmeta_value'],
                'img_size' => filesize( ROOT_PATH . $d['lmeta_value'] ),
                'img_low_res' => '//' . $site_url . '/lumonata-functions/mthumb.php?src=' . HTSERVER . '//' . $site_url . $d['lmeta_value'] . '&w=15',
                'img_thumb' => '//' . $site_url . '/lumonata-functions/mthumb.php?src=' . HTSERVER . '//' . $site_url . $d['lmeta_value'] . '&w=120&h=120'
            );
        }

        ksort( $data );

        return $data;
    }
}

function add_triple_box_media()
{
    global $db;

    $data   = array();
    $folder = upload_folder_name();

    if( isset( $_FILES[ 'file' ]['error'] ) && $_FILES[ 'file' ]['error'] == 0 )
    {
        if( !defined( 'FILES_LOCATION' ) )
        {
            define( 'FILES_LOCATION', '/lumonata-content/files' );
        }

        //-- Create destination folder if folder is not exist yet
        if( is_dir( FILES_PATH .'/'. $folder ) === FALSE )
        {
            if( create_dir( FILES_PATH .'/'. $folder ) === FALSE )
            {
                return $data;
            }
        }

        $file_name   = $_FILES[ 'file' ][ 'name' ];
        $file_size   = $_FILES[ 'file' ][ 'size' ];
        $file_type   = $_FILES[ 'file' ][ 'type' ];
        $file_source = $_FILES[ 'file' ][ 'tmp_name' ];

        extract( $_POST );

        if( is_allow_file_type( $file_type, 'image' ) )
        {
            if( is_allow_file_size( $file_size ) )
            {
                $default_title   = file_name_filter( $file_name );
                $file_name       = character_filter( $file_name );
                $file_name       = file_name_filter( $file_name ) . '-' . time() . file_name_filter( $file_name, true );

                //-- Resize Image
                $file_location    = FILES_LOCATION . '/' . $folder . '/' . $file_name;
                $file_destination = FILES_PATH . '/' . $folder . '/' . $file_name;

                if( upload_resize( $file_source, $file_destination, $file_type, 1024, 1024 ) )
                {
                    $meta_id = set_meta_data( 'triple_box_image', $file_location, 'global_setting' );

                    if( $meta_id )
                    {
                        update_triple_box_order( $meta_id );

                        return $meta_id;
                    }
                }
            }
        }
    }
}

function update_triple_box_order( $meta_id )
{
    global $db;

    $s = 'UPDATE lumonata_meta_data SET lmeta_value = lmeta_value + 1 WHERE lmeta_name = %s';
    $q = $db->prepare_query( $s, 'triple_box_image_order' );
    $r = $db->do_query( $q );

    if( is_array( $r ) )
    {
        return false;
    }
    else
    {
        update_meta_data( 'triple_box_image_order', 1, 'global_setting', $meta_id );
    }
}

function reorder_triple_box_media( $object )
{
    $object = json_decode( $object, true );

    foreach( $object as $meta_value => $meta_id )
    {
        update_meta_data( 'triple_box_image_order', $meta_value, 'global_setting', $meta_id );
    }
}

function delete_triple_box_media( $meta_id )
{
    $image_loc = get_meta_data_by_id( $meta_id );

    if( file_exists( ROOT_PATH . $image_loc ) && !empty( $image_loc ) )
    {
        unlink( ROOT_PATH . $image_loc );
    }

    if( delete_meta_data_by_id( $meta_id ) )
    {
        delete_meta_data( 'triple_box_image_order', 'global_setting', $meta_id );

        return true;
    }
    else
    {
        return false;
    }
}

function upload_app_image()
{
    global $db;

    $result = array();

    $app_img      = get_meta_data($_POST['meta_name'], $_POST['app_name']);
    $result_image = empty($app_img) ? '' : $app_img;

    //-- CHECK FOLDER EXIST
    if (!file_exists(PLUGINS_PATH . '/setting/background/')) {
        mkdir(PLUGINS_PATH . '/setting/background/');
    }

    if (!file_exists(PLUGINS_PATH . '/setting/background/thumbnail/')) {
        mkdir(PLUGINS_PATH . '/setting/background/thumbnail/');
    }

    $file_name   = $_FILES['file_name']['name'];
    $file_size   = $_FILES['file_name']['size'];
    $file_type   = $_FILES['file_name']['type'];
    $file_source = $_FILES['file_name']['tmp_name'];

    if (is_allow_file_size($file_size)) {
        if (is_allow_file_type($file_type, 'image')) {
            $fix_file_name = file_name_filter(character_filter($file_name)) . '-' . time();
            $file_ext      = file_name_filter($file_name, true);
            $file_name_ori = $fix_file_name . $file_ext;

            $destination       = PLUGINS_PATH . '/setting/background/' . $file_name_ori;
            $destination_thumb = PLUGINS_PATH . '/setting/background/thumbnail/' . $file_name_ori;

            $is_croped = upload_crop($file_source, $destination_thumb, $file_type, thumbnail_image_width(), thumbnail_image_height());
            $is_upload = upload($file_source, $destination);

            if ($is_upload && $is_croped) {
                if (update_meta_data($_POST['meta_name'], $file_name_ori, $_POST['app_name'])) {
                    if (file_exists(PLUGINS_PATH . '/setting/background/' . $result_image)) {
                        unlink(PLUGINS_PATH . '/setting/background/' . $result_image);
                    }

                    if (file_exists(PLUGINS_PATH . '/setting/background/thumbnail/' . $result_image)) {
                        unlink(PLUGINS_PATH . '/setting/background/thumbnail/' . $result_image);
                    }

                    $result = array( 'filename' => $file_name_ori );
                }
            }
        }
    }

    return $result;
}


function execute_global_setting_ajax_request()
{
    global $db;

    if( $_POST[ 'ajax_key' ] == 'get-triple-box-media' )
    {
        $data = get_triple_box_media();

        if( empty( $data ) )
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'success', 'data' => $data ) );
        }
    }

    if ($_POST['ajax_key'] == 'add-app-image') {
        $data = upload_app_image();

        if (empty($data)) {
            $res['result'] = 'failed';
        } else {
            $res['result'] = 'success';
            $res['data']   = $data;
        }

        echo json_encode($res);
    }

    if( $_POST[ 'ajax_key' ] == 'add-triple-box-media' )
    {
        $meta_id = add_triple_box_media();

        if( empty( $meta_id ) )
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'success', 'id' => $meta_id ) );
        }
    }

    if( $_POST[ 'ajax_key' ] == 'reorder-triple-box-media' )
    {
        if( isset( $_POST['value'] ) && !empty( $_POST['value'] ) )
        {
            reorder_triple_box_media( $_POST['value'] );

            echo json_encode( array( 'result' => 'success' ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
    }

    if( $_POST[ 'ajax_key' ] == 'delete-triple-box-media' )
    {
        if( delete_triple_box_media( $_POST['meta_id'] ) )
        {
            echo json_encode( array( 'result' => 'success' ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
    }

    if( $_POST[ 'ajax_key' ] == 'get-homepage-slideshow' )
    {
        $data = get_homepage_slideshow();

        if( empty( $data ) )
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'success', 'data' => $data ) );
        }
    }

    if( $_POST[ 'ajax_key' ] == 'add-homepage-slideshow' )
    {
        $meta_id = add_homepage_slideshow();

        if( empty( $meta_id ) )
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'success', 'id' => $meta_id ) );
        }
    }

    if( $_POST[ 'ajax_key' ] == 'reorder-homepage-slideshow' )
    {
        if( isset( $_POST['value'] ) && !empty( $_POST['value'] ) )
        {
            reorder_homepage_slideshow( $_POST['value'] );

            echo json_encode( array( 'result' => 'success' ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
    }

    if( $_POST[ 'ajax_key' ] == 'delete-homepage-slideshow' )
    {
        if( delete_homepage_slideshow( $_POST['meta_id'] ) )
        {
            echo json_encode( array( 'result' => 'success' ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
    }

    if( $_POST[ 'ajax_key' ] == 'get-homepage-slide' )
    {
        $data = get_homepage_slide();

        if( empty( $data ) )
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'success', 'data' => $data ) );
        }
    }

    if( $_POST[ 'ajax_key' ] == 'add-homepage-slide' )
    {
        $meta_id = add_homepage_slide();

        if( empty( $meta_id ) )
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'success', 'id' => $meta_id ) );
        }
    }

    if( $_POST[ 'ajax_key' ] == 'reorder-homepage-slide' )
    {
        if( isset( $_POST['value'] ) && !empty( $_POST['value'] ) )
        {
            reorder_homepage_slide( $_POST['value'] );

            echo json_encode( array( 'result' => 'success' ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
    }

    if( $_POST[ 'ajax_key' ] == 'delete-homepage-slide' )
    {
        if( delete_homepage_slide( $_POST['meta_id'] ) )
        {
            echo json_encode( array( 'result' => 'success' ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
    }

    if( $_POST[ 'ajax_key' ] == 'edit-homepage-slide-info' )
    {
        if( update_homepage_slide_field( $_POST['meta_id'] ) )
        {
            echo json_encode( array( 'result' => 'success' ) );
        }
        else
        {
            echo json_encode( array( 'result' => 'failed' ) );
        }
    }

    exit;
}

?>
