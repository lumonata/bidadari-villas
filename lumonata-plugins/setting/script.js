Dropzone.autoDiscover = false;

function init_triple_box_dropzone_func()
{
    var url = jQuery('[name=state_url]').val();
    var prm = new Object;
        prm.ajax_key = 'add-triple-box-media';
        
    var dropzone = new Dropzone('#media-upload', {
        dictRemoveFileConfirmation: 'Are you sure want to delete this image?',
        dictRemoveFile: 'Delete',
        addRemoveLinks: true,
        thumbnailHeight: 120,
        thumbnailWidth: 120,
        parallelUploads: 2,
        filesizeBase: 1000,
        maxFiles: 3,
        params:prm,
        url: url,
        init: function() { 
            var sel = this;                                  
            var prm = new Object;
                prm.ajax_key = 'get-triple-box-media';
                
            jQuery.ajax({
                url: url,
                data: prm,
                method: 'POST',
                dataType : 'json',
                success: function( e ){
                    if( e.result == 'success' )
                    {
                        jQuery.each( e.data, function( i, e ){
                            var mockFile = { id:e.img_id, url:e.img_url, size:e.img_size, name:e.img_name };

                            dropzone.emit( 'addedfile', mockFile );
                            dropzone.emit( 'thumbnail', mockFile, e.img_thumb );
                            dropzone.emit( 'complete', mockFile );

                            dropzone.files.push( mockFile );
                        });
                    }
                }
            });

            this.on('addedfile', function( file ){
                var id = typeof file.id != 'undefined' ? file.id : '';

                jQuery(file.previewElement).attr('id', 'media-' + id);
                jQuery(file.previewElement).find('.dz-remove').attr('data-id', id);
            });
        }
    });

    dropzone.on( 'success', function( file, response ){
        var e = jQuery.parseJSON( response );

        if( e.result == 'failed' )
        {
            dropzone.removeFile( file );
        }
        else
        {
            var selector  = jQuery( file.previewElement );

            selector.attr('id', 'media-' + e.id);
            selector.find('.dz-remove').attr('data-id', e.id);
        }
    });

    dropzone.on( 'removedfile', function( file, response ){
        var prm = new Object;
            prm.ajax_key = 'delete-triple-box-media';
            prm.meta_id  = jQuery( file.previewElement ).find('.dz-remove').data('id');
            
        jQuery.ajax({
            url: url,
            data: prm,
            method: 'POST',
            dataType : 'json',
            success: function( e ){
                console.log(e);
            }
        });
    });

    jQuery('#media-upload').nestedSortable({
        handle: 'div',
        items: '.dz-preview',
        listType:'div',
        opacity: .6,
        tolerance: 'pointer',
        maxLevels:0,
        stop: function(eve, ui){
            var list = jQuery('#media-upload').nestedSortable('toArray');
            var objc = new Array;
                jQuery.each(list, function( i, e ){
                    if( jQuery.isEmptyObject(e.item_id) === false )
                    {
                        objc.push(e.item_id);
                    }
                });
                
            var prm = new Object;
                prm.ajax_key = 'reorder-triple-box-media';
                prm.value    = JSON.stringify(objc);
            
            jQuery.ajax({
                url: url,
                data: prm,
                method: 'POST',
                dataType : 'json',
                success: function( e ){
                    console.log(e);
                }
            });
        }
    });
}

function init_slide_dropzone_func()
{
    var url = jQuery('[name=state_url]').val();
    var prm = new Object;
        prm.ajax_key = 'add-homepage-slide';
        
    var dropzone = new Dropzone('#slide-upload', {
        dictRemoveFileConfirmation: 'Are you sure want to delete this slide image?',
        previewTemplate: document.querySelector('#slide-dropzone-preview').innerHTML,
        dictRemoveFile: 'Delete',
        thumbnailHeight: 120,
        thumbnailWidth: 120,
        parallelUploads: 2,
        filesizeBase: 1000,
        params:prm,
        url: url,
        init: function() { 
            var sel = this;                                  
            var prm = new Object;
                prm.ajax_key = 'get-homepage-slide';
                
            jQuery.ajax({
                url: url,
                data: prm,
                method: 'POST',
                dataType : 'json',
                success: function( e ){
                    if( e.result == 'success' )
                    {
                        jQuery.each( e.data, function( i, e ){
                            var mockFile = { 
                                id: e.img_id,
                                url: e.img_url,
                                title: e.title,
                                size: e.img_size,
                                name: e.img_name,
                                use_link: e.use_link,
                                subtitle: e.subtitle,
                                link_url: e.link_url,
                                link_type: e.link_type,
                                link_target: e.link_target,
                                post_link_id: e.post_link_id
                            };

                            dropzone.emit( 'addedfile', mockFile );
                            dropzone.emit( 'thumbnail', mockFile, e.img_thumb );
                            dropzone.emit( 'complete', mockFile );

                            dropzone.files.push( mockFile );
                        });
                    }
                }
            });

            this.on('addedfile', function( file ){
                var id    = typeof file.id != 'undefined' ? file.id : '';
                var title = typeof file.title != 'undefined' ? file.title : '';
                var ulink = typeof file.use_link != 'undefined' ? file.use_link : '';
                var sub   = typeof file.subtitle != 'undefined' ? file.subtitle : '';
                var lurl  = typeof file.link_url != 'undefined' ? file.link_url : '';
                var ltype = typeof file.link_type != 'undefined' ? file.link_type : '';
                var ltgt  = typeof file.link_target != 'undefined' ? file.link_target : '';
                var pid   = typeof file.post_link_id != 'undefined' ? file.post_link_id : '';

                if( ulink == 1 )
                {
                    jQuery( file.previewElement ).find('.slide-post-link-id').prop('disabled', false);  
                    jQuery( file.previewElement ).find('.slide-link-target').prop('disabled', false); 
                    jQuery( file.previewElement ).find('.slide-link-type').prop('disabled', false);  
                    jQuery( file.previewElement ).find('.slide-link-url').prop('disabled', false);
                }

                jQuery( file.previewElement ).attr('id', 'slide-' + id);
                jQuery( file.previewElement ).find('.slide-title').val( title );
                jQuery( file.previewElement ).find('.slide-subtitle').val( sub );
                jQuery( file.previewElement ).find('.slide-link-url').val( lurl );
                jQuery( file.previewElement ).find('.slide-link-type').val( ltype );
                jQuery( file.previewElement ).find('.slide-link-target').val( ltgt );
                jQuery( file.previewElement ).find('.slide-post-link-id').val( pid );
                jQuery( file.previewElement ).find('.slide-use-link-opt').val( ulink );

                jQuery( file.previewElement ).find('.dz-delete').attr('data-id', id);
                jQuery( file.previewElement ).find('.dz-detail-box .save-detail').attr('data-id', id);

                jQuery( file.previewElement ).find('.dz-edit').unbind('click');
                jQuery( file.previewElement ).find('.dz-edit').click(function(){
                    jQuery( file.previewElement ).toggleClass('opened');
                });

                jQuery( file.previewElement ).find('.dz-delete').unbind('click');
                jQuery( file.previewElement ).find('.dz-delete').click(function(){
                    var prm = new Object;
                        prm.ajax_key = 'delete-homepage-slide';
                        prm.meta_id  = jQuery(this).data('id');

                    jQuery.ajax({
                        url: url,
                        data: prm,
                        method: 'POST',
                        dataType : 'json',
                        success: function( e ){
                            if( e.result=='success' )
                            {
                                dropzone.removeFile( file );
                            }
                            else
                            {
                                alert( alert );
                            }
                        }
                    });
                });

                jQuery( file.previewElement ).find('.slide-post-link-id').unbind('change');
                jQuery( file.previewElement ).find('.slide-post-link-id').change(function(){
                    var link = jQuery('option:selected', this).data('link');
                    var type = jQuery('option:selected', this).data('type');

                    jQuery( file.previewElement ).find('.slide-link-url').val( link );
                    jQuery( file.previewElement ).find('.slide-link-type').val( type );
                });

                jQuery( file.previewElement ).find('.slide-use-link-opt').unbind('change');
                jQuery( file.previewElement ).find('.slide-use-link-opt').change(function(){
                    if( jQuery(this).val() == 1 )
                    {
                        jQuery( file.previewElement ).find('.slide-post-link-id').prop('disabled', false);  
                        jQuery( file.previewElement ).find('.slide-link-target').prop('disabled', false); 
                        jQuery( file.previewElement ).find('.slide-link-type').prop('disabled', false);  
                        jQuery( file.previewElement ).find('.slide-link-url').prop('disabled', false);
                    }
                    else
                    {
                        jQuery( file.previewElement ).find('.slide-post-link-id').prop('disabled', true);  
                        jQuery( file.previewElement ).find('.slide-link-target').prop('disabled', true); 
                        jQuery( file.previewElement ).find('.slide-link-type').prop('disabled', true);    
                        jQuery( file.previewElement ).find('.slide-link-url').prop('disabled', true);
                    }
                }); 

                jQuery( file.previewElement ).find('.save-detail').unbind('click');
                jQuery( file.previewElement ).find('.save-detail').click(function(){
                    var prm = new Object;
                        prm.ajax_key     = 'edit-homepage-slide-info';
                        prm.meta_id      = jQuery(this).data('id');
                        prm.title        = jQuery( file.previewElement ).find('.slide-title').val();
                        prm.subtitle     = jQuery( file.previewElement ).find('.slide-subtitle').val();
                        prm.link_url     = jQuery( file.previewElement ).find('.slide-link-url').val();
                        prm.link_type    = jQuery( file.previewElement ).find('.slide-link-type').val();
                        prm.link_target  = jQuery( file.previewElement ).find('.slide-link-target').val();
                        prm.post_link_id = jQuery( file.previewElement ).find('.slide-post-link-id').val();
                        prm.use_link     = jQuery( file.previewElement ).find('.slide-use-link-opt').val();
                        
                    jQuery.ajax({
                        url: url,
                        data: prm,
                        method: 'POST',
                        dataType : 'json',
                        success: function( e ){                            
                            if( e.result == 'success' )
                            {
                                jQuery( file.previewElement ).removeClass('opened');
                            }
                            else
                            {
                                alert( 'Something wrong, please try again later' );
                            }
                        }
                    });

                    return false;
                });

                jQuery( file.previewElement ).find('.close-detail').unbind('click');
                jQuery( file.previewElement ).find('.close-detail').click(function(){
                    jQuery( file.previewElement ).removeClass('opened');
                });
            });
        }
    });

    dropzone.on( 'success', function( file, response ){
        var e = jQuery.parseJSON( response );

        if( e.result == 'failed' )
        {
            dropzone.removeFile( file );
        }
        else
        {
            jQuery( file.previewElement ).attr('id', 'slide-' + e.id);
            jQuery( file.previewElement ).find('.dz-delete').attr('data-id', e.id);
            jQuery( file.previewElement ).find('.dz-detail-box .save-detail').attr('data-id', e.id);
        }
    });

    jQuery('#slide-upload').nestedSortable({
        handle: 'div',
        items: '.dz-preview',
        listType:'div',
        opacity: .6,
        tolerance: 'pointer',
        maxLevels:0,
        stop: function(eve, ui){
            var list = jQuery('#slide-upload').nestedSortable('toArray');
            var objc = new Array;
                jQuery.each(list, function( i, e ){
                    if( jQuery.isEmptyObject(e.item_id) === false )
                    {
                        objc.push(e.item_id);
                    }
                });
                
            var prm = new Object;
                prm.ajax_key = 'reorder-homepage-slide';
                prm.value    = JSON.stringify(objc);
            
            jQuery.ajax({
                url: url,
                data: prm,
                method: 'POST',
                dataType : 'json',
                success: function( e ){
                    console.log(e);
                }
            });
        }
    });
}

function init_slideshow_dropzone_func()
{
    var url = jQuery('[name=state_url]').val();
    var prm = new Object;
        prm.ajax_key = 'add-homepage-slideshow';
        
    var dropzone = new Dropzone('#slideshow-upload', {
        dictRemoveFileConfirmation: 'Are you sure want to delete this slideshow image?',
        dictRemoveFile: 'Delete',
        addRemoveLinks: true,
        thumbnailHeight: 120,
        thumbnailWidth: 120,
        parallelUploads: 2,
        filesizeBase: 1000,
        maxFiles: 3,
        params:prm,
        url: url,
        init: function() { 
            var sel = this;                                  
            var prm = new Object;
                prm.ajax_key = 'get-homepage-slideshow';
                
            jQuery.ajax({
                url: url,
                data: prm,
                method: 'POST',
                dataType : 'json',
                success: function( e ){
                    if( e.result == 'success' )
                    {
                        jQuery.each( e.data, function( i, e ){
                            var mockFile = { id:e.img_id, url:e.img_url, size:e.img_size, name:e.img_name };

                            dropzone.emit( 'addedfile', mockFile );
                            dropzone.emit( 'thumbnail', mockFile, e.img_thumb );
                            dropzone.emit( 'complete', mockFile );

                            dropzone.files.push( mockFile );
                        });
                    }
                }
            });

            this.on('addedfile', function( file ){
                var id = typeof file.id != 'undefined' ? file.id : '';

                jQuery(file.previewElement).attr('id', 'slideshow-' + id);
                jQuery(file.previewElement).find('.dz-remove').attr('data-id', id);
            });
        }
    });

    dropzone.on( 'success', function( file, response ){
        var e = jQuery.parseJSON( response );

        if( e.result == 'failed' )
        {
            dropzone.removeFile( file );
        }
        else
        {
            var selector  = jQuery( file.previewElement );

            selector.attr('id', 'slideshow-' + e.id);
            selector.find('.dz-remove').attr('data-id', e.id);
        }
    });

    dropzone.on( 'removedfile', function( file, response ){
        var prm = new Object;
            prm.ajax_key = 'delete-homepage-slideshow';
            prm.meta_id  = jQuery( file.previewElement ).find('.dz-remove').data('id');
            
        jQuery.ajax({
            url: url,
            data: prm,
            method: 'POST',
            dataType : 'json',
            success: function( e ){
                console.log(e);
            }
        });
    });

    jQuery('#slideshow-upload').nestedSortable({
        handle: 'div',
        items: '.dz-preview',
        listType:'div',
        opacity: .6,
        tolerance: 'pointer',
        maxLevels:0,
        stop: function(eve, ui){
            var list = jQuery('#slideshow-upload').nestedSortable('toArray');
            var objc = new Array;
                jQuery.each(list, function( i, e ){
                    if( jQuery.isEmptyObject(e.item_id) === false )
                    {
                        objc.push(e.item_id);
                    }
                });
                
            var prm = new Object;
                prm.ajax_key = 'reorder-homepage-slideshow';
                prm.value    = JSON.stringify(objc);
            
            jQuery.ajax({
                url: url,
                data: prm,
                method: 'POST',
                dataType : 'json',
                success: function( e ){
                    console.log(e);
                }
            });
        }
    });
}

function init_landing_page_setting_func()
{
	if( jQuery('.landing-page').length > 0 )
	{
		init_triple_box_dropzone_func();
		init_slideshow_dropzone_func();
        init_slide_dropzone_func();
	}
}

jQuery(document).ready(function(){
	init_landing_page_setting_func();
})