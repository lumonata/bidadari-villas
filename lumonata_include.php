<?php

define( 'PLUGINS_PATH', ROOT_PATH . '/lumonata-plugins' );
define( 'APPS_PATH', ROOT_PATH . '/lumonata-apps' );
define( 'FUNCTIONS_PATH', ROOT_PATH . '/lumonata-functions' );
define( 'CLASSES_PATH', ROOT_PATH . '/lumonata-classes' );
define( 'ADMIN_PATH', ROOT_PATH . '/lumonata-admin' );
define( 'CONTENT_PATH', ROOT_PATH . '/lumonata-content' );

define( 'UPDATE_SUCCESS', 'Update process successfully.' );
define( 'UPDATE_FAILED', 'Update process failed.' );

require_once( ROOT_PATH . '/lumonata-classes/user_privileges.php' );
require_once( ROOT_PATH . '/lumonata-classes/flash-message.php' );
require_once( ROOT_PATH . '/lumonata-classes/admin_menu.php' );
require_once( ROOT_PATH . '/lumonata-classes/directory.php' );
require_once( ROOT_PATH . '/lumonata-classes/actions.php' );
require_once( ROOT_PATH . '/lumonata-classes/post.php' );

require_once( ROOT_PATH . '/lumonata-functions/settings.php' );
require_once( ROOT_PATH . '/lumonata-functions/themes.php' );
require_once( ROOT_PATH . '/lumonata-functions/paging.php' );
require_once( ROOT_PATH . '/lumonata-functions/kses.php' );

require_once( ROOT_PATH . '/lumonata-admin/admin_functions.php' );
require_once( ROOT_PATH . '/lumonata_settings.php' );

define( 'SMTP_SERVER', get_meta_data( 'smtp_server' ) );

if( !defined( 'FILES_PATH' ) );
{
    define('FILES_PATH', ROOT_PATH . '/lumonata-content/files');
}

if( !defined( 'SITE_URL' ) )
{
    $site = get_meta_data( 'site_url' );
    $part = explode( '.', $_SERVER[ 'HTTP_HOST' ] );
    $surl = ( $part[ 0 ] == 'www' ? 'www.' . $site : $site );
    
    define( 'SITE_URL', $site );
}

if( !defined( 'HTSERVER' ) )
{
	$ssl = get_meta_data( 'ssl_config' );
    
    define( 'HTSERVER', $ssl == 1 ? 'https:' : 'http:' );
}

set_timezone( get_meta_data( 'time_zone' ) );

require_once( ROOT_PATH . '/lumonata-functions/mail.php' );
require_once( ROOT_PATH . '/lumonata-functions/rewrite.php' );
require_once( ROOT_PATH . '/lumonata-functions/upload.php' );
require_once( ROOT_PATH . '/lumonata-functions/shortcodes.php' );
require_once( ROOT_PATH . '/lumonata-functions/languages.php' );
require_once( ROOT_PATH . '/lumonata-functions/articles.php' );
require_once( ROOT_PATH . '/lumonata-functions/pages.php' );
require_once( ROOT_PATH . '/lumonata-functions/media.php' );
require_once( ROOT_PATH . '/lumonata-functions/notifications.php' );
require_once( ROOT_PATH . '/lumonata-functions/taxonomy.php' );
require_once( ROOT_PATH . '/lumonata-functions/plugins.php' );
require_once( ROOT_PATH . '/lumonata-functions/personal-settings.php' );
require_once( ROOT_PATH . '/lumonata-functions/menus.php' );
require_once( ROOT_PATH . '/lumonata_functions.php' );
require_once( ROOT_PATH . '/lumonata_themes.php' );

?>