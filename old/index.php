<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Home - Bidadari</title>
        <link href="//fonts.googleapis.com/css?family=EB+Garamond|Muli" rel="stylesheet"> 
        <link href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" rel="stylesheet" />
        <link href="//stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet" />
        <link href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" rel="stylesheet" />
        <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

        <link href="css/style.css?v=1.0.0" rel="stylesheet">
        <link href="css/mobile.css?v=1.0.0" rel="stylesheet">
    </head>
    <body class="">
        <header>
            <div class="container-wrapp">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="logo">
                                <a href="index.php">
                                    <img src="img/logo.svg" alt="Bidadari Villa" />
                                </a>
                            </div>

                            <nav>
                                <ul class="top-menu">
                                    <li><a href="rooms.php">Rooms</a></li>
                                    <li><a href="services.php">Services &amp; Facilities</a></li>
                                    <li><a href="restaurant.php">Restaurant</a></li>
                                    <li><a href="spa.php">Spa</a></li>
                                    <li><a href="activities.php">Activities</a></li>
                                    <li><a href="gallery.php">Gallery</a></li>
                                    <li><a href="location.php">Location</a></li>
                                    <li><a href="offers.php">Offers</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <a href="javascript:;" class="book-now">Book Now</a>
        </header>

        <section class="homepage-hero white-cream-half-bg">            
            <div class="container-wrapp">
                <div class="hero-left">
                    <figure>
                        <img src="img/homepage/home-hero.jpg" alt="" />
                    </figure>
                    <figure>
                        <img src="img/homepage/home-hero-2.jpg" alt="" />
                    </figure>
                    <figure>
                        <img src="img/homepage/home-hero-3.jpg" alt="" />
                    </figure>
                </div>
                <div class="hero-right">
                    <div class="booking-form">
                        <form name="booking_form" action="#" method="POST">
                            <h1>Luxury villas in the heart of Bali</h1>
                            <fieldset>
                                <label>Check In</label>
                                <input type="text" name="check_in" class="date-picker" placeholder="Choose date" autocomplete="off" readonly />
                            </fieldset>
                            <fieldset>
                                <label>Check Out</label>
                                <input type="text" name="check_out" class="date-picker" placeholder="Choose date" autocomplete="off" readonly />
                            </fieldset>
                            <fieldset>
                                <label>Guest</label>
                                <select name="num_of_person" class="select-option" placeholder="No. of person" autocomplete="off">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                </select>
                            </fieldset>
                            <button class="submit book-now-btn" type="submit">Book Now</button>
                        </form>
                    </div>
                    <div class="navigation">
                        <div class="hero-nav"></div>
                        <a class="discover-more">Discover More <img src="img/discover.svg" alt="" /></a>
                    </div>
                </div>
            </div>
        </section>

        <section class="homepage-brief next-section white-cream-half-bg">            
            <div class="container-wrapp">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="brief-left">
                                <div class="inner">
                                    <figure class="bottom">
                                        <img src="img/homepage/about-1.jpg" alt="" />
                                    </figure>
                                    <figure class="middle">
                                        <img src="img/homepage/about-2.jpg" alt="" />
                                    </figure>
                                    <figure class="top">
                                        <img src="img/homepage/about-3.jpg" alt="" />
                                    </figure>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="brief-right">
                                <small>Welcome To</small>
                                <h2>The Bidadari Luxury Villas & Spa</h2>
                                <article>
                                    <p>Tranquility surrounding, warm & friendly hospitality. Located in a relaxing place of Umalas area, around 30 minutes from Ngurah Rai Bali International airport and 15 minutes away to popular tourist sport in Seminyak or Canggu-Brawa.</p>
                                    <p>The resort comprises of 19 Units villas with 7 units of One bedroom pool villa, 11 units of Two bedroom pool villa and 1 unit of Three bedroom pool villa.</p>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="homepage-room-type white-cream-half-bg">            
            <div class="container-wrapp">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <small>The Bidadari Luxury Villas &amp; Spa</small>
                            <h2>Room Types</h2>
                        </div>
                        <div class="col-md-6">
                            <p class="more">
                                <a href="javascript:;">View All Room Types <img src="img/arrow-right-long.svg" alt="" /></a>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="room-type-list">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="item">
                                            <figure>
                                                <img src="img/rooms/room-type-1.jpg" alt="" />
                                            </figure>
                                            <article>
                                                <div class="inner">
                                                    <small>Room Type</small>
                                                    <h3>One Bedroom Pool Villa</h3>
                                                    <p>The villa has a two-storey, the roof pavilion with soaring ceilings with separate bathrooms, indoor bath-tub and showers</p>
                                                    <a href="javascript:;">View Room</a>
                                                </div>
                                            </article>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="item">
                                            <figure>
                                                <img src="img/rooms/room-type-2.jpg" alt="" />
                                            </figure>
                                            <article>
                                                <div class="inner">
                                                    <small>Room Type</small>
                                                    <h3>Two BedroomsPool Villa</h3>
                                                    <p>The villa has a two-storey, the roof pavilion with soaring ceilings with separate bathrooms, indoor bath-tub and showers</p>
                                                    <a href="javascript:;">View Room</a>
                                                </div>
                                            </article>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="item">
                                            <figure>
                                                <img src="img/rooms/room-type-3.jpg" alt="" />
                                            </figure>
                                            <article>
                                                <div class="inner">
                                                    <small>Room Type</small>
                                                    <h3>Three Bedrooms Pool Villa</h3>
                                                    <p>The villa has a two-storey, the roof pavilion with soaring ceilings with separate bathrooms, indoor bath-tub and showers</p>
                                                    <a href="javascript:;">View Room</a>
                                                </div>
                                            </article>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="homepage-special-promo">
            <div class="container-wrapp">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <h2>Special Promotions</h2>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="navigation"></div>
                                </div>
                                <div class="col-md-6">
                                    <p class="more">
                                        <a href="javascript:;">See All Promotions <img src="img/arrow-right-long.svg" alt="" /></a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-slide">
                <div class="inner">
                    <div class="item">
                        <a href="javascript:;">
                            <img src="img/offers/offers-1.jpg" alt="" />
                            <div>
                                <small>Package</small>
                                <h3>Bidadari Celebration</h3>
                            </div>
                        </a>
                    </div>
                    <div class="item">
                        <a href="javascript:;">
                            <img src="img/offers/offers-2.jpg" alt="" />
                            <div>
                                <small>Package</small>
                                <h3>Bidadari Romantic</h3>
                            </div>
                        </a>
                    </div>
                    <div class="item">
                        <a href="javascript:;">
                            <img src="img/offers/offers-3.jpg" alt="" />
                            <div>
                                <small>Package</small>
                                <h3>Bidadari Experience</h3>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </section>

        <section class="homepage-activity cream-white-large-bg">
            <div class="container-wrapp">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <p class="more">
                                <a href="javascript:;">See All Activities <img src="img/arrow-right-long.svg" alt="" /></a>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="inner">
                                <div class="row ">
                                    <div class="col-md-6">
                                        <div class="boxs">
                                            <article>
                                                <small>Activity</small>
                                                <h2><a href="javascript:;">Yoga Activity</a></h2>
                                                <hr />
                                                <div class="desc">
                                                    <p>Most yoga practices focus on physical postures called “asanas,” breathing exercises called “pranayama,” 
                                                    and meditation to bring your body and mind together through slow, careful movements. But, there’s more to it than that! 
                                                    Yoga leads to improved physical fitness, increased ability to concentrate, and decreased stress. 
                                                    Yoga is an activity that helps both your body and mind work a little better.</p>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <a href="javascript:;" class="read-more">Learn More</a>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <nav></nav>
                                                    </div>
                                                </div>
                                            </article>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="slide">
                                            <figure data-title="Yoga Activity" data-content="Most yoga practices focus on physical postures called “asanas,” breathing exercises called “pranayama,” and meditation to bring your body and mind together through slow, careful movements. But, there’s more to it than that! Yoga leads to improved physical fitness, increased ability to concentrate, and decreased stress. Yoga is an activity that helps both your body and mind work a little better." data-link="#1">
                                                <a href="javascript:;">
                                                    <img src="img/activities/activity.jpg" alt="">
                                                </a>
                                            </figure>
                                            <figure data-title="Cooking Class" data-content="Discover the spiced flavors of Bali’s virtually unknown cuisine. Classes offer a fascinating introduction in to the exotic ingredients and unique culinary heritage of Bali. They provide a valuable insight into the various techniques, traditional and modern, of food preparation and the cooking style used in homes across the island." data-link="#2">
                                                <a href="javascript:;">
                                                    <img src="img/activities/activity-2.jpg" alt="">
                                                </a>
                                            </figure>
                                            <figure data-title="Children  Activity" data-content="Find indoor and outdoor games and activities that will challenge and develop your child's creativity, imagination, thinking skills, and social skills. Search our Activities Center for more great ideas for all ages." data-link="#3">
                                                <a href="javascript:;">
                                                    <img src="img/activities/activity-3.jpg" alt="">
                                                </a>
                                            </figure>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="homepage-location">
            <div class="container-wrapp">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="inner">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h2>The Bidadari Luxury Villas & Spa</h2>
                                    </div>
                                    <div class="col-md-6">
                                        <p>Located in Umalas, Seminyak, set amidst a tropical garden, approximately 10-minutes drive from Petitenget Beach, Seminyak and easy to go to others destination around villa.</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="map-wrapp">
                                            <div class="map-boxs">
                                                <div id="map-block" class="map-block"></div>
                                                <div id="map-list-location" class="map-list-location">
                                                    <div class="location">
                                                        <h3>Location</h3>
                                                        <ol>
                                                            <li data-type="restaurants" data-content="-8.654776|115.1511393|Café Cous Cous|ico-pin-restaurants.png|ico-pin-restaurants-active.png|0"><span class="ico-restaurants">Café Cous Cous</span></li>
                                                            <li data-type="restaurants" data-content="-8.658237|115.1505733|Souphoria|ico-pin-restaurants.png|ico-pin-restaurants-active.png|0"><span class="ico-restaurants">Souphoria</span></li>
                                                            <li data-type="restaurants" data-content="-8.6610555|115.1503492|Bumbak Coffee|ico-pin-restaurants.png|ico-pin-restaurants-active.png|0"><span class="ico-restaurants">Bumbak Coffee</span></li>
                                                            <li data-type="worships" data-content="-8.6468341|115.1304461|Bali Protestant Church|ico-pin-worships.png|ico-pin-worships-active.png|0"><span class="ico-worships">Bali Protestant Church</span></li>
                                                            <li data-type="attractions" data-content="-8.6617537|115.1450708|Splash Waterpark Bali|ico-pin-attractions.png|ico-pin-attractions-active.png|0"><span class="ico-attractions">Splash Waterpark Bali</span></li>
                                                            <li data-type="attractions" data-content="-8.6624618|115.1449431|Finns Recreation Club|ico-pin-attractions.png|ico-pin-attractions-active.png|0"><span class="ico-attractions">Finns Recreation Club</span></li>
                                                            <li data-type="attractions" data-content="-8.6628773|115.1442514|Strike Bowling|ico-pin-attractions.png|ico-pin-attractions-active.png|0"><span class="ico-attractions">Strike Bowling</span></li>
                                                            <li data-type="restaurants" data-content="-8.6657281|115.1455224|MyWarung Berawa Canggu|ico-pin-restaurants.png|ico-pin-restaurants-active.png|0"><span class="ico-restaurants">MyWarung Berawa Canggu</span></li>
                                                            <li data-type="attractions" data-content="-8.66599|115.1412673|Wave House|ico-pin-attractions.png|ico-pin-attractions-active.png|0"><span class="ico-attractions">Wave House</span></li>
                                                            <li data-type="restaurants" data-content="-8.6559592|115.1513084|Primo Chocolate Factory|ico-pin-restaurants.png|ico-pin-restaurants-active.png|0"><span class="ico-restaurants">Primo Chocolate Factory</span></li>
                                                            <li data-type="restaurants" data-content="-8.6539832|115.1536386|Thai Spice|ico-pin-restaurants.png|ico-pin-restaurants-active.png|0"><span class="ico-restaurants">Thai Spice</span></li>
                                                            <li data-type="restaurants" data-content="-8.6546302|115.1556771|One Bean Coffee House|ico-pin-restaurants.png|ico-pin-restaurants-active.png|0"><span class="ico-restaurants">One Bean Coffee House</span></li>
                                                            <li data-type="shops" data-content="-8.6568364|115.1478558|Maidenlove Store|ico-pin-shops.png|ico-pin-shops-active.png|0"><span class="ico-shops">Maidenlove Store</span></li>
                                                            <li data-type="shops" data-content="-8.662723|115.1482956|Canggu Plaza|ico-pin-shops.png|ico-pin-shops-active.png|0"><span class="ico-shops">Canggu Plaza</span></li>
                                                            <li data-type="beachs" data-content="-8.668355|115.1452057|Pantai Kayu Putih|ico-pin-beachs.png|ico-pin-beachs-active.png|0"><span class="ico-beachs">Pantai Kayu Putih</span></li>
                                                            <li data-type="beachs" data-content="-8.6644519|115.1377707|Pantai Perancak|ico-pin-beachs.png|ico-pin-beachs-active.png|0"><span class="ico-beachs">Pantai Perancak</span></li>
                                                            <li data-type="beachs" data-content="-8.6723429|115.1464396|Pantai Batu Belig|ico-pin-beachs.png|ico-pin-beachs-active.png|0"><span class="ico-beachs">Pantai Batu Belig</span></li>
                                                            <li data-type="shops" data-content="-8.6568364,115.1478558|Green Ginger Noodle House|ico-pin-shops.png|ico-pin-shops-active.png|0"><span class="ico-shops">Green Ginger Noodle House</span></li>
                                                        </ol>
                                                    </div>

                                                    <div class="category">
                                                        <a class="ico-active ico-all" data-filter="all">All</a>
                                                        <a class="ico-shops" data-filter="shops">Shops</a>
                                                        <a class="ico-restaurants" data-filter="restaurants">Restaurants</a>
                                                        <a class="ico-beachs" data-filter="beachs">Beachs</a>
                                                        <a class="ico-worships" data-filter="worships">Worship</a>
                                                        <a class="ico-attractions" data-filter="attractions">Attractions</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="newsletter-subscribe">
            <div class="container-wrapp">
                <div class="container-fluid">
                    <div class="row align-items-end">
                        <div class="col-md-6">
                            <small>Stay Connected</small>
                            <p>Keep up to date on the luxury villas in Seminyak and all stories around.</p>
                        </div>
                        <div class="col-md-6">
                            <form name="newsletter-form" method="POST" action="">
                                <fieldset>
                                    <input type="email" placeholder="Your Email Address">
                                    <button type="submit">Sign Up</button>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <footer>
            <div class="inner gray-white-half-bg">
                <div class="container-wrapp">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6">
                                <article>
                                    <small>The Bidadari Luxury Villas &amp; Spa</small>
                                    <p>Located in Umalas, Seminyak, set amidst a tropical garden, approximately 10-minutes drive from Petitenget Beach, Seminyak. It features 19 villas with a private pavilion and swimming pool.</p>
                                    <ul>
                                        <li><a href="https://goo.gl/maps/2aqoFrMaEm72" target="_blank">Jl. Bumbak No. 27 Banjar Anyar Kelod - Kerobokan, Kuta Utara - Bali - Indonesia</a></li>
                                        <li><a href="tel:+623618475566"><b>T.</b> (+62 361) 847-5566</a></li>
                                        <li><a href="tel:+623618475566"><b>F.</b> (+62 361) 847-5566</a></li>
                                        <li><a href="https://api.whatsapp.com/send?phone=081558206656" target="_blank"><b>WA.</b> (+62) 8786-0634-896</a></li>
                                        <li><a href="mailto:info@thebidadarivillasandspa.com"><b>E.</b> info@thebidadarivillasandspa.com</a></li>
                                    </ul>
                                </article>
                            </div>
                            <div class="col-md-6">
                                <div class="awards">
                                    <small>Awards</small>
                                    <div class="slide">
                                        <div class="item">
                                            <figure>
                                                <img src="img/tripadvisor.png" alt="" />
                                            </figure>
                                            <p>Recommended On Tripadvisor</p>
                                        </div>
                                        <div class="item">
                                            <figure>
                                                <img src="img/tripadvisor.png" alt="" />
                                            </figure>
                                            <p>Certificate of Excellence 2017</p>
                                        </div>
                                        <div class="item">
                                            <figure>
                                                <img src="img/tripadvisor.png" alt="" />
                                            </figure>
                                            <p>Certificate of Excellence 2016</p>
                                        </div>
                                        <div class="item">
                                            <figure>
                                                <img src="img/tripadvisor.png" alt="" />
                                            </figure>
                                            <p>Certificate of Excellence 2015</p>
                                        </div>
                                        <div class="item">
                                            <figure>
                                                <img src="img/tripadvisor.png" alt="" />
                                            </figure>
                                            <p>The Bidadari Luxury Villas and Spa Rated "Excellent" by 85 Traveller</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="other-properties">
                                    <small>Others Properties</small>
                                    <div class="item">
                                        <figure>
                                            <a href="https://www.thelightvillas.com/" target="_blank">
                                                <img src="img/the-light-villas-logo.png" alt="" width="100" />
                                            </a>
                                        </figure>
                                    </div>
                                    <div class="item">
                                        <figure>
                                            <a href="http://www.baliyubivillas.com/" target="_blank">
                                                <img src="img/yubi-logo.png" alt="" width="120" />
                                            </a>
                                        </figure>
                                    </div>
                                </div>
                                <nav>
                                    <ul>
                                        <li><a href="contact.php">Contact</a></li>
                                        <li><a href="#">FAQ</a></li>
                                        <li><a href="#">Term &amp; Conditions</a></li>
                                    </ul>

                                    <small>&copy; 2018 The Bidadari Luxury Villas and Spa</small>
                                </nav>

                                <a class="scroll-top">Return To The Top <img src="img/arrow-right-long.svg" alt="" /></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
        <script src="//maps.googleapis.com/maps/api/js?key=AIzaSyC9mKKb71I104S_XsT8jhw1DQ2FGqtLkds&callback=init_map_location" async defer></script>

        <script src="js/script.js?v=1.0.0"></script>
    </body>
</html>