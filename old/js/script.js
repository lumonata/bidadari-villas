/* 
| -----------------------------------------------------------------------------
| Init Map Location
| -----------------------------------------------------------------------------
*/
var map;
var markers  = [];
var selected = [];
var hzindex  = 0;

function init_map()
{
    var cor = '-8.6559592|115.1513084|ico-marker.png';
    var arr = cor.split( '|' );

    if( jQuery.isEmptyObject( arr ) !== true )
    {
        var mapdiv  = document.getElementById( 'map-block' );
        var latlng  = new google.maps.LatLng( arr[0], arr[1] );
        var image = {
            size: new google.maps.Size( 70, 96 ),
            scaledSize: new google.maps.Size( 70, 96 ),
            url: '//bidadari.lumonatalabs.com/img/' + arr[2],
            defurl: '//bidadari.lumonatalabs.com/img/' + arr[2]
        };

        map = new google.maps.Map( mapdiv, {
            zoom: 15,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeControl: false,
            mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                position: google.maps.ControlPosition.LEFT_BOTTOM
            },
            navigationControl: false,
            navigationControlOptions: {
                style: google.maps.NavigationControlStyle.SMALL,
                position: google.maps.ControlPosition.LEFT_CENTER 
            },
            scaleControl: false,
            scaleControlOptions: {
                position: google.maps.ControlPosition.BOTTOM_LEFT
            },
            zoomControl: true,
            disableDefaultUI: false,
            scrollwheel: false,
            draggable: true,
            styles: [{
                'featureType': 'all',
                'elementType': 'labels.text.fill',
                'stylers': [{
                    'saturation': 36
                }, {
                    'color': '#000000'
                }, {
                    'lightness': 40
                }]
            }, {
                'featureType': 'all',
                'elementType': 'labels.text.stroke',
                'stylers': [{
                    'visibility': 'on'
                }, {
                    'color': '#000000'
                }, {
                    'lightness': 16
                }]
            }, {
                'featureType': 'all',
                'elementType': 'labels.icon',
                'stylers': [{
                    'visibility': 'off'
                }]
            }, {
                'featureType': 'administrative',
                'elementType': 'geometry.fill',
                'stylers': [{
                    'color': '#000000'
                }, {
                    'lightness': 20
                }]
            }, {
                'featureType': 'administrative',
                'elementType': 'geometry.stroke',
                'stylers': [{
                    'color': '#000000'
                }, {
                    'lightness': 17
                }, {
                    'weight': 1.2
                }]
            }, {
                'featureType': 'administrative',
                'elementType': 'labels',
                'stylers': [{
                    'visibility': 'off'
                }]
            }, {
                'featureType': 'administrative.country',
                'elementType': 'all',
                'stylers': [{
                    'visibility': 'simplified'
                }]
            }, {
                'featureType': 'administrative.country',
                'elementType': 'geometry',
                'stylers': [{
                    'visibility': 'simplified'
                }]
            }, {
                'featureType': 'administrative.country',
                'elementType': 'labels.text',
                'stylers': [{
                    'visibility': 'simplified'
                }]
            }, {
                'featureType': 'administrative.province',
                'elementType': 'all',
                'stylers': [{
                    'visibility': 'off'
                }]
            }, {
                'featureType': 'administrative.locality',
                'elementType': 'all',
                'stylers': [{
                    'visibility': 'simplified'
                }, {
                    'saturation': '-100'
                }, {
                    'lightness': '30'
                }]
            }, {
                'featureType': 'administrative.neighborhood',
                'elementType': 'all',
                'stylers': [{
                    'visibility': 'off'
                }]
            }, {
                'featureType': 'administrative.land_parcel',
                'elementType': 'all',
                'stylers': [{
                    'visibility': 'off'
                }]
            }, {
                'featureType': 'landscape',
                'elementType': 'all',
                'stylers': [{
                    'visibility': 'simplified'
                }, {
                    'gamma': '0.00'
                }, {
                    'lightness': '74'
                }]
            }, {
                'featureType': 'landscape',
                'elementType': 'geometry',
                'stylers': [{
                    'color': '#000000'
                }, {
                    'lightness': 20
                }]
            }, {
                'featureType': 'landscape.man_made',
                'elementType': 'all',
                'stylers': [{
                    'lightness': '3'
                }]
            }, {
                'featureType': 'poi',
                'elementType': 'all',
                'stylers': [{
                    'visibility': 'off'
                }]
            }, {
                'featureType': 'poi',
                'elementType': 'geometry',
                'stylers': [{
                    'color': '#000000'
                }, {
                    'lightness': 21
                }]
            }, {
                'featureType': 'road',
                'elementType': 'geometry',
                'stylers': [{
                    'visibility': 'simplified'
                }]
            }, {
                'featureType': 'road.highway',
                'elementType': 'geometry.fill',
                'stylers': [{
                    'color': '#000000'
                }, {
                    'lightness': 17
                }]
            }, {
                'featureType': 'road.highway',
                'elementType': 'geometry.stroke',
                'stylers': [{
                    'color': '#000000'
                }, {
                    'lightness': 29
                }, {
                    'weight': 0.2
                }]
            }, {
                'featureType': 'road.arterial',
                'elementType': 'geometry',
                'stylers': [{
                    'color': '#000000'
                }, {
                    'lightness': 18
                }]
            }, {
                'featureType': 'road.local',
                'elementType': 'geometry',
                'stylers': [{
                    'color': '#000000'
                }, {
                    'lightness': 16
                }]
            }, {
                'featureType': 'transit',
                'elementType': 'geometry',
                'stylers': [{
                    'color': '#000000'
                }, {
                    'lightness': 19
                }]
            }, {
                'featureType': 'water',
                'elementType': 'geometry',
                'stylers': [{
                    'color': '#000000'
                }, {
                    'lightness': 17
                }]
            }]
        });

        new google.maps.Marker({ position: latlng, map: map, icon: image, zIndex: 1 });

        google.maps.event.addDomListener(window, 'resize', function() {
            var center = map.getCenter();
            google.maps.event.trigger(map, 'resize');
            map.setCenter(center); 
        });
    }
}

function init_map_location(){
    var cor = '-8.6559592|115.1513084|ico-marker.png';
    var arr = cor.split( '|' );

    if( jQuery.isEmptyObject( arr ) !== true )
    {
        var mapdiv  = document.getElementById( 'map-block' );
        var latlng  = new google.maps.LatLng( arr[0], arr[1] );
        var image = {
            size: new google.maps.Size( 70, 96 ),
            scaledSize: new google.maps.Size( 70, 96 ),
            url: '//bidadari.lumonatalabs.com/img/' + arr[2],
            defurl: '//bidadari.lumonatalabs.com/img/' + arr[2]
        };

        map = new google.maps.Map( mapdiv, {
            zoom: 15,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeControl: false,
            mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                position: google.maps.ControlPosition.LEFT_BOTTOM
            },
            navigationControl: false,
            navigationControlOptions: {
                style: google.maps.NavigationControlStyle.SMALL,
                position: google.maps.ControlPosition.LEFT_CENTER 
            },
            scaleControl: false,
            scaleControlOptions: {
                position: google.maps.ControlPosition.BOTTOM_LEFT
            },
            zoomControl: true,
            disableDefaultUI: false,
            scrollwheel: false,
            draggable: true,
            styles: [{
                'featureType': 'all',
                'elementType': 'labels.text.fill',
                'stylers': [{
                    'saturation': 36
                }, {
                    'color': '#000000'
                }, {
                    'lightness': 40
                }]
            }, {
                'featureType': 'all',
                'elementType': 'labels.text.stroke',
                'stylers': [{
                    'visibility': 'on'
                }, {
                    'color': '#000000'
                }, {
                    'lightness': 16
                }]
            }, {
                'featureType': 'all',
                'elementType': 'labels.icon',
                'stylers': [{
                    'visibility': 'off'
                }]
            }, {
                'featureType': 'administrative',
                'elementType': 'geometry.fill',
                'stylers': [{
                    'color': '#000000'
                }, {
                    'lightness': 20
                }]
            }, {
                'featureType': 'administrative',
                'elementType': 'geometry.stroke',
                'stylers': [{
                    'color': '#000000'
                }, {
                    'lightness': 17
                }, {
                    'weight': 1.2
                }]
            }, {
                'featureType': 'administrative',
                'elementType': 'labels',
                'stylers': [{
                    'visibility': 'off'
                }]
            }, {
                'featureType': 'administrative.country',
                'elementType': 'all',
                'stylers': [{
                    'visibility': 'simplified'
                }]
            }, {
                'featureType': 'administrative.country',
                'elementType': 'geometry',
                'stylers': [{
                    'visibility': 'simplified'
                }]
            }, {
                'featureType': 'administrative.country',
                'elementType': 'labels.text',
                'stylers': [{
                    'visibility': 'simplified'
                }]
            }, {
                'featureType': 'administrative.province',
                'elementType': 'all',
                'stylers': [{
                    'visibility': 'off'
                }]
            }, {
                'featureType': 'administrative.locality',
                'elementType': 'all',
                'stylers': [{
                    'visibility': 'simplified'
                }, {
                    'saturation': '-100'
                }, {
                    'lightness': '30'
                }]
            }, {
                'featureType': 'administrative.neighborhood',
                'elementType': 'all',
                'stylers': [{
                    'visibility': 'off'
                }]
            }, {
                'featureType': 'administrative.land_parcel',
                'elementType': 'all',
                'stylers': [{
                    'visibility': 'off'
                }]
            }, {
                'featureType': 'landscape',
                'elementType': 'all',
                'stylers': [{
                    'visibility': 'simplified'
                }, {
                    'gamma': '0.00'
                }, {
                    'lightness': '74'
                }]
            }, {
                'featureType': 'landscape',
                'elementType': 'geometry',
                'stylers': [{
                    'color': '#000000'
                }, {
                    'lightness': 20
                }]
            }, {
                'featureType': 'landscape.man_made',
                'elementType': 'all',
                'stylers': [{
                    'lightness': '3'
                }]
            }, {
                'featureType': 'poi',
                'elementType': 'all',
                'stylers': [{
                    'visibility': 'off'
                }]
            }, {
                'featureType': 'poi',
                'elementType': 'geometry',
                'stylers': [{
                    'color': '#000000'
                }, {
                    'lightness': 21
                }]
            }, {
                'featureType': 'road',
                'elementType': 'geometry',
                'stylers': [{
                    'visibility': 'simplified'
                }]
            }, {
                'featureType': 'road.highway',
                'elementType': 'geometry.fill',
                'stylers': [{
                    'color': '#000000'
                }, {
                    'lightness': 17
                }]
            }, {
                'featureType': 'road.highway',
                'elementType': 'geometry.stroke',
                'stylers': [{
                    'color': '#000000'
                }, {
                    'lightness': 29
                }, {
                    'weight': 0.2
                }]
            }, {
                'featureType': 'road.arterial',
                'elementType': 'geometry',
                'stylers': [{
                    'color': '#000000'
                }, {
                    'lightness': 18
                }]
            }, {
                'featureType': 'road.local',
                'elementType': 'geometry',
                'stylers': [{
                    'color': '#000000'
                }, {
                    'lightness': 16
                }]
            }, {
                'featureType': 'transit',
                'elementType': 'geometry',
                'stylers': [{
                    'color': '#000000'
                }, {
                    'lightness': 19
                }]
            }, {
                'featureType': 'water',
                'elementType': 'geometry',
                'stylers': [{
                    'color': '#000000'
                }, {
                    'lightness': 17
                }]
            }]
        });

        new google.maps.Marker({ position: latlng, map: map, icon: image, zIndex: 1 });

        init_markers();
        init_location_filter();

        google.maps.event.addDomListener(window, 'resize', function() {
            var center = map.getCenter();
            google.maps.event.trigger(map, 'resize');
            map.setCenter(center); 
        });
    }
}

/* 
| -----------------------------------------------------------------------------
| Init Map Markers
| -----------------------------------------------------------------------------
*/
function init_markers()
{
    jQuery('.location ol li:visible').each(function(i, e){
        var sel = jQuery(this);
        var loc = sel.data('content');
        var prm = loc.split('|');

        if( prm.length == 6 )
        {
            if( prm[5] == 0 )
            {
                var image = {
                    size: new google.maps.Size( 30, 41 ),
                    scaledSize: new google.maps.Size( 30, 41 ),
                    url: '//bidadari.lumonatalabs.com/img/' + prm[3],
                    defurl: '//bidadari.lumonatalabs.com/img/' + prm[3],
                    acturl: '//bidadari.lumonatalabs.com/img/' + prm[4]
                };
            }
            else
            {
                var image = {
                    url: '//bidadari.lumonatalabs.com/img/' + prm[3],
                    defurl: '//bidadari.lumonatalabs.com/img/' + prm[3],
                    acturl: '//bidadari.lumonatalabs.com/img/' + prm[4]
                };
            }

            var latlang = new google.maps.LatLng( prm[0], prm[1] );
            var info    = new google.maps.InfoWindow({ content : prm[2], position: latlang });
            var marker  = new google.maps.Marker({ animation: google.maps.Animation.DROP, position: latlang, infowindow: info, map: map, zIndex: 0, icon: image });
            
            selected[i] = false;

            markers.push( marker );

            sel.on('click', function(el){
                set_marker_icon( i, prm, marker );

                sel.parent().find('li').removeClass('active');
                sel.addClass('active');

                map.panTo( latlang );
            });

            google.maps.event.addListener( marker, 'click', function(el){
                var nice = jQuery('.location').getNiceScroll(0);
                var top  = sel.position().top;

                set_marker_icon( i, prm, marker );

                sel.parent().find('li').removeClass('active');
                sel.addClass('active'); 

                nice.doScrollTop( top );       
                map.panTo( latlang );
            });
        }
    });
}

/* 
| -----------------------------------------------------------------------------
| Init Map Location Filter
| -----------------------------------------------------------------------------
*/
function init_location_filter()
{    
    jQuery('.category a').on('click', function(){
        var filter = jQuery(this).data('filter');

        jQuery('.location ol li').css('display', 'block');

        if( filter !== 'all' )
        {
            jQuery('.location ol li[data-type!="' + filter + '"]').css('display', 'none');
        }
            
        remove_all_marker();

        init_markers();
    });
}

/* 
| -----------------------------------------------------------------------------
| Set Marker Icon on Map
| -----------------------------------------------------------------------------
*/
function set_marker_icon( idx, prm, marker )
{
    unset_marker( idx, prm );

    var icon = {
        size: new google.maps.Size( 30, 41 ),
        scaledSize: new google.maps.Size( 30, 41 ),
        defurl: '//bidadari.lumonatalabs.com/img/' + prm[3],
        acturl: '//bidadari.lumonatalabs.com/img/' + prm[4],
        url: '//bidadari.lumonatalabs.com/img/' + ( selected[ idx ] ? prm[3] : prm[4] )
    };

    if( markers.length > 0 )
    {  
        for( var i = 0; i < markers.length; i++ )
        {  
            tmp_zindex = markers[i].getZIndex();

            if( tmp_zindex > hzindex )
            {  
                hzindex = tmp_zindex;  
            }  
        }  
    }

    marker.setIcon( icon );
    marker.setZIndex( hzindex + 1 );
}

/* 
| -----------------------------------------------------------------------------
| Unset Marker Icon on Map
| -----------------------------------------------------------------------------
*/
function unset_marker( idx, prm )
{
    for( var i = 0; i < markers.length; i++)
    {
        if( i != idx )
        {
            if( prm[5] == 0 )
            {
                var icon = {
                    url: markers[i].icon.defurl,
                    defurl: markers[i].icon.defurl,
                    size: new google.maps.Size( 30, 41 ),
                    scaledSize: new google.maps.Size( 30, 41 )
                };
            }
            else
            {
                var icon = {
                    url: markers[i].icon.defurl,
                    defurl: markers[i].icon.defurl
                };
            }

            selected[i] = false;
            markers[i].setIcon( icon );
        }
    }
}

function remove_all_marker()
{
    for( var i = 0; i < markers.length; i++)
    {
        markers[i].setMap(null);
    }

    markers = [];
}

/* 
| -----------------------------------------------------------------------------
| Init Header Class On Scroll
| -----------------------------------------------------------------------------
*/
function init_header_class()
{
    var top = 0;

    if( jQuery('.book-now-btn').length > 0 )
    {
        top = jQuery('.book-now-btn').offset().top;
    }

    if( jQuery(window).scrollTop() > 150 )
    {
        jQuery('body').addClass('fixed');

        if( jQuery(window).scrollTop() > top )
        {
            jQuery('body').addClass('fixed-button');
        }
        else
        {
            jQuery('body').removeClass('fixed-button');
        }
    }
    else
    {
        jQuery('body').removeClass('fixed').removeClass('fixed-button');
    }
}

/* 
| -----------------------------------------------------------------------------
| Init All Slider
| -----------------------------------------------------------------------------
*/
function init_slider()
{
    //-- Homepage Hero Slide
    if( jQuery('.hero-left').length > 0 )
    {
        var dotsnav = jQuery('.hero-nav');

        jQuery('.hero-left').slick({
            dots: true,
            arrows: false,
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            appendDots: dotsnav
        });
    }

    //-- Homepage Promo Slide
    if( jQuery('.homepage-special-promo').length > 0 )
    {
        var arrownav = jQuery('.homepage-special-promo .navigation');

        jQuery('.homepage-special-promo .container-slide .inner').slick({
            infinite: true,
            slidesToScroll: 1,
            variableWidth: true,
            appendArrows: arrownav
        });
    }

    //-- Homepage Activities Slide
    if( jQuery('.homepage-activity').length > 0 )
    {
        var arrownav = jQuery('.homepage-activity nav');

        jQuery('.homepage-activity .slide').slick({
            infinite: true,
            slidesToScroll: 1,
            appendArrows: arrownav
        });

        jQuery('.homepage-activity .slide').on('beforeChange', function( slick, currentSlide ){
            jQuery('.homepage-activity article').addClass('fadeout');
        });

        jQuery('.homepage-activity .slide').on('afterChange', function( slick, currentSlide ){
            var link     = jQuery('.homepage-activity .slide .slick-current').attr('data-link');
            var title    = jQuery('.homepage-activity .slide .slick-current').attr('data-title');
            var content  = jQuery('.homepage-activity .slide .slick-current').attr('data-content'); 

            jQuery('.homepage-activity article h2').text( title );
            jQuery('.homepage-activity article .desc p').text( content );
            jQuery('.homepage-activity article .read-more').attr( 'href', link );

            jQuery('.homepage-activity article').removeClass('fadeout');
        });
    }

    //-- Archive Rooms Slide
    if( jQuery('.archive-list .slide').length > 0 )
    {
        jQuery('.archive-list .slide').each(function( i, e ){
            if( jQuery(this).find('figure').length > 1 )
            {
                var arrownav = jQuery(this).parent().parent().find('nav');

                jQuery(this).slick({
                    infinite: true,
                    slidesToScroll: 1,
                    appendArrows: arrownav
                });
            }
        })
    }

    //-- Detail Rooms Slide
    if( jQuery('.detail-room-gallery .container-full-gallery figure').length > 0 )
    {
        var arrownav = jQuery('.detail-room-gallery nav');

        jQuery('.container-full-gallery .inner').slick({
            infinite: true,
            centerMode: true,
            slidesToScroll: 1,
            variableWidth: true,
            focusOnSelect: true,
            appendArrows: arrownav
        });
    }

    //-- Detail Hero Slide
    if( jQuery('.detail-hero .slide figure').length > 0 )
    {
        jQuery('.detail-hero .slide').slick({
            infinite: false,
            slidesToScroll: 1
        });
    }

    //-- Footer Awards Slide
    jQuery('.awards .slide').slick({
        arrows: false,
        autoplay: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1
    });
}

/* 
| -----------------------------------------------------------------------------
| Init Booking Form Action
| -----------------------------------------------------------------------------
*/
function init_booking_form()
{
    if( jQuery('.booking-form').length > 0 )
    {
        var temp = new Date();
        var now  = new Date(temp.getFullYear(), temp.getMonth(), temp.getDate(), 0, 0, 0, 0);

        var person = jQuery('[name=num_of_person]').select2();

        var checkin = jQuery('[name=check_in]').datepicker({
            buttonImage: 'img/arrow-down.svg',
            dateFormat: 'dd MM yy',
            buttonImageOnly: true,
            showOn: 'both',
            minDate: 0,
            onSelect: function(date){
                var date2 = jQuery('[name=check_out]').datepicker('getDate');

                if( date2 == null )
                {
                    jQuery('[name=check_out]').datepicker('setDate', date);
                    jQuery('[name=check_out]').parent().removeClass('has-error');
                }

                jQuery('[name=check_out]').datepicker('option', 'minDate', date);
            }
        });

        var checkout = jQuery('[name=check_out]').datepicker({
            buttonImage: 'img/arrow-down.svg',
            dateFormat: 'dd MM yy',
            buttonImageOnly: true,
            showOn: 'both',
            minDate: 0,
            onClose: function(date){
                var date2 = jQuery('[name=check_in]').datepicker('getDate');

                if( date2 == null )
                {
                    jQuery('[name=check_in]').datepicker('setDate', date);
                    jQuery('[name=check_in]').parent().removeClass('has-error');
                }
                
                jQuery('[name=check_in]').datepicker('option', 'maxDate', date);
            }
        });
    }
}

/* 
| -----------------------------------------------------------------------------
| Init Scroll To Top
| -----------------------------------------------------------------------------
*/
function init_scroll_to()
{
    jQuery('.scroll-top').on('click', function(){
        jQuery('html, body').animate({
            scrollTop: 0
        }, 1000);
    });

    jQuery('.discover-more').on('click', function(){
        var top = jQuery('.next-section').offset().top;

        jQuery('html, body').animate({
            scrollTop: top
        }, 1000);
    });
}

/* 
| -----------------------------------------------------------------------------
| Init Scroll Bar Style
| -----------------------------------------------------------------------------
*/
function init_scroll_bar()
{
    if( jQuery('.map-list-location .location').length > 0 )
    {
        jQuery('.map-list-location .location').niceScroll({
            railpadding: { top: 70, right: 1, left: 0, bottom: 5 },         
            cursorcolor:'#C8B178',
            cursorborderradius:0,
            cursorborder:'none',
            cursorwidth:'5px'
        });
    }
}

/* 
| -----------------------------------------------------------------------------
| Init Gallery Box
| -----------------------------------------------------------------------------
*/
function init_gallery_box()
{
    if( jQuery('.gallery-list img').length > 0 )
    {
        var container = jQuery('.gallery-list .row');
    
        container.isotope({
            itemSelector : '.col-item'
        });

        var iso = container.data('isotope');
          
        container.infiniteScroll({
            path    : '.pagination .next',
            status  : '.scroller-status',
            append  : '.col-item',
            history : false,
            outlayer: iso,
        });

        jQuery('.filter li').click(function(){
            var selector = jQuery(this).attr('data-filter');
                selector = selector == '*' ? '' : '.' + selector;

            container.isotope({ filter: selector });

            return false;
        });
    }
}

/* 
| -----------------------------------------------------------------------------
| Init Contact Form
| -----------------------------------------------------------------------------
*/
function init_contact_form()
{
    if( jQuery('.contact-from').length > 0 )
    {
        jQuery('[name=send-contact]').on('click', function(){
            var action = jQuery('[name=contact_form]').attr('action');
            var param  = jQuery('[name=contact_form]').serializeArray();

            jQuery('.loader').removeClass('sr-only');
            jQuery('.response .alert').fadeOut(200);

            jQuery.ajax({
                url: action,
                data: param,
                method: 'POST',
                dataType : 'json',
                success: function(e){
                    grecaptcha.reset();

                    jQuery('.loader').addClass('sr-only');

                    if( e.result == 'success' )
                    {
                        window.location.href = e.target_url;
                    }
                    else if( e.result == 'failed' )
                    {
                        jQuery.fancybox.open({
                            src  : '#contact-response',
                            type : 'inline',
                            opts : {
                                afterLoad : function( instance, slide ) {
                                    slide.$content.find('h2').text('Error Sending Message');
                                    slide.$content.find('p').text( e.message );
                                }
                            }
                        });
                    }
                },
                error: function(e){
                    grecaptcha.reset();

                    jQuery('.response').html( '<div class="alert alert-danger" role="alert">Failed to send this message. Try again later</div>' );
                    jQuery('.loader').addClass('sr-only');
                }
            });

            return false;
        });
    }
}

jQuery(document).ready(function(){
    init_slider();
    init_scroll_to();
    init_scroll_bar();
    init_gallery_box();
	init_header_class();
    init_booking_form();
    init_contact_form();
});

jQuery(window).on('scroll', function(){
    init_header_class();
});