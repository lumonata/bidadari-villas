<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Services &amp; Facilities - Bidadari</title>
        <link href="//fonts.googleapis.com/css?family=EB+Garamond|Muli" rel="stylesheet"> 
        <link href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" rel="stylesheet" />
        <link href="//stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet" />

        <link href="css/style.css?v=1.0.0" rel="stylesheet">
        <link href="css/mobile.css?v=1.0.0" rel="stylesheet">
    </head>
    <body class="services">
        <header>
            <div class="container-wrapp">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="logo">
                                <a href="index.php">
                                    <img src="img/logo.svg" alt="Bidadari Villa" />
                                </a>
                            </div>

                            <nav>
                                <ul class="top-menu">
                                    <li><a href="rooms.php">Rooms</a></li>
                                    <li><a href="services.php">Services &amp; Facilities</a></li>
                                    <li><a href="restaurant.php">Restaurant</a></li>
                                    <li><a href="spa.php">Spa</a></li>
                                    <li><a href="activities.php">Activities</a></li>
                                    <li><a href="gallery.php">Gallery</a></li>
                                    <li><a href="location.php">Location</a></li>
                                    <li><a href="offers.php">Offers</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <a href="javascript:;" class="book-now">Book Now</a>
        </header>
        
        <section class="archive-hero">
            <div class="container-wrapp">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <figure>
                                <img src="img/services/service-hero.jpg" alt="">
                            </figure>
                        </div>
                        <div class="col-md-6">
                            <div class="inner">
                                <div class="boxs">
                                    <h1>The Bidadari Villas &amp; Spa Hospitality Services</h1>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent nec enim eros. 
                                    Nulla aliquet fringilla arcu sit amet bibendum. Nunc elit dolor, tempor vel sapien non, placerat egestas leo.</p>
                                    
                                    <button class="submit book-now-btn" type="submit">Book Now</button>
                                </div>
                            </div>
                            
                            <a class="discover-more">See All Services <img src="img/discover.svg" alt="" /></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <section class="archive-list next-section cream-bg">
            <div class="container-wrapp">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="service-list">
                                <small>Services</small>
                                <ul>
                                    <li><img src="img/icon/travel.svg" alt="" />Free shuttle service to Seminyak and Canggu / Brawa</li>
                                    <li><img src="img/icon/washing-machine.svg" alt="" />Laundry and dry cleaning service</li>
                                    <li><img src="img/icon/doctor-stethoscope.svg" alt="" />Doctor on call</li>
                                    <li><img src="img/icon/tour-arrangement.svg" alt="" />Tour arrangement</li>
                                    <li><img src="img/icon/car.svg" alt="" />Car rental</li>
                                    <li><img src="img/icon/wifi.svg" alt="" />Internet facilities</li>
                                    <li><img src="img/icon/airplane.svg" alt="" />Air port transfers</li>
                                    <li><img src="img/icon/exchange.svg" alt="" />Currency exchange</li>
                                    <li><img src="img/icon/dinning-table.svg" alt="" />In villa dinning</li>
                                    <li><img src="img/icon/toasts.svg" alt="" />Floating Breakfast</li>
                                    <li><img src="img/icon/chef.svg" alt="" />Cooking Class</li>
                                    <li><img src="img/icon/barbeque.svg" alt="" />BBQ</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <section class="newsletter-subscribe">
            <div class="container-wrapp">
                <div class="container-fluid">
                    <div class="row align-items-end">
                        <div class="col-md-6">
                            <small>Stay Connected</small>
                            <p>Keep up to date on the luxury villas in Seminyak and all stories around.</p>
                        </div>
                        <div class="col-md-6">
                            <form name="newsletter-form" method="POST" action="">
                                <fieldset>
                                    <input type="email" placeholder="Your Email Address">
                                    <button type="submit">Sign Up</button>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <footer>
            <div class="inner gray-white-half-bg">
                <div class="container-wrapp">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6">
                                <article>
                                    <small>The Bidadari Luxury Villas &amp; Spa</small>
                                    <p>Located in Umalas, Seminyak, set amidst a tropical garden, approximately 10-minutes drive from Petitenget Beach, Seminyak. It features 19 villas with a private pavilion and swimming pool.</p>
                                    <ul>
                                        <li><a href="https://goo.gl/maps/2aqoFrMaEm72" target="_blank">Jl. Bumbak No. 27 Banjar Anyar Kelod - Kerobokan, Kuta Utara - Bali - Indonesia</a></li>
                                        <li><a href="tel:+623618475566"><b>T.</b> (+62 361) 847-5566</a></li>
                                        <li><a href="tel:+623618475566"><b>F.</b> (+62 361) 847-5566</a></li>
                                        <li><a href="https://api.whatsapp.com/send?phone=081558206656" target="_blank"><b>WA.</b> (+62) 8786-0634-896</a></li>
                                        <li><a href="mailto:info@thebidadarivillasandspa.com"><b>E.</b> info@thebidadarivillasandspa.com</a></li>
                                    </ul>
                                </article>
                            </div>
                            <div class="col-md-6">
                                <div class="awards">
                                    <small>Awards</small>
                                    <div class="slide">
                                        <div class="item">
                                            <figure>
                                                <img src="img/tripadvisor.png" alt="" />
                                            </figure>
                                            <p>Recommended On Tripadvisor</p>
                                        </div>
                                        <div class="item">
                                            <figure>
                                                <img src="img/tripadvisor.png" alt="" />
                                            </figure>
                                            <p>Certificate of Excellence 2017</p>
                                        </div>
                                        <div class="item">
                                            <figure>
                                                <img src="img/tripadvisor.png" alt="" />
                                            </figure>
                                            <p>Certificate of Excellence 2016</p>
                                        </div>
                                        <div class="item">
                                            <figure>
                                                <img src="img/tripadvisor.png" alt="" />
                                            </figure>
                                            <p>Certificate of Excellence 2015</p>
                                        </div>
                                        <div class="item">
                                            <figure>
                                                <img src="img/tripadvisor.png" alt="" />
                                            </figure>
                                            <p>The Bidadari Luxury Villas and Spa Rated "Excellent" by 85 Traveller</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="other-properties">
                                    <small>Others Properties</small>
                                    <div class="item">
                                        <figure>
                                            <a href="https://www.thelightvillas.com/" target="_blank">
                                                <img src="img/the-light-villas-logo.png" alt="" width="100" />
                                            </a>
                                        </figure>
                                    </div>
                                    <div class="item">
                                        <figure>
                                            <a href="http://www.baliyubivillas.com/" target="_blank">
                                                <img src="img/yubi-logo.png" alt="" width="120" />
                                            </a>
                                        </figure>
                                    </div>
                                </div>
                                <nav>
                                    <ul>
                                        <li><a href="contact.php">Contact</a></li>
                                        <li><a href="#">FAQ</a></li>
                                        <li><a href="#">Term &amp; Conditions</a></li>
                                    </ul>

                                    <small>&copy; 2018 The Bidadari Luxury Villas and Spa</small>
                                </nav>

                                <a class="scroll-top">Return To The Top <img src="img/arrow-right-long.svg" alt="" /></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

        <script src="js/script.js?v=1.0.0"></script>
    </body>
</html>